unit BBEnc;

interface
uses dialogs,types,sysutils;

procedure TestBF();
var key: array [0 .. 1] of array [0 .. $530] of Dword;

implementation

Function PSOBBCreatekey(s:ansistring):ansistring;
var x,y:integer;
  re:ansistring;
  ebp,ebx,edx,ecx,eax,esi,edi,tmp1,tmp2,ou:DWORD;
  tmp:pansichar;
begin
  key[0][0]:=$243f6a88;
  key[0][1]:=$85a308d3;
  key[0][2]:=$13198a2e;
  key[0][3]:=$03707344;
  key[0][4]:=$a4093822;
  key[0][5]:=$299f31d0;
  key[0][6]:=$082efa98;
  key[0][7]:=$ec4e6c89;
  key[0][8]:=$452821e6;
  key[0][9]:=$38d01377;
  key[0][10]:=$be5466cf;
  key[0][11]:=$34e90c6c;
  key[0][12]:=$c0ac29b7;
  key[0][13]:=$c97c50dd;
  key[0][14]:=$3f84d5b5;
  key[0][15]:=$b5470917;
  key[0][16]:=$9216d5d9;
  key[0][17]:=$8979fb1b;
  x:=fileopen('default.dat',$40);
  fileread(x,key[0][18],$1000);
  fileclose(x);

  ecx:=0;
  //total key[0] length is min $412
  ebx:=0;

  while ebx < $12 do begin
  //in a loop
      ebp:=integer(s[1+ecx]) shl $18;
      eax:=ecx+1;
      edx:=eax-((eax div 48)*48);
      eax:=(integer(s[edx+1]) shl $10) and $ff0000;
      ebp:=(ebp or eax) and $ffff00ff;
      eax:=ecx+2;
      edx:=eax-((eax div 48)*48);
      eax:=(integer(s[edx+1]) shl $8) and $ff00;
      ebp:=(ebp or eax) and $ffffff00;
      eax:=ecx+3;
      ecx:=ecx+4;
      edx:=eax-((eax div 48)*48);
      eax:=integer(s[edx+1]);
      ebp:=ebp or eax;
      eax:=ecx;
      edx:=eax-((eax div 48)*48);
      key[0][ebx]:=key[0][ebx] xor ebp;
      ecx:=edx;
      inc(ebx);
  end;
  ebp:=0;
  esi:=0;
  ecx:=0;
  edi:=0;
  ebx:=0;
  edx:=$48;

  while edi < edx do begin
      esi:=esi xor key[0][0];
      eax:=esi shr $18;
      ebx:=(esi shr $10) and $ff;
      eax:=key[0][eax+$12]+key[0][ebx+$112];
      ebx:=(esi shr 8) and $FF;
      eax:=eax xor key[0][ebx+$212];
      ebx:=esi and $ff;
      eax:=eax + key[0][ebx+$312];

      eax:=eax xor key[0][1];
      ecx:= ecx xor eax;
      ebx:=ecx shr $18;
      eax:=(ecx shr $10) and $FF;
      ebx:=key[0][ebx+$12]+key[0][eax+$112];
      eax:=(ecx shr 8) and $ff;
      ebx:=ebx xor key[0][eax+$212];
      eax:=ecx and $ff;
      ebx:=ebx + key[0][eax+$312];

      for x:=0 to 5 do begin
          ebx:=ebx xor key[0][(x*2)+2];
          esi:= esi xor ebx;
          ebx:=esi shr $18;
          eax:=(esi shr $10) and $FF;
          ebx:=key[0][ebx+$12]+key[0][eax+$112];
          eax:=(esi shr 8) and $ff;
          ebx:=ebx xor key[0][eax+$212];
          eax:=esi and $ff;
          ebx:=ebx + key[0][eax+$312];

          ebx:=ebx xor key[0][(x*2)+3];
          ecx:= ecx xor ebx;
          ebx:=ecx shr $18;
          eax:=(ecx shr $10) and $FF;
          ebx:=key[0][ebx+$12]+key[0][eax+$112];
          eax:=(ecx shr 8) and $ff;
          ebx:=ebx xor key[0][eax+$212];
          eax:=ecx and $ff;
          ebx:=ebx + key[0][eax+$312];
      end;

      ebx:=ebx xor key[0][14];
      esi:= esi xor ebx;
      eax:=esi shr $18;
      ebx:=(esi shr $10) and $FF;
      eax:=key[0][eax+$12]+key[0][ebx+$112];
      ebx:=(esi shr 8) and $ff;
      eax:=eax xor key[0][ebx+$212];
      ebx:=esi and $ff;
      eax:=eax + key[0][ebx+$312];

      eax:=eax xor key[0][15];
      eax:= ecx xor eax;
      ecx:=eax shr $18;
      ebx:=(eax shr $10) and $FF;
      ecx:=key[0][ecx+$12]+key[0][ebx+$112];
      ebx:=(eax shr 8) and $ff;
      ecx:=ecx xor key[0][ebx+$212];
      ebx:=eax and $ff;
      ecx:=ecx + key[0][ebx+$312];

      ecx:=ecx xor key[0][16];
      ecx:=ecx xor esi;
      esi:= key[0][17];
      esi:=esi xor eax;
      key[0][(edi div 4)]:=esi;
      key[0][(edi div 4)+1]:=ecx;
      edi:=edi+8;
  end;




  eax:=0;
  edx:=0;
  ou:=0;
  while ou < $1000 do begin
      edi:=$48;
      edx:=$448;
      while edi < edx do begin
          esi:=esi xor key[0][0];
          eax:=esi shr $18;
          ebx:=(esi shr $10) and $ff;
          eax:=key[0][eax+$12]+key[0][ebx+$112];
          ebx:=(esi shr 8) and $FF;
          eax:=eax xor key[0][ebx+$212];
          ebx:=esi and $ff;
          eax:=eax + key[0][ebx+$312];

          eax:=eax xor key[0][1];
          ecx:= ecx xor eax;
          ebx:=ecx shr $18;
          eax:=(ecx shr $10) and $FF;
          ebx:=key[0][ebx+$12]+key[0][eax+$112];
          eax:=(ecx shr 8) and $ff;
          ebx:=ebx xor key[0][eax+$212];
          eax:=ecx and $ff;
          ebx:=ebx + key[0][eax+$312];

          for x:=0 to 5 do begin
          ebx:=ebx xor key[0][(x*2)+2];
          esi:= esi xor ebx;
          ebx:=esi shr $18;
          eax:=(esi shr $10) and $FF;
          ebx:=key[0][ebx+$12]+key[0][eax+$112];
          eax:=(esi shr 8) and $ff;
          ebx:=ebx xor key[0][eax+$212];
          eax:=esi and $ff;
          ebx:=ebx + key[0][eax+$312];

          ebx:=ebx xor key[0][(x*2)+3];
          ecx:= ecx xor ebx;
          ebx:=ecx shr $18;
          eax:=(ecx shr $10) and $FF;
          ebx:=key[0][ebx+$12]+key[0][eax+$112];
          eax:=(ecx shr 8) and $ff;
          ebx:=ebx xor key[0][eax+$212];
          eax:=ecx and $ff;
          ebx:=ebx + key[0][eax+$312];
          end;

          ebx:=ebx xor key[0][14];
          esi:= esi xor ebx;
          eax:=esi shr $18;
          ebx:=(esi shr $10) and $FF;
          eax:=key[0][eax+$12]+key[0][ebx+$112];
          ebx:=(esi shr 8) and $ff;
          eax:=eax xor key[0][ebx+$212];
          ebx:=esi and $ff;
          eax:=eax + key[0][ebx+$312];

          eax:=eax xor key[0][15];
          eax:= ecx xor eax;
          ecx:=eax shr $18;
          ebx:=(eax shr $10) and $FF;
          ecx:=key[0][ecx+$12]+key[0][ebx+$112];
          ebx:=(eax shr 8) and $ff;
          ecx:=ecx xor key[0][ebx+$212];
          ebx:=eax and $ff;
          ecx:=ecx + key[0][ebx+$312];

          ecx:=ecx xor key[0][16];
          ecx:=ecx xor esi;
          esi:= key[0][17];
          esi:=esi xor eax;
          key[0][(ou div 4)+(edi div 4)]:=esi;
          key[0][(ou div 4)+(edi div 4)+1]:=ecx;
          edi:=edi+8;
      end;
      ou:=ou+$400;
  end;
end;

Function PSOBBDec(s:ansistring):ansistring;
var x,i:integer;
  re:ansistring;
  ebp,ebx,edx,ecx,eax,esi,edi:DWORD;
begin
  i:=length(s);
  setlength(re,i);
  x:=1;
  edx:=0;
  ecx:=0;
  eax:=0;
  while x < i do begin
      ebx:=integer(s[edx+1])+(integer(s[edx+2])*256)+(integer(s[edx+3])*$10000)+(integer(s[edx+4])*$1000000);
      ebx:=ebx xor key[0][5];
      ebp:=key[0][(ebx shr $18) + $12]+key[0][((ebx shr $10)and $ff) + $112]
      xor key[0][((ebx shr $8)and $ff) + $212] + key[0][(ebx and $ff) + $312];
      ebp:=ebp xor key[0][4];
      ebp:=ebp xor (integer(s[edx+5])+(integer(s[edx+6])*256)+(integer(s[edx+7])*$10000)+(integer(s[edx+8])*$1000000));
      edi:=key[0][(ebp shr $18) + $12]+key[0][((ebp shr $10)and $ff) + $112]
      xor key[0][((ebp shr $8)and $ff) + $212] + key[0][(ebp and $ff) + $312];
      edi:=edi xor key[0][3];
      ebx:=ebx xor edi;
      esi:=key[0][(ebx shr $18) + $12]+key[0][((ebx shr $10)and $ff) + $112]
      xor key[0][((ebx shr $8)and $ff) + $212] + key[0][(ebx and $ff) + $312];
      ebp:=ebp xor esi xor key[0][2];
      edi:=key[0][(ebp shr $18) + $12]+key[0][((ebp shr $10)and $ff) + $112]
      xor key[0][((ebp shr $8)and $ff) + $212] + key[0][(ebp and $ff) + $312];
      edi:=edi xor key[0][1];
      ebp:=ebp xor key[0][0];
      ebx:=ebx xor edi;
      re[x]:=pansichar(@ebp)[0];
      re[x+1]:=pansichar(@ebp)[1];
      re[x+2]:=pansichar(@ebp)[2];
      re[x+3]:=pansichar(@ebp)[3];
      re[x+4]:=pansichar(@ebx)[0];
      re[x+5]:=pansichar(@ebx)[1];
      re[x+6]:=pansichar(@ebx)[2];
      re[x+7]:=pansichar(@ebx)[3];


      edx:=edx+8;
      x:=x+8;
  end;
  result:=re;
end;

Function PSOBBEnc(s:ansistring):ansistring;
var x,i:integer;
  re:ansistring;
  ebp,ebx,edx,ecx,eax,esi,edi:DWORD;
begin
  x:=1;
  i:=length(s);
  setlength(re,i);
  edx:=0;
  ecx:=0;
  eax:=0;
  while x < i do begin
      ebx:=integer(s[edx+1])+(integer(s[edx+2])*256)+(integer(s[edx+3])*$10000)+(integer(s[edx+4])*$1000000);
      ebx:=ebx xor key[0][0];
      ebp:=key[0][(ebx shr $18) + $12]+key[0][((ebx shr $10)and $ff) + $112]
      xor key[0][((ebx shr $8)and $ff) + $212] + key[0][(ebx and $ff) + $312];
      ebp:=ebp xor key[0][1];
      ebp:=ebp xor (integer(s[edx+5])+(integer(s[edx+6])*256)+(integer(s[edx+7])*$10000)+(integer(s[edx+8])*$1000000));
      edi:=key[0][(ebp shr $18) + $12]+key[0][((ebp shr $10)and $ff) + $112]
      xor key[0][((ebp shr $8)and $ff) + $212] + key[0][(ebp and $ff) + $312];
      edi:=edi xor key[0][2];
      ebx:=ebx xor edi;
      esi:=key[0][(ebx shr $18) + $12]+key[0][((ebx shr $10)and $ff) + $112]
      xor key[0][((ebx shr $8)and $ff) + $212] + key[0][(ebx and $ff) + $312];
      ebp:=ebp xor esi xor key[0][3];
      edi:=key[0][(ebp shr $18) + $12]+key[0][((ebp shr $10)and $ff) + $112]
      xor key[0][((ebp shr $8)and $ff) + $212] + key[0][(ebp and $ff) + $312];
      edi:=edi xor key[0][4];
      ebp:=ebp xor key[0][5];
      ebx:=ebx xor edi;

      re[x]:=pansichar(@ebp)[0];
      re[x+1]:=pansichar(@ebp)[1];
      re[x+2]:=pansichar(@ebp)[2];
      re[x+3]:=pansichar(@ebp)[3];
      re[x+4]:=pansichar(@ebx)[0];
      re[x+5]:=pansichar(@ebx)[1];
      re[x+6]:=pansichar(@ebx)[2];
      re[x+7]:=pansichar(@ebx)[3];
      edx:=edx+8;
      x:=x+8;
  end;
  result:=re;
end;


procedure TestBF();
var s:string;
begin
    PSOBBCreatekey(#$9A#$46#$D7#$C8); //key unknown for now
    s:=PSOBBDec(#$F1#$32#$EC#$73#$AC#$50#$EE#$BB );
    showmessage(s);
end;

end.
