unit UQuestBoard;

interface
uses windows,types,sysutils,classes;

type
    TPSO2Vector = record
        x,y,z,w:single;
    end;
    tinteraction = record
        types:integer;
        rot,pos:TPSO2Vector;
    end;
    TMapSection = record
        id:dword;
        data:array[0..15] of byte;
    end;
    TMonsterEntry = record
        id,wave,group,area,owner,special:dword;
        ctype:string[64];
        atype:string[200];
        rot:array[0..3] of single;
        pos:array[0..3] of single;
        events:array[0..7] of string[128];
        eventcount:integer;
        dies:array[0..7] of string[128];
        diecount:integer;
        status,target:dword;
        hp,mhp,SATK,RATK,TATK,SDEF,RDEF,TDEF,APT,exp,lvl:integer;
        hit:array[0..3] of byte;
        DieTime:dword;
        respawn:dword;
        DieTime2:dword;
        pts,rare,once,apear,boostflg,boss,miniboss:integer;
        bosszone:single;

        //npc section
        ShiftaTick,DebandTick:dword;
        ShiftaBoost,DebandBoost:integer;

        statustime:dword;
        statuseffect:integer;
        statusefft:integer;
        isnestmaster:integer;
        passive,disapear:integer;
        curpos:array[0..3] of word;
        properties:ansistring;
    end;

    TWepDrop = record
        TypePC:byte; //chance to get that type of weapon
        pcs:array[0..511] of byte;  //pc per eatch lvl
    end;

    TDropInfo = record
        mesetas:array[0..1] of integer;
        DropType:array[0..$f] of byte; //% for eatch type
        Wepons:array[0..18] of tWepDrop;
        items:array[0..1] of tWepDrop;
        units:array[0..5] of tWepDrop;
        palvl:array[0..30] of byte; //% for eatch lvl
    end;

    TDataGroup = record
        name:ansistring;
        data:ansistring;
        modcount:integer;
        mods:array[0..15] of ansistring;
    end;

    twarp = record
        id:integer;
        pos:array[0..15] of byte;
    end;

    tswitch = record
        id:integer;
        cmd,target:integer;
    end;

    tdoorref = record
        fl,owner,uni:word;
        pos:array[0..7] of word;
    end;

    TGroupInfo = record
        id,active,alt,trigger:integer;
        linked:integer;
        isevent,doevent:integer;
        onClear:ansistring;
    end;

    TCodeEvent = record
        used:integer;
        name,obj,ty:ansistring;
        count,max,floor:integer;
        id1,id2,id3,exp,mesetas:integer;
    end;

    TCallBack =record
        name:string;
        cmd:tstringlist;
    end;
    tdoorinfo = record
        used:byte;
        posa,posb:array[0..7] of word;
    end;
    TTriggerObj = record
        id,action:integer;
        pos:array[0..7] of word;
        name:string[32];
    end;

    TTeamEntry = record
        used:integer;
        Multi,p1,p2,p3:integer;
        party1floor,party2floor,party3floor:integer;
        NextItemID:dword;
        splitfloor:array[0..4] of integer;

        //current mission data
        questid:dword;
        QuestId2:dword;
        FloorCount:integer;
        MapSection,PipePos:array[0..16,0..127] of TMapSection;
        Mission:ansistring;
        floorspawn:array[0..1,0..15] of ansistring;

        //in mission data
        FloorObj:array[0..15] of ansistring;
        ObjTrigger:array[0..15,0..63] of TTriggerObj;
        GroupInfo:array[0..15,0..63] of TGroupInfo;
        Monst:array[0..15,0..127] of TMonsterEntry;
        MonstCount:array[0..15] of integer;
        FloorEventCount,FloorInEvent:array[0..15] of integer;

        npccount:integer;
        npcs:array[0..15] of TMonsterEntry;

        //scrypt data
        missioncount:integer;
        rewardexp,rewardmst:integer;

        //queststart:ansistring;

        datacount:integer;
        data:array[0..63] of TDataGroup;
        countid:integer;
        doorref:array[0..15,0..15] of tdoorref;

        //warp
        warpcount:integer;
        warps:array[0..15] of twarp;
        bosswarpid:array[0..15] of  integer;
        bosswarpdata:array[0..15] of ansistring;

        //switch
        switchcount:integer;
        switchs:array[0..31] of tswitch;

        //drop data
        drop:TDropInfo;

        //meteo
        meteo:tstringlist;
        cmeteo,planet:integer;

        //special event
        ActiveCode:array[0..7] of TCodeEvent;
        CallBacks:array[0..127] of TCallBack;

        //floor variation info for when you reload
        floorvar:array[0..15] of byte;
        FloorMap:array[0..15] of ansistring;
        floor_num:array[0..15] of integer;

        //interactions
        floor_inter:array[0..15,0..15] of tinteraction;
        //once limiter
        floor_once: array[0..15] of tstringlist;

        door_entry_p:array[0..15] of dword;
        door_pos:array[0..15] of tdoorinfo;

        ChalengeTicks:int64;
        ChalengeStartTicks:dword;
        ChalengeMax:single;
        ChalengeCurrent:single;
    end;

    TQuestList = record
        mask:int64;
        offset,tag:integer;
        name:ansistring;
    end;

    TQuestDif = record
        rlvl,mlvl:integer;
        item:array[0..1,0..7] of byte;
        monster:array[0..2,0..1] of dword;
    end;

    TQuestInfo = record
        id:integer;
        gameid:integer;
        time,team,planet:integer;
        difflag:integer;
        difinfo:array[0..7] of TQuestDif;
    end;




procedure RequestQuestList(x:integer;id:pdword);
procedure SendQuestBoard(x:integer);
procedure RequestQuestList2(x:integer;CID,QID:pdword);
procedure RequestQuestInfo(x:integer;s:ansistring);
procedure InitTeam();
procedure CreateTeam(x:integer;s:ansistring);
function GetNextTeamItemID(t:integer):dword;
procedure SetQuestID(x:integer;id,multi:pdword);
Procedure InitQuests();
procedure SendQuestResult(x:integer);
procedure TestTeam(t:integer);
function CreateQuestStart(p,x:integer):ansistring;
procedure RefreshInTeamCount(t:integer);
procedure SendQuestFailled(x:integer);
procedure recountteam();



var Team:array[0..99] of TTeamEntry;
    CurrentQuestMask:int64 = 0;
    QuestsCount:array[0..47] of word;
    QuestInfo:array[0..200] of TQuestInfo;
    //QuestStart:tansistringlist;
    QuestCount:integer=0;
    QuestId:array[0..200] of integer;
    QuestMask:array[0..200] of int64;


const QuestListData:array[0..4] of TQuestList = (
  (mask: 8; offset: 3; tag : 3 ; name: 'arks' ),
  (mask: $10; offset: 4; tag : 4 ; name: 'limited' ),
  (mask: $1000; offset: 12; tag : 12 ; name: 'exploration' ),
  (mask: $10000; offset: 16; tag : 16 ; name: 'chalenge' ),
  (mask:  -1;offset: -1;tag : 0 ;name: '')
);

implementation

uses UPlayer, UPsoUtil, Uerror, fmain, UDB, UForest, UParty, UDrop, ULobby,
  clientorder, ItemGenerator;


procedure RefreshInTeamCount(t:integer);
var i,c:integer;
    a:ansistring;
begin
    if t = -1 then exit;
    c:=0;
    for i:=0 to srvcfg.MaxPlayer-1 do
        if playerok(i) then
         if player[i].TeamID = t then inc(c);
    a:=#$0C#$00#$00#$00#$0E#$4B#$00#$00#$01#$04#$00#$00;
    a[9]:=ansichar(c);
    if team[t].Multi = 1 then a[10]:=#$c;

    for i:=0 to srvcfg.MaxPlayer-1 do
         if playerok(i) and (player[i].TeamID = t) then sendtoplayer(i,a);
end;

function fixquestinfo(s:ansistring;id:integer):ansistring;
begin
    result:=s;
    move(id,result[$21],4);
end;

function fixqueststart(s:ansistring;id:integer):ansistring;
begin
    result:=s;
    move(id,result[$35],4);
    move(id,result[$20d],4);

end;

procedure CleanQuestInfo(x:integer);
var i:integer;
begin
    Questinfo[x].time:=1;
    Questinfo[x].team:=1;
    Questinfo[x].planet :=3;
    Questinfo[x].difflag:=0;
    for i:=0 to 7 do begin
        Questinfo[x].difinfo[i].rlvl:=1+(i*20);
        Questinfo[x].difinfo[i].mlvl:=1+(i*20);
        fillchar(Questinfo[x].difinfo[i].item[0],8,0);
        fillchar(Questinfo[x].difinfo[i].item[1],8,0);
        fillchar(Questinfo[x].difinfo[i].monster[0,0],24,0);
        Questinfo[x].difinfo[i].monster[0,0]:=$ffffffff;
        Questinfo[x].difinfo[i].monster[1,0]:=$ffffffff;
        Questinfo[x].difinfo[i].monster[2,0]:=$ffffffff;
    end;
end;

Procedure InitQuests();
var i,l,x,k,c:integer;
    sr: TSearchRec;
    tmp:tstringlist;
    a:ansistring;
begin
    tmp:=tstringlist.Create;
    //QuestInfo:=tansistringlist.Create;
    //QuestStart:=tansistringlist.Create;
    fillchar(QuestsCount[0],48*2,0);
    x:=0;
    if FindFirst(path+'quest\*.*', faAnyfile, sr) = 0 then
    begin
      repeat
        if sr.Attr and faDirectory = faDirectory then
        if (sr.Name <> '.') and (sr.Name <> '..') then begin
            cleanQuestInfo(x);
            tmp.LoadFromFile(path+'quest\'+sr.Name+'\info.txt');
            for i:=0 to tmp.Count-1 do begin
                if copy(tmp[i],1,7) = 'folder:' then begin
                    a:= lowercase(tmp[i]);
                    delete(a,1,8);
                    l:=0;
                    while QuestListData[l].mask <> -1 do begin
                        if QuestListData[l].name = a then begin
                            inc(QuestsCount[QuestListData[l].offset]);
                            QuestMask[x]:= QuestListData[l].tag;
                            if QuestsCount[QuestListData[l].offset] = 1 then //add it to the mask
                               CurrentQuestMask:=CurrentQuestMask or QuestListData[l].mask;
                        end;
                        inc(l);
                    end;
                end;

                if lowercase(copy(tmp[i],1,3)) = 'id:' then begin
                    a:= tmp[i];
                    delete(a,1,4);
                    QuestInfo[x].id:=strtoint(a);
                end;

                if lowercase(copy(tmp[i],1,8)) = 'planet: ' then begin
                    a:= tmp[i];
                    delete(a,1,8);
                    QuestInfo[x].planet:=strtoint(a);
                end;

                if lowercase(copy(tmp[i],1,5)) = 'time ' then begin
                    a:= tmp[i];
                    delete(a,1,5);
                    QuestInfo[x].time:=strtoint(a);
                end;

                if lowercase(copy(tmp[i],1,5)) = 'team ' then begin
                    a:= tmp[i];
                    delete(a,1,5);
                    QuestInfo[x].team:=strtoint(a);
                end;

                if lowercase(copy(tmp[i],1,10)) = 'dificulty ' then begin
                    a:= tmp[i];
                    delete(a,1,10);
                    k:=strtoint(a)-1;
                    QuestInfo[x].difflag:=QuestInfo[x].difflag or (1 shl k);
                end;

                if lowercase(copy(tmp[i],1,8)) = 'req_lvl ' then begin
                    a:= tmp[i];
                    delete(a,1,8);
                    QuestInfo[x].difinfo[k].rlvl:=strtoint(a);
                end;

                if lowercase(copy(tmp[i],1,10)) = 'monst_lvl ' then begin
                    a:= tmp[i];
                    delete(a,1,10);
                    QuestInfo[x].difinfo[k].mlvl:=strtoint(a);
                end;

                if lowercase(copy(tmp[i],1,6)) = 'item1 ' then begin
                    a:= tmp[i];
                    delete(a,1,6);
                    for c:=0 to 7 do begin
                        QuestInfo[x].difinfo[k].item[0,c]:=strtoint('$'+copy(a,1,2));
                        delete(a,1,3);
                    end;
                end;

                if lowercase(copy(tmp[i],1,6)) = 'item2 ' then begin
                    a:= tmp[i];
                    delete(a,1,6);
                    for c:=0 to 7 do begin
                        QuestInfo[x].difinfo[k].item[1,c]:=strtoint('$'+copy(a,1,2));
                        delete(a,1,3);
                    end;
                end;

                if lowercase(copy(tmp[i],1,7)) = 'monst1 ' then begin
                    a:= tmp[i];
                    delete(a,1,7);
                    c:=pos(' ',a);
                    QuestInfo[x].difinfo[k].monster[0,0]:=strtoint64(copy(a,1,c-1));
                    delete(a,1,c);
                    QuestInfo[x].difinfo[k].monster[0,1]:=strtoint(a);
                end;

                if lowercase(copy(tmp[i],1,7)) = 'monst2 ' then begin
                    a:= tmp[i];
                    delete(a,1,7);
                    c:=pos(' ',a);
                    QuestInfo[x].difinfo[k].monster[1,0]:=strtoint64(copy(a,1,c-1));
                    delete(a,1,c);
                    QuestInfo[x].difinfo[k].monster[1,1]:=strtoint(a);
                end;

                if lowercase(copy(tmp[i],1,7)) = 'monst3 ' then begin
                    a:= tmp[i];
                    delete(a,1,7);
                    c:=pos(' ',a);
                    QuestInfo[x].difinfo[k].monster[2,0]:=strtoint64(copy(a,1,c-1));
                    delete(a,1,c);
                    QuestInfo[x].difinfo[k].monster[2,1]:=strtoint(a);
                end;

            end;
            QuestId[x]:=strtoint('$'+sr.Name);
            inc(x);
            QuestCount:=x;
         end;


      until FindNext(sr) <> 0;
      FindClose(sr);
    end;


    tmp.Free;
end;

function GetNextTeamItemID(t:integer):dword;
begin
    result:=team[t].NextItemID or $10000000;
    inc(team[t].NextItemID);
end;


procedure CreateTeam(x:integer;s:ansistring);
var a:ansistring;
    i,l,p:integer;
begin
    //magic  max 127 char
    //Xor: 114B  Sub: 18
    //Xor: 11CB  Sub: 98
    //player request creation of team
    p:=player[x].partyid;
          i:=9; //start of data
          l:=ReadMagic(copy(s,i,4),$11cb,$98);
          inc(i,4);
          partys[p].used:=1;
          partys[p].name:=copy(s,i,l*2);
          inc(i,l*2);
          if l and 1 = 1 then inc(i,2);
          l:=ReadMagic(copy(s,i,4),$11cb,$98);
          inc(i,4);
          partys[p].pass:=copy(s,i,l*2)+#0#0;
          inc(i,l*2);
          if l and 1 = 1 then inc(i,2);
          l:=ReadMagic(copy(s,i,4),$11cb,$98);
          inc(i,4);
          partys[p].comment:=copy(s,i,l*2);
          inc(i,l*2);
          if l and 1 = 1 then inc(i,2);
          l:=ReadMagic(copy(s,i,4),$11cb,$98);
          inc(i,4);
          partys[p].questname:=copy(s,i,l*2);//unknown for now
          inc(i,l*2);
          if l and 1 = 1 then inc(i,2);
          partys[p].lvlmin:=byte(s[i]);
          partys[p].lvlmax:=byte(s[i+1]);
          //find other options
          partys[p].options:=copy(s,i,12);
          partys[p].limitation:=byte(s[i+3]);

          //server magic
          //XOR: 9789   SUB: E3
          a:=#$58#$00#$00#$00#$0E#$0D#$04#$00+UniToUniMagic(partys[p].name,$9789,$e3)
          +UniToUniMagic(partys[p].pass,$9789,$e3)+UniToUniMagic(partys[p].comment,$9789,$e3)
          +partys[p].options;

          i:=length(a);
          move(i,a[1],4);

          //should send to party
          sendtoparty(x,a);

end;


procedure fixitemparty(t,l,id:integer); //fix the party id of the object of the floor L
var x,i:integer;
begin
    x:=1;
    while x<length(team[t].FloorObj[l]) do begin
        move(team[t].FloorObj[l][x],i,4);
        move(id,team[t].FloorObj[l][x+$12],2);
        inc(x,i);
    end;
end;

procedure SetQuestID(x:integer;id,multi:pdword);
var a,pf:ansistring;
    st:tstringlist;
    l,t,f,p,k,mp,fl:integer;
begin
    //remove from team
    l:=player[x].TeamID;
    for f:=0 to srvcfg.MaxPlayer-1 do
        if (playerok(f)) and (player[x].partyid = player[f].partyid) then
            player[f].TeamID:=-1;
    //free team is aviable
    if l > -1 then
    if team[l].Multi = 0 then team[l].used:=0 //not multi so free
    else begin
        //multi team
        if team[l].p1 = player[x].partyid then team[l].p1:=0;
        if team[l].p2 = player[x].partyid then team[l].p2:=0;
        if team[l].p3 = player[x].partyid then team[l].p3:=0;
        if team[l].p1+team[l].p2+team[l].p3 = 0 then team[l].used:=0; //no more team clean it
    end;

    //is it a multi quest ?
    mp:=0; //no multi
    fl:=1;
    for p:=0 to questcount-1 do
        if questid[p] = id^ then if questinfo[p].team = 2 then mp:=1; //yup multi party
    //if yes find an aviable room with it
    if mp = 1 then begin
        for p:=0 to 99 do
        if team[p].used = 1 then
        if team[p].questid = id^ then
        if (team[p].p1 = 0) or (team[p].p2 = 0) or (team[p].p3 = 0) then begin
            //set the team id
            fl:=0;
            for f:=0 to srvcfg.MaxPlayer-1 do
                if (playerok(f)) and (player[x].partyid = player[f].partyid) then
                    player[f].TeamID:=p;
            {if (team[p].p1 = 0) then team[p].p1 := player[x].partyid
            else if (team[p].p2 = 0) then team[p].p2 := player[x].partyid
            else if (team[p].p3 = 0) then team[p].p3 := player[x].partyid;  }
            //randomize position
            f:=random(3);
            while f <> -1 do begin
                //loop until valid pos is found
                if (f = 0) and (team[p].p1 = 0) then begin
                    team[p].p1 := player[x].partyid;
                    break;
                end;
                if (f = 1) and (team[p].p2 = 0) then begin
                    team[p].p2 := player[x].partyid;
                    break;
                end;
                if (f = 2) and (team[p].p3 = 0) then begin
                    team[p].p3 := player[x].partyid;
                    break;
                end;
                f:=random(3);
            end;
        end;
    end;



    //load quest data
    if player[x].teamid = -1 then begin
        for p:=0 to 99 do
        if team[p].used = 0 then begin
            team[p].used:=1;
            team[p].Multi:=mp;
            team[p].p1:=0;
            team[p].p2:=0;
            team[p].p3:=0;
            f:=random(3);
            if (f = 0) then team[p].p1 := player[x].partyid;
            if (f = 1) then team[p].p2 := player[x].partyid;
            if (f = 2) then team[p].p3 := player[x].partyid;

            player[x].teamid:=p;
            for f:=0 to srvcfg.MaxPlayer-1 do
                if (playerok(f)) and (player[x].partyid = player[f].partyid) then
                    player[f].TeamID:=p;
            break;
        end;
        if player[x].teamid = -1 then begin
            sendtoplayer(x,makeerror(ERR_TEAMLIMIT,'Team limit reatched.'));
            exit;
        end;
    end;

    recountteam();

    p:=player[x].teamid;
    team[p].ChalengeTicks:=0;
    partys[player[x].partyid].onTick.Clear;
    partys[player[x].partyid].time:=gettickcount;
    partys[player[x].partyid].bosswarp:=0;
    partys[player[x].partyid].death:=0;
    partys[player[x].partyid].result:=3;


    if fl = 1 then begin
        team[p].NextItemID:=$0;
        fillchar(team[player[x].TeamID].floorvar[0],16,0);
        fillchar(team[player[x].TeamID].door_entry_p[0],64,255);
        fillchar(team[player[x].TeamID].door_pos[0],sizeof(team[player[x].TeamID].door_pos),0);


        st:=tstringlist.create;
        team[player[x].TeamID].questid:=id^;
        team[player[x].TeamID].meteo.clear;
        team[player[x].TeamID].cmeteo:=0;
        team[player[x].TeamID].planet:=3;
        team[player[x].TeamID].splitfloor[0]:=-1;    //disable split
        team[player[x].TeamID].splitfloor[1]:=-1;
        team[player[x].TeamID].splitfloor[2]:=-1;
        team[player[x].TeamID].splitfloor[3]:=-1;

        st.LoadFromFile(path+'quest\'+inttohex(id^,2)+'\info.txt');
        for l:=0 to 7 do
            team[player[x].teamid].ActiveCode[l].used:=0;
        for l:=0 to 127 do
            team[player[x].teamid].callbacks[l].name:='';
        for l:=0 to st.Count-1 do begin
            a:=st[l];
            if copy(a,1,3) = 'ID:' then begin
                delete(a,1,4);
                team[player[x].TeamID].QuestId2 := strtoint(a);
            end;
            if copy(a,1,6) = 'Floor:' then begin
                delete(a,1,7);
                team[player[x].TeamID].FloorCount := strtoint(a);
            end;
            if copy(a,1,8) = 'planet: ' then begin
                delete(a,1,8);
                team[player[x].TeamID].planet := strtoint(a);
            end;
            if copy(a,1,4) = 'exp:' then begin
                delete(a,1,5);
                team[player[x].TeamID].rewardexp := strtoint(a);
            end;
            if copy(a,1,8) = 'mesetas:' then begin
                delete(a,1,9);
                team[player[x].TeamID].rewardmst := strtoint(a);
            end;
            if copy(a,1,6) = 'team1 ' then begin
                delete(a,1,6);
                team[player[x].TeamID].party1floor := strtoint(a);
            end;
            if copy(a,1,6) = 'team2 ' then begin
                delete(a,1,6);
                team[player[x].TeamID].party2floor := strtoint(a);
            end;
            if copy(a,1,6) = 'team3 ' then begin
                delete(a,1,6);
                team[player[x].TeamID].party3floor := strtoint(a);
            end;
            if copy(a,1,11) = 'splitfloor ' then begin
                delete(a,1,11);
                f:=0;
                k:=pos(' ',a);
                while k > 0 do begin
                    team[player[x].TeamID].splitfloor[f] := strtoint(copy(a,1,k-1));
                    k:=pos(' ',a);
                    inc(f);
                    delete(a,1,k);
                end;
                team[player[x].TeamID].splitfloor[f] := strtoint(a);
            end;
            if copy(a,1,8) = 'weather ' then begin
                delete(a,1,8);
                team[player[x].TeamID].meteo.add(a);
            end;
            if copy(a,1,9) = 'floorvar ' then begin
                delete(a,1,9);
                k:=pos(' ',a);
                f:=strtoint(copy(a,1,k-1));
                delete(a,1,k);
                team[player[x].TeamID].floorvar[f]:=strtoint(a);
            end;
        end;
        st.Free;

        //load drop file
        LoadDropFile(player[x].TeamID,path+'quest\'+inttohex(id^,2)+'\drop1.txt');

        //load spawn point
        t:=player[x].TeamID;
        //team[t].death:=0;
        team[t].datacount:=0;
        team[t].warpcount:=0;
        team[t].switchcount:=0;
        t:=player[x].TeamID;
        partys[player[x].partyid].varcount:=0;
        for l:=0 to team[t].FloorCount-1 do begin
            pf:='';
            fillchar(team[t].ObjTrigger[l],sizeof(team[t].ObjTrigger[l]),0);
            team[t].floor_num[l]:=l;
            if team[t].floorvar[l] <> 0 then begin
                k:=random(team[t].floorvar[l]);
                //k:=team[t].floorvar[l]-1;
                if k > 0 then pf:='_'+inttostr(k);
            end;
            team[t].floorspawn[0,l]:=filegetpacket( 'quest\'+inttohex(id^,2)+'\floor'+inttostr(l+1)+pf+'.spawn1');
            team[t].floorspawn[1,l]:=filegetpacket( 'quest\'+inttohex(id^,2)+'\floor'+inttostr(l+1)+pf+'.spawn2');
            //load map area
            f:=fileopen(path+'quest\'+inttohex(id^,2)+'\floor'+inttostr(l+1)+pf+'.mapsection',$40);
            fileread(f,team[t].MapSection[l],2000);
            fileclose(f);
            for k:=0 to 127 do team[t].PipePos[l,k].id:=255;
            if fileexists(path+'quest\'+inttohex(id^,2)+'\floor'+inttostr(l+1)+pf+'.pipe') then begin
                f:=fileopen(path+'quest\'+inttohex(id^,2)+'\floor'+inttostr(l+1)+pf+'.pipe',$40);
                fileread(f,team[t].PipePos[l],2000);
                fileclose(f);
            end;

            team[t].FloorMap[l]:=filegetpacket( 'quest\'+inttohex(id^,2)+'\floor'+inttostr(l+1)+pf+'.map');

            team[t].FloorObj[l]:=GenerateObjFile(path+ 'quest\'+inttohex(id^,2)+'\floor'+inttostr(l+1)+pf+'.obj',t,l);
            fixitemparty(t,l,$11a9);
            //team[t].FloorItems[l]:='';
            loadmonsterlist(player[x].partyid, t,l,'quest\'+inttohex(id^,2)+'\floor'+inttostr(l+1)+pf+'.monster'
                ,'quest\'+inttohex(id^,2)+'\floor'+inttostr(l+1)+'.code');
        end;
        //random meteo
        team[t].cmeteo:=random(team[t].meteo.Count);
        team[t].npccount:=0;
    end else begin
        //load only the proper floor
        partys[player[x].partyid].varcount:=0;
        t:=player[x].TeamID;
        l:=0;
        if team[t].p1 = player[x].partyid then l:=team[t].party1floor-1;
        if team[t].p2 = player[x].partyid then l:=team[t].party2floor-1;
        if team[t].p3 = player[x].partyid then l:=team[t].party3floor-1;

        fillchar(team[t].ObjTrigger[l],sizeof(team[t].ObjTrigger[l]),0);
        team[t].floor_num[l]:=l;
        pf:='';
        if team[t].floorvar[l] <> 0 then begin
            k:=random(team[t].floorvar[l]);
            if k > 0 then pf:='_'+inttostr(k);
        end;

        team[t].floorspawn[0,l]:=filegetpacket( 'quest\'+inttohex(id^,2)+'\floor'+inttostr(l+1)+pf+'.spawn1');
        team[t].floorspawn[1,l]:=filegetpacket( 'quest\'+inttohex(id^,2)+'\floor'+inttostr(l+1)+pf+'.spawn2');
        //load map area
        f:=fileopen(path+'quest\'+inttohex(id^,2)+'\floor'+inttostr(l+1)+pf+'.mapsection',$40);
        fileread(f,team[t].MapSection[l],1000);
        fileclose(f);

        team[t].FloorMap[l]:=filegetpacket( 'quest\'+inttohex(id^,2)+'\floor'+inttostr(l+1)+pf+'.map');

        team[t].FloorObj[l]:=GenerateObjFile(path+  'quest\'+inttohex(id^,2)+'\floor'+inttostr(l+1)+pf+'.obj',t,l);
        fixitemparty(t,l,$11a9);
        //team[t].FloorItems[l]:='';
        loadmonsterlist(player[x].partyid, t,l,'quest\'+inttohex(id^,2)+'\floor'+inttostr(l+1)+pf+'.monster',
            'quest\'+inttohex(id^,2)+'\floor'+inttostr(l+1)+'.code');
    end;



    //load mission
    team[t].Mission:=filegetpacket( 'quest\'+inttohex(id^,2)+'\quest.mission');
    move(player[x].partyid,team[t].Mission[$15],4);
    l:=$153;
    move(l,team[t].Mission[$1f],2);
    l:=$11a9;
    move(l,team[t].Mission[$9],4);

    for k:=0 to srvcfg.MaxPlayer-1 do
    if playerok(k) and (player[k].partyid = player[x].partyid) then begin
    for l:=0 to 15 do begin
        player[k].FloorArea[l]:=#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0;
        player[k].FloorObj[l]:='';
        player[k].FloorItems[l]:='';
    end;
    player[k].floor:=-1;
    end;

    if multi^ = 1 then begin
        a:=#$2C#$00#$00#$00#$0E#$61#$04#$00#$03#$00#$00#$00#$4C#$79#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00 ;
        sendtoparty(x,a);
    end;

    a:=#$40#$00#$00#$00#$0E#$25#$00#$00#$3F#$75#$00#$00#$00#$00#$00#$00
      +#$00#$00#$00#$00#$00#$00#$03#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
      +#$04#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
      +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$03#$00#$00#$00;
   { if team[player[x].TeamID].Multi = 1 then begin
        a[$39]:=#1;
        a[$3d]:=#12;     //12 player
    end;  }
    move(player[x].gid,a[$19],4);
    move(team[player[x].TeamID].QuestId2,a[$9],4);
    sendtoparty(x,a);

    sendtoplayer(x,#$1C#$00#$00#$00#$0E#$58#$04#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$FF#$C6#$00#$00#$FF#$C6#$00#$00#$FF#$C6#$00#$00);

    //use the server memory copy
    for l:=0 to questcount-1 do
        if questid[l] = id^ then
            a:=CreateQuestStart(x,l);//fixqueststart(FileGetPacket('quest\'+inttohex(id^,1)+'\quest.start'),id^);
    partys[player[x].partyid].queststart:=a;
    //move(player[x].gid,a[$31d],4);
    sendtoparty(x,a);

    //set stuff for later
    for l:=0 to 200 do
        if (playerok(l)) then if player[l].partyid = player[x].partyid then begin
            player[l].teammission:=0;
            player[l].questcompleted:=0;
            player[l].failfrom:=''; //empty = no fail
            if (player[l].room >= ROOM_TEAM) or (multi^ = 1) then GoBootCamp(l,0); //force reload new maps
        end;

    RefreshInTeamCount(player[x].teamid);
end;

procedure InitTeam();
var i,l:integer;
begin
    for i:=0 to 99 do begin
        Team[i].used:=0;
        Team[i].meteo :=tstringlist.create;
        for l:=0 to 127 do
            Team[i].callbacks[l].cmd:=tstringlist.create;
        for l:=0 to 15 do
            team[i].floor_once[l]:=tstringlist.create;
    end;
    for i:=1 to 200 do begin
        partys[i].used:=0;
        partys[i].onTick:=tstringlist.create;
    end;
end;

procedure SendQuestBoard(x:integer);
begin
    sendtoplayer(x,#$A0#$01#$00#$00#$0B#$22#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00
	+#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00
	+#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00
	+#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00
	+#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00
	+#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00
	+#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00
	+#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00
	+#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00
	+#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00
	+#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00
	+#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00
	+#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00
	+#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00
	+#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00
	+#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00
	+#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00
	+#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00
	+#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00
	+#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00
	+#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00
	+#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00
	+#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00
	+#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00
	+#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00);
end;

procedure RequestQuestList(x:integer;id:pdword);
var s:ansistring;
  qm:int64;
begin

{000000: 9C 00 00 00 0B 16 00 00 00 00 00 00 00 00 07 00     �...............
000010: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
000020: 01 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
000030: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
000040: 00 00 00 00 00 00 00 00 03 00 00 00 00 00 00 00     ................
000050: 00 00 00 00 02 00 01 00 01 00 01 00 01 00 01 00     ................
000060: 01 00 01 00 01 00 01 00 00 00 00 00 00 00 00 00     ................
000070: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
000080: 00 00 00 00 08 00 00 00 C1 7F 00 00 08 10 00 00     ........�......
000090: C1 FF 00 00 00 00 00 00 00 00 00 00     ��..........
}
    s:=#$9C#$00#$00#$00#$0B#$16#$00#$00#$00#$00#$00#$00+#$00#$00#$01#$00   //arks count  2 byte per count, need to find who is who
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$03#$00#$00#$00#$00#$00    //emergency
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$04#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 //this is the aviable quest list,gave me 4 of them
	+#$00#$00#$00#$00#$01#$00#$00#$00#$08#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;

    move(QuestsCount[0],s[$9],$60);
    qm:=CurrentQuestMask and (not $10000); //remove chalenge

    if player[x].room = Room_ChalengeLobby then begin
        fillchar(s[9],$78,0); //clear quest count
        move(QuestsCount[16],s[$29],2); //put only chalenge
        qm:=$10000; //chalenge
    end else s[$29]:=#0; //no chalenge


    move(qm,s[$85],8);
    move(qm,s[$8d],8);
    sendtoplayer(x,s);


    //unknow but now needed since ep5
    sendtoplayer(x,#$30#$00#$00#$00#$49#$01#$00#$00#$02#$00#$03#$00#$00#$00#$03#$00
    +#$64#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$F4#$01#$00#$00
    +#$00#$00#$00#$00#$B8#$0B#$00#$00#$02#$00#$00#$00#$03#$00#$00#$00);


    sendtoplayer(x,#$0C#$00#$00#$00#$0B#$62#$00#$00#$7D#$13#$00#$00);

end;

function CreateQuestInfo(x,p:integer):ansistring;
var i,c:integer;
begin
    result:=#$32#$30#$31#$32#$2F#$30#$31#$2F#$30#$35#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$3C#$00#$00#$00#$00#$00#$00#$00#$0B#$00#$00#$00#$4E#$75#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$01#$00#$02#$02#$0F#$0F#$00#$01#$00#$05#$00#$03  //01 = new
    +#$F1#$00#$04#$0C#$FF#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00          //100
    +#$00#$00#$00#$00#$32#$00#$0C#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$55#$00#$00#$00#$00#$00   //32 = 50% bonus
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00          //200
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00          //300
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00          //400
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;//#$FF#$FF#$FF#$FF;
    move(Questid[x],result[$21],4);
    move(QuestInfo[x].id,result[$2d],4);
    if QuestInfo[x].difflag = player[p].activeChar.questflag[Questid[x]] then result[$f5]:=#0; //nothing new
    result[$f7]:=ansichar(QuestInfo[x].time);
    result[$f8]:=ansichar(QuestInfo[x].team);
    result[$f9]:=ansichar(QuestInfo[x].difflag);
    result[$fa]:=ansichar(QuestInfo[x].difflag xor player[p].activeChar.questflag[Questid[x]]); //need to filter the one already done
    result[$fe]:=ansichar(QuestInfo[x].difinfo[0].mlvl);
    i:=$12d;
    for c:=0 to 7 do begin
        move(QuestInfo[x].difinfo[c].item[0],result[i],8);
        move(QuestInfo[x].difinfo[c].item[1],result[i+12],8);
        inc(i,$60);
    end;
end;

procedure RequestQuestList2(x:integer;CID,QID:pdword);
var s,a:ansistring;
    i,c:integer;
begin
    //#$10#$00#$00#$00#$0B#$17#$00#$00#$E8#$03#$00#$00#$03#$00#$00#$00

    s:=#$E0#$01#$00#$00#$0B#$18#$04#$00#$76#$1D#$00#$00;
    c:=0;
    for i:=0 to QuestCount-1 do
    if questmask[i] = qid^ then begin
        inc(c);
        s:=s+CreateQuestInfo(i,x);
    end;
    a:=GetMagic(c,$1DB0, $C5);
    move(a[1],s[9],4);
    i:=length(s);
    move(i,s[1],4);
    sendtoplayer(x,s);

    sendtoplayer(x,#$08#$00#$00#$00#$0B#$1B#$00#$00);
    {
    32 30 31 32 2F 30 31 2F 30 35 00 00 00 00 00 00  //date
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
3C 00 00 00 00 00 00 00 0B 00 00 00 4E 75 00 00   //list id:0b 00 00 00, quest internal id
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 01 00 02 02 0F 0F 00 01 00 05 00 03 0x4 = (01) = new, 0x6 = time icon, 0x7 = team count, 0x8 = bit flag for aviable lvl, 0x9 = success flag, tell you wich one you didnt do, 0xd = min lvl , 0xf (3) = folder
F1 00 04 0C FF 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 01 00 0C 00 00 00 55 00 00 00 00 00  0x4 = special drop 1  (12 bytes per item)
01 00 10 00 00 00 D3 00 00 00 00 00 01 00 02 00   special drop #2
00 00 23 00 00 00 00 00 01 00 01 00 00 00 54 00    keep going for other lvl
00 00 00 00 01 00 07 00 00 00 22 00 00 00 00 00
01 00 08 00 00 00 06 01 00 00 00 00 01 00 0D 00
00 00 79 00 00 00 00 00 01 00 0B 00 00 00 2F 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 FF FF FF FF
}
end;


function CreateQuestStart(p,x:integer):ansistring;
var i,c:integer;
begin
    result:=#$e4#$05#$00#$00#$0E#$31#$00#$00#$3A#$75#$00#$00#$00#$00#$00#$00
    +#$03#$00#$00#$00#$32#$30#$31#$32#$2F#$30#$31#$2F#$30#$35#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$40#$00#$00#$00#$00#$00#$00#$00#$0B#$00#$00#$00
    +#$3A#$75#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$01#$01#$0F#$00#$00#$01
    +#$00#$01#$00#$03#$00#$00#$04#$04#$FF#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00

    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00


    +#$32#$30#$31#$32
    +#$2F#$30#$31#$2F#$30#$35#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$40#$00#$00#$00
    +#$00#$00#$00#$00#$0B#$00#$00#$00#$3A#$75#$00#$00#$01#$04#$03#$00
    +#$01#$00#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00

    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00

    +#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00#$00#$00#$00#$00;
    move(Questid[x],result[$35],4);
    move(QuestInfo[x].id,result[$41],4);
    move(QuestInfo[x].id,result[$9],4);
    result[$f7+$14]:=ansichar(QuestInfo[x].time);
    result[$f8+$14]:=ansichar(QuestInfo[x].team);
    result[$f9+$14]:=ansichar(QuestInfo[x].difflag);
    result[$fa+$14]:=ansichar(QuestInfo[x].difflag); //need to filter the one already done
    result[$fe+$14]:=ansichar(QuestInfo[x].difinfo[0].mlvl);
    i:=$141;
    for c:=0 to 7 do begin
        move(QuestInfo[x].difinfo[0].item[0],result[i],8);     //only first dif for now
        move(QuestInfo[x].difinfo[0].item[1],result[i+12],8);
        inc(i,$60);
    end;
    //part #2
    move(Questid[x],result[$461],4);
    move(QuestInfo[x].id,result[$46d],4);
    result[$472]:=ansichar(QuestInfo[x].planet);
    i:=$475;

    for c:=0 to 7 do begin
        result[i]:=ansichar(QuestInfo[x].difinfo[c].rlvl);
        result[i+2]:=ansichar(QuestInfo[x].difinfo[c].mlvl);
        move(QuestInfo[x].difinfo[c].monster[0,0],result[i+$14],24);
        inc(i,$2c);
    end;
    move(player[p].gid,result[i],4);
end;

procedure RequestQuestInfo(x:integer;s:ansistring);
var i,l,c:integer;
    d:dword;
    a,b:ansistring;
begin
    //0x292C, 0x5B
    {
    000000: 24 00 00 00 0B 19 04 00 7C A3 00 00 2A 00 00 00     $.......|�..*...
000010: 00 00 00 00 0B 00 00 00 9D 00 00 00 00 00 00 00     ........�.......
000020: 0B 00 00 00     ....      }
    //the client request the list of quest here, 12byte per quest id
    a:= #$40#$01#$00#$00#$0B#$1A#$04#$00#$70#$29#$00#$00;
    c:=0;
    l:=13;
    while l < length(s) do begin
        move(s[l],d,4);
        for i:=0 to questCount-1 do
            if questid[i] = d then begin
                a:=a+copy(CreateQuestStart(x,i),$441,$194);
                inc(c);
                break;
            end;
        inc(l,12);
    end;
     b:=GetMagic(c,$292c, $5b);
    move(b[1],a[9],4);
    i:=length(a);
    move(i,a[1],4);
    sendtoplayer(x,a);

    sendtoplayer(x,#$08#$00#$00#$00#$0B#$1C#$00#$00);

    //#$0C#$00#$00#$00#$0B#$62#$00#$00#$7E#$13#$00#$00
    {
     50 03 00 00 0E 31 00 00 3A 75 00 00 00 00 00 00
03 00 00 00 32 30 31 32 2F 30 31 2F 30 35 00 00      //same as quest list but fill the items by the one of the lvl selected
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 40 00 00 00 00 00 00 00 0B 00 00 00
3A 75 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 01 01 0F 00 00 01     //100
00 01 00 03 00 00 04 04 FF 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 01 00 11 00 00 00 F1 00
00 00 00 00 01 00 0B 00 00 00 2A 01 00 00 00 00
01 00 11 00 00 00 F1 00 00 00 00 00 01 00 0B 00
00 00 2A 01 00 00 00 00 01 00 11 00 00 00 F1 00
00 00 00 00 01 00 0B 00 00 00 2A 01 00 00 00 00
01 00 11 00 00 00 F1 00 00 00 00 00 01 00 0B 00
00 00 2A 01 00 00 00 00 01 00 11 00 00 00 F1 00
00 00 00 00 01 00 0B 00 00 00 2A 01 00 00 00 00
01 00 11 00 00 00 F1 00 00 00 00 00 01 00 0B 00
00 00 2A 01 00 00 00 00 01 00 11 00 00 00 F1 00
00 00 00 00 01 00 0B 00 00 00 2A 01 00 00 00 00
01 00 11 00 00 00 F1 00 00 00 00 00 01 00 0B 00
00 00 2A 01 00 00 00 00 FF FF FF FF 32 30 31 32   //date
2F 30 31 2F 30 35 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 40 00 00 00    //200//list id
00 00 00 00 0B 00 00 00 3A 75 00 00 01 03 03 00   //internal id
01 00 01 00 00 00 00 00 00 00 00 00 E6 CA 8B 26   //min lvl, monster lvl  (entry #1, 0x24 byte per entry) the 3 monster info fallow id+02000000?
02 00 00 00 D8 B5 E4 29 02 00 00 00 B4 98 FE 29
02 00 00 00 22 00 15 00 00 00 00 00 00 00 00 00
E6 CA 8B 26 02 00 00 00 D8 B5 E4 29 02 00 00 00 
B4 98 FE 29 02 00 00 00 28 00 29 00 00 00 00 00
00 00 00 00 E6 CA 8B 26 02 00 00 00 D8 B5 E4 29 
02 00 00 00 B4 98 FE 29 02 00 00 00 32 00 3D 00 
00 00 00 00 00 00 00 00 E6 CA 8B 26 02 00 00 00
D8 B5 E4 29 02 00 00 00 B4 98 FE 29 02 00 00 00 
50 00 4B 00 00 00 00 00 00 00 00 00 FF FF FF FF 
00 00 00 00 FF FF FF FF 00 00 00 00 FF FF FF FF
00 00 00 00 69 00 64 00 00 00 00 00 00 00 00 00 
FF FF FF FF 00 00 00 00 FF FF FF FF 00 00 00 00 
FF FF FF FF 00 00 00 00 AA 00 96 00 00 00 00 00
00 00 00 00 FF FF FF FF 00 00 00 00 FF FF FF FF 
00 00 00 00 FF FF FF FF 00 00 00 00 BE 00 C8 00 
00 00 00 00 00 00 00 00 FF FF FF FF 00 00 00 00
FF FF FF FF 00 00 00 00 FF FF FF FF 00 00 00 00
56 5F 9C 00 00 00 00 00 04 00 00 00 00 00 00 00

    }
end;


procedure SendQuestFailled(x:integer);
var s:ansistring;
begin
  {
  Xor: F305  Sub: E4
  Xor: F307  Sub: E6
  Xor: F30D  Sub: EC
  Xor: F30F  Sub: EE
  Xor: F325  Sub: C4
  Xor: F327  Sub: C6
  Xor: F32D  Sub: CC
  Xor: F32F  Sub: CE
  Xor: F345  Sub: A4
  Xor: F347  Sub: A6
  Xor: F34D  Sub: AC
  Xor: F34F  Sub: AE
  Xor: F365  Sub: 84
  Xor: F367  Sub: 86
  Xor: F36D  Sub: 8C
  Xor: F36F  Sub: 8E
  Xor: F385  Sub: 64
  Xor: F387  Sub: 66
  Xor: F38D  Sub: 6C
  Xor: F38F  Sub: 6E
  Xor: F3A5  Sub: 44
  Xor: F3A7  Sub: 46
  Xor: F3AD  Sub: 4C
  Xor: F3AF  Sub: 4E
  Xor: F3C5  Sub: 24
  Xor: F3C7  Sub: 26
  Xor: F3CD  Sub: 2C
  Xor: F3CF  Sub: 2E
  Xor: F3E5  Sub: 04
  Xor: F3E7  Sub: 06
  Xor: F3ED  Sub: 0C
  Xor: F3EF  Sub: 0E}

   s:=#$70#$00#$00#$00#$0B#$27#$04#$00#$A9#$11#$00#$00#$00#$00#$00#$00
    +#$10#$00#$00#$00#$E0#$F3#$00#$00#$00#$00#$00#$00#$F4#$F3#$00#$00
    +#$49#$6E#$63#$69#$64#$65#$6E#$74#$4E#$61#$6D#$65#$00#$00#$00#$00
    +#$E0#$F3#$00#$00#$42#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$02#$0C#$00#$00#$00#$F5#$F3#$00#$00#$69#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$02#$00#$00#$00;

   // move(player[x].partyid,s[9],4);
    move(player[x].failfrom[1],s[$35],length(player[x].failfrom));
    move(player[x].failtype[1],s[$5d],length(player[x].failtype));
    sendtoplayer(x,s);
end;

procedure SendQuestResult(x:integer);
var s:ansistring;
    i,l,k,c:integer;
begin
    player[x].questcompleted:=0; //remove flag

    s:=#$B8#$05#$00#$00#$0B#$14#$04#$00#$10#$27#$00#$00#$BC#$02#$00#$00
	+#$BC#$02#$00#$00#$00#$00#$00#$00#$0B#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$2C#$01#$00#$00
	+#$2C#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$23#$06#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$23#$06#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00
	+#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$23#$06#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$01#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$23#$06#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$01#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$23#$06#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$23#$06#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$23#$06#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$23#$06#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$23#$06#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$23#$06#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$23#$06#$00#$00#$00#$00#$00#$00
	+#$E8#$03#$00#$00#$E8#$03#$00#$00#$3A#$75#$00#$00#$03#$00#$00#$00
	+#$32#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$00#$00#$00#$00#$22#$06#$00#$00#$22#$06#$00#$00
	+#$22#$06#$00#$00#$22#$06#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$00#$00#$00#$00;

    //kill points
    c:=0;
    k:=0;
    for i:=0 to team[player[x].TeamID].FloorCount-1 do
        for l:=0 to team[player[x].TeamID].MonstCount[i]-1 do begin
            inc(c,50);
            if team[player[x].TeamID].Monst[i,l].wave = 0 then inc(k,50);
        end;
    move(k,s[$d],4);
    move(c,s[$11],4);

    i:=3-partys[player[x].partyid].death;
    if i< 0 then i:=0;
    i:=i * 100;
    move(i,s[$3d],4);

    c:=c+300;
    k:=k+i;

    move(k,s[$221],4);
    move(c,s[$225],4);

    k:=(k*100) div c;
    i:=partys[player[x].partyid].result - partys[player[x].partyid].death;
    if i < 0 then i:=0;

    s[$22d]:=ansichar(i);

    sendtoplayer(x,s);

    TestForClientOrder(x);
end;

procedure recountteam() ;
var x,c:integer;
begin
    c:=0;
    for x:=0 to 99 do
        if team[x].used = 1 then inc(c);
    form1.label22.caption:=inttostr(c);
end;

procedure TestTeam(t:integer);
var i:integer;
begin
    for i:=0 to srvcfg.MaxPlayer-1 do
        if playerok(i) and (player[i].TeamID = t) then exit;
    //if we get there clean it up

    team[t].used:=0;
    team[t].NextItemID:=0;

    recountteam();
    //team[t].queststart:='';
end;

end.
