unit UPsoUtil;

interface
uses types,sysutils,windows;

Function ReadMagic(s:ansistring;MagicXor,MagicSub:integer):integer;
function TextToPSO(s:ansistring):ansistring;
Function GetMagic(size,MagicXor,MagicSub:integer):ansistring;
function TextToUni(s:ansistring;mx,ms:integer):ansistring;
function GetCharNameWithMagic(x,mx,ms:integer):ansistring;
function UniToUniMagic(s:ansistring;mx,ms:integer):ansistring;
function HalfPrecisionToFloat(v:word):single;
function FloatToHalf(Float: Single): word;
procedure sendsystemmsg(x:integer;s:ansistring);
function WideToAnsi(s:widestring):ansistring;
procedure safemove(const Source;var s:ansistring;org,size:integer);

const
  HalfMin:     Single = 5.96046448e-08; // Smallest positive half
  HalfMinNorm: Single = 6.10351562e-05; // Smallest positive normalized half
  HalfMax:     Single = 65504.0;        // Largest positive half
  // Smallest positive e for which half (1.0 + e) != half (1.0)
  HalfEpsilon: Single = 0.00097656;
  HalfNaN:     word = 65535;
  HalfPosInf:  word = 31744;
  HalfNegInf:  word = 64512;


implementation

uses fmain, UPlayer, UDebug;

procedure safemove(const Source;var s:ansistring;org,size:integer);
begin
    if (org+size > length(s)+1) or (org <1) then begin
        //error
        raise exception.Create('Move outside string boundary');
        exit;
    end;
    move(Source,s[org],size);
end;

function WideToAnsi(s:widestring):ansistring;
begin
    setlength(result,length(s)*2);
    move(s[1],result[1],length(result));

end;


procedure sendsystemmsg(x:integer;s:ansistring);
var a:ansistring;
    i:integer;
begin
    //XOR: 2126   SUB: B0
    a:=#$50#$00#$00#$00#$19#$08#$04#$00+TextToUni(s,$2126,$b0)+#$96#$21#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;
    i:=length(a);
    move(i,a[1],4);
    sendtoplayer(x,a);
end;

Function GetMagic(size,MagicXor,MagicSub:integer):ansistring;
var i:integer;
begin
    i:=(size + MagicSub) xor MagicXor;
    result:=#0#0#0#0;
    move(i,result[1],2);
end;

Function ReadMagic(s:ansistring;MagicXor,MagicSub:integer):integer;
var i:integer;
begin
    move(s[1],i,4);
    i:=(i xor MagicXor) - MagicSub;
    result:=i;
end;

function TextToPSO(s:ansistring):ansistring;
var i:integer;
begin
    //compute the magic number
    s:=s+#0;
    if length(s) and 1 = 1 then s:=s+#0;

    result:=GetMagic(length(s),$78F7,$a2);
    for i:=1 to length(s) do
        result:=result+s[i]+#0;
    

end;

function TextToUni(s:ansistring;mx,ms:integer):ansistring;
var i:integer;
begin
    //compute the magic number
    //s:=s+#0;


    result:=GetMagic(length(s),mx,ms);
    for i:=1 to length(s) do
        result:=result+s[i]+#0;

    if length(s) and 1 = 1 then result:=result+#0#0;

end;

function UniToUniMagic(s:ansistring;mx,ms:integer):ansistring;
var i:integer;
begin
    //compute the magic number
    //s:=s+#0;


    result:=GetMagic(length(s) div 2,mx,ms);
    result:=result+s;

    if length(s) and 3 <> 0 then result:=result+#0#0;

end;

function GetCharNameWithMagic(x,mx,ms:integer):ansistring;
var i,l:integer;
begin
    l:=0;
    i:=0;
    while player[x].activechar.base.name[i] <> #0 do begin
        inc(l);
        inc(i);
        if l >= 15 then break;
    end;
    inc(l);
    setlength(result,l*2);
    move(player[x].activechar.base.name[0],result[1],l*2);
    if l and 1 = 1 then result:=result+#0#0;
    result:=getmagic(l,mx,ms)+result;
end;


function HalfPrecisionToFloat(v:word):single;
var d:dword;
begin
    if v and $7fff = 0 then begin
        d:=0;
    end else begin
        d:=(v and $8000) shl 16;
        d:=d or ((((v and $7c00) shr 10)+$70) shl 23);
        d:=d or ((v and $3ff) shl 13);
    end;
    move(d,result,4);
end;

function FloatToHalf(Float: Single): word;
var i,sign,exponent,a,b:dword;
begin
    result:=0;
    move(float,i,4);
    if ((i and $7FFFFFFF) <> 0) then begin
        sign := (i shr 16) and $8000;
        exponent := ((i and $7F800000) shr 23) - $70;
        if exponent and $FFFFFFE0 <> 0 then begin
             result:=(exponent shr 17) xor $7fff or sign;
        end else begin
            a:=(i and $7fffff) shr 13;
            b:=exponent shl 10;
            result:= a or b or sign;
        end;
    end else  result:= i shr 16;

end;
{var
  Src: LongWord;
  Sign, Exp, Mantissa: LongInt;
begin
  Src := PLongWord(@Float)^;
  // Extract sign, exponent, and mantissa from Single number
  Sign := Src shr 31;
  Exp := LongInt((Src and $7F800000) shr 23) - 127 + 15;
  Mantissa := Src and $007FFFFF;
 
  if (Exp > 0) and (Exp < 30) then
  begin
    // Simple case - round the significand and combine it with the sign and exponent
    Result := (Sign shl 15) or (Exp shl 10) or ((Mantissa + $00001000) shr 13);
  end
  else if Src = 0 then
  begin
    // Input float is zero - return zero
    Result := 0;
  end
  else
  begin
    // Difficult case - lengthy conversion
    if Exp <= 0 then
    begin
      if Exp < -10 then
      begin        
        // Input float's value is less than HalfMin, return zero
         Result := 0;
      end
      else
      begin
        // Float is a normalized Single whose magnitude is less than HalfNormMin.  
        // We convert it to denormalized half.
        Mantissa := (Mantissa or $00800000) shr (1 - Exp);
        // Round to nearest
        if (Mantissa and $00001000) > 0 then
          Mantissa := Mantissa + $00002000;
        // Assemble Sign and Mantissa (Exp is zero to get denormalized number)
        Result := (Sign shl 15) or (Mantissa shr 13);
      end;
    end
    else if Exp = 255 - 127 + 15 then
    begin
      if Mantissa = 0 then
      begin
        // Input float is infinity, create infinity half with original sign
        Result := (Sign shl 15) or $7C00;
      end
      else
      begin
        // Input float is NaN, create half NaN with original sign and mantissa
        Result := (Sign shl 15) or $7C00 or (Mantissa shr 13);
      end;
    end
    else
    begin
      // Exp is > 0 so input float is normalized Single
 
      // Round to nearest
      if (Mantissa and $00001000) > 0 then
      begin
        Mantissa := Mantissa + $00002000;
        if (Mantissa and $00800000) > 0 then
        begin
          Mantissa := 0;
          Exp := Exp + 1;
        end;
      end;
 
      if Exp > 30 then
      begin
        // Exponent overflow - return infinity half
        Result := (Sign shl 15) or $7C00;
      end
      else
        // Assemble normalized half
        Result := (Sign shl 15) or (Exp shl 10) or (Mantissa shr 13);
    end;
  end;
end;    }

end.

