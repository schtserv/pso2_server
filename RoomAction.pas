unit RoomAction;

interface
uses types,sysutils,windows,Generics.Collections;

Procedure Packet1404(x:integer;d:ansistring);

implementation

uses UPlayer, UDB, fmain, UInventory, UMovement, UDebug, UQuestBoard,
  casino, UParty, UPsoUtil, UForest, Uerror, clientorder, ItemGenerator,
  UMyRoom, UTimerHelper, ULobby;



procedure RoomAction_GET_TARGET(x:integer;d:ansistring);
var s:ansistring;
begin
    s:=#$50#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
      +#$04#$00#$00#$00#$B7#$02#$00#$00#$00#$00#$00#$00#$06#$00#$5B#$0D
      +#$00#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
      +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
      +#$EF#$5C#$00#$00#$53#$45#$54#$5F#$54#$41#$52#$47#$45#$54#$00#$00;
    safemove(player[x].gid,s,9,4);
    safemove(d[$15],s,$15,12); //copy the id
     safemove(d[$25],s,$25,12);
    SendtoLobby(player[x].room,-1,s);
    s:=#$50#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
      +#$04#$00#$00#$00#$B7#$02#$00#$00#$00#$00#$00#$00#$06#$00#$5B#$0D
      +#$00#$00#$00#$00#$B7#$02#$00#$00#$00#$00#$00#$00#$06#$00#$5B#$0D
      +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
      +#$EF#$5C#$00#$00#$45#$4E#$44#$5F#$54#$41#$52#$47#$45#$54#$00#$00;
    safemove(player[x].gid,s,9,4);
    safemove(d[$15],s,$15,12); //copy the id
    safemove(d[$15],s,$25,12); //copy the id
    SendtoLobby(player[x].room,-1,s);
end;

procedure RoomAction_EXPLOSION(x:integer;d:ansistring);
var s:ansistring;
begin
    s:=#$24#$00#$00#$00#$04#$79#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
      +#$04#$00#$00#$00#$B7#$02#$00#$00#$00#$00#$00#$00#$06#$00#$5B#$0D
      +#$04#$00#$00#$00;
     safemove(player[x].gid,s,9,4);
    safemove(d[$15],s,$15,12); //copy the id
    SendtoLobby(player[x].room,-1,s);
end;

procedure RoomAction_TRAP_TARGET(x:integer;d:ansistring);
var s:ansistring;
begin
    s:=#$24#$00#$00#$00#$04#$79#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
      +#$04#$00#$00#$00#$B7#$02#$00#$00#$00#$00#$00#$00#$06#$00#$5B#$0D
      +#$02#$00#$00#$00;
     safemove(player[x].gid,s,9,4);
    safemove(d[$15],s,$15,12); //copy the id
    SendtoLobby(player[x].room,-1,s);
end;

procedure RoomAction_UniteMaster(x:integer;d:ansistring);
var s:ansistring;
begin
    s:=#$48#$00#$00#$00#$04#$24#$40#$00#$7C#$79#$9B#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00#$AB#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
    +#$AB#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11#$33#$11#$03#$00
    +#$FF#$FF#$FF#$FF#$0A#$59#$49#$CA#$00#$00#$00#$00#$04#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00;
    safemove(player[x].gid,s,9,4);
    safemove(d[$15],s,$15,12); //copy the id
    safemove(d[$15],s,$21,12); //copy the id
    SendtoLobby(player[x].room,-1,s);

    s:=#$48#$00#$00#$00#$04#$24#$40#$00#$7C#$79#$9B#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00#$AB#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
    +#$AB#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11#$bc#$12#$03#$00
    +#$FF#$FF#$FF#$FF#$0A#$59#$49#$CA#$00#$00#$00#$00#$04#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00;
    safemove(player[x].gid,s,9,4);
    safemove(d[$15],s,$15,12); //copy the id
    safemove(d[$15],s,$21,12); //copy the id
    SendtoLobby(player[x].room,-1,s);
end;

procedure RoomAction_UniteSlave(x:integer;d:ansistring);
var s:ansistring;
begin
    s:=#$48#$00#$00#$00#$04#$24#$40#$00#$7C#$79#$9B#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00#$AB#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
    +#$AB#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11#$12#$12#$03#$00
    +#$FF#$FF#$FF#$FF#$0A#$59#$49#$CA#$00#$00#$00#$00#$04#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00;
    safemove(player[x].gid,s,9,4);
    safemove(d[$15],s,$15,12); //copy the id
    safemove(d[$15],s,$21,12); //copy the id
    SendtoLobby(player[x].room,-1,s);
end;

procedure RoomAction_DeleteCheck(x:integer;d:ansistring);
var s:ansistring;
begin
    s:=#$50#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
      +#$04#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$06#$00#$0A#$3C
      +#$00#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$06#$00#$0A#$3C
      +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
      +#$EE#$5C#$00#$00#$44#$65#$6C#$65#$74#$65#$53#$74#$61#$72#$74#$00;
    safemove(player[x].gid,s,9,4);
    safemove(d[$15],s,$15,12); //copy the id
    safemove(d[$15],s,$25,12); //copy the id
    SendtoLobby(player[x].room,-1,s);
end;

procedure RoomAction_MoveCheck(x:integer;d:ansistring);
var s:ansistring;
begin
    s:=#$50#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
      +#$04#$00#$00#$00#$07#$00#$00#$00#$00#$00#$00#$00#$06#$00#$91#$43
      +#$00#$00#$00#$00#$07#$00#$00#$00#$00#$00#$00#$00#$06#$00#$91#$43
      +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
      +#$D0#$5C#$00#$00#$4D#$6F#$76#$65#$53#$74#$61#$72#$74#$00#$00#$00;
      safemove(player[x].gid,s,9,4);
      safemove(d[$15],s,$15,12); //copy the id
      safemove(d[$15],s,$25,12); //copy the id
      SendtoLobby(player[x].room,-1,s);

      s:=#$50#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
        +#$04#$00#$00#$00#$07#$00#$00#$00#$00#$00#$00#$00#$06#$00#$91#$43
        +#$00#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$D0#$5C#$00#$00#$4D#$6F#$76#$65#$53#$65#$74#$55#$70#$00#$00#$00;
      safemove(player[x].gid,s,9,4);
      safemove(d[$15],s,$15,12); //copy the id
      safemove(d[$15],s,$25,12); //copy the id
      SendtoLobby(player[x].room,-1,s);
end;

procedure RoomAction_Ready2b(x:integer;d:ansistring);
var a:ansistring;
    i,l,id:integer;
begin
      if player[x].Floor < 0 then exit;
      i:=1;
      id:=0;
      while i < length(team[player[x].TeamID].FloorObj[player[x].Floor]) do begin
          move(team[player[x].TeamID].FloorObj[player[x].Floor][i],l,4);
          if pdword(@team[player[x].TeamID].FloorObj[player[x].Floor][i+$48])^ = pdword(@d[$15])^ then
            id:=pdword(@team[player[x].TeamID].FloorObj[player[x].Floor][i+$8])^;

          inc(i,l);
      end;
      a:=#$54#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
      +#$04#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
      +#$00#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
      +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
      +#$EC#$5C#$00#$00#$41#$63#$63#$65#$73#$73#$41#$6C#$72#$65#$61#$64
      +#$79#$00#$00#$00;

      safemove(player[x].gid,a,9,4);
      safemove(d[$15],a,$15,12);
      safemove(id,a,$15,4);
      safemove(player[x].gid,a,$25,4);
      sendtoplayer(x,a);
end;

procedure RoomAction_Ready2(x:integer;d:ansistring);
var s:ansistring;
    i:integer;
begin
    s:=#$4c#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
      +#$04#$00#$00#$00#$16#$02#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
      +#$00#$00#$00#$00#$16#$02#$00#$00#$00#$00#$00#$00#$04#$00#$00#$00
      +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
      +#$D2#$5C#$00#$00#$41#$6C#$72#$65#$61#$64#$79#$00;
    safemove(player[x].gid,s,9,4);
    safemove(player[x].gid,s,$25,4);
    safemove(d[$15],s,$15,12);
    sendtoplayer(x,s);

    RoomAction_Ready2b(x,d);
end;

procedure RoomAction_STEP_ON(x:integer;d:ansistring);
var s:ansistring;
    i,l:integer;
begin
    //switch pressed
    s:=#$48#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00#$16#$02#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
    +#$00#$00#$00#$00#$16#$02#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$D7#$5C#$00#$00#$4F#$6E#$00#$00;
    safemove(player[x].gid,s,9,4);
    safemove(d[$15],s,$15,12); //copy the id
    safemove(d[$15],s,$25,12); //copy the id
    SendtoLobby(player[x].room,-1,s);

    //find the switch and do the action
    move(d[$15],l,4);
    for i:=0 to team[player[x].TeamID].switchcount-1 do begin
        if team[player[x].TeamID].switchs[i].id = l then begin
            //command?
            if team[player[x].TeamID].switchs[i].cmd = 1 then begin
                s:=#$4c#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
                +#$04#$00#$00#$00#$16#$02#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
                +#$00#$00#$00#$00#$16#$02#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
                +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                +#$D5#$5C#$00#$00#$4F#$50#$45#$4E#$00#$00#$00#$00;
                safemove(player[x].gid,s,9,4);
                safemove(team[player[x].TeamID].switchs[i].target,s,$15,4); //copy the id
                safemove(team[player[x].TeamID].switchs[i].target,s,$25,4); //copy the id
                SendtoLobby(player[x].room,-1,s);
                setfencestatus(player[x].TeamID,player[x].floor,team[player[x].TeamID].switchs[i].target,1);
                team[player[x].TeamID].switchs[i].cmd:=0; //no more action
            end;

        end;
    end;
end;

procedure RoomAction_ACTIVE(x:integer;d:ansistring);
var s:ansistring;
begin
    //player activated a mine
    s:=#$24#$00#$00#$00#$04#$79#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00#$A7#$01#$00#$00#$00#$00#$00#$00#$06#$00#$11#$1C#$03#$00#$00#$00;
    safemove(d[$15],s,$15,12);
    safemove(player[x].gid,s,9,4);
    sendtofloor(player[x].room,player[x].Floor,s);
end;

procedure RoomAction_RESET_POSITION(x:integer;d:ansistring);
var s:ansistring;
begin
    //ball need to be reset
    s:=#$44#$00#$00#$00#$04#$BE#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
        +#$04#$00#$00#$00#$8D#$01#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
        +#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$C6#$60#$D1#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00;
    safemove(d[$15],s,$15,12); //copy the id
    SendtoLobby(player[x].room,-1,s);
end;

procedure RoomAction_Transfer(x:integer;d:ansistring);
var s:ansistring;
    i,y,l:integer;
begin
    //support teleporter
    move(d[$15],i,4);

    //to put in a 2 stage transport
    if player[x].room>= room_team then begin
        if i = team[player[x].TeamID].bosswarpid[player[x].Floor] then begin
            //setup the warp

            s:=#$50#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
              +#$04#$00#$00#$00#$53#$05#$00#$00#$00#$00#$00#$00#$06#$00#$08#$09
              +#$00#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
              +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
              +#$EF#$5C#$00#$00#$41#$63#$63#$65#$73#$73#$4D#$65#$6E#$75#$00#$00;
            safemove(player[x].gid,s,9,4);
            safemove(player[x].gid,s,$25,4);
            safemove(d[$15],s,$15,12);
            sendtoparty(x,s);

            player[x].inboss:=1;

            //test if all there tell them its ok
            s:=#$54#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
              +#$04#$00#$00#$00#$00#$03#$00#$00#$00#$00#$00#$00#$06#$00#$A9#$11
              +#$00#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
              +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
              +#$EC#$5C#$00#$00#$54#$72#$61#$6E#$73#$66#$65#$72#$53#$74#$61#$72
              +#$74#$00#$00#$00;
            for y:=0 to srvcfg.MaxPlayer-1 do
                if playerok(y) and (player[y].partyid = player[x].partyid) then
                    if player[y].inboss = 0 then exit;
           for y:=0 to srvcfg.MaxPlayer-1 do
                if playerok(y) and (player[y].partyid = player[x].partyid) then begin
              safemove(player[y].gid,s,9,4);
              safemove(player[y].gid,s,$25,4);
              safemove(team[player[y].TeamID].bosswarpid[player[x].Floor],s,$15,4);
              sendtoparty(x,s);
           end;
          // WarpParty(player[x].partyid);
            exit;
        end;
    end;

    s:=#$50#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
      +#$04#$00#$00#$00#$13#$02#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
      +#$00#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
      +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
      +#$D0#$5C#$00#$00#$46#$6F#$72#$77#$61#$72#$64#$65#$64#$00#$00#$00;

    if player[x].room >= room_team then begin
        s[$1f]:=#$a9;
        s[$20]:=#$11;
    end;
    safemove(player[x].gid,s,$25,4);
    safemove(i,s,$15,4);
    SendtoLobby(player[x].room,-1,s);
    debug(1,inttohex(i,4));

    y:=i;
    if player[x].room = Room_lobby then begin
    s:=#$30#$00#$00#$00#$04#$02#$40#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$13#$02#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00;
    case i of
        $211: s:=s+#$00#$00#$00#$00#$00#$00#$FF#$3B#$00#$00#$19#$4B#$38#$D9#$00#$00;//to central shop
        $20d: s:=s+#$00#$00#$FF#$3B#$00#$00#$00#$00#$00#$00#$00#$00#$50#$58#$00#$00; // to central lobby
        $213: s:=s+#$00#$00#$1F#$36#$00#$00#$64#$3B#$0A#$C8#$00#$00#$78#$D8#$00#$00; // to right shop
        $209: s:=s+#$00#$00#$BA#$3B#$00#$00#$24#$34#$1F#$CF#$00#$00#$A6#$58#$00#$00; // to right lobby
        $216: s:=s+#$00#$00#$1F#$B6#$00#$00#$64#$3B#$0A#$48#$00#$00#$78#$D8#$00#$00; // to left shop
        $20b: s:=s+#$00#$00#$BA#$3B#$00#$00#$23#$B4#$20#$4F#$00#$00#$A6#$58#$00#$00; // to left lobby
        else begin
             s:=s+#$00#$00#$FF#$3B#$00#$00#$00#$00#$00#$00#$00#$00#$50#$58#$00#$00;
        end;
    end;
    end else begin
        s:=#$30#$00#$00#$00#$04#$02#$40#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$13#$02#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11;
        if team[player[x].TeamID].warpcount = 0 then begin
            s:=s+#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0; //add the current player pos
            safemove(player[x].Rotation,s,$21,16);
        end else begin

            for l:=0 to team[player[x].TeamID].warpcount-1 do begin
                if team[player[x].TeamID].warps[l].id = i then begin
                    s:=s+#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0;
                    safemove(team[player[x].TeamID].warps[l].pos[0],s,$21,16);
                    break;
                end;
            end;
            if length(s) = $20 then begin
                //not found, send the first entry
                s:=s+#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0;
                safemove(team[player[x].TeamID].warps[0].pos[0],s,$21,16);
            end;
        end;
    end;


    safemove(i,s,$15,4);
    sendtoplayer(x,s);

    s:=#$54#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
    +#$00#$00#$00#$00#$10#$02#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$EB#$5C#$00#$00#$4F#$62#$6A#$65#$63#$74#$54#$72#$61#$6E#$73#$66
    +#$65#$72#$00#$00;
    if player[x].room >= room_team then begin
        s[$2f]:=#$a9;
        s[$30]:=#$11;
    end;

    safemove(player[x].gid,s,$15,4);
    safemove(i,s,$25,4);
    SendtoLobby(player[x].room,-1,s);
end;

procedure RoomAction_MultiAbort(x:integer;d:ansistring);
var s:ansistring;
begin
    //this is a special transporter
    s:=#$50#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
      +#$04#$00#$00#$00#$D1#$00#$00#$00#$00#$00#$00#$00#$06#$00#$B2#$0C
      +#$00#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
      +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
      +#$EF#$5C#$00#$00#$4D#$75#$6C#$74#$69#$41#$62#$6F#$72#$74#$00#$00;
    safemove(player[x].gid,s,9,4);
    safemove(player[x].gid,s,$25,4);
    safemove(d[$15],s,$15,12);
    sendtoparty(x,s);

    RemoveTimer(x,player[x].GID,1);

    player[x].inboss := 0; //multi warp
end;

procedure RoomAction_MultiStart(x:integer;d:ansistring);
var s:ansistring;
begin
    s:=#$50#$00#$00#$00#$04#$15#$44#$00#$7C#$79#$9B#$00#$00#$00#$00#$00
      +#$04#$00#$00#$00#$EA#$00#$00#$00#$00#$00#$00#$00#$06#$00#$18#$3A
      +#$00#$00#$00#$00#$7C#$79#$9B#$00#$00#$00#$00#$00#$04#$00#$00#$00
      +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
      +#$EF#$5C#$00#$00#$4D#$75#$6C#$74#$69#$53#$74#$61#$72#$74#$00#$00;
    safemove(player[x].gid,s,9,4);
    safemove(player[x].gid,s,$25,4);
    safemove(d[$15],s,$15,12);
    sendtoparty(x,s);

    s:=#$48#$00#$00#$00#$1E#$06#$04#$00#$46#$C9#$00#$00#$75#$69#$5F#$6D
      +#$61#$69#$6E#$6D#$6F#$64#$65#$00#$46#$C9#$00#$00#$53#$79#$6E#$63
      +#$68#$72#$6F#$6E#$69#$7A#$65#$00#$39#$C9#$00#$00#$53#$79#$6E#$63
      +#$68#$72#$6F#$6E#$69#$7A#$65#$4D#$75#$6C#$74#$69#$42#$6F#$73#$73
      +#$47#$6F#$74#$6F#$00#$00#$00#$00;
    sendtoparty(x,s);

    RegisterTimer(x,player[x].GID,30,1,pdword(@d[$15])^);

    player[x].inboss := 2; //multi warp
    if copy(team[player[x].TeamID].bosswarpdata[player[x].Floor],1,5) = 'door:' then
      player[x].inboss := 3; //warp act as a door
end;

procedure RoomAction_Cancel(x:integer;d:ansistring);
var s:ansistring;
begin
    player[x].inboss:=0;  //cancel boss warp
    s:=#$50#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
      +#$04#$00#$00#$00#$53#$05#$00#$00#$00#$00#$00#$00#$06#$00#$08#$09
      +#$00#$00#$00#$00#$DC#$A6#$9B#$00#$00#$00#$00#$00#$04#$00#$00#$00
      +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
      +#$D0#$5C#$00#$00#$4C#$65#$61#$76#$65#$4D#$65#$6E#$75#$00#$00#$00;
    safemove(player[x].gid,s,9,4);
    safemove(player[x].gid,s,$25,4);
    safemove(d[$15],s,$15,12);
    sendtoparty(x,s);

    s:=#$4C#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
      +#$04#$00#$00#$00#$6C#$04#$00#$00#$00#$00#$00#$00#$06#$00#$A5#$0E
      +#$00#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
      +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
      +#$D3#$5C#$00#$00#$43#$61#$6E#$63#$65#$6C#$00#$00;
    safemove(player[x].gid,s,9,4);
    safemove(player[x].gid,s,$25,4);
    safemove(d[$15],s,$15,12);
    sendtoplayer(x,s);
end;


procedure RoomAction_Sit(x:integer;d:ansistring);
var s,a:ansistring;
    i,l,c:integer;
begin
    //allow it to sit
    move(d[$15],i,4);
    s:=#$50#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
          +#$04#$00#$00#$00#$8D#$04#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
          +#$00#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
          +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
          +#$EF#$5C#$00#$00#$53#$69#$74#$53#$75#$63#$63#$65#$73#$73#$00#$00;
    safemove(player[x].gid,s,$25,4);
    safemove(player[x].gid,s,$9,4);
    safemove(d[$15],s,$15,4);
    if (player[x].room = Room_Casino) and (i>=CASINOID_BLACKJACK) and (i<CASINOID_BLACKJACK+30) then begin
        if casinobjobj[i-CASINOID_BLACKJACK] <> -1 then begin
            a:='SitFailed'#0;
            safemove(a[1],s,$45,10);
            sendtolobby(player[x].room,-1,s);
        end else begin //success
            sendtolobby(player[x].room,-1,s); //send success
            casinobjobj[i-CASINOID_BLACKJACK]:=x;

            //set spot status to null
            l:=((i-CASINOID_BLACKJACK) div 5);
            c:=(i-CASINOID_BLACKJACK) mod 5;
            BJTable[l].player[c+1].stat:=0;
            BJTable[l].player[c+1].count:=0;
            BJTable[l].player[c+1].bet:=0;

            //prepare for game
            {000000: 24 00 00 00 04 79 40 00 56 5F 9C 00 00 00 00 00     $....y@.V_�..... //remove sit place
            000010: 04 00 00 00 19 04 00 00 00 00 00 00 06 00 00 00     ................
            000020: 02 00 00 00     ....}

            player[x].shooter_status:=i-CASINOID_BLACKJACK;

        end;
    end else begin

        sendtolobby(player[x].room,-1,s);
    end;
    //asdasd
end;

procedure RoomAction_Request(x:integer;d:ansistring);
var s:ansistring;
    i,l:integer;
begin
    move(d[$15],i,4);
    if (player[x].room = Room_Casino) and (i>=CASINOID_BLACKJACK) and (i<CASINOID_BLACKJACK+30) then
    if casinobjobj[i-CASINOID_BLACKJACK] <> -1 then begin
        //fail

    end else begin
        s:=#$4C#$00#$00#$00#$04#$B3#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00   //this tell the bet from 10 to 50
          +#$04#$00#$00#$00#$8D#$04#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
          +#$01#$00#$00#$00#$00#$00#$00#$00#$A8#$E7#$00#$00#$0A#$00#$00#$00
          +#$14#$00#$00#$00#$1E#$00#$00#$00#$28#$00#$00#$00#$32#$00#$00#$00
          +#$AF#$E7#$00#$00#$AF#$E7#$00#$00#$00#$00#$00#$00;
        l:=((i-CASINOID_BLACKJACK) div 5) * 5;
        safemove(player[x].gid,s,$9,4);
        safemove(d[$15],s,$15,4);
        sendtoplayer(x,s);
    end;
end;

procedure RoomAction_ACCESS_SHOP(x:integer;d:ansistring);
var a:ansistring;
begin
    if player[x].room = ROOM_LOBBY then begin
        a:=#$54#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
          +#$04#$00#$00#$00#$94#$02#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
          +#$00#$00#$00#$00#$94#$02#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
          +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
          +#$EA#$5C#$00#$00#$41#$63#$63#$65#$73#$73#$53#$68#$6F#$70#$28#$30
          +#$2C#$31#$29#$00;
          safemove(player[x].gid,a,9,4);
          safemove(d[$15],a,$15,12);
          safemove(d[$15],a,$25,12);
          sendtoplayer(x,a);
    end else begin
        a:=#$58#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
        +#$04#$00#$00#$00#$28#$02#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
        +#$00#$00#$00#$00#$28#$02#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$E8#$5C#$00#$00#$41#$63#$63#$65#$73#$73#$53#$68#$6F#$70#$28#$31
        +#$36#$33#$38#$30#$29#$00#$00#$00;

        safemove(player[x].gid,a,9,4);
        //move(player[x].gid,a[$59],4);
        safemove(d[$15],a,$15,12);
        safemove(d[$15],a,$25,12);

        sendtoplayer(x,a);
    end;
end;

procedure RoomAction_READY(x:integer;d:ansistring);
var a:ansistring;
begin
  a:=#$50#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00#$97#$02#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
    +#$00#$00#$00#$00#$97#$02#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$EE#$5C#$00#$00#$46#$61#$76#$73#$4E#$65#$75#$74#$72#$61#$6C#$00

    +#$48#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00#$97#$02#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
    +#$00#$00#$00#$00#$97#$02#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$D7#$5C#$00#$00#$41#$50#$00#$00;

  //do we have exception?
  if (pdword(@d[$15])^ = $26e) or (pdword(@d[$15])^ = $265) or (pdword(@d[$15])^ = $240) then begin
      a:=#$50#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
        +#$04#$00#$00#$00#$97#$02#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
        +#$00#$00#$00#$00#$97#$02#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$D0#$5C#$00#$00#$46#$61#$76#$73#$48#$61#$74#$65#$32#$00#$00#$00

        +#$48#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
        +#$04#$00#$00#$00#$97#$02#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
        +#$00#$00#$00#$00#$97#$02#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$D7#$5C#$00#$00#$41#$50#$00#$00;

  end;

  if (pdword(@d[$15])^ = $26d) or (pdword(@d[$15])^ = $26a) or (pdword(@d[$15])^ = $234) or (pdword(@d[$15])^ = $233) then begin
      a:=#$50#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
        +#$04#$00#$00#$00#$97#$02#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
        +#$00#$00#$00#$00#$97#$02#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$EE#$5C#$00#$00#$46#$61#$76#$73#$4E#$65#$75#$74#$72#$61#$6C#$00

        +#$50#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
        +#$04#$00#$00#$00#$72#$02#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
        +#$00#$00#$00#$00#$72#$02#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;

      if (pdword(@d[$15])^ = $26d) then a:=a+#$EF#$5C#$00#$00#$41#$50#$3A#$53#$65#$74#$2E#$31#$32#$37#$00#$00
      else if (pdword(@d[$15])^ = $26a) then a:=a+#$EF#$5C#$00#$00#$41#$50#$3A#$53#$65#$74#$2E#$31#$31#$30#$00#$00
      else a:=a+#$D1#$5C#$00#$00#$41#$50#$3A#$53#$65#$74#$2E#$30#$00#$00#$00#$00;

      a:=a+#$50#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
        +#$04#$00#$00#$00#$72#$02#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
        +#$00#$00#$00#$00#$72#$02#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;

      if (pdword(@d[$15])^ = $26d) then a:=a+#$EE#$5C#$00#$00#$41#$50#$3A#$53#$65#$74#$31#$2E#$31#$32#$37#$00
      else if (pdword(@d[$15])^ = $26a) then a:=a+#$EE#$5C#$00#$00#$41#$50#$3A#$53#$65#$74#$31#$2E#$31#$31#$30#$00
      else a:=a+#$D0#$5C#$00#$00#$41#$50#$3A#$53#$65#$74#$31#$2E#$30#$00#$00#$00;

       safemove(player[x].gid,a,$a9,4);
      safemove(d[$15],a,$b5,12);
      safemove(d[$15],a,$c5,12);
  end;

  safemove(player[x].gid,a,9,4);
  safemove(player[x].gid,a,$59,4);
  safemove(d[$15],a,$15,12);
  safemove(d[$15],a,$25,12);

  safemove(d[$15],a,$65,12);
  safemove(d[$15],a,$75,12);
  sendtoplayer(x,a);
end;


procedure RoomAction_Kill(x:integer;d:ansistring);
var a:ansistring;
begin
  if pdword(@d[$15])^ = $106c3 then begin
      a:=#$20#$00#$00#$00#$04#$06#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
      +#$04#$00#$00#$00#$c3#$06#$01#$00#$00#$00#$00#$00#$06#$00#$a9#$11;
      safemove(player[x].gid,a,9,4);
      sendtofloor(player[x].room,player[x].Floor,a);
  end;
end;

procedure RoomAction_GetEncore(x:integer;d:ansistring);
var a:ansistring;
begin
  if player[x].room = ROOM_LOBBY then begin
      a:=#$60#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
      +#$04#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
      +#$00#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
      +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
      +#$E1#$5C#$00#$00#$45#$6E#$63#$6F#$72#$65#$28#$6D#$75#$5F#$31#$31
      +#$5F#$62#$67#$6D#$5F#$65#$6C#$6F#$62#$62#$79#$29#$00#$00#$00#$00;

      safemove(player[x].gid,a,9,4);
      safemove(player[x].gid,a,$25,4);
      sendtoplayer(x,a);

  end;
end;

procedure RoomAction_PickUp(x:integer;d:ansistring);
var a:ansistring;
    l,c,i:integer;
    dw:dword;
begin
    a:=#$48#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
    +#$00#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$D6#$5C#$00#$00#$47#$65#$74#$00;

    safemove(player[x].gid,a,9,4);
    safemove(d[$15],a,$15,12);
    safemove(d[$25],a,$25,12);
    sendtolobby(player[x].room,-1,a);

    //update timer
    //form1.MemoDebug.Lines.Add('chl id: '+inttostr(pdword(@d[$15])^));
    //compute base value
    dw:=gettickcount()-team[player[x].teamid].ChalengeStartTicks;
    team[player[x].teamid].ChalengeCurrent:=team[player[x].teamid].ChalengeCurrent-(dw / 27);
    if team[player[x].teamid].ChalengeCurrent < 0 then team[player[x].teamid].ChalengeCurrent:=0;
    team[player[x].teamid].ChalengeStartTicks:=gettickcount();

    a:=RemoveObjFromFloor(x,player[x].TeamID,player[x].floor,pdword(@d[$15])^);
    if a = 'ob_9900_0403' then begin
      team[player[x].teamid].ChalengeCurrent:=team[player[x].teamid].ChalengeCurrent+500; //= 5% (500)
      l:=$11;
      c:=5;
    end;
    if a = 'ob_9900_0404' then begin
      team[player[x].teamid].ChalengeCurrent:=team[player[x].teamid].ChalengeCurrent+1500; //= 5% (500)
      l:=$11;
      c:=15;
    end;
    if a = 'ob_9900_0405' then begin
      team[player[x].teamid].ChalengeCurrent:=team[player[x].teamid].ChalengeCurrent+3000; //= 5% (500)
      l:=$11;
      c:=30;
    end;
    if a = 'ob_9900_0406' then begin
      l:=$10;
      c:=10;
    end;
    if a = 'ob_9900_0407' then begin
      l:=$10;
      c:=50;
    end;
    if a = 'ob_9900_0408' then begin
      l:=$10;
      c:=100;
    end;
    if team[player[x].teamid].ChalengeCurrent > team[player[x].teamid].ChalengeMax then
      team[player[x].teamid].ChalengeCurrent:=team[player[x].teamid].ChalengeMax;

    //XOR: 6EB3   SUB: 89
    //tell who did
    a:=#$2C#$00#$00#$00#$0B#$69#$04#$00+ansichar(l)+#$00#$00#$00+ansichar(c)+#$00#$00#$00
    +#$00#$00#$00#$00+GetCharNameWithMagic(x,$6eb3,$89)+#$3A#$6E#$00#$00;
    i:=length(a);
    move(i,a[1],4);
    sendtolobby(player[x].room,-1,a);

    a:=#$1C#$00#$00#$00#$0B#$64#$00#$00#$0C#$4A#$B7#$B6#$F6#$0B#$00#$00
    +#$48#$2C#$16#$46#$00#$00#$0C#$C2#$00#$00#$00#$00;
    psingle(@a[$11])^:=team[player[x].teamid].ChalengeCurrent;
    pint64(@a[$9])^ := player[x].ping+(gettickcount() - player[x].pingticks);
    sendtolobby(player[x].room,-1,a);

    //make it vanish
    a:=#$20#$00#$00#$00#$04#$06#$40#$00#$7D#$FF#$CF#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00#$8A#$02#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11;
    safemove(player[x].gid,a,9,4);
    safemove(d[$15],a,$15,12);
    sendtolobby(player[x].room,-1,a);
end;



procedure RoomAction_ReqEvent(x:integer;d:ansistring);
var a:ansistring;
    i:integer;
begin
    if player[x].Floor < 0 then exit;
      a:=#$54#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
      +#$04#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
      +#$00#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
      +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
      +#$ED#$5C#$00#$00#$45#$56#$5F#$6D#$70#$5F#$30#$30#$30#$31#$34#$30
      +#$00#$00#$00#$00;

      //have a text to say?
      i:=1;
      while i < length(team[player[x].TeamID].FloorObj[player[x].Floor]) do begin
          if pdword(@team[player[x].TeamID].FloorObj[player[x].Floor][i+8])^ = pdword(@d[$15])^ then begin
              if team[player[x].TeamID].FloorObj[player[x].Floor][i+$30]<>#0 then
                safemove(team[player[x].TeamID].FloorObj[player[x].Floor][i+$30],a,$45,12); //yes copy id
              break;
          end;
          inc(i,pdword(@team[player[x].TeamID].FloorObj[player[x].Floor][i])^);
      end;

      safemove(player[x].gid,a,9,4);
      safemove(d[$15],a,$15,12);
      safemove(player[x].gid,a,$25,4);
      sendtoplayer(x,a);

      RoomAction_Ready2b(x,d);
end;

procedure RoomAction_Reset(x:integer;d:ansistring);
var a:ansistring;
    i,l,id:integer;
begin
      if player[x].Floor < 0 then exit;
      i:=1;
      id:=0;
      while i < length(team[player[x].TeamID].FloorObj[player[x].Floor]) do begin
          move(team[player[x].TeamID].FloorObj[player[x].Floor][i],l,4);
          if pdword(@team[player[x].TeamID].FloorObj[player[x].Floor][i+$48])^ = pdword(@d[$15])^ then
            id:=pdword(@team[player[x].TeamID].FloorObj[player[x].Floor][i+$8])^;

          inc(i,l);
      end;
      a:=#$4c#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
      +#$04#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
      +#$00#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
      +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
      +#$D4#$5C#$00#$00#$52#$65#$73#$65#$74#$00#$00#$00;

      safemove(player[x].gid,a,9,4);
      safemove(d[$15],a,$15,12);
      safemove(id,a,$15,4);
      safemove(player[x].gid,a,$25,4);
      sendtoplayer(x,a);
end;

procedure RoomAction_MOVE_END1(x:integer;d:ansistring);
var a:ansistring;
    i,l,id:integer;
begin
      if player[x].Floor < 0 then exit;
      i:=1;
      id:=0;
      while i < length(team[player[x].TeamID].FloorObj[player[x].Floor]) do begin
          move(team[player[x].TeamID].FloorObj[player[x].Floor][i],l,4);
          if pdword(@team[player[x].TeamID].FloorObj[player[x].Floor][i+$8])^ = pdword(@d[$15])^+1 then begin
              //per item action
              if pansichar(@team[player[x].TeamID].FloorObj[player[x].Floor][i+$24]) = 'ob_0210_0154' then begin
                  a:=#$24#$00#$00#$00#$04#$79#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
                    +#$04#$00#$00#$00#$53#$00#$00#$00#$00#$00#$00#$00#$06#$00#$A9#$11
                    +#$02#$00#$00#$00;
                  safemove(player[x].GID,a,9,4);
                  safemove(d[$15],a,$15,12);
                  sendtofloor(player[x].room,player[x].Floor,a);
              end;

              break;
          end;
          inc(i,l);
      end;

end;

procedure RoomAction_MOVE_END2(x:integer;d:ansistring);
var a:ansistring;
    i,l,id:integer;
begin
      if player[x].Floor < 0 then exit;
      i:=1;
      id:=0;
      while i < length(team[player[x].TeamID].FloorObj[player[x].Floor]) do begin
          move(team[player[x].TeamID].FloorObj[player[x].Floor][i],l,4);
          if pdword(@team[player[x].TeamID].FloorObj[player[x].Floor][i+$8])^ = pdword(@d[$15])^+1 then begin
              //per item action
              if pansichar(@team[player[x].TeamID].FloorObj[player[x].Floor][i+$24]) = 'ob_0210_0154' then begin
                  a:=#$24#$00#$00#$00#$04#$79#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
                    +#$04#$00#$00#$00#$53#$00#$00#$00#$00#$00#$00#$00#$06#$00#$A9#$11
                    +#$03#$00#$00#$00;
                  safemove(player[x].GID,a,9,4);
                  safemove(d[$15],a,$15,12);
                  sendtofloor(player[x].room,player[x].Floor,a);
              end;

              break;
          end;
          inc(i,l);
      end;

end;

procedure RoomAction_ContainerFree(x:integer;d:ansistring);
var a:ansistring;
begin
      a:=#$54#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
      +#$04#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
      +#$00#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
      +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
      +#$EA#$5C#$00#$00#$43#$6F#$6E#$74#$61#$69#$6E#$65#$72#$46#$72#$65
      +#$65#$53#$43#$00;

      safemove(player[x].gid,a,9,4);
      safemove(d[$15],a,$15,12);
      safemove(d[$15],a,$25,12);
      sendtofloor(player[x].room,player[x].Floor,a);

      a:=#$24#$00#$00#$00#$04#$79#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
        +#$04#$00#$00#$00#$53#$00#$00#$00#$00#$00#$00#$00#$06#$00#$A9#$11
        +#$04#$00#$00#$00;
      safemove(player[x].GID,a,9,4);
      safemove(d[$15],a,$15,12);
      sendtofloor(player[x].room,player[x].Floor,a);
end;



//game send this packet for multiple action
Procedure Packet1404(x:integer;d:ansistring);
var a:ansistring;
begin
    if length(d) > $45 then begin
        //form1.Memo1.Lines.add(pansichar(@d[$45]));
        a:=pansichar(@d[$45]);

        if a = 'GetEncore' then RoomAction_GetEncore(x,d);
        if a = 'PickUp' then RoomAction_PickUp(x,d);
        if a = 'Kill' then RoomAction_Kill(x,d);
        if a = 'READY' then RoomAction_READY(x,d);
        if a = 'ACCESS_SHOP' then RoomAction_ACCESS_SHOP(x,d);
        if a = 'Request' then RoomAction_Request(x,d);
        if a = 'Sit' then RoomAction_Sit(x,d);
        if a = 'ItemInitialized' then GetPartyID(x,copy(d,$15,12));
        if a = 'Cancel' then RoomAction_Cancel(x,d);
        if a = 'RequestTransfer' then WarpParty(player[x].partyid);
        if a = 'TransferForce' then WarpParty(player[x].partyid);
        if a = 'MultiStart' then RoomAction_MultiStart(x,d);
        if a = 'MultiAbort' then RoomAction_MultiAbort(x,d);
        if a = 'Transfer' then RoomAction_Transfer(x,d);
        if a = 'RESET_POSITION' then RoomAction_RESET_POSITION(x,d);
        if a = 'ACTIVE(1)' then RoomAction_ACTIVE(x,d);
        if a = 'STEP_ON' then RoomAction_STEP_ON(x,d);
        if a = 'GetState' then SendFenceStatus(x,pdword(@d[$15])^,team[player[x].TeamID].FloorObj[player[x].Floor]);
        if a = 'Ready' then RoomAction_Ready2(x,d);
        if a = 'MoveCheck' then RoomAction_MoveCheck(x,d);
        if a = 'BALL_SHOOT_GOAL_1' then AddPtsToGoal(x,1,pdword(@d[$15])^);
        if a = 'BALL_SHOOT_GOAL_2' then AddPtsToGoal(x,2,pdword(@d[$15])^);
        if a = 'DeleteCheck' then RoomAction_DeleteCheck(x,d);
        if a = 'UniteSlave' then RoomAction_UniteSlave(x,d);
        if a = 'UniteMaster' then RoomAction_UniteMaster(x,d);
        if a = 'TRAP_TARGET' then RoomAction_TRAP_TARGET(x,d);
        if a = 'EXPLOSION(3)' then RoomAction_EXPLOSION(x,d);
        if a = 'GET_TARGET' then RoomAction_GET_TARGET(x,d);
        if a = 'ReqEvent' then RoomAction_ReqEvent(x,d);
        if a = 'Reset' then RoomAction_Reset(x,d);
        if a = 'MOVE_END(1)' then RoomAction_MOVE_END1(x,d);
        if a = 'MOVE_END(2)' then RoomAction_MOVE_END2(x,d);
        if a = 'ContainerFree(3)' then RoomAction_ContainerFree(x,d);
    end;


end;

end.
