unit UDrop;

interface

uses types,sysutils,classes;

type
    TRareDrop = record
        lvl,rate:word;
        drop:array[0..3] of word;
    end;
    TDropData = record
        dar:integer;
        mesetas_min,mesetas_max:integer;
        drop:array[0..14] of traredrop;
    end;

    function GetDropItem(x,t,m:integer):ansistring;
    procedure LoadDropFile(t:integer;df:ansistring);
    function GetItemLayout(s:ansistring):ansistring;
    procedure GenerateDrop(x,m:integer;px,py,pz:single);
    function ReadWord(var s:ansistring):ansistring;
    Function ReadInt(var s:ansistring):integer;

var DropModel:ansistring = '';


const
    drop:array[0..6] of TDropData = (
      //rappy
      (dar:100;
      mesetas_min:6;
      mesetas_max:12;
      drop:(
        (lvl:0;rate:100;drop:(3,1,0,2)),  //trimate
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0))
        )
      ),
      //native
      (dar:70;
      mesetas_min:6;
      mesetas_max:12;
      drop:(
        (lvl:0;rate:100;drop:(3,1,0,0)),  //trimate
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)))
      ),
      //native trower
      (dar:60;
      mesetas_min:15;
      mesetas_max:21;
      drop:(
        (lvl:0;rate:100;drop:(3,1,0,0)),  //trimate
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)))
      ),
      //wolf
      (dar:90;
      mesetas_min:10;
      mesetas_max:15;
      drop:(
        (lvl:0;rate:100;drop:(3,1,0,0)),  //trimate
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)))
      ),
      //spider ant
      (dar:70;
      mesetas_min:5;
      mesetas_max:12;
      drop:(
        (lvl:0;rate:100;drop:(3,1,0,0)),  //trimate
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)))
      ),
      (dar:70;    //wolfA (fangus)
      mesetas_min:15;
      mesetas_max:21;
      drop:(
        (lvl:0;rate:100;drop:(3,1,0,0)),  //trimate
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)))
      ) ,
      (dar:70;    //Pterosaure (fangus)
      mesetas_min:15;
      mesetas_max:21;
      drop:(
        (lvl:0;rate:100;drop:(3,1,0,0)),  //trimate
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)),
        (lvl:255;rate:100;drop:(0,0,0,0)))
      )
    );

implementation

uses UQuestBoard, UPlayer, UPsoUtil, UForest, UInventory, fmain,
  clientorder, ItemGenerator;


function ReadWord(var s:ansistring):ansistring;
var i,l:integer;
begin
    i:=1;
    result:='';
    if s = '' then exit;
    l:=0;
    while l = 0 do begin
        if i <= length(s) then begin
            if (s[i] <> #9) and (s[i] <> #32) then begin
                result:=result+s[i];
                inc(i);
            end else l:=1;
        end else l:=1;
    end;
    delete(s,1,i);
    l:=0;
    while l = 0 do begin
        if (length(s)>0) then begin
             if (s[1] = #9) or (s[1] = #32) then delete(s,1,1)
             else l:=1;
        end else l:=1;
    end;
end;

Function ReadInt(var s:ansistring):integer;
begin
    result:= strtointdef(ReadWord(s),0);
end;

function ReadWepDrop(s:ansistring):tWepDrop;
var a,b,x:integer;
begin
    fillchar(result,sizeof(tWepDrop),0);
    result.TypePC:=ReadInt(s);
    a:=ReadInt(s);
    b:=ReadInt(s);
    for x:=a to b do
        result.pcs[x]:=ReadInt(s);
end;

procedure LoadDropFile2(t:integer;df:ansistring);
var sl:tstringlist;
    l,i,y,j:integer;
    s,a:ansistring;
begin
    sl:=tstringlist.create;
    sl.LoadFromFile(df);
    for l:=0 to sl.Count-1 do begin
        s:=lowercase(sl[l]);
        a:=ReadWord(s);
        if a = 'template' then LoadDropFile2(t,path+'data\'+ ReadWord(s));
        if a = 'mesetas_min' then team[t].drop.mesetas[0]:=ReadInt(s);
        if a = 'mesetas_max' then team[t].drop.mesetas[1]:=ReadInt(s);
        if a = 'dropt' then begin
            for i:=0 to $f do
                team[t].drop.DropType[i]:=ReadInt(s);
        end;
        if a = 'set_weapon' then begin
            y:=ReadInt(s);
            j:=ReadInt(s);
            team[t].drop.Wepons[y].pcs[j]:=ReadInt(s);
        end;

        if a = 'i_consumable' then team[t].drop.items[1].TypePC:= ReadInt(s);
        if a = 'monomate' then team[t].drop.items[1].pcs[0]:= ReadInt(s);
        if a = 'dimate' then team[t].drop.items[1].pcs[1]:= ReadInt(s);
        if a = 'trimate' then team[t].drop.items[1].pcs[2]:= ReadInt(s);
        if a = 'monofluide' then team[t].drop.items[1].pcs[3]:= ReadInt(s);
        if a = 'difluide' then team[t].drop.items[1].pcs[4]:= ReadInt(s);
        if a = 'trifluide' then team[t].drop.items[1].pcs[5]:= ReadInt(s);
        if a = 'sol_atomizer' then team[t].drop.items[1].pcs[6]:= ReadInt(s);
        if a = 'moon_atomizer' then team[t].drop.items[1].pcs[7]:= ReadInt(s);
        if a = 'star_atomizer' then team[t].drop.items[1].pcs[8]:= ReadInt(s);
        if a = 'shiftaride' then  team[t].drop.items[1].pcs[11]:= ReadInt(s);
        if a = 'debanride' then  team[t].drop.items[1].pcs[12]:= ReadInt(s);

        if a = 'u_type' then team[t].drop.units[ReadInt(s)].TypePC:=ReadInt(s);
        if a = 'u_entry' then team[t].drop.units[ReadInt(s)].pcs[ReadInt(s)] :=ReadInt(s);

        if a = 'pa_lvl' then begin
            y:=ReadInt(s);
            j:=ReadInt(s);
            for i:=y to j do
                team[t].drop.palvl[i]:=ReadInt(s);
        end;

        if a = 'sword' then team[t].drop.Wepons[1]:=ReadWepDrop(s);
        if a = 'wired_lance' then team[t].drop.Wepons[2]:=ReadWepDrop(s);
        if a = 'partizan' then team[t].drop.Wepons[3]:=ReadWepDrop(s);
        if a = 'twin_dagger' then team[t].drop.Wepons[4]:=ReadWepDrop(s);
        if a = 'double_saber' then team[t].drop.Wepons[5]:=ReadWepDrop(s);
        if a = 'knuckles' then team[t].drop.Wepons[6]:=ReadWepDrop(s);
        if a = 'katana' then team[t].drop.Wepons[7]:=ReadWepDrop(s);
        if a = 'dual_blade' then team[t].drop.Wepons[8]:=ReadWepDrop(s);
        if a = 'gunslash' then team[t].drop.Wepons[9]:=ReadWepDrop(s);
        if a = 'assault_rifle' then team[t].drop.Wepons[10]:=ReadWepDrop(s);
        if a = 'launcher' then team[t].drop.Wepons[11]:=ReadWepDrop(s);
        if a = 'twin_machinegun' then team[t].drop.Wepons[12]:=ReadWepDrop(s);
        if a = 'bullet_bow' then team[t].drop.Wepons[13]:=ReadWepDrop(s);
        if a = 'rod' then team[t].drop.Wepons[14]:=ReadWepDrop(s);
        if a = 'talis' then team[t].drop.Wepons[15]:=ReadWepDrop(s);
        if a = 'wand' then team[t].drop.Wepons[16]:=ReadWepDrop(s);
        if a = 'jet_boots' then team[t].drop.Wepons[17]:=ReadWepDrop(s);

    end;
    sl.Free;
end;

procedure LoadDropFile(t:integer;df:ansistring);
begin
    //empty it
    fillchar(team[t].drop,sizeof(tdropinfo),0);
    LoadDropFile2(t,df);
end;

function GetItemLayout(s:ansistring):ansistring;
var a:ansistring;
    i:integer;
    st:tstringlist;
begin
    if DropModel = '' then begin
      //create base model
      st:=tstringlist.Create;
      st.Add('ver 5');
      st.Add('define OPT_INT_62 = 62');
      st.Add('define OPT_INT_53 = 53');
      st.Add('define OPT_INT_51 = 51');
      st.Add('define OPT_INT_54 = 54');
      st.Add('define OPT_VERTEX_4 = 4');

      st.Add('object ob_9900_0005');  //23 = no effect
      st.Add('id 101');
      st.Add('rotation 0,000 -0,857 0,000 -0,515');
      st.Add('position -17,609 -0,189 -77,938 0,000');
      st.Add('item_flag 09 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 04 00 00 00');
      st.Add('byte_flag 02 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 16 B8');
      st.Add('OPT_INT_54 -1');
      st.Add('OPT_INT_51 1');
      st.Add('OPT_INT_53 4');
      st.Add('OPT_INT_62 1');
      st.Add('OPT_VERTEX_4 -18,500 -0,189 -77,500 0,000');
      DropModel := GenerateObjData(st,'\quest\');
      st.Free;
    end;

    result:=DropModel;


    //18 = green box
    //14 = unit?
    //0 = giant red key?
    //1 = mesetas
    //2 = sword
    //3 = wire lance
    //4 = partisan
    //5 = twin dagger
    //6 = dual sable
    //7 = dknuckles
    //8 = gunslash
    //9 = riffle
    //10 = canon
    //11 =  twin MG
    //12 = rod
    //13 = talis
    //14 = rear unit
    //15 = blue box
    //17 = red box
    //20 = gold box
    //21 = disk
    //22 = jet boots
    //23 = jet boots
    //24 = 2 drive?
    //25 = christmas gift
    //26 = robot part?
    // 27 = very red box
    //28 = red sphere
    //29 = rubi cube
    //30 = katana
    //31 = bow
    //32 = robot part? helmet?
    //33 = dual blade
    //34 = takts
    //35 = egg
    //36 = candy
    //37 = big candy
    //38 =


    i:=17;
    if s[1] = #$fe then i:=1; //mesetas
    if s[1] = #1 then begin
        if s[3] = #1 then i:=2;
        if s[3] = #2 then i:=3;
        if s[3] = #3 then i:=4;
        if s[3] = #4 then i:=5;
        if s[3] = #5 then i:=6;
        if s[3] = #6 then i:=7;
        if s[3] = #7 then i:=8;
        if s[3] = #8 then i:=9;
        if s[3] = #9 then i:=10;
        if s[3] = #10 then i:=11;
        if s[3] = #11 then i:=12;
        if s[3] = #12 then i:=13;
        if s[3] = #13 then i:=13;
        if s[3] = #14 then i:=30;
        if s[3] = #15 then i:=31;
        if s[3] = #16 then i:=23;
        if s[3] = #17 then i:=33;
        if s[3] = #18 then i:=34;
    end;
    if s[1] = #2 then i:=15;
    if s[1] = #3 then i:=18;
    if s[1] = #5 then i:=14;
    if s[1] = #9 then i:=21;

    a:=inttostr(i);
    if length(a) <> 2 then a:='0'+a;

    safemove(a[1],result,$2f,2);
end;


function GetDropItem(x,t,m:integer):ansistring;
var i,c,l:integer;
    v1,v2:integer;
begin
    result:=#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0;
    c:=random(100); //find type of drop
    i:=0;
    while team[t].drop.DropType[i] < c do begin dec(c,team[t].drop.DropType[i]); inc(i); end;

    if TestForOrderDrop(x,t,m,i,result) then exit;


    if i = 0 then begin //mesetas drop
        result[1]:=#$fe;
        if m>-1 then begin
            v1:=drop[m].mesetas_min;
            v2:=drop[m].mesetas_max;
        end else begin
            v1:=team[t].drop.mesetas[0];
            v2:=team[t].drop.mesetas[1];
        end;
        c:=random(v2-v1)+v1;
        move(c,result[9],4);

    end else
    if i = 1 then begin //weapon
        result[1]:=#1;
        c:=0;
        for i:=0 to 18 do
            inc(c,team[t].drop.Wepons[i].TypePC);  //get max value
        c:=random(c);
        i:=1;
        while team[t].drop.Wepons[i].TypePC < c do begin
            dec(c,team[t].drop.Wepons[i].TypePC); inc(i);
        end;
        //here got the type to drop, get the proper sub
        result[3]:=ansichar(i);

        c:=0;
        for l:=0 to 511 do
            inc(c,team[t].drop.Wepons[i].pcs[l]);  //get max value
        c:=random(c);
        l:=0;
        while team[t].drop.Wepons[i].pcs[l] < c do begin
            dec(c,team[t].drop.Wepons[i].pcs[l]); inc(l);
        end;
        move(l,result[7],2);

        //process boost here when i know more

        
    end else
    if i = 3 then begin //items
        result[1]:=#3;
        c:=0;
        for i:=0 to 1 do
            inc(c,team[t].drop.items[i].TypePC);  //get max value
        c:=random(c);
        i:=1;
        while integer(team[t].drop.items[i].TypePC) < c do begin
            dec(c,team[t].drop.items[i].TypePC); inc(i);
        end;
        //here got the type to drop, get the proper sub
        result[3]:=ansichar(i);
        result[9]:=#1;
        c:=0;
        for l:=0 to 511 do
            inc(c,team[t].drop.items[i].pcs[l]);  //get max value
        c:=random(c);
        l:=0;
        //form1.memo1.lines.Add(inttostr(c));
        while integer(team[t].drop.items[i].pcs[l]) < c do begin
            dec(c,team[t].drop.items[i].pcs[l]); inc(l);
        end;
        move(l,result[7],2);

    end else
    if i = 5 then begin //unit
        result[1]:=#5;
        c:=0;
        for i:=0 to 5 do
            inc(c,team[t].drop.units[i].TypePC);  //get max value
        c:=random(c);
        i:=1;
        while team[t].drop.units[i].TypePC < c do begin
            dec(c,team[t].drop.units[i].TypePC); inc(i);
        end;
        //here got the type to drop, get the proper sub
        result[3]:=ansichar(i);
        c:=0;
        for l:=0 to 511 do
            inc(c,team[t].drop.units[i].pcs[l]);  //get max value
        c:=random(c);
        l:=0;
        while team[t].drop.units[i].pcs[l] < c do begin
            dec(c,team[t].drop.units[i].pcs[l]); inc(l);
        end;
        move(l,result[7],2);

    end else
    if i = 9 then begin //disk
        result[1]:=#9;
        c:=random(2)+1;
        i:=random(12)+1;
        if c = 2 then i:=random(6)+1;
        //here got the type to drop, get the proper sub
        result[3]:=ansichar(c);
        result[5]:=ansichar(i);
        c:=0;
        for l:=0 to 30 do
            inc(c,team[t].drop.palvl[l]);  //get max value
        c:=random(c);
        l:=0;
        while team[t].drop.palvl[l] < c do begin
            dec(c,team[t].drop.palvl[l]); inc(l);
        end;
        move(l,result[9],2);
        //miss lvl?
    end;

    //if result[3]=#0 then
    //    result[3]:=#1;

end;

function GetItemStars(s:ansistring):integer;
var i:integer;
begin
  result:=1; //default 1 star
  for i:=0 to itemcount-1 do
    if (items[i].ItemType = pword(@s[1])^) and (items[i].ItemSub = pword(@s[3])^)
      and (items[i].unk = pword(@s[5])^) and (items[i].ItemEntry = pword(@s[7])^) then begin
          result:=items[i].star;
          break;
      end;

end;


procedure GenerateDrop(x,m:integer;px,py,pz:single);
var itm,a:ansistring;
    it:dword;
    w:word;
    i:integer;
begin
    itm:=GetDropItem(x,player[x].teamid,m);
    a:=GetItemLayout(itm);
    it:=GetNextTeamItemID(player[x].TeamID);

    move(it,a[$9],4);
    //move(s[$31],a[$15],6); //rotation
    //move(s[$31],a[$1d],8); //pos
    w:=floattohalf(px);
    safemove(w,a,$1d,2);
    safemove(w,a,$a1,2);
    w:=floattohalf(py);
    safemove(w,a,$1f,2);
    safemove(w,a,$a3,2);
    w:=floattohalf(pz);
    safemove(w,a,$21,2);
    safemove(w,a,$a5,2);
    //move(s[$25],a[$a1],8);
    AddObjectToPlayerFloor(x,player[x].Floor,a);

    a:=#$44#$00#$00#$00#$0F#$04#$00#$00#$B4#$00#$00#$00#$00#$00#$00#$00
    +#$06#$00#$a9#$11+itm
    +#$00#$60#$61#$41#$00#$40#$8B#$BF#$00#$60#$97#$42
    +#$00#$00#$00#$00#$00#$00#$03#$00#$00#$00#$00#$00#$05#$00#$00#$00  //03 = color value of 7 = rare
    +#$00#$00#$00#$00;

    //get the amt of stars
    i:=GetItemStars(itm);

    if i > 6 then //rare
      a[$37]:=#7;

    //set item pos
    safemove(px,a,$25,4);
    safemove(py,a,$29,4);
    safemove(pz,a,$2d,4);

    SendItemsName(x,1,$38,@a[13]);
    safemove(it,a,$9,4);
    safemove(it,a,$3d,4);
    //sendtolobby(player[x].room,-1,a);
    sendtoplayer(x,a);
    AddItemToFloor(x,player[x].Floor,a);

end;

end.
