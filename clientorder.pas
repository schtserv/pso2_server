unit clientorder;

interface
uses windows, types, sysutils, UPlayer;

type
    TClientOrder = record
        shortid:integer;
        longid:integer;
        npc:ansistring;
        repete:integer;
    end;
    PTinventoryItem = ^TinventoryItem;
procedure SendClientOrder(x:integer);
procedure SendClientList1(x:integer;s:ansistring);
procedure SendClientList2(x:integer;s:ansistring);
procedure TestForClientOrder(x:integer);
procedure TakeOrder(x:integer;s:ansistring);
function TestForOrderDrop(x,t,m,i:integer; var s:ansistring):boolean;
procedure TestForOrderItemPickup(x:integer; itm:PTinventoryItem);
procedure TestOrderCompletion(x:integer);


const
    ClientOrderCount = 1;
    ClientOrders:array[0..0] of TClientOrder = (
        (ShortID:$13ba; LongID: $0f6964; npc:'npc_50'; repete:0)
);

implementation

uses  UPsoUtil, UUseItem, UInventory, UDB, UMag, UForest;

procedure SendClientOrder(x:integer);
var a,b:ansistring;
    i,c,k:integer;
begin
    TestOrderCompletion(x);
    //Xor: 9EA0  Sub: F7
    c:=0;
    b:=#0#0#0#0#1#0#0#0;
    a:='';
    for i:=0 to ClientOrderCount-1 do
        if (player[x].ClientOrder[i]=1) or (player[x].ClientOrder[i]=3) or (player[x].ClientOrder[i]=4) then begin
            b[5]:=ansichar(player[x].ClientOrder[i]);
            if player[x].ClientOrder[i] = 3 then b[5]:=#2;
            if player[x].ClientOrder[i] = 4 then b[5]:=#0;
            safemove(ClientOrders[i].shortid,b,1,4);
            a:=a+b;
            inc(c);
        end;
    a:=#$78#$00#$00#$00#$1F#$0A#$04#$00#$00#$00#$00#$00+getmagic(c,$9EA0,$f7)+a;
    i:=length(a);
    safemove(i,a,1,4);
    if c > 0 then sendtoplayer(x,a);

    c:=0;
    k:=0;
    a:=#$4C#$06#$00#$00#$1F#$06#$00#$00#$07#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF   //100
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF   //200
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF   //300
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF   //400
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF   //500
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
    +#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
    +#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
    +#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
    +#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
    +#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
    +#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
    +#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
    +#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
    +#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
    +#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$FF#$FF#$FF#$FF
	+#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$00#$00#$00#$00;
    for i:=0 to ClientOrderCount-1 do
        if (player[x].ClientOrder[i]=2) or (player[x].ClientOrder[i]=3) then begin
            k:=0;
            safemove(k,a,13+(c*16),4);
            safemove(clientorders[i].longid,a,17+(c*16),4);
            k:=4;
            safemove(k,a,$15+(c*16),4);
            k:=0;
            safemove(k,a,$19+(c*16),4);
            sendtoplayer(x,a);
        end;

end;


procedure SendClientList1(x:integer;s:ansistring);
var a:ansistring;
    l,i:integer;
begin
    TestOrderCompletion(x);
    a:=#$F0#$07#$00#$00#$1F#$08#$00#$00#$11#$11#$11#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$0B#$00#$00#$00#$03#$00#$00#$00;

    safemove(s[$15],a,$7e9,4);
    safemove(player[x].gid,a,9,4);

    //completed list
    //fill the list
    l:=0;
    for i:=0 to ClientOrderCount-1 do
        if (player[x].ClientOrder[i]=3) then begin
            a[$15+(l*16)]:=#0; //always 0?
            a[$16+(l*16)]:=#0;
            a[$17+(l*16)]:=#0;
            a[$18+(l*16)]:=#0;

            safemove(clientorders[i].longid,a,$19+(l*16),4);

            if player[x].ClientOrder[i] = 3 then a[$1d+(l*16)]:= #4; //doing it
            a[$1e+(l*16)]:=#0;
            a[$1f+(l*16)]:=#0;
            a[$20+(l*16)]:=#0;

            a[$21+(l*16)]:=#0; //always 0?
            a[$22+(l*16)]:=#0;
            a[$23+(l*16)]:=#0;
            a[$24+(l*16)]:=#0;

            a[$335+(l*16)]:=#1;
            inc(l);
        end;


    sendtoplayer(x,a);
end;

procedure SendClientList2(x:integer;s:ansistring);
var a:ansistring;
    i,c,l:integer;
begin
    a:=#$5C#$06#$00#$00#$1F#$03#$00#$00#$11#$11#$11#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF    //02 = aviable, 05 = done and disabled
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00#$03#$00#$00#$00#$0C#$00#$00#$00;

    safemove(player[x].gid,a,9,4);

    //XOR: 70B2   SUB: 9E

    l:=ReadMagic(copy(s,13,4),$70B2,$9e);
    l:=(l and $fffC)+4;
    safemove(s[$11+l],a,$659,4);
    move(s[$11+l],c,4);

    //fill the list
    l:=0;
    for i:=0 to ClientOrderCount-1 do
        if (player[x].ClientOrder[i]<>0) and (clientorders[i].npc = lowercase(pansichar(@s[$11]))) then begin
            a[$15+(l*16)]:=#0; //always 0?
            a[$16+(l*16)]:=#0;
            a[$17+(l*16)]:=#0;
            a[$18+(l*16)]:=#0;

            safemove(clientorders[i].longid,a,$19+(l*16),4);

            if player[x].ClientOrder[i] = 1 then a[$1d+(l*16)]:= #2; //aviable
            if player[x].ClientOrder[i] = 2 then a[$1d+(l*16)]:= #4; //doing it
            if player[x].ClientOrder[i] = 3 then a[$1d+(l*16)]:= #4; //doing it
            if player[x].ClientOrder[i] = 4 then a[$1d+(l*16)]:= #5; //completed and disabled
            a[$1e+(l*16)]:=#0;
            a[$1f+(l*16)]:=#0;
            a[$20+(l*16)]:=#0;

            a[$21+(l*16)]:=#0; //always 0?
            a[$22+(l*16)]:=#0;
            a[$23+(l*16)]:=#0;
            a[$24+(l*16)]:=#0;
            inc(l);
        end;

    sendtoplayer(x,a);
end;

procedure AddOrder(x,id:integer);
var s:ansistring;
begin
    player[x].ClientOrder[id]:=1; //enabled
    s:=#$10#$00#$00#$00#$1F#$0C#$04#$00#$95#$AA#$00#$00#$BA#$13#$00#$00;
    safemove(ClientOrders[id].shortid,s,13,4);
    sendtoplayer(x,s);
end;

procedure TestForClientOrder(x:integer);
begin
    //test if any order need to be added
    if player[x].ClientOrder[0] = 0 then    //mag order
        if player[x].activeChar.base.jobstat[player[x].activeChar.base.job].lvl >=5 then begin
            //add it
            AddOrder(x,0);
        end;

end;

procedure TestOrderCompletion(x:integer);
begin
    if player[x].ClientOrder[0] = 3 then begin
        if FindItemPos(x,3,3,0,$52) = -1 then player[x].ClientOrder[0]:=2; //item is not there anymore
    end;


end;

procedure CompleteOrder(x,o,id1,id2:integer);
var i:integer;
    s:ansistring;
begin
    if o = 0 then begin
        i:=FindItemPos(x,3,3,0,$52);
        if i>-1 then begin
            //if the item is present procede
            //remove the item
            s:=#$1C#$00#$00#$00#$0F#$06#$04#$00#$F0#$AD#$00#$00#$35#$D7#$AD#$A1
            +#$B9#$28#$A6#$04#$02#$00#$01#$00#$02#$00#$00#$00;
            safemove(player[x].activeChar.PersonalBank[i].id1,s,13,4);
            safemove(player[x].activeChar.PersonalBank[i].id2,s,17,4);
            s[$15]:=ansichar(player[x].activeChar.PersonalBank[i].count-1);

            removeFromStorage(x,player[x].activeChar.PersonalBank[i].id1,player[x].activeChar.PersonalBank[i].id2,1
                ,player[x].activeChar.PersonalBank,@player[x].activechar.bankcount,50);
            sendtoplayer(x,s);

            //give a mag
            fillchar(player[x].maglist.mags[0],sizeof(tmag),0);
            player[x].maglist.mags[0].id1:=CreateItemID(x);
            player[x].maglist.mags[0].id2:=player[x].activechar.charID;
            move(DefaultMag[1],player[x].maglist.mags[0].cath,$50);
            player[x].maglist.count:=1;
            player[x].maglist.equiped:=-1;

            SendMagList(x);

            //add exp
            AddPlayerExp(x,5000);

            //refresh order list
            player[x].ClientOrder[o]:=4;
            SendClientOrder(x);

            //send explaination
            sendtoplayer(x,#$74#$00#$00#$00#$0B#$06#$04#$00#$DD#$B6#$00#$00#$63#$6F#$5F#$30
                +#$31#$31#$30#$32#$38#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$24#$B6#$00#$00
                +#$79#$02#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00#$00#$00#$00#$00
                +#$00#$00#$00#$00#$00#$00#$00#$00#$04#$00#$04#$00#$27#$B6#$00#$00
                +#$27#$B6#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                +#$00#$00#$00#$00);

            //send cleared
            s:=#$28#$00#$00#$00#$1F#$05#$00#$00#$02#$00#$00#$00#$00#$00#$00#$00
        	    +#$64#$69#$0F#$00#$FF#$FF#$FF#$FF#$1A#$00#$00#$00#$FF#$FF#$FF#$FF
	            +#$FF#$FF#$FF#$FF#$00#$00#$00#$00;

            safemove(id2,s,$19,4);
            sendtoplayer(x,s);

            s:=#$0C#$00#$00#$00#$1F#$07#$00#$00#$19#$00#$00#$00;
            safemove(id1,s,$9,4);
            sendtoplayer(x,s);
            

        end;
    end;
end;

procedure TakeOrder(x:integer;s:ansistring);
var i,id:integer;
    a:ansistring;
begin
    move(s[$11],id,4);
    for i:=0 to clientordercount-1 do
    if clientorders[i].longid = id then begin
        if player[x].ClientOrder[i] = 3 then CompleteOrder(x,i,pdword(@s[$2d])^,pdword(@s[$31])^)
        else begin
            a:=#$10#$00#$00#$00#$1F#$0B#$00#$00#$BA#$13#$00#$00#$01#$00#$00#$00;
            safemove(clientorders[i].shortid,a,9,4);
            sendtoplayer(x,a);

            a:=#$28#$00#$00#$00#$1F#$05#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                +#$64#$69#$0F#$00#$FF#$FF#$FF#$FF#$0E#$00#$00#$00#$FF#$FF#$FF#$FF
                +#$FF#$FF#$FF#$FF#$00#$00#$00#$00;
            safemove(clientorders[i].longid,a,$11,4);
            safemove(s[$31],a,$19,4);
            sendtoplayer(x,a);

            a:=#$0C#$00#$00#$00#$1F#$07#$00#$00#$0D#$00#$00#$00;
            safemove(s[$2d],a,$9,4);
            sendtoplayer(x,a);

            //set it in the save
            player[x].ClientOrder[i]:=2;
        end;
    end;
end;

function TestForOrderDrop(x,t,m,i:integer; var s:ansistring):boolean;
begin
    result:=false;
    //special order drop filter
    if player[x].ClientOrder[0] = 2 then begin
        //mag request order
        if (m = 5) and (i <> 0) then begin
            s:=#$03#$00#$03#$00#$00#$00#$52#$00#$01#$00#$00#$00#$00#$00#$00#$00;
            result:=true;
            exit;
        end;
    end;
end;


procedure TestForOrderItemPickup(x:integer; itm:PTinventoryItem);
var s:ansistring;
begin
    //mag order
    if player[x].ClientOrder[0] = 2 then begin
        if (itm.ItemType = 3) and (itm.ItemSub = 3) and (itm.unk = 0) and (itm.ItemEntry = $52) then begin
         s:=#$10#$00#$00#$00#$1F#$0B#$00#$00#$BA#$13#$00#$00#$02#$00#$00#$00;   //tag as completed
         sendtoplayer(x,s);
         player[x].ClientOrder[0]:=3;

        s:=#$28#$00#$00#$00#$1F#$05#$00#$00#$0C#$00#$00#$00#$00#$00#$00#$00
            +#$64#$69#$0F#$00#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
            +#$FF#$FF#$FF#$FF#$00#$00#$00#$00;
        sendtoplayer(x,s);

        end;
    end;
end;


end.
