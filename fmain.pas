unit fmain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, Spin, ScktComp, ExtCtrls, clipbrd, Grids;

type
    TShipEntry = record
        id:dword;
        name:array[0..15] of widechar;
        ip:dword;
        empty:dword;
        status,pos:word;
        unk:dword;
    end;
    TSkillIds = record
        id,classe:dword;
    end;
  TSrvConfig = record
      MaxPlayer,sPort,bPort:dword;
      ip:string[64];
      lobby:integer;
      Ann:integer;
      gmpass:string[64];
      cafe:integer;
  end;

  TItemData = record
        ItemType,ItemSub,unk,ItemEntry:word;
        name:array[0..47] of widechar;
        desc:array[0..$FF] of widechar;
        buyingprice:integer;
        sellingprice,star:word;
    end;

  TItemCVS = record
        jp,name:array[0..47] of widechar;
        desc:array[0..$FF] of widechar;
    end;

  TPacketHeader = record
        size:integer;
        fnc:word;
        channel,unused:byte;
    end;
  TForm1 = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    SpinEdit1: TSpinEdit;
    Label2: TLabel;
    Edit1: TEdit;
    Label3: TLabel;
    SpinEdit2: TSpinEdit;
    Button1: TButton;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Button2: TButton;
    Button3: TButton;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    TabSheet2: TTabSheet;
    Button4: TButton;
    ServerSocket1: TServerSocket;
    SpinEdit3: TSpinEdit;
    Label10: TLabel;
    ServerSocket2: TServerSocket;
    Timer1: TTimer;
    ClientSocket1: TClientSocket;
    Timer2: TTimer;
    Timer3: TTimer;
    TabSheet3: TTabSheet;
    ListBox1: TListBox;
    Button5: TButton;
    Memo2: TMemo;
    Label11: TLabel;
    Button6: TButton;
    Button7: TButton;
    Button8: TButton;
    Button9: TButton;
    SaveDialog1: TSaveDialog;
    Button10: TButton;
    Button11: TButton;
    ComboBox1: TComboBox;
    Button12: TButton;
    GroupBox3: TGroupBox;
    Edit2: TEdit;
    Button13: TButton;
    ComboBox2: TComboBox;
    Label12: TLabel;
    Button14: TButton;
    TabSheet4: TTabSheet;
    GroupBox4: TGroupBox;
    stringGrid1: TstringGrid;
    Label13: TLabel;
    Button15: TButton;
    Memo3: TMemo;
    Button16: TButton;
    GroupBox5: TGroupBox;
    ListBox2: TListBox;
    Button17: TButton;
    Button18: TButton;
    Edit3: TEdit;
    Button19: TButton;
    Button20: TButton;
    TabSheet5: TTabSheet;
    GroupBox6: TGroupBox;
    Image1: TImage;
    ListBox3: TListBox;
    Button21: TButton;
    Button22: TButton;
    ComboBox3: TComboBox;
    Label14: TLabel;
    ComboBox4: TComboBox;
    CheckBox1: TCheckBox;
    ServerSocket3: TServerSocket;
    ServerSocket4: TServerSocket;
    ServerSocket5: TServerSocket;
    ServerSocket6: TServerSocket;
    ServerSocket7: TServerSocket;
    ServerSocket8: TServerSocket;
    ServerSocket9: TServerSocket;
    ServerSocket10: TServerSocket;
    ServerSocket11: TServerSocket;
    ServerSocket12: TServerSocket;
    Button23: TButton;
    Timer4: TTimer;
    Edit4: TEdit;
    Button24: TButton;
    TabSheet6: TTabSheet;
    CheckBox2: TCheckBox;
    Memo4: TMemo;
    Button25: TButton;
    Button26: TButton;
    OpenDialog1: TOpenDialog;
    Button27: TButton;
    Button28: TButton;
    Label15: TLabel;
    Button29: TButton;
    Button30: TButton;
    PageControl2: TPageControl;
    TabSheet7: TTabSheet;
    MemoDebug: TMemo;
    TabSheet8: TTabSheet;
    MemoTest: TMemo;
    Button31: TButton;
    TabSheet9: TTabSheet;
    GroupBox7: TGroupBox;
    Memo6: TMemo;
    Button32: TButton;
    Label16: TLabel;
    Edit5: TEdit;
    GroupBox8: TGroupBox;
    Memo7: TMemo;
    Button33: TButton;
    Button34: TButton;
    Button35: TButton;
    Button36: TButton;
    Label17: TLabel;
    Memo8: TMemo;
    Button37: TButton;
    Button38: TButton;
    TabSheet10: TTabSheet;
    ListBox4: TListBox;
    Button39: TButton;
    Button40: TButton;
    Button41: TButton;
    Button42: TButton;
    Button43: TButton;
    Button44: TButton;
    Memo9: TMemo;
    Send: TButton;
    Button45: TButton;
    test5: TButton;
    ServerSocket13: TServerSocket;
    GroupBox9: TGroupBox;
    Edit6: TEdit;
    Label18: TLabel;
    Label19: TLabel;
    Edit7: TEdit;
    Button46: TButton;
    Label20: TLabel;
    ComboBox5: TComboBox;
    Button47: TButton;
    Label21: TLabel;
    Label22: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure ServerSocket1Accept(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure ServerSocket1ClientError(Sender: TObject;
      Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
      var ErrorCode: Integer);
    procedure ServerSocket1ClientDisconnect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure ServerSocket1ClientRead(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure Timer1Timer(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure Timer3Timer(Sender: TObject);
    procedure ClientSocket1Error(Sender: TObject; Socket: TCustomWinSocket;
      ErrorEvent: TErrorEvent; var ErrorCode: Integer);
    procedure ClientSocket1Connect(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure Button5Click(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure Button11Click(Sender: TObject);
    procedure Button12Click(Sender: TObject);
    procedure Button13Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button14Click(Sender: TObject);
    procedure Button15Click(Sender: TObject);
    procedure Button16Click(Sender: TObject);
    procedure Button17Click(Sender: TObject);
    procedure Button18Click(Sender: TObject);
    procedure Button19Click(Sender: TObject);
    procedure Button20Click(Sender: TObject);
    procedure Button21Click(Sender: TObject);
    procedure ListBox3Click(Sender: TObject);
    procedure Button22Click(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure ServerSocket3Accept(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure ServerSocket3ClientError(Sender: TObject;
      Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
      var ErrorCode: Integer);
    procedure Button23Click(Sender: TObject);
    procedure Timer4Timer(Sender: TObject);
    procedure Button24Click(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure Button25Click(Sender: TObject);
    procedure Button26Click(Sender: TObject);
    procedure Button27Click(Sender: TObject);
    procedure ComboBox4Change(Sender: TObject);
    procedure Button28Click(Sender: TObject);
    procedure Button29Click(Sender: TObject);
    procedure ListBox2Click(Sender: TObject);
    procedure Button30Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button31Click(Sender: TObject);
    procedure Button32Click(Sender: TObject);
    procedure Button33Click(Sender: TObject);
    procedure Button34Click(Sender: TObject);
    procedure Button35Click(Sender: TObject);
    procedure Button36Click(Sender: TObject);
    procedure Button37Click(Sender: TObject);
    procedure Button38Click(Sender: TObject);
    procedure Button39Click(Sender: TObject);
    procedure Button40Click(Sender: TObject);
    procedure Button41Click(Sender: TObject);
    procedure Button42Click(Sender: TObject);
    procedure Button43Click(Sender: TObject);
    procedure Button44Click(Sender: TObject);
    procedure SendClick(Sender: TObject);
    procedure Button45Click(Sender: TObject);
    procedure test5Click(Sender: TObject);
    procedure ServerSocket13ClientRead(Sender: TObject;
      Socket: TCustomWinSocket);
    procedure ServerSocket13ClientError(Sender: TObject;
      Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
      var ErrorCode: Integer);
    procedure Button46Click(Sender: TObject);
    procedure Button47Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  function compareuni(a,b:array of widechar):boolean;


var
  Form1: TForm1;
  SrvCfg:TSrvConfig;
  path:ansistring;
  ActiveIP:array [0..3] of byte;
  maxplayer:integer = 0;

  logdata:ansistring;
  ppos:array[0..1000000] of integer;
  pid:ansistring;
  PacketDesc:tstringlist;
  items:array[0..20000] of TItemData;
  itemcount:integer = 0;

  ObjMap,lobbydata:ansistring;
  testpos:integer=1;
  redalert:integer = 0;
  debugme : integer = 0;
  test:integer=0;
  meteo:integer=0;

  CurTargId:integer;

  UnknownAction:tstringlist;



implementation

uses UDebug, UEnc, UPlayer, Uerror, Ulogin, ULobby, UPsoUtil, UMovement,
  UDB, UQuestBoard, UForest, UInventory, UShop, UStats, BBEnc, casino,
  UParty, UFriendList, UMail, UDrop, UPAid, Uskills, clientorder, UMag,
  ItemGenerator, UTreasureShop, UTicketShop, UTeamFnc, UMyRoom, UTimerHelper,
  UTitleAssistant, RoomAction, StackTrace;

{$R *.dfm}


procedure LoadDocs;
var sr: TSearchRec;
    tmp:tstringlist;
begin
    PacketDesc:=tstringlist.Create;
    tmp:=tstringlist.Create;
    if FindFirst(path+'doc\*.txt', faAnyfile, sr) = 0 then

    begin
      repeat
         PacketDesc.Add(copy(sr.Name,1,4));
         tmp.LoadFromFile(path+'doc\'+sr.Name);
         PacketDesc.Add(tmp[0]);
      until FindNext(sr) <> 0;
      FindClose(sr);
    end;

    form1.ComboBox1.Clear;
    if FindFirst(path+'log\*.dat', faAnyfile, sr) = 0 then

    begin
      repeat
           form1.ComboBox1.Items.Add(sr.Name);
      until FindNext(sr) <> 0;
      FindClose(sr);
    end;

end;


procedure UpdatePlayerCount();
var x,i:integer;
begin
    i:=0;
    for x:=0 to srvcfg.MaxPlayer-1 do
        if player[x].GID > 0 then inc(i);
    if i>maxplayer then maxplayer:=i;
    form1.Label8.Caption:=inttostr(i);
    form1.Label9.Caption:=inttostr(maxplayer);
end;

procedure GetIP();
begin
    //get ip
    try
        form1.ClientSocket1.Host:=form1.Edit1.Text;
        form1.ClientSocket1.Port := form1.SpinEdit2.Value;
        form1.ClientSocket1.Open;
    except
    end;
end;

procedure TForm1.Button1Click(Sender: TObject);
var f:integer;
begin
    SrvCfg.MaxPlayer:=spinedit1.Value;
    SrvCfg.sPort:=spinedit2.Value;
    SrvCfg.bPort:=spinedit3.Value;
    SrvCfg.ip:=edit1.Text;
    SrvCfg.Lobby:=combobox4.itemindex;
    srvcfg.cafe:=combobox5.itemindex;
    f:=filecreate(path+'server.cfg');
    filewrite(f,srvcfg,sizeof(srvcfg));
    fileclose(f);
end;

procedure TForm1.FormCreate(Sender: TObject);
var f:integer;
    id:TSkillIds;
begin
    path:=extractfilepath(application.ExeName);
    fillchar(srvcfg, sizeof(srvcfg),0);
    f:=fileopen(path+'server.cfg',$40);
    if f>0 then begin
        fileread(f,srvcfg,sizeof(srvcfg));
        fileclose(f);

        spinedit1.Value:=SrvCfg.MaxPlayer;
        spinedit2.Value:= SrvCfg.sPort;
        spinedit3.Value:= SrvCfg.bPort;
        combobox4.ItemIndex:=srvcfg.lobby;
        combobox5.ItemIndex:=srvcfg.cafe;
        edit1.Text:=SrvCfg.ip;
        checkbox2.Checked:=false;
        edit5.Text:=SrvCfg.gmpass;
        if srvcfg.Ann = 1 then checkbox2.Checked:=true;
        if fileexists('data\announcement.txt') then
        memo4.Lines.LoadFromFile('data\announcement.txt');
        if fileexists('gm.txt') then
            memo6.Lines.LoadFromFile('gm.txt');
        if fileexists('banlist.txt') then
            memo7.Lines.LoadFromFile('banlist.txt');
        if fileexists('data\tester.txt') then
        memo8.Lines.LoadFromFile('data\tester.txt');
    end;
    LoadDocs;
    initcasino();

    f:=fileopen('data\skillid.dat',$40);
    while fileread(f,id,8) = 8 do begin
        listbox4.Items.Add(inttostr(id.id));

    end;
    listbox4.Sorted:=true;
    fileclose(f);
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
    UnknownAction:=tstringlist.Create;
    //System.SysUtils.FormatSettings.decimalseparator:=',';
    sysutils.FormatSettings.DecimalSeparator:=',';
    initLog;
    FixStats();
    button2.Enabled:=false;
    debug(DBG_LOG,'Team size: '+inttostr(sizeof(TMonsterEntry)));
    debug(DBG_LOG,'Quest info size: '+inttostr(sizeof(QuestInfo[0])));
    debug(DBG_LOG,'Player size: '+inttostr(sizeof(TPlayer)));
    //setting up socket
    debug(DBG_LOG,'Loading RSA...');
    InitRSA();
    debug(DBG_LOG,'Setting up player array...');
    initusers;
    debug(DBG_LOG,'Setting up socket...');
    serversocket1.Port:=spinedit2.Value;
    serversocket2.Port:=spinedit3.Value;
    serversocket2.Active:=true;
    serversocket1.Active:=true;
    serversocket3.Active:=true;
    serversocket4.Active:=true;
    serversocket5.Active:=true;
    serversocket6.Active:=true;
    serversocket7.Active:=true;
    serversocket8.Active:=true;
    serversocket9.Active:=true;
    serversocket10.Active:=true;
    serversocket11.Active:=true;
    serversocket12.Active:=true;
    debug(DBG_LOG,'Setting up timers...');
    timer1.Enabled:=true;
    timer2.Enabled:=true;
    timer3.Enabled:=true;
    debug(DBG_LOG,'Setting up teams...');
    InitTeam();
    debug(DBG_LOG,'Loading Items data...');
    LoadItemsDesc();
    LoadOutfit();
    debug(DBG_LOG,'Init quest list...');
    InitQuests();
    initcasino;
    //lobbydata:=KeepStaticItem(FileGetPacket('map\lobby.obj'));
    lobbydata:=KeepStaticItem(GenerateObjFile(path+'map\lobby.obj',-1,-1));
    //form1.ComboBox4.ItemIndex := 0;//form1.ComboBox4.Items.Count-1;
    form1.ComboBox4Change(nil);
    debug(DBG_LOG,'Init mail system...');
    initMail();
    debug(DBG_LOG,'Init Rooms system...');
    initRooms();
    InitPso2Timer();
    InitTimers();

    //raise exception.Create('patate!!');

    GetIP;
    label7.Caption:='Running';
    serversocket13.active:=true;
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
    MemoDebug.Clear;
end;

procedure TForm1.SendClick(Sender: TObject);
var s:ansistring;
    x,y:integer;
begin
    s:='';
    for x:=0 to memo9.Lines.Count-1 do begin
        y:=9;
        while (y<57) and (memo9.Lines[x][y] <> ' ') do begin
            s:=s+ansichar(strtoint('$'+copy(memo9.Lines[x],y,2)));
            y:=y+3;
        end;

    end;
    sendtoplayer(0,s);
end;

procedure TForm1.ServerSocket13ClientError(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent; var ErrorCode: Integer);
begin
    errorcode:=0;
    socket.Close;
end;

procedure TForm1.ServerSocket13ClientRead(Sender: TObject;
  Socket: TCustomWinSocket);
var x,c:integer;
    s,a:ansistring;
begin
    //return current count
    s:=socket.ReceiveText;
    if pos('announcement.html',s) > 0 then begin
        s:=memo4.Text;
        a:='';
        c:=1;
        while c < length(s) do begin
            if s[c] = '<' then begin
                if s[c+1] = 'c' then begin
                    if s[c+2] = '>' then a:=a+'</b>'
                    else a:=a+'<b>';
                end else a:=a+'<b>';
                while s[c] <> '>' do inc(c);
            end else if s[c] = #10 then begin
                a:=a+'<br>';
            end else if s[c] = #13 then begin

            end else begin
                a:=a+s[c];
            end;
            inc(c);
        end;
        socket.SendText('HTTP/1.1 200 OK'#13#10'Content-Length: '+inttostr(length(a))
        +#13#10'Content-Type: text/html'#13#10'Connection: Closed'#13#10#13#10+a);

    end else begin
        c:=0;
        for x:=0 to srvcfg.MaxPlayer-1 do
            if playerok(x) then inc(c);
        socket.SendText('HTTP/1.1 200 OK'#13#10'Content-Length: '+inttostr(length(inttostr(c)))
        +#13#10'Content-Type: text/html'#13#10'Connection: Closed'#13#10#13#10+inttostr(c));

    end;
    //socket.close;
end;

procedure TForm1.ServerSocket1Accept(Sender: TObject;
  Socket: TCustomWinSocket);
var x:integer;
begin
    x:=GetPlayerSlot();
   // x:=-1;
    if x>-1 then begin
        player[x].Socket:=socket;
        player[x].din:='';
        //player[x].dout:=#$14#$00#$00#$00#$03#$08#$00#$00#$03#$00#$53#$01#$00#$00#$00#$00#$00#$00#$00#$00; //request rsa xfer
        player[x].dout:=#$14#$00#$00#$00#$03#$08#$00#$00#$04#$00#$DE#$03#$00#$00#$00#$00#$00#$00#$00#$00;
        player[x].username:='';
        player[x].cipher:=0;
        player[x].todc:=0;
        player[x].TeamID:=-1;

        UpdatePlayerCount();
    end else begin
        //send server full
        socket.SendText(MakeError(ERR_Full,'Server has reatched his player limit, please try again later!'));
        socket.Close;
    end;

end;

procedure TForm1.ServerSocket1ClientError(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
    errorcode:=0;
    socket.Close;
end;

procedure TForm1.ServerSocket1ClientDisconnect(Sender: TObject;
  Socket: TCustomWinSocket);
var x,i:integer;
    dw:dword;
begin
    x:=FindPlayer(socket);
    if x > -1 then begin
        //save player data
        if player[x].CountTime = 1 then begin
            dw:=gettickcount() - player[x].LogedTime;
            dw:=dw div 1000; //value in second
            inc(player[x].activeChar.playtime,dw);

            //mag energy
            for i:=0 to player[x].maglist.count-1 do begin
                dw:=(gettickcount-player[x].magfeedtime[i]) div 1000;
                if dw > player[x].maglist.mags[i].energy then player[x].maglist.mags[i].energy:=0
                else dec(player[x].maglist.mags[i].energy,dw);
            end;
            for i:=0 to 19 do
                if player[x].save.char[i].charID = player[x].activeChar.charID then begin
                    player[x].save.LastPlayTime[i]:=Round((now() - 25569) * 86400);
                    //last time recorded for player
                end;

        end;
        if playerok(x) then begin
        SaveAccount(player[x].username,@player[x].save);

        //SendLobbyChar(player[x].room,-1,#$0C#$00#$00#$00#$11#$0A#$00#$00#$56#$5F#$9C#$00); //make it disapear

        leavelobby(x);
        LeaveParty(x);
        if player[x].teamid > -1 then begin
            i:=player[x].TeamID;
            player[x].TeamID:=-1;
            TestTeam(i);
        end;
        end;

        //clean the entry
        player[x].GID:=0;
        player[x].din:='';
        player[x].dout:='';
        player[x].TeamID:=-1;

        UpdatePlayerCount();
    end;

end;

procedure TForm1.ServerSocket1ClientRead(Sender: TObject;
  Socket: TCustomWinSocket);
var x:integer;
    pk:^TPacketHeader;
begin
    x:=FindPlayer(socket);
    if x > -1 then begin
        if player[x].cipher = 1 then player[x].din:=player[x].din+psodec(socket.ReceiveText,x)
        else player[x].din:=player[x].din+socket.ReceiveText;
        while length(player[x].din)> 4 do begin
            pk:=@player[x].din[1];
            if pk.size <= length(player[x].din) then begin
                WriteToLog('Client:',copy(player[x].din,1,pk.size));
                debug(DBG_LOG,inttohex(pk.fnc,4));
                case pk.fnc of
                    //login function
                    $0011: LoginWithUserPass(x,copy(player[x].din,1,pk.size));
                    $0b11: processKey(x,copy(player[x].din,1,pk.size));
                    $1411: LoginWithToken(x,copy(player[x].din,1,pk.size));
                    $0619: begin end; //unknown at login
                    $0419: begin end; //unknown at login
                    $2d11: begin end; //player client and os spec
                    $002b: begin end; //unknown
                    $b011: BringClientToCharList(x);
                    $0211: SendCharList(x); //get char list
                    $4111: CreateMode(x);
                    $5411: Packet1155(x);
                    $0511: NewChar(x,@player[x].din[$11]);
                    $0611: DeleteChar(x,@player[x].din[$9]);
                    $0d11: ping(x,@player[x].din[$9]);
                    $9711: RenameChar(x,@player[x].din[$9]);
                    $9911: RenameChar2(x);
                    $9b11: finalrename(x,copy(player[x].din,1,pk.size));
                    $012b: StoreConfig(x,copy(player[x].din,1,pk.size));
                    $0411: SelectChar(x,@player[x].din[$9]);
                    $2B11: player[x].todc:=1; //player log out
                    $0223: ClearTutorialScreen(x,pdword(@player[x].din[13])^);
                    $0133: saveoption1(x,copy(player[x].din,1,pk.size));
                    $002C: ChangeStatus(x,copy(player[x].din,1,pk.size));
                    $2311: LoadLobby(x);

                    //lobby change and load
                    $1511: GoMyRoom(x,pdword(@player[x].din[9])^);
                    $1F11: GoBattleLobby(x,pdword(@player[x].din[9])^);
                    $0303: LoadLobby(x);
                    $1111: LoadLobby(x);
                    $3503: GoCasino(x,0);
                    $3C03: GoCafe(x);
                    $3903: GoBridge(x);
                    $3803: GoLobby(x,1);//shop from cafe?
                    $3403: GoLobby(x,0);//gate from casino
                    $3b03: GoLobby(x,byte(player[x].din[$19]));
                    $1c03: GoLobby(x,2);//from campship
                    $120E: LeaveToLobby(x);
                    $0f11: BlockSelect(x);
                    $1203: GoBootCamp(x,0);
                    $1903: GoBootCamp(x,1); //from planet start
                    $1703: GoBootCamp(x,1); //from planet end
                    $1803: begin
                            move(player[x].din[$11],player[x].pipeid,4);
                            player[x].pipefloor:=player[x].floor;
                            GoBootCamp(x,1); //from planet pipe
                        end;
                    $1a03: begin
                            ResurectPlayer(x,0);
                            GoBootCamp(x,1); //resurect player and bring him to campship
                        end;
                    $1603: GoForest1(x,0);
                    $1103: GoForest1(x,1);
                    $3203: LeaveShooter(x); //return from shooter game
                    $0503: ChangeFloor(x,copy(player[x].din,1,pk.size));
                    $2003: GoDressingRoom(x);
                    $2103: GoLobby(x,3);
                    $0A23: MakeMonsterTarget(x,copy(player[x].din,1,pk.size));

                    //my room
                    $0c1D: unlockroom(x);
                    $101D: PlaceObjectInRoom(x,copy(player[x].din,1,pk.size));
                    $0A1D: MoveItemInRoom(x,copy(player[x].din,1,pk.size));
                    $181D: EditRoomLock(x,copy(player[x].din,1,pk.size));
                    $371D: RemoveAllItemInRoom(x);
                    $121D: RemoveItemInRoom(x,copy(player[x].din,1,pk.size));
                    $2f11: DoorMoveTo(x,pdword(@player[x].din[$15])^);

                    //inventory
                    $0521: SetQuickPalette(x,copy(player[x].din,1,pk.size));
                    $1c0f: GetItemDescription(x,copy(player[x].din,1,pk.size));
                    $0a21: SavePalette2(x,copy(player[x].din,1,pk.size));
                    $0621: SetWeaponPalette(x,copy(player[x].din,1,pk.size));
                    $0221: SendFullPalette(x);
                    $0421: UseWeaponPalette(x,byte(player[x].din[9]));
                    $0821: UseSubPalette(x,byte(player[x].din[9]));
                    $010f: PickupItem(x,@player[x].din[$9]);
                    $080f: EquipSomething(x,@player[x].din[$9],@player[x].din[$d],@player[x].din[$11]);
                    $0a0f: unEquipSomething(x,@player[x].din[$9],@player[x].din[$d],@player[x].din[$11],@player[x].din[$15]);
                    $200f: LearnDisk(x,copy(player[x].din,1,pk.size));
                    $b70f: FashionEdit(x,copy(player[x].din,1,pk.size));
                    $c111: UnlockFashionMenu(x);
                    $870f: SetAccessoryFashion(x,copy(player[x].din,1,pk.size));
                    $170f: TrashStuff(x,copy(player[x].din,1,pk.size));
                    $250f: TrashStuffBank(x,copy(player[x].din,1,pk.size));


                    //shop
                    $0034: RequestShop(x,byte(player[x].din[9]));
                    $0234: BuyItemsFromShop(x,copy(player[x].din,1,pk.size));
                    $0434: SellItemsToShop(x,copy(player[x].din,1,pk.size));
                    $4234: CasinoOutfitShop(x);
                    $0440: ChangePetName(x,copy(player[x].din,1,pk.size));
                    $0f0f: DepositeToBank(x,copy(player[x].din,1,pk.size));
                    $110f: WidrawFromBank(x,copy(player[x].din,1,pk.size));
                    $180f: DepositeFromBank(x,copy(player[x].din,1,pk.size));
                    $150f: bankmesetas(x,copy(player[x].din,1,pk.size));
                    $6611: sendtoplayer(x,#$10#$00#$00#$00#$11#$67#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00);
                    $3e11: EditDressing(x,copy(player[x].din,1,pk.size));
                    $5834: GetTSList(x,byte(player[x].din[9]));
                    $5a34: TSBuyItem(x,copy(player[x].din,1,pk.size));
                    $2134: GetStaticTicketList(x);
                    $2834: GetDatedTicketList(x);
                    $0049: sendtoplayer(x,#$30#$00#$00#$00#$49#$01#$00#$00#$02#$00#$03#$00#$00#$00#$03#$00
                                        +#$64#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$F4#$01#$00#$00
                                        +#$00#$00#$00#$00#$B8#$0B#$00#$00#$02#$00#$00#$00#$03#$00#$00#$00);

                    //casino function
                    $283E: TicketExchange(x);
                    $263E: ExchangeTicket(x,pdword(@player[x].din[$9])^); //qty
                    $453E: SitInGame(x,copy(player[x].din,1,pk.size));
                    $3103: GoInCasinoGame(x,copy(player[x].din,1,pk.size));
                    $053E: Shooter_HitTarget(x,pdword(@player[x].din[$9])^);
                    $0e3e: Shooter_End(x,copy(player[x].din,1,pk.size));
                    $113e: Shooter_keepplaying(x,copy(player[x].din,1,pk.size));    //need to figure out how to much to remove
                    $3d3e: RequestBJOption(x,pdword(@player[x].din[$9])^);
                    $3f3e: CasinoBJBet(x,copy(player[x].din,1,pk.size));
                    $413e: HitOrStay(x,pdword(@player[x].din[$9])^);
                    
                    //team function
                    $0c0e: CreateTeam(x,copy(player[x].din,1,pk.size));
                    $2f0b: SetQuestID(x,@player[x].din[$9],@player[x].din[$29]);
                    $200b: SetQuestID(x,@player[x].din[$9],@player[x].din[$29]);
                    $320E: SetQuestID(x,@team[player[x].teamid].questid,@player[x].din[$1]);   //size will never be 1 so ok
                    //$350e: SetQuestID(x,@player[x].din[$9]);

                    //real team function
                    $001C: TeamCreate(x,pwidechar(@player[x].din[$d]));
                    $021C: TeamDisband(x);
                    $461C: GetMyTeam(x);
                    $041C: GetTeamInfo(x,pdword(@player[x].din[$9])^);
                    $261C: sendtoplayer(x,#$10#$00#$00#$00#$1C#$27#$04#$00#$01#$00#$00#$00#$3C#$8C#$00#$00);
                    $061C: SetTeamComment(x,copy(player[x].din,1,pk.size));
                    $451C: SetTeamWelcome(x,copy(player[x].din,1,pk.size));
                    $141C: RemovePlayerFromTeam(x,copy(player[x].din,1,pk.size));
                    $161C: ChangePlayerRightinTeam(x,copy(player[x].din,1,pk.size));
                    $101C: SendTeamMemberInfo(x);
                    $281C: sendtoplayer(x,#$10#$00#$00#$00#$1C#$29#$04#$00#$01#$00#$00#$00#$2F#$97#$00#$00);
                    $311C: TeamSomething1(x,copy(player[x].din,1,pk.size));
                    $121C: RemoveMyselfFromTeam(x);
                    $081C: InvitPlayerToTeam(x,copy(player[x].din,1,pk.size));
                    $0E1C: SendTeamRequestList(x);
                    $0a1C: AwnserTeamOffer(x,copy(player[x].din,1,pk.size));

                    //skills
                    $2704: RaiseSkill(x,copy(player[x].din,1,pk.size));
                    $2004: ChangeJob(x,copy(player[x].din,1,pk.size));

                    //in quest
                    $0106: KickSomething(x,copy(player[x].din,1,pk.size));
                    $090b: EnterSectionMap(x,@player[x].din[$d]);
                    $2b04: triggerMonster(x,copy(player[x].din,1,pk.size));
                    $1304: TriggerObject(x ,copy(player[x].din,1,pk.size));
                    $2304: GetMonsterAttention(x,copy(player[x].din,1,pk.size));


                    //party
                    $050e: invitePlayerToParty(x ,copy(player[x].din,1,pk.size));
                    $2e0e: PartyQuerryInfo1(x,copy(player[x].din,1,pk.size));
                    $1d0e: PartyQuerryInfo2(x,copy(player[x].din,1,pk.size));
                    $070e: AcceptParty(x,copy(player[x].din,1,pk.size));
                    $090e: LeaveParty(x);
                    $100e: KickPlayerFromParty(x,copy(player[x].din,1,pk.size));
                    $0e0e: SetPartyLeader(x,pdword(@player[x].din[$9])^);
                    $1709: DisbandParty(x);
                    $350e: GetPartyList(x, copy(player[x].din,1,pk.size));
                    $0e0b: JoinGame(x,copy(player[x].din,1,pk.size));


                    //mail
                    $0b1a: sendmail(x,copy(player[x].din,1,pk.size));
                    $001A: GetMailBox(x,copy(player[x].din,1,pk.size));
                    $061a: GetMessageBody(x,pdword(@player[x].din[$9])^);
                    $021A: DeleteMail(x,copy(player[x].din,1,pk.size));

                    //friendlist
                    $1e1f: GetPlayerQuickInfo(x,copy(player[x].din,1,pk.size));
                    $1418: GetFriendList(x);
                    //$101c: UnlockPlayerMenu(x);     changed to a team function
                    $1818: InviteFriend(x,copy(player[x].din,1,pk.size));
                    $1b18: AcceptFriend(x,copy(player[x].din,1,pk.size));
                    $2118: DeleteFriend(x,pdword(@player[x].din[$9])^);
                    $0216: AddToBlackList(x,pdword(@player[x].din[$9])^);
                    $0416: RemoveFromBlackList(x,pdword(@player[x].din[$9])^);
                    $0127: EditArksCard(x,copy(player[x].din,1,pk.size));
                    $0027: RequestArksCard(x,pdword(@player[x].din[$15])^);
                    $3911: GetPlayerInfo(x,pdword(@player[x].din[$9])^);
                    $0324: GetShortcut(x);
                    $0424: SaveShortcut(x,copy(player[x].din,1,pk.size));
                    $0024: GetAutoWord(x);
                    $0124: SaveAutoWord(x,copy(player[x].din,1,pk.size));
                    $0624: GetVictoryPose(x);
                    $0724: SaveVictoryPose(x,copy(player[x].din,1,pk.size));

                    //forwarding
                    $0007: PlayerChat(x,copy(player[x].din,1,pk.size));
                    $1404: Packet1404(x,copy(player[x].din,1,pk.size));
                    $3c04: Motion3c04(x,copy(player[x].din,1,pk.size));
                    $0804: Motion0804(x,copy(player[x].din,1,pk.size));
                    $7104: Motion7104(x,copy(player[x].din,1,pk.size));
                    $0704: Motion0704(x,copy(player[x].din,1,pk.size));
                    $290e: PlayerIsNotBusy(x);
                    $280e: PlayerIsBusy(x);
                    $300b: SendQuestBoard(x);
                    $150b: RequestQuestList(x,@player[x].din[$9]);
                    $170b: RequestQuestList2(x,@player[x].din[$9],@player[x].din[13]);
                    $190b: RequestQuestInfo(x,copy(player[x].din,1,pk.size));
                    $190e: PlayerMenuStatus(x,copy(player[x].din,1,pk.size));
                    $bd04: KickBall(x,copy(player[x].din,1,pk.size));
                    $1e1c: RetrivePlayerInfo(x,copy(player[x].din,1,pk.size));
                    $4a0e: RefreshInTeamCount(player[x].TeamID);//sendtoplayer(x,#$0C#$00#$00#$00#$0E#$4B#$00#$00#$05#$0c#$00#$00); //unknown yet

                    //client order
                    $011f: SendClientList1(x,copy(player[x].din,1,pk.size));
                    $021f: SendClientList2(x,copy(player[x].din,1,pk.size));
                    $001F: TakeOrder(x,copy(player[x].din,1,pk.size));

                    //mag
                    $062A: EquipMag(x,copy(player[x].din,1,pk.size));
                    $022A: FeedMag(x,copy(player[x].din,1,pk.size));
                    $0f2A: unequipMag(x);

                    //title assistant
                    $0131: SendEarnedTitle(x);
                    $0331: GetFullTitleList(x);
                    $0631: GetTitleDescription(x,pdword(@player[x].din[$9])^);

                    {[Log] 0223  //credit
                    [Log] 9911 //ask rename?
                    [Log] 8611  //log list }




                end;

                delete(player[x].din,1,pk.size);
            end else break;
        end;
    end;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
var i,c,l,pet:integer;
    s:ansistring;
begin
    //ping

    //test if must resurect pet
    for i:=0 to srvcfg.MaxPlayer-1 do
        if playerok(i) then begin
            if player[i].activeChar.PetCount > 0 then begin
                //test if a pet had to reborn
                for c:=0 to player[i].activeChar.PetCount-1 do
                if player[i].pet_stunned[c] <> 0 then begin
                    //stunned
                    if gettickcount-player[i].pet_stunned[c] >=60000 then begin
                        //unstunn it
                        player[i].pet_stunned[c]:=0;
                        player[i].pet_hp:=player[i].pet_maxhp;

                        //is it active? if yes send packet
                        l:=player[i].activeChar.weppal;
                        move(player[i].activeChar.palette[$24+(l*$40)],pet,4);
                        if pet = player[i].activeChar.Pets[c].PID then begin
                            //used, send the packet

                            s:=#$2C#$00#$00#$00#$04#$25#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
                            +#$04#$00#$00#$00#$19#$01#$00#$00#$01#$00#$00#$00#$06#$00#$a9#$11
                            +#$49#$E6#$02#$00#$00#$00#$00#$00#$00#$00#$00#$00;
                            move(player[i].gid,s[9],4);
                            move(player[i].gid,s[$15],4);
                            sendtolobby(player[i].room,-1,s);
                        end;
                    end;
                end;
            end;
        end;
    
end;



procedure TForm1.Timer2Timer(Sender: TObject);
var i,c,l,t,d:integer;
    s,p:ansistring;
begin
    //status effect
    //do player
    for i:=0 to srvcfg.MaxPlayer-1 do
        if playerok(i) and (player[i].room >= Room_Team) and (player[i].statuseffect > 0) then begin
            if player[i].statustime > gettickcount() then begin
                inc(player[i].statusefft);
                if (player[i].statuseffect >= Status_Shock) and (player[i].statuseffect <= Status_Shock+4) then begin
                    if player[i].statusefft=5-((player[i].statuseffect-Status_Shock) div 3) then begin
                        //faint
                        player[i].statusefft:=0;
                        s:=#$50#$00#$00#$00#$06#$02#$00#$00#$c3#$06#$01#$00#$00#$00#$00#$00
                        +#$06#$00#$a9#$11#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
                        +#$08#$4B#$4E#$70#$00#$00#$00#$00#$00#$00#$00#$00#$FF#$FF#$FF#$FF
                        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                        +#$24#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;
                        move(player[i].gid,s[$15],4);
                        sendtofloor(player[i].room,player[i].Floor,s);
                    end;
                end;

                if (player[i].statuseffect >= 10010) and (player[i].statuseffect <= 10014) then begin
                    if player[i].statusefft=2 then begin
                        //burn
                        player[i].statusefft:=0;
                        if player[i].statuseffect = 10010 then d:=round(player[i].maxhp*0.03);
                        if player[i].statuseffect = 10011 then d:=round(player[i].maxhp*0.03);
                        if player[i].statuseffect = 10012 then d:=round(player[i].maxhp*0.04);
                        if player[i].statuseffect = 10013 then d:=round(player[i].maxhp*0.04);
                        if player[i].statuseffect = 10014 then d:=round(player[i].maxhp*0.05);
                        p:=#0#0#0#0#0#0#0#0;
                        move(player[i].curpos,p[1],8);
                        ApplyDamageToPlayer(i,player[i].TeamID,player[i].Floor,d,#0#0#0#0#0#0#0#0#0#0#0#0,p);
                    end;
                end;



            end else player[i].statuseffect := 0;

        end;

    //do monster status effect
    for t:=0 to 99 do
        if team[t].used = 1 then begin
            for c:=0 to team[t].FloorCount-1 do
            for i:=0 to team[t].monstcount[c]-1 do
            if team[t].monst[c,i].wave = 1 then
            if team[t].monst[c,i].statuseffect > 0 then begin
                inc(team[t].monst[c,i].statusefft);
                if (team[t].monst[c,i].statuseffect >= Status_Shock) and (team[t].monst[c,i].statuseffect <= Status_Shock+4) then begin
                    if team[t].monst[c,i].statusefft=5-((team[t].monst[c,i].statuseffect-Status_Shock) div 3) then begin
                        //faint
                        team[t].monst[c,i].statusefft:=0;
                        s:=#$50#$00#$00#$00#$06#$02#$00#$00#$c3#$06#$01#$00#$00#$00#$00#$00
                        +#$06#$00#$a9#$11#$56#$5F#$9C#$00#$00#$00#$00#$00#$06#$00#$a9#$11
                        +#$08#$4B#$4E#$70#$00#$00#$00#$00#$00#$00#$00#$00#$FF#$FF#$FF#$FF
                        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                        +#$24#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;
                        move(team[t].monst[c,i].id,s[$15],4);
                        sendtofloor(room_team+t,c,s);
                    end;
                end;

                if (team[t].monst[c,i].statuseffect >= 10010) and (team[t].monst[c,i].statuseffect <= 10014) then begin
                    if team[t].monst[c,i].statusefft=2 then begin
                        //burn
                        team[t].monst[c,i].statusefft:=0;
                        if team[t].monst[c,i].statuseffect = 10010 then d:=round(team[t].monst[c,i].mhp*0.03);
                        if team[t].monst[c,i].statuseffect = 10011 then d:=round(team[t].monst[c,i].mhp*0.03);
                        if team[t].monst[c,i].statuseffect = 10012 then d:=round(team[t].monst[c,i].mhp*0.04);
                        if team[t].monst[c,i].statuseffect = 10013 then d:=round(team[t].monst[c,i].mhp*0.04);
                        if team[t].monst[c,i].statuseffect = 10014 then d:=round(team[t].monst[c,i].mhp*0.05);
                        p:=#0#0#0#0#0#0#0#0;
                        move(team[t].monst[c,i].curpos,p[1],8);
                        ApplyDamageToMonster(0,t,i,c,d,0,p);
                        //i,player[i].TeamID,player[i].Floor,d,#0#0#0#0#0#0#0#0#0#0#0#0,p);
                    end;
                end;


            end;
        end;

    //npc remove
    for t:=0 to 99 do
        if team[t].used = 1 then begin
            c:=0;
            while c < team[t].npccount do
            if (team[t].npcs[c].dietime+10000 < gettickcount()) and (team[t].npcs[c].dietime<>0) then begin
              s:=#$20#$00#$00#$00#$04#$3B#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
                +#$04#$00#$00#$00#$15#$32#$00#$00#$00#$00#$00#$00#$16#$00#$8A#$03;
              //move(player[x].gid,s[9],4);
              i:=team[t].npcs[c].id;
              move(i,s[$15],4);
              sendtofloor(t+room_team,team[t].npcs[c].wave,s);
              move(team[t].npcs[c+1],team[t].npcs[c],(team[t].npccount-1-c)*sizeof(team[t].npcs[c]));
              dec(team[t].npccount);
            end else inc(c);
        end;

    //meteo test for thunder
    if meteo and 15 = 0 then
    for i:=0 to 99 do
        if team[i].used = 1 then begin
            if team[i].meteo.Count > 0 then 
            if team[i].meteo[team[i].cmeteo] = 'ThunderyRain' then begin
                if random(4) = 1 then begin
                    //target a player
                    c:=0;
                    for l:=0 to srvcfg.MaxPlayer-1 do
                        if playerok(l) and (player[l].TeamID = i) and (player[l].Floor >=0) then inc(c);
                    c:=random(c);

                    for l:=0 to srvcfg.MaxPlayer-1 do
                        if playerok(l) and (player[l].TeamID = i) and (player[l].Floor >=0) then begin

                        if c <= 0 then begin
                            s:=#$A4#$00#$00#$00#$08#$0B#$00#$00#$C3#$06#$01#$00#$00#$00#$00#$00
                                +#$06#$00#$a9#$11#$00#$00#$C1#$38#$00#$00#$6E#$3A#$33#$59#$13#$3E
                                +#$55#$57#$00#$00#$6F#$61#$5F#$6C#$69#$67#$68#$74#$6E#$69#$6E#$67
                                +#$5F#$73#$74#$72#$69#$6B#$65#$00#$6F#$6C#$6C#$69#$73#$69#$6F#$6E
                                +#$00#$6E#$67#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                                +#$00#$00#$00#$00#$00#$00#$30#$DD#$05#$00#$00#$00#$11#$00#$00#$00
                                +#$14#$03#$02#$00#$00#$02#$CB#$72#$03#$00#$00#$00#$00#$00#$00#$00
                                +#$00#$00#$00#$00#$00#$00#$88#$00#$40#$AD#$7D#$31#$2F#$00#$00#$00
                                +#$FF#$FF#$FF#$FF#$2C#$00#$00#$00#$01#$00#$00#$00#$2E#$00#$00#$00
                                +#$02#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00
                                +#$00#$00#$00#$00;
                            move(player[l].CurPos,s[$1d],8);
                            sendtofloor(player[l].room,player[l].Floor,s);
                            break;
                        end else dec(c);
                    end;
                end;
            end;
        end;



    //change meteo
    inc(meteo);
    if meteo > 120 then begin
    meteo:=0;
    for i:=0 to 99 do
        if team[i].used = 1 then begin
            team[i].cmeteo:=random(team[i].meteo.Count);
            SendMeteo(i);
        end;
    end;

    Pso2TimerTicks();
end;

procedure TForm1.Timer3Timer(Sender: TObject);
var i,x:integer;
    s:ansistring;
begin
    for x:=0 to 200 do
        if player[x].GID > 0 then begin
            //send data
            if length(player[x].dout)>0 then begin
                i:=player[x].Socket.SendText(player[x].dout);
                delete(player[x].dout,1,i);
            end else if player[x].todc = 1 then player[x].Socket.Close;

            //timer check
            if player[x].movetimer>0 then begin
                dec (player[x].movetimer);
                if player[x].movetimer = 0 then makeplayermove2(x);

            end;
        end;
    //player count
    x:=filecreate('c:\xampp\htdocs\pso2c.txt');
    s:=label8.Caption;
    if x > 0 then begin
        filewrite(x,s[1],length(s));
        fileclose(x);
    end;
end;

procedure TForm1.ClientSocket1Error(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
    errorcode:=0;
    socket.Close;
end;

procedure TForm1.ClientSocket1Connect(Sender: TObject;
  Socket: TCustomWinSocket);
var s:ansistring;
    i:integer;
begin
    s:=socket.RemoteAddress;
    i:=pos('.',s);
    ActiveIP[0]:=strtoint(copy(s,1,i-1));
    delete(s,1,i);
    i:=pos('.',s);
    ActiveIP[1]:=strtoint(copy(s,1,i-1));
    delete(s,1,i);
    i:=pos('.',s);
    ActiveIP[2]:=strtoint(copy(s,1,i-1));
    delete(s,1,i);

    ActiveIP[3]:=strtoint(s);

    socket.Close;
end;

procedure TForm1.Button5Click(Sender: TObject);
var f,i,y,c,o:integer;
    s:ansistring;
    D:pdword;
begin
    f:=fileopen( path+'log\'+combobox1.Text,$40);
    i:=fileseek(f,0,2);
    fileseek(f,0,0);
    setlength(logdata,i);
    fileread(f,logdata[1],i);
    fileclose(f);

    //make the list
    y:=1;
    c:=0;
    listbox1.Clear;
    listbox1.Items.BeginUpdate;
    while y <= i do begin
        move(logdata[y+4],f,4); //size
        ppos[c]:=y;
        s:='S ';
        if logdata[y] = #1 then s:='C ';
        s:=inttostr(c)+': '+s+inttohex(f,6)+', '+inttohex(byte(logdata[y+9]),2)+inttohex(byte(logdata[y+8]),2)+' '+inttohex(byte(logdata[y+10]),2)+'   ';


        if (logdata[y+8] = #$11) and (logdata[y+9] = #$13) then begin
            //get the player id used
            pid:='    ';
            move(logdata[y+$24],pid[1],4);
        end;

        if (logdata[y+8] = #$7) and (logdata[y+9] = #$0) then begin
            //get the player id used
            s:=s+'chat: '+pwidechar(@logdata[y+$20]);
        end else if (logdata[y+8] = #$8) and ((logdata[y+9] = #$b) or (logdata[y+9] = #$9)) then begin
            //get the player id used
            d:=@logdata[y+$c];
            s:=s+'obj: '+pansichar(@logdata[y+$28])+' '+inttohex(d^,2);
        end else if (logdata[y+8] = #$8) and ((logdata[y+9] = #$c) ) then begin
            //get the player id used
            d:=@logdata[y+$c];
            s:=s+'NPC: '+pansichar(@logdata[y+$28])+' '+inttohex(d^,2);
        end else if (logdata[y+8] = #$8) and (logdata[y+9] = #$d) then begin
            //get the player id used
            s:=s+'Monster: '+pansichar(@logdata[y+$28]);
        end  else if (logdata[y+8] = #$4) and (logdata[y+9] = #$15) then begin
            //get the player id used
            s:=s+'Action: '+pansichar(@logdata[y+$48]);
        end else if (logdata[y+8] = #$4) and (logdata[y+9] = #$14) then begin
            //get the player id used
            s:=s+'player Action: '+pansichar(@logdata[y+$48]);
        end else if (logdata[y+8] = #$4) and (logdata[y+9] = #$7) then begin
            //get the player id used
            {s:=s+'player id: '+inttohex(byte(logdata[y+$c]),2)+inttohex(byte(logdata[y+$d]),2)
                +inttohex(byte(logdata[y+$e]),2)+inttohex(byte(logdata[y+$f]),2)
                +inttohex(byte(logdata[y+$10]),2)+inttohex(byte(logdata[y+$11]),2);  }
            s:=s+'pm';
        end else begin
            o:=PacketDesc.IndexOf(inttohex(byte(logdata[y+9]),2)+inttohex(byte(logdata[y+8]),2));
            if o>-1 then s:=s+PacketDesc[o+1];

        end;


        listbox1.Items.Add(s);
        inc(c);
        inc(y,f+4);
    end;

    listbox1.Items.EndUpdate;

end;

procedure TForm1.ListBox1Click(Sender: TObject);
var i:integer;
    s:ansistring;
begin
    i:=listbox1.ItemIndex;
    if i>-1 then begin
        s:='doc\';
        s:=s+inttohex(byte(logdata[ppos[i]+9]),2)+inttohex(byte(logdata[ppos[i]+8]),2)+'.txt';
        memo2.Clear;
        if fileexists(path+s) then memo2.Lines.LoadFromFile(path+s);
    end;
end;

procedure TForm1.Button7Click(Sender: TObject);
var i,l:integer;
    s:ansistring;
begin
    i:=listbox1.ItemIndex;
    if i>-1 then begin
        move(logdata[ppos[i]+4],l,4);
        s:=copy(logdata,ppos[i]+4,l);
        sendtoplayer(0,s);
    end;
end;

procedure TForm1.Button6Click(Sender: TObject);
var i:integer;
    s:ansistring;
begin
    i:=listbox1.ItemIndex;
    if i>-1 then begin
        s:='doc\';
        s:=s+inttohex(byte(logdata[ppos[i]+9]),2)+inttohex(byte(logdata[ppos[i]+8]),2)+'.txt';
        memo2.Lines.SaveToFile(path+s);
    end;
end;

procedure TForm1.Button8Click(Sender: TObject);
var f,i:integer;
begin
    i:=listbox1.ItemIndex;
    if i>-1 then begin
        if savedialog1.Execute then begin
            f:=filecreate(savedialog1.FileName);
            filewrite(f,logdata[ppos[i]+4],pdword(@logdata[ppos[i]+4])^);
            fileclose(f);
        end;
    end;
end;

procedure TForm1.Button9Click(Sender: TObject);
var f,i:integer;
    s:ansistring;
begin
    i:=listbox1.ItemIndex;
    if i>-1 then begin
        if savedialog1.Execute then begin
            f:=filecreate(savedialog1.FileName);
            while i < listbox1.Count do begin
                if logdata[ppos[i]+8] = #8 then
                    if logdata[ppos[i]+9] <> #4 then filewrite(f,logdata[ppos[i]+4],pdword(@logdata[ppos[i]+4])^);

                if logdata[ppos[i]+8] = #$3 then
                    if logdata[ppos[i]+9] = #$23 then break;
                inc(i);
            end;
            fileclose(f);
        end;
    end;
end;

procedure TForm1.Button10Click(Sender: TObject);
var i,l:integer;
    s:ansistring;
begin
    i:=listbox1.ItemIndex;
    inc(i);
    listbox1.ItemIndex:=i;
    if i>-1 then begin
        if logdata[ppos[i]] = #0 then begin
            move(logdata[ppos[i]+4],l,4);
            s:=copy(logdata,ppos[i]+4,l);
            sendtoplayer(0,s);
        end;
    end;
end;

procedure TForm1.Button11Click(Sender: TObject);
var f,i:integer;
    s:ansistring;
begin
    i:=listbox1.ItemIndex;
    if i>-1 then begin
        if savedialog1.Execute then begin
            f:=filecreate(savedialog1.FileName);
            while i < listbox1.Count do begin
                if logdata[ppos[i]+8] = #4 then
                    if logdata[ppos[i]+9] = #$15 then filewrite(f,logdata[ppos[i]+4],pdword(@logdata[ppos[i]+4])^);


                inc(i);
            end;
            fileclose(f);
        end;
    end;
end;

procedure TForm1.Button12Click(Sender: TObject);
    var f,i,l,k:integer;
    s:ansistring;
begin
    i:=listbox1.ItemIndex;
    if i>-1 then begin
        s:='';
        move(logdata[ppos[i]+4],k,4);
        l:=-1;
        for f:=0 to k-1 do begin
            inc(l);
            if l = 16 then begin s:=s+#13#10#9'+'; l:=0; end;
            s:=s+'#$'+inttohex(byte(logdata[ppos[i]+4+f]),2);
        end;
        Clipboard.AsText:=s;
    end;
end;

procedure TForm1.Button13Click(Sender: TObject);
var x:integer;
    s:ansistring;
begin
    //19 08 = system message bottom

    s:=#$8C#$00#$00#$00#$19#$01#$04#$00+TextToPSO(edit2.Text)+ansichar(combobox2.ItemIndex)+#0#0#0;
    x:=length(s);
    move(x,s[1],4);
    for x:=0 to 200 do
    if player[x].GID > 0 then
        sendtoplayer(x,s);

end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
var i:integer;
begin
    //force disconnection + save
    for i:=0 to 199 do
        if player[i].GID > 0 then player[i].Socket.Close;
end;

procedure TForm1.Button14Click(Sender: TObject);
var f,i,l,k:integer;
begin
    i:=listbox1.ItemIndex;
    if i>-1 then begin
        move(logdata[ppos[i]+4],k,4);

        Clipboard.AsText:=DataToHex(copy(logdata,ppos[i]+4,k));
    end;
end;

procedure TForm1.Button15Click(Sender: TObject);
var HighBase,low,sub,re:byte;
    base:array[0..65535] of word;
    i,x,y:integer;
begin
    fillchar(base[0],65536*2,0);
    i:=0;
    while stringgrid1.Cells[0,i] <> '' do begin
        //find base
        y:=strtoint('$'+stringgrid1.Cells[0,i]);
        HighBase:=y shr 8;
        low:=y;
        re:=strtoint('$'+stringgrid1.Cells[1,i]);
        for y:=0 to 255 do
            for x:=0 to 255 do begin
                if (low xor x) - y = re then inc(base[(x shl 8)+y]);
            end;
        inc(i);
    end;
    for y:=0 to 65535 do
        if base[y] = i then memo3.Lines.add('Xor: '+inttohex((HighBase shl 8)+(y shr 8),4)+'  Sub: '+inttohex(y and 255,2));
end;

procedure TForm1.Button16Click(Sender: TObject);
var s:ansistring;
    x:integer;
begin
//0221
    x:=listbox1.itemindex+1;
    while x < listbox1.Count do
        if pos(edit3.Text,listbox1.Items[x])>0 then begin
            listbox1.ItemIndex:=x;
            break;
        end else inc(x);

end;

procedure SetClipboardText(const Text: Widestring);
var
  Count: Integer;
  Handle: HGLOBAL;
  Ptr: Pointer;
begin
  Count := (Length(Text)+1)*SizeOf(WideChar);
  Handle := GlobalAlloc(GMEM_MOVEABLE, Count);
  Try
    Win32Check(Handle<>0);
    Ptr := GlobalLock(Handle);
    Win32Check(Assigned(Ptr));
    Move(PWideChar(Text)^, Ptr^, Count);
    GlobalUnlock(Handle);
    Clipboard.SetAsHandle(CF_UNICODETEXT, Handle);
  Except
    GlobalFree(Handle);
    raise;
  End;
end;

procedure TForm1.Button17Click(Sender: TObject);
begin
    if listbox2.itemindex > -1 then
        SetClipboardText(items[listbox2.itemindex].name);
end;

procedure TForm1.Button18Click(Sender: TObject);
var i,f:integer;
    s:widestring;
begin
    s:=clipboard.AsText;
    if listbox2.itemindex > -1 then begin
        fillchar(items[listbox2.itemindex].name[0],64,0);
        move(s[1],items[listbox2.itemindex].name[0],length(s)*2);
        listbox2.Items[ listbox2.itemindex]:=s;

        f:=filecreate('data\items.dat');
        filewrite(f,items[0],sizeof(Items[0])*itemcount);
        fileclose(f);
    end;

end;

procedure TForm1.Button19Click(Sender: TObject);
var s:ansistring;
begin
    //HalfPrecisionToFloat($d292);
    {s:=#$48#$00#$00#$00#$04#$07#$70#$00#$7d#$71#$02#$00#$00#$10#$44#$5f
	+#$9c#$00#$00#$00#$00#$00#$04#$00#$00#$00#$44#$5f#$9c#$00#$00#$00
	+#$00#$00#$04#$00#$00#$00#$A4#$70#$FD#$3F#$00#$00#$00#$00#$00#$00
	+#$00#$3C#$B8#$22#$00#$00#$7F#$1A#$00#$00#$B8#$22#$99#$3C#$00#$49
	+#$00#$00#$FE#$FF#$FF#$FF#$9B#$BD;
    //move(testpos,s[$31],2);
    //move(lvlstats[0,testpos,1],s[$3d],4);
    sendtoplayer(0,s);        }
    //inc(testpos);
    //makeplayermove(0);
    LoadDropFile(0,'C:\pso2 server\server\quest\2A\drop1.txt');
end;

procedure TForm1.Button20Click(Sender: TObject);
var f,i,l,k,z:integer;
    s,a:ansistring;
begin
    i:=listbox1.ItemIndex;
    s:='';
    k:=0;
    if i>-1 then begin
        if savedialog1.Execute then begin
            f:=filecreate(savedialog1.FileName);
            while i < listbox1.Count do begin
                if logdata[ppos[i]+8] = #$b then
                    if logdata[ppos[i]+9] = #$9 then begin
                        move(logdata[ppos[i]+16],z,4);
                        k:=1;
                    end;
                if k = 1 then
                if logdata[ppos[i]+8] = #$b then
                    if logdata[ppos[i]+9] = #$13 then begin
                        a:=copy(logdata,ppos[i]+4+$24,$10);
                        if s<>'' then begin
                            for l:=1 to length(s) do
                                a[l]:=ansichar(byte(a[l]) xor byte(s[l]));
                        end;
                        filewrite(f,z,4);
                        filewrite(f,a[1],$10);
                        s:=copy(logdata,ppos[i]+4+$24,$10);
                        k:=0;
                    end;

                if logdata[ppos[i]+8] = #$3 then
                    if logdata[ppos[i]+9] = #$0 then break;
                inc(i);
            end;
            fileclose(f);
        end;
    end;
end;


procedure RepaintMap();
var x,i,c:integer;
    px,py:single;
    v:word;
begin
    form1.Image1.Canvas.Brush.Color:=$ffffff;
    form1.Image1.Canvas.FillRect(rect(0,0,420,420));
    x:=1;
    c:=0;
    while x < length(objmap) do begin
        move(objmap[x+$1c],v,2);
        px:=((HalfPrecisionToFloat(v)*1)+200);
        move(objmap[x+$20],v,2);
        py:=((HalfPrecisionToFloat(v)*1)+200);
        form1.Image1.Canvas.Brush.Color:=$0;
        if form1.ListBox3.ItemIndex = c then begin
            form1.Image1.Canvas.Brush.Color:=$ff00;
            form1.Image1.Canvas.FillRect(rect(round(px)-3,round(py)-3,round(px)+6,round(py)+6));
        end else
            form1.Image1.Canvas.FillRect(rect(round(px),round(py),round(px)+3,round(py)+3));
        move(objmap[x],i,4);
        inc(x,i);
        inc(c);
    end;
end;

procedure TForm1.Button21Click(Sender: TObject);
var x,i:integer;
begin
    if opendialog1.Execute then begin
    //objmap:=filegetpacket('map\lobby.obj');
    x:=fileopen(opendialog1.FileName,$40);
    i:=fileseek(x,0,2);
    fileseek(x,0,0);
    setlength(objmap,i);
    fileread(x,objmap[1],i);
    fileclose(x);
    RepaintMap();
    listbox3.Clear;
    x:=1;
    while x < length(objmap) do begin
        move(objmap[x+8],i,4);
        listbox3.Items.Add(pansichar(@objmap[x+$24]) +' - ' +inttohex(i,2));
        move(objmap[x],i,4);
        inc(x,i);
    end;
    end;
end;

procedure TForm1.ListBox3Click(Sender: TObject);
begin
    RepaintMap();
end;

procedure TForm1.Button22Click(Sender: TObject);
var s:ansistring;
    i:integer;
    d:pdword;
    w:pword;
    f:single;
begin
//0221
    i:=listbox1.itemindex;
    if i>-1 then begin
        if (logdata[ppos[i]+8] = #8) and (logdata[ppos[i]+9] = #$d) then begin
            //only if monster data
            d:=@logdata[ppos[i]+$c];
            s:=s+'id '+inttostr(d^);
            s:=s+ #13#10'type '+ pansichar(@logdata[ppos[i]+$28]);
            s:=s+#13#10'rotation ';
            s:=s+format('%.3f %.3f %.3f %.3f',[HalfPrecisionToFloat(pword(@logdata[ppos[i]+$18])^)
            ,HalfPrecisionToFloat(pword(@logdata[ppos[i]+$1a])^),HalfPrecisionToFloat(pword(@logdata[ppos[i]+$1c])^)
            ,HalfPrecisionToFloat(pword(@logdata[ppos[i]+$1e])^)]);
            s:=s+#13#10'position ';
            s:=s+format('%.3f %.3f %.3f %.3f',[HalfPrecisionToFloat(pword(@logdata[ppos[i]+$20])^)
            ,HalfPrecisionToFloat(pword(@logdata[ppos[i]+$22])^),HalfPrecisionToFloat(pword(@logdata[ppos[i]+$24])^)
            ,HalfPrecisionToFloat(pword(@logdata[ppos[i]+$26])^)]);
            s:=s+#13#10'area '+inttostr(byte(logdata[ppos[i]+$56+4]));
            s:=s+#13#10'apear '+inttostr(pword(@logdata[ppos[i]+$54+4])^);
            s:=s+#13#10'lvl '+inttostr(byte(logdata[ppos[i]+$50+4]));
            clipboard.AsText:=s;
        end;

    end;
end;

procedure TForm1.CheckBox1Click(Sender: TObject);
var s,a:ansistring;
    i,l,o:integer;
begin
    redalert := 0;
    if checkbox1.Checked then redalert:=1;

    s:=LobbyEventObj;
    i:=1;
    while i < length(s) do begin
        move(s[i],l,4);
        //form1.Memo1.Lines.Add('id: '+inttohex(byte(s[i+4]),2)+' '+inttohex(byte(s[i+5]),2)+' '+inttohex(l,2)+' '+pansichar(@s[i+$24]));
        if (s[i+4] = #8) and (s[i+5] = #$b) then
            if pansichar(@s[i+$24]) = 'oa_movie_object' then begin
                //make it no warning first
                {o:=0;
                //if warning then set it
                if redalert = 1 then o:=$3e8;
                move(o,s[i+$a0],2);

                a:=#$20#$00#$00#$00#$04#$06#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
                +#$04#$00#$00#$00#$B9#$00#$00#$00#$00#$00#$00#$00#$06#$00#$A9#$11;

                move(s[i+8],a[$15],12); //real id
                SendLobbyChar(room_lobby,-1,a);       }
                a:=#$54#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
                +#$04#$00#$00#$00#$AF#$03#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
                +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                +#$EB#$5C#$00#$00;
                move(s[i+8],a[$15],12); //item id
                if redalert = 1 then a:=a+'EmergencyStart'+#0#0
                else a:=a+'EmergencyStop'+#0#0#0 ;

                sendtolobby(room_lobby,-1,a);
            end;

        i:=i+l;
    end;  


end;

procedure TForm1.ServerSocket3Accept(Sender: TObject;
  Socket: TCustomWinSocket);
var s:ansistring;
    pk:^TShipEntry;
    i:integer;
    v:dword;
    a:widestring;
begin
    s:=#$50#$02#$00#$00#$11#$3d#$04#$00#$44#$e4#$00#$00;
    setlength(s,$250);
    v:=Round((now() - 25569) * 86400);
    move(v,s[$249],4);
    v:=1;
    move(v,s[$24d],4);
    for i:=1 to 11 do begin
        a:='Ship0'+inttostr(i)+#0#0;
        if i>9 then a:='Ship'+inttostr(i)+#0;
        pk:=@s[$d+((i-1)*$34)];
        pk.id:=i;
        pk.ip:=$0100007f;
        pk.empty:=0;
        pk.status:=1;
        pk.pos:=i;
        pk.unk:=$8e0a3;
        move(a[1],pk.name[0],14);
    end;
    socket.SendText(s);
    socket.close;
end;

procedure TForm1.ServerSocket3ClientError(Sender: TObject;
  Socket: TCustomWinSocket; ErrorEvent: TErrorEvent;
  var ErrorCode: Integer);
begin
    errorcode:=0;
    socket.Close;
end;

procedure TForm1.test5Click(Sender: TObject);
var f,i,c,t,l,o,k:integer;
    s:ansistring;
    sl:tstringlist;
    jp,en,dsc:array[0..255] of widechar;
begin
    f:=fileopen('translation_items.bin',$40);
    fileseek(f,4,0);
    fileread(f,t,4);
    sl:=tstringlist.Create;
    c:=0;
    i:=0;
    o:=0;
    while fileread(f,i,2) = 2 do begin

        if i = 0 then begin
            sl.Add('');
            if o = 0 then jp[0]:=#0;
            if o = 1 then en[0]:=#0;
            if o = 2 then dsc[0]:=#0;
        end else begin
            setlength(s,i);
            fileread(f,s[1],i);
            l:=1;
            while l < i do begin
                if o = 0 then pword(@s[l])^:=pword(@s[l])^ xor $1717;
                if o = 1 then pword(@s[l])^:=pword(@s[l])^ xor $4848;
                if o = 2 then pword(@s[l])^:=pword(@s[l])^ xor $5555;
                inc(l,2);
            end;


            s:=s+#0#0;
            if o = 0 then move(s[1],jp[0],length(s));
            if o = 1 then move(s[1],en[0],length(s));
            if o = 2 then move(s[1],dsc[0],length(s));
            sl.Add(pwidechar(@s[1]));
            inc(c);
        end;
        inc(o);
        if o = 3 then begin
            o:=0;

            for k:=0 to itemcount-1 do begin
                if compareuni(items[k].name , jp) then begin
                    move(en[0],items[k].name[0],48*2);
                    if dsc[0] <> #0 then  //keep the jp text
                        move(dsc[0],items[k].desc[0],256*2);
                    listbox2.Items[k]:=en;
                end;
            end;
        end;
    end;
    fileclose(f);


    f:=filecreate(path+'data\items.dat');
    filewrite(f,items[0],sizeof(Items[0])*itemcount);
    fileclose(f);

    sl.SaveToFile('ItemTrans.txt');
end;

procedure TForm1.Button23Click(Sender: TObject);
begin
    TestBF();
end;

procedure TForm1.Timer4Timer(Sender: TObject);
var x:integer;
begin
    for x:=0 to 13 do
         begin
            //casino tick
            CasinoTicks(x);
        end;

    for x:=0 to 300 do
      if timers[x].teamid > -1 then HandleTimer(x);
end;

procedure TForm1.Button24Click(Sender: TObject);
var v:word;
begin
    v:=strtoint('$'+edit4.Text);
    showmessage(floattostr(HalfPrecisionToFloat(v)));
end;

procedure TForm1.CheckBox2Click(Sender: TObject);
begin
    srvcfg.Ann:=0;
    if checkbox2.Checked then srvcfg.ann:=1;
end;

procedure TForm1.Button25Click(Sender: TObject);
var f:integer;
begin
    memo4.Lines.SaveToFile('data\announcement.txt');
    f:=filecreate(path+'server.cfg');
    filewrite(f,srvcfg,sizeof(srvcfg));
    fileclose(f);
end;

function compareuni(a,b:array of widechar):boolean;
var i:integer;
begin
    result:=true;
    i:=0;
    while a[i] <> #0 do begin
        if a[i] <> b[i] then result:=false;
        inc(i);
    end;
    if a[i] <> b[i] then result:=false;
end;

procedure TForm1.Button26Click(Sender: TObject);
var itm:array[0..2000] of TItemCVS;
    itmcount:integer;
    f,i:integer;
    w:widechar;
    s:widestring;
begin
    itmcount:=0;
    fillchar(itm[0],sizeof(TItemCVS)*2000,0);
    if opendialog1.Execute then begin
        f:=fileopen(opendialog1.Filename,$40);
        i:=fileread(f,w,2); //read 1 char
        while i > 0 do begin
            //read eatch entry
            while w <> '"' do begin
                i:=fileread(f,w,2); //find begining
                if i <= 0 then break;
            end;
            if i <= 0 then break;
            s:='';
            fileread(f,w,2);
            while w <> #10 do begin  //jp name
                if w = '"' then break;
                s:=s+w;
                fileread(f,w,2);
            end;
            if w <> '"' then fileread(f,w,2);
            move(s[1],itm[itmcount].jp[0],length(s)*2);
            s:='';

            while w <> '"' do begin  //en name
                s:=s+w;
                fileread(f,w,2);
            end;
            fileread(f,w,2);
            move(s[1],itm[itmcount].name[0],length(s)*2);
            s:='';

           { //skip 1 part
            while w <> '"' do fileread(f,w,2); //begining
            fileread(f,w,2);
            while w <> '"' do fileread(f,w,2); //end
            fileread(f,w,2);        }

            //description begin
            while w <> '"' do begin
                if w = #13 then break;
                fileread(f,w,2);
            end;
            if w <> #13 then begin
            fileread(f,w,2);
            while w <> '"' do begin  //en name
                s:=s+w;
                fileread(f,w,2);
            end;
            i:=fileread(f,w,2);
            move(s[1],itm[itmcount].desc[0],length(s)*2);
            end;
            inc(itmcount);
        end;
        fileclose(f);
    end;

    for f:=0 to itemcount-1 do begin
        for i:=0 to itmcount-1 do
        if compareuni(items[f].name , itm[i].jp) then begin
            move(itm[i].name[0],items[f].name[0],48*2);
            if itm[i].desc[0] <> #0 then  //keep the jp text
                move(itm[i].desc[0],items[f].desc[0],256*2);
            listbox2.Items[ f]:=itm[i].name;
        end;
    end;

    f:=filecreate(path+'data\items.dat');
    filewrite(f,items[0],sizeof(Items[0])*itemcount);
    fileclose(f);
end;

procedure TForm1.Button27Click(Sender: TObject);
var org:ansistring;
    new:ansistring;
    i,k,s:integer;
    id,id2:dword;
begin
    if opendialog1.Execute then begin
        new:=FileGetPacket('map\lobby.obj.old');
        org:=FileGetPacket('map\lobby.obj.default');

        i:=1;
        while i < length(org) do begin
            move(org[i+8],id,4);

            k:=1;
            while k < length(new) do begin
                move(new[k+8],id2,4);
                if (id = id2) then begin
                    move(new[k],s,4);
                    delete(new,k,s);
                    break;
                end;
                move(new[k],s,4);
                inc(k,s);
            end;

            move(org[i],s,4);
            inc(i,s);
        end;

        if savedialog1.Execute then begin
            i:=filecreate(savedialog1.FileName);
            filewrite(i,new[1],length(new));
            fileclose(i);
        end;
    end;
end;

procedure TForm1.ComboBox4Change(Sender: TObject);
begin
    if combobox4.ItemIndex = 2 then ChangeLobbyEvent('map\lobby.obj.calinight');
    if combobox4.ItemIndex = 1 then ChangeLobbyEvent('map\lobby.obj.godzilla');
    if combobox4.ItemIndex = 0 then ChangeLobbyEvent('map\lobby.obj.aqua');
    if combobox4.ItemIndex = 3 then ChangeLobbyEvent('map\lobby.obj.halloween');
    if combobox4.ItemIndex = 4 then ChangeLobbyEvent('map\lobby.obj.november');
    if combobox4.ItemIndex = 5 then ChangeLobbyEvent('map\lobby.obj.xmas');
    if combobox4.ItemIndex = 6 then ChangeLobbyEvent('map\lobby.obj.newyear');
    if combobox4.ItemIndex = 7 then ChangeLobbyEvent('map\lobby.obj.valentine');
    if combobox4.ItemIndex = 8 then ChangeLobbyEvent('map\lobby.obj.spring');
    if combobox4.ItemIndex = 9 then ChangeLobbyEvent('map\lobby.obj.easter');
    if combobox4.ItemIndex = 10 then ChangeLobbyEvent('map\lobby.obj.weding');
    if combobox4.ItemIndex = 11 then ChangeLobbyEvent('map\lobby.obj.juin');
end;

procedure TForm1.Button28Click(Sender: TObject);
var f,y,i:integer;
    s:ansistring;
begin
    y:=strtoint('$'+stringgrid1.Cells[0,0]);
    y:=y shr 8;
    s:=FileGetPacket('data\dump.exe');
    //search the value
    for i:=1 to length(s)-10 do
    if (s[i] = #$68) and //(s[i+1] = #$0) and (s[i+2] = #$0) and
    (s[i+5] = #$68) and (s[i+7] = ansichar(y)) and (s[i+8] = #0) and (s[i+9] = #0)
    then begin
        //retrive sub and xor!
        move(s[i+6],f,4);
        memo3.Lines.Add('XOR: '+inttohex(f,4)+'   SUB: '+inttohex(byte(s[i+1]),2));
    end;
end;

procedure TForm1.Button29Click(Sender: TObject);
var sl:tstringlist;
    i:integer;
begin
{sendtoplayer(0,#$48#$00#$00#$00#$0E#$06#$04#$00#$AB#$2E#$00#$00#$00#$00#$00#$00
	+#$0D#$00#$85#$03#$DC#$A6#$9B#$00#$00#$00#$00#$00#$04#$00#$00#$00
	+#$85#$EF#$00#$00#$43#$00#$65#$00#$72#$00#$65#$00#$7A#$00#$61#$00
	+#$00#$00#$00#$00#$85#$EF#$00#$00#$43#$00#$65#$00#$72#$00#$65#$00
	+#$7A#$00#$61#$00#$00#$00#$00#$00);      }
    sl:=tstringlist.Create;
    for i:=0 to listbox2.Count-1 do begin
        sl.Add(inttohex(items[i].ItemType,4)+' '
        +inttohex(items[i].ItemSub,4)+' '+inttohex(items[i].unk,4)+' '
        +inttohex(items[i].ItemEntry,4)+' : ' + listbox2.Items[i]);
    end;
    sl.Sort;
    clipboard.AsText:=sl.Text;
end;

procedure TForm1.ListBox2Click(Sender: TObject);
var i:integer;
begin
    i:=listbox2.ItemIndex;
    label15.Caption:=inttohex(items[i].ItemType,4)+' '
        +inttohex(items[i].ItemSub,4)+' '+inttohex(items[i].unk,4)+' '
        +inttohex(items[i].ItemEntry,4);
end;

procedure TForm1.Button30Click(Sender: TObject);
var s:ansistring;
begin
    s:=#$58#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
    +#$00#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$E9#$5C#$00#$00#$53#$74#$61#$72#$74#$28#$6C#$73#$5F#$30#$31#$30
    +#$30#$33#$30#$29#$00#$00#$00#$00;    //10 20 30 50 60 70

    sendtolobby(room_shop,-1,s);
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
    serversocket1.active:=false;
    serversocket2.active:=false;
    label7.Caption:='Stoped';
end;

procedure TForm1.Button31Click(Sender: TObject);
var x,c,i:integer;
begin
    x:=1;
    c:=0;
    while x < length(objmap) do begin

        move(objmap[x],i,4);
        if c = listbox3.ItemIndex then begin
            Clipboard.AsText:=DataToHex(copy(objmap,x,i));
            exit;
        end;
        inc(x,i);
        inc(c);
    end;

end;

procedure TForm1.Button32Click(Sender: TObject);
var f:integer;
begin
    srvcfg.gmpass:=edit5.Text;
    memo6.Lines.SaveToFile(path+'gm.txt');
    memo7.Lines.SaveToFile(path+'banlist.txt');
    f:=filecreate(path+'server.cfg');
    filewrite(f,srvcfg,sizeof(srvcfg));
    fileclose(f);
end;

procedure TForm1.Button33Click(Sender: TObject);
var f,i,l,k,c,v:integer;
    s,li:ansistring;
    st:tstringlist;
begin
    i:=listbox1.ItemIndex;
    v:=0;
    if i>-1 then begin
        if savedialog1.Execute then begin
            while i < listbox1.Count do begin
                //find the list packet
                if logdata[ppos[i]+8] = #$b then
                    if logdata[ppos[i]+9] = #$18 then begin
                        li:=copy(logdata,ppos[i]+4,pdword(@logdata[ppos[i]+4])^);
                    end;

                //find the start packet
                    //save the info.txt
                if logdata[ppos[i]+8] = #$e then
                    if logdata[ppos[i]+9] = #$31 then begin
                        s:=copy(logdata,ppos[i]+4,pdword(@logdata[ppos[i]+4])^);
                        st:=tstringlist.create;
                        st.Add('ID: '+inttostr(pdword(@s[9])^));
                        st.Add('//fixme floor and folder');
                        st.Add('Floor: 1');
                        st.Add('folder: arks');
                        st.Add('');
                        st.Add('//fixme reward');
                        st.Add('exp: 14');
                        st.Add('mesetas: 14');
                        st.Add('');

                        l:=13;
                        while l < length(li) do
                        if pdword(@s[9])^ = pdword(@li[l+$2c])^ then begin
                            st.Add('//icon');
                            st.Add('time '+inttostr(byte(li[l+$f6])));
                            st.Add('team '+inttostr(byte(li[l+$f7])));
                            st.Add('');
                            st.Add('//difficulty');
                            for k:=0 to 3 do
                                if byte(li[l+$f8]) and (1 shl k) <> 0 then begin
                                    //dificulty used
                                    st.Add('dificulty '+inttostr(k+1));
                                    c:=$221+(k*$24);
                                    st.Add('req_lvl '+inttostr(byte(s[c])));
                                    st.Add('monst_lvl '+inttostr(byte(s[c+2])));
                                    st.Add('item1 '+inttohex(byte(li[l+$114+(k*24)]),2)+' '+inttohex(byte(li[l+$115+(k*24)]),2)+' '
                                        +inttohex(byte(li[l+$116+(k*24)]),2)+' '+inttohex(byte(li[l+$117+(k*24)]),2)+' '
                                        +inttohex(byte(li[l+$118+(k*24)]),2)+' '+inttohex(byte(li[l+$119+(k*24)]),2)+' '
                                        +inttohex(byte(li[l+$11a+(k*24)]),2)+' '+inttohex(byte(li[l+$11b+(k*24)]),2) );

                                    st.Add('item2 '+inttohex(byte(li[l+$120+(k*24)]),2)+' '+inttohex(byte(li[l+$121+(k*24)]),2)+' '
                                        +inttohex(byte(li[l+$122+(k*24)]),2)+' '+inttohex(byte(li[l+$123+(k*24)]),2)+' '
                                        +inttohex(byte(li[l+$124+(k*24)]),2)+' '+inttohex(byte(li[l+$125+(k*24)]),2)+' '
                                        +inttohex(byte(li[l+$126+(k*24)]),2)+' '+inttohex(byte(li[l+$127+(k*24)]),2) );
                                    st.Add('monst1 '+inttostr(pdword(@s[$22d+(k*$24)])^)+' '+inttostr(pdword(@s[$231+(k*$24)])^));
                                    st.Add('monst2 '+inttostr(pdword(@s[$235+(k*$24)])^)+' '+inttostr(pdword(@s[$239+(k*$24)])^));
                                    st.Add('monst3 '+inttostr(pdword(@s[$23d+(k*$24)])^)+' '+inttostr(pdword(@s[$241+(k*$24)])^));
                                    st.Add('');
                                end;
                            break;
                        end else l:=l+472;

                        st.SaveToFile(extractfilepath(savedialog1.FileName)+'info.txt');
                        v:=1;
                    end;

                //find the campship
                    //save the floor0.map + .obj
                if v = 1 then begin
                    if logdata[ppos[i]+8] = #$3 then
                    if logdata[ppos[i]+9] = #$24 then begin
                        li:=copy(logdata,ppos[i]+4,pdword(@logdata[ppos[i]+4])^);
                        f:=filecreate(extractfilepath(savedialog1.FileName)+'floor0.map');
                        filewrite(f,li[1],length(li));
                        fileclose(f);
                        li:='';
                    end;

                    if logdata[ppos[i]+8] = #8 then
                        if logdata[ppos[i]+9] <> #4 then li:=li+copy(logdata,ppos[i]+4,pdword(@logdata[ppos[i]+4])^);

                    if logdata[ppos[i]+8] = #$3 then
                        if logdata[ppos[i]+9] = #$23 then begin
                            f:=filecreate(extractfilepath(savedialog1.FileName)+'floor0.obj');
                            filewrite(f,li[1],length(li));
                            fileclose(f);
                            break;
                        end;
                end;

                inc(i);
            end;
        end;
    end;
end;


procedure TForm1.Button34Click(Sender: TObject);
var f,i:integer;
    s:ansistring;
begin
    i:=listbox1.ItemIndex;
    if i>-1 then begin

            s:=format('%.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f',[HalfPrecisionToFloat(pword(@logdata[ppos[i]+$18])^) ,
            HalfPrecisionToFloat(pword(@logdata[ppos[i]+$1a])^),HalfPrecisionToFloat(pword(@logdata[ppos[i]+$1c])^)
            ,HalfPrecisionToFloat(pword(@logdata[ppos[i]+$1e])^),HalfPrecisionToFloat(pword(@logdata[ppos[i]+$20])^)
            ,HalfPrecisionToFloat(pword(@logdata[ppos[i]+$22])^),HalfPrecisionToFloat(pword(@logdata[ppos[i]+$24])^)
            ,HalfPrecisionToFloat(pword(@logdata[ppos[i]+$26])^)]);
            clipboard.AsText:=s;
 
    end;
end;

procedure TForm1.Button35Click(Sender: TObject);
var i,l:integer;
    s,a:ansistring;
begin
    {setlength(s,sizeof(tmag));

    s:=#$60#$00#$00#$00#$2A#$01#$00#$00#$42#$5B#$8A#$94#$42#$A2#$B9#$0D
	+#$01#$00#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$FF#$00#$00#$00#$00#$00#$03#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$01#$00#$00#$FF#$00#$00#$00#$FF#$00#$00#$00#$FF#$00#$00#$00
	+#$FF#$00#$00#$00#$FF#$00#$00#$00#$FF#$00#$00#$00#$FF#$00#$00#$00;
    sendtoplayer(0,s);
    
    s:=#$6C#$00#$00#$00#$2A#$07#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00#$42#$5B#$8A#$94#$42#$A2#$B9#$0D#$04#$00#$01#$00
	+#$87#$00#$33#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$41#$00#$55#$00#$01#$00#$00#$00
    +#$00#$00#$20#$05#$00#$01#$00#$00#$00#$00#$00#$00#$00#$01#$01#$0F
    +#$00#$01#$01#$0F#$00#$01#$01#$0F#$00#$01#$01#$0F#$00#$01#$01#$0F
    +#$FF#$00#$00#$0F#$FF#$00#$00#$0F#$FF#$00#$00#$0F ;
    //1d = mag type
    //1f = mag form
        {
         type 1
            form 1 = mag

         type 2
            0 = Lyra
            1 = Fornax
            2 = Lepus
            3 = Antila

         type 3
            0 = Libra
            1 = Delphinus
            2 = Cygnus
            3 = Caelum
            4 = Cepheus
            5 = Tucana
            6 = Carina
            7 = Monoceros
            8 = Orion
            9 = Apus
            10 = Corvus
            11 = Leo
            12 = Crux

        }
    //21 = striking lvl
    //23 = striking value
    //25 = ranged lvl
    //27 = ranged value
    //29 = tech
    //2b = tech lvl
    //2d = striking def lvl
    //2f = sdef value
    //31 = rdef lvl
    //33 =  rdef value
    //35 = tdef lvl
    //37 = tdef value
    //39 = dex lvl
    //3a = dex value
    //3d = equiped
    //3e = photon blast
    //3f = energy?
    //43 = auto action 1
    //44 = trigger count
    //46 = auto action 2?
    //4d = first trigger //0 = aviable
    //4e = something related to trigger
    //4f = A B or C
    //move(test,s[$1d],2);
    {s[$50]:=ansichar(test);
    sendtoplayer(0,s);

    button35.Caption:=inttohex(test,2);
    inc(test);

    {s:=#$24#$00#$00#$00#$0B#$10#$00#$00#$a9#$11#$00#$00#$00#$00#$00#$00  //unlock door
	+#$10#$00#$00#$00#$2D#$4F#$00#$00#$00#$00#$00#$00#$0D#$00#$53#$01
	+#$04#$00#$00#$00;
    move(player[0].partyid,s[$15],4);
    sendtoplayer(0,s);  }
    test:=byte(s[2312412]);

    s:=#$9C#$00#$00#$00#$08#$0B#$00#$00#$B4#$00#$00#$00#$00#$00#$00#$00
                +#$06#$00#$a9#$11#$00#$80#$E1#$B7#$00#$80#$F6#$3A#$00#$00#$00#$00
                +#$00#$00#$00#$00#$6F#$62#$5F#$39#$39#$30#$30#$5F#$30#$30#$32#$33
                +#$00#$6E#$00#$5F#$61#$73#$79#$6E#$63#$00#$6C#$69#$73#$69#$6F#$6E
                +#$00#$6E#$67#$00#$09#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                +#$00#$00#$00#$00#$00#$00#$00#$00#$04#$00#$00#$00#$0F#$00#$00#$00
                +#$14#$04#$00#$00#$00#$30#$04#$00#$02#$00#$00#$00#$00#$00#$00#$00
                +#$00#$00#$00#$00#$00#$00#$00#$00#$E0#$30#$A2#$30#$2E#$00#$00#$00
                +#$FF#$FF#$FF#$FF#$2B#$00#$00#$00#$01#$00#$00#$00#$2D#$00#$00#$00
                +#$04#$00#$00#$00#$34#$00#$00#$00#$00#$00#$00#$00;
    move(player[0].CurPos,s[$1d],8);
    a:=inttostr(test);
    if length(a) <> 2 then a:='0'+a;

    move(a[1],s[$2f],2);
    sendtoplayer(0,s);
    a:=#$44#$00#$00#$00#$0F#$04#$00#$00#$B4#$00#$00#$00#$00#$00#$00#$00
    +#$06#$00#$a9#$11#3#0#1#0#0#0#1#0#1#1#0#0#0#0#0#0
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$03#$00#$00#$00#$00#$00#$01#$00#$00#$00  //03
    +#$00#$00#$00#$00;
    sendtoplayer(0,a);

    button35.Caption:=inttohex(test,2);
    inc(test);
end;

procedure TForm1.Button36Click(Sender: TObject);
var f,i:integer;
    it:TItemData;
begin
    if opendialog1.Execute then begin
        f:=fileopen(opendialog1.FileName,$40);
        while fileread(f,it,sizeof(it)) > 0 do begin
            for i:=0 to itemcount-1 do
             if (items[i].ItemType = it.ItemType) and (items[i].ItemSub = it.ItemSub)
                and (items[i].unk = it.unk) and (items[i].ItemEntry = it.ItemEntry) then break;
             if i = itemcount then begin
                 move(it,items[itemcount],sizeof(it));
                 listbox2.Items.Add(it.name);
                 inc(itemcount);
             end else begin
                 //test selling price :o
                 if it.buyingprice <> items[i].buyingprice then
                    items[i].buyingprice:=it.buyingprice;
                 if it.sellingprice <> items[i].sellingprice then
                    items[i].sellingprice:=it.sellingprice;
             end;
        end;
    end;
end;

procedure TForm1.Button37Click(Sender: TObject);
begin
    memo8.Lines.SaveToFile(path+'data\tester.txt');
end;

procedure TForm1.Button38Click(Sender: TObject);
begin
    SendSkillBonus(0,strtoint(edit4.Text),1);
end;

procedure TForm1.Button39Click(Sender: TObject);
begin
    listbox4.itemindex:=listbox4.itemindex+1;
    SendSkillBonus(0,strtoint(listbox4.Items[listbox4.itemindex]),1);

end;

procedure TForm1.Button40Click(Sender: TObject);
begin
    listbox4.itemindex:=listbox4.itemindex+1;
    RemoveSkillBonus(0,strtoint(listbox4.Items[listbox4.itemindex]));
end;

procedure TForm1.Button41Click(Sender: TObject);
var i:integer;
begin
    listbox4.Clear;
    for i:=0 to 255 do
        listbox4.Items.Add(inttostr($2700+i));
end;

procedure TForm1.Button42Click(Sender: TObject);
var x,c,i:integer;
begin
    x:=1;
    c:=0;
    while x < length(objmap) do begin

        move(objmap[x],i,4);
        if c = listbox3.ItemIndex then begin
            sendtolobby(room_lobby,-1,copy(objmap,x,i));
            listbox3.ItemIndex:=listbox3.ItemIndex+1;
            exit;
        end;
        inc(x,i);
        inc(c);
    end;

end;

procedure TForm1.Button43Click(Sender: TObject);
var x,c,i:integer;
    a:ansistring;
begin
    x:=1;
    c:=0;
    while x < length(objmap) do begin
                move(objmap[x],i,4);
        if c = listbox3.ItemIndex then begin
            a:=#$20#$00#$00#$00#$04#$06#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
            +#$04#$00#$00#$00+copy(objmap,x+8,12);

            sendtolobby(room_lobby,-1,a);
        end;
        inc(x,i);
        inc(c);
    end;
    listbox3.ItemIndex:=listbox3.ItemIndex+1;
end;

procedure TForm1.Button44Click(Sender: TObject);
var x,c,i,l,p:integer;
    a:ansistring;
    st,op:tstringlist;
begin
    x:=1;
    c:=0;
    st:=tstringlist.Create;
    op:=tstringlist.Create;
    while x < length(objmap) do begin
        move(objmap[x],i,4);
        if objmap[x+5] = #$b then begin
            //object
            st.Add('object '+pansichar(@objmap[x+$24]));
            st.Add('id '+inttostr(pdword(@objmap[x+$8])^));
            st.Add(format('rotation %.3f %.3f %.3f %.3f',[HalfPrecisionToFloat(pword(@objmap[x+$14])^)
                ,HalfPrecisionToFloat(pword(@objmap[x+$16])^),HalfPrecisionToFloat(pword(@objmap[x+$18])^)
                ,HalfPrecisionToFloat(pword(@objmap[x+$1a])^)]));
            st.Add(format('position %.3f %.3f %.3f %.3f',[HalfPrecisionToFloat(pword(@objmap[x+$1c])^)
                ,HalfPrecisionToFloat(pword(@objmap[x+$1e])^),HalfPrecisionToFloat(pword(@objmap[x+$20])^)
                ,HalfPrecisionToFloat(pword(@objmap[x+$22])^)]));
            a:='';
            for l:=0 to $17 do
                a:=a+' '+inttohex(byte(objmap[x+$44+l]),2);
            st.Add('item_flag'+a);

            if byte(objmap[x+$5c]) > 0 then begin
              a:='';
              for l:=0 to byte(objmap[x+$60])-1 do
                  a:=a+' '+inttohex(byte(objmap[x+$68+l]),2);
              st.Add('byte_flag'+a);
              p:=byte(objmap[x+$60]);

              //do int param
              for l:=0 to byte(objmap[x+$61])-1 do begin
                  st.Add('OPT_INT_'+inttostr(pinteger(@objmap[x+$68+p])^)+' '+inttostr(pinteger(@objmap[x+$6c+p])^));
                  a:='OPT_INT_'+inttostr(pinteger(@objmap[x+$68+p])^)+' = '+inttostr(pinteger(@objmap[x+$68+p])^);
                  if op.IndexOf(a) = -1 then op.add(a);
                  inc(p,8);
              end;

              //do float param
              for l:=0 to byte(objmap[x+$62])-1 do begin
                  st.Add('OPT_FLOAT_'+inttostr(pinteger(@objmap[x+$68+p])^)+' '+floattostr(psingle(@objmap[x+$6c+p])^));
                  a:='OPT_FLOAT_'+inttostr(pinteger(@objmap[x+$68+p])^)+' = '+inttostr(pinteger(@objmap[x+$68+p])^);
                  if op.IndexOf(a) = -1 then op.add(a);
                  inc(p,8);
              end;

              //do id param
              for l:=0 to byte(objmap[x+$64])-1 do begin
                  st.Add('OPT_ID_'+inttostr(pinteger(@objmap[x+$68+p])^)+' '+inttostr(pinteger(@objmap[x+$6c+p])^));
                  a:='OPT_ID_'+inttostr(pinteger(@objmap[x+$68+p])^)+' = '+inttostr(pinteger(@objmap[x+$68+p])^);
                  if op.IndexOf(a) = -1 then op.add(a);
                  inc(p,16);
              end;

              //do vertex
              for l:=0 to byte(objmap[x+$63])-1 do begin
                a:='OPT_VERTEX_'+inttostr(pinteger(@objmap[x+$68+p])^)+format(' %.3f %.3f %.3f %.3f',[HalfPrecisionToFloat(pword(@objmap[x+$6c+p])^)
                ,HalfPrecisionToFloat(pword(@objmap[x+$6e+p])^),HalfPrecisionToFloat(pword(@objmap[x+$70+p])^)
                ,HalfPrecisionToFloat(pword(@objmap[x+$72+p])^)]);
                  st.Add(a);
                  a:='OPT_VERTEX_'+inttostr(pinteger(@objmap[x+$68+p])^)+' = '+inttostr(pinteger(@objmap[x+$68+p])^);
                  if op.IndexOf(a) = -1 then op.add(a);
                  inc(p,12);
              end;
            end;

            st.Add('');

        end;
        if objmap[x+5] = #$c then begin
            //NPC
            st.Add('npc '+pansichar(@objmap[x+$24]));
            st.Add('id '+inttostr(pdword(@objmap[x+$8])^));
            st.Add(format('rotation %.3f %.3f %.3f %.3f',[HalfPrecisionToFloat(pword(@objmap[x+$14])^)
                ,HalfPrecisionToFloat(pword(@objmap[x+$16])^),HalfPrecisionToFloat(pword(@objmap[x+$18])^)
                ,HalfPrecisionToFloat(pword(@objmap[x+$1a])^)]));
            st.Add(format('position %.3f %.3f %.3f %.3f',[HalfPrecisionToFloat(pword(@objmap[x+$1c])^)
                ,HalfPrecisionToFloat(pword(@objmap[x+$1e])^),HalfPrecisionToFloat(pword(@objmap[x+$20])^)
                ,HalfPrecisionToFloat(pword(@objmap[x+$22])^)]));

            st.Add('');
        end;
        if objmap[x+5] = #$5 then begin
            //special trigger
            st.Add('trigger '+pansichar(@objmap[x+$24]));
            st.Add('id '+inttostr(pdword(@objmap[x+$8])^));
            st.Add(format('rotation %.3f %.3f %.3f %.3f',[HalfPrecisionToFloat(pword(@objmap[x+$14])^)
                ,HalfPrecisionToFloat(pword(@objmap[x+$16])^),HalfPrecisionToFloat(pword(@objmap[x+$18])^)
                ,HalfPrecisionToFloat(pword(@objmap[x+$1a])^)]));
            st.Add(format('position %.3f %.3f %.3f %.3f',[HalfPrecisionToFloat(pword(@objmap[x+$1c])^)
                ,HalfPrecisionToFloat(pword(@objmap[x+$1e])^),HalfPrecisionToFloat(pword(@objmap[x+$20])^)
                ,HalfPrecisionToFloat(pword(@objmap[x+$22])^)]));
            a:='';
            for l:=0 to $13 do
                a:=a+' '+inttohex(byte(objmap[x+$44+l]),2);
            st.Add('item_flag'+a);
            st.Add('');
        end;
        if objmap[x+5] = #$9 then begin
            //special trigger
            st.Add('colision '+pansichar(@objmap[x+$24]));
            st.Add('id '+inttostr(pdword(@objmap[x+$8])^));
            st.Add(format('rotation %.3f %.3f %.3f %.3f',[HalfPrecisionToFloat(pword(@objmap[x+$14])^)
                ,HalfPrecisionToFloat(pword(@objmap[x+$16])^),HalfPrecisionToFloat(pword(@objmap[x+$18])^)
                ,HalfPrecisionToFloat(pword(@objmap[x+$1a])^)]));
            st.Add(format('position %.3f %.3f %.3f %.3f',[HalfPrecisionToFloat(pword(@objmap[x+$1c])^)
                ,HalfPrecisionToFloat(pword(@objmap[x+$1e])^),HalfPrecisionToFloat(pword(@objmap[x+$20])^)
                ,HalfPrecisionToFloat(pword(@objmap[x+$22])^)]));
            a:='';
            for l:=$44 to i-1 do
                a:=a+' '+inttohex(byte(objmap[x+l]),2);
            st.Add('byte_flag'+a);
            st.Add('');
        end;

        inc(x,i);
    end;

    //process id
    st.Insert(0,'');
    for l:=0 to op.Count-1 do
        st.Insert(0,'define '+op[l]);

    if savedialog1.Execute() then begin
        st.SaveToFile(savedialog1.FileName);

    end;

end;

procedure TForm1.Button45Click(Sender: TObject);
var f,i,l,k,z:integer;
    s,a:ansistring;
begin
    i:=listbox1.ItemIndex;
    s:='';
    k:=0;
    if i>-1 then begin
        if savedialog1.Execute then begin
            f:=filecreate(savedialog1.FileName);
            while i < listbox1.Count do begin
                if logdata[ppos[i]+8] = #$b then
                    if logdata[ppos[i]+9] = #$9 then begin
                        move(logdata[ppos[i]+16],z,4);
                        k:=1;
                    end;
                if k = 1 then
                if logdata[ppos[i]+8] = #$4 then
                    if logdata[ppos[i]+9] = #$2 then begin
                        a:=copy(logdata,ppos[i]+4+$20,$10);
                        filewrite(f,z,4);
                        filewrite(f,a[1],$10);
                        k:=0;
                    end;

                if logdata[ppos[i]+8] = #$3 then
                    if logdata[ppos[i]+9] = #$0 then break;
                inc(i);
            end;
            fileclose(f);
        end;
    end;
end;

procedure TForm1.Button46Click(Sender: TObject);
var ts:tstringlist;
    pl:TAccount;
    s:ansistring;
    x:integer;
begin
    if fileexists(path+'DB\'+edit6.Text) then raise exception.Create('Account name already taken');
    ts:=tstringlist.Create;
    ts.LoadFromFile(path+'db.txt');
    fillchar(pl,sizeof(TAccount),0);
    pl.GID:=strtoint(ts[0]);
    ts[0]:=inttostr(strtoint(ts[0])+1);
    s:=edit7.Text;
    move(s[1], pl.pass[0],length(s));
    x:=filecreate(path+'DB\'+edit6.Text);
    filewrite(x,pl,sizeof(taccount));
    fileclose(x);

    ts.SaveToFile(path+'db.txt');
    ts.Free;
end;

procedure TForm1.Button47Click(Sender: TObject);
var e:integer;
    a:string;
begin

    e:=123123123;
    activeip[e]:=0;

end;

end.

