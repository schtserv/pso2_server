unit UShop;

interface

uses windows,types,sysutils;

type
    TItemCode = record
        iType,sub,unk,entry:word;
    end;
    PItemCode = ^TItemCode;


procedure RequestShop(x,id:integer);
procedure BuyItemsFromShop(x:integer; s:ansistring);
procedure SellItemsToShop(x:integer;s:ansistring);
procedure CasinoOutfitShop(x:integer);
procedure ChangePetName(x:integer;s:ansistring);
procedure EditDressing(x:integer;s:ansistring);

implementation

uses UDB, UPlayer, UPsoUtil, fmain, UInventory, Uerror;

procedure EditDressing(x:integer;s:ansistring);
var a:ansistring;
begin
    //right now belive what has been sent
    move(s[$49], player[x].activeChar.base.apearance, 348);

    //confirm it
    setlength(a,$264);
    a:=#$80#$02#$00#$00#$11#$3F#$00#$00#$24#$10#$67#$05#$56#$5F#$9C#$00
    +#$F0#$87#$F0#$57#$0D#$00#$28#$23#$00#$00#$00#$00+a;
    move(player[x].activeChar.base.name[0],a[$1d],$264);
    move(player[x].gid,a[$d],4);
    move(player[x].activechar.charID,a[$9],4);

    sendtoplayer(x,a);
end;

procedure CasinoOutfitShop(x:integer);
var s,a:ansistring;
    i,l:integer;
begin
    s:=filegetpacket('data\shop_casino.dat');
    player[x].lastshop:=s;
    if length(s) > 0 then begin
        SendItemsName(x,byte(s[$b]),$40,@s[$d]);
        sendtoplayer(x,s);

        //must send the selling rate
        s:=#$EC#$02#$00#$00#$34#$0A#$04#$00+GetMagic(0,$2D99,$62)+GetMagic(player[x].activechar.BankCount,$2D99,$62);
        a:=#0#0#0#0#0#0#0#0#0#0#0#0;
        for i:=0 to player[x].activechar.BankCount-1 do begin
            move(player[x].activechar.personalbank[i].ItemType,a[1],4);
            move(player[x].activechar.personalbank[i].unk,a[5],4);
            l:=FindItem(player[x].activechar.personalbank[i].ItemType,player[x].activechar.personalbank[i].ItemSub,
                player[x].activechar.personalbank[i].unk, player[x].activechar.personalbank[i].ItemEntry);
            if l = -1 then l:=0
            else l:=items[l].sellingprice;
            move(l,a[9],4);
            s:=s+a;
        end;
        l:=length(s);
        move(l,s[1],4);
        sendtoplayer(x,s);
    end else begin
        sendtoplayer(x,makeerror(ERR_SHOPMISSING,'Shop data missing'));

    end;
end;

procedure RequestShop(x,id:integer);
var s,a:ansistring;
    i,l:integer;
begin
    s:=filegetpacket('data\shop_'+inttohex(id,2)+'.dat');
    player[x].lastshop:=s;
    if length(s) > 0 then begin
        SendItemsName(x,byte(s[$b]),$40,@s[$d]);
        sendtoplayer(x,s);

        //must send the selling rate
        s:=#$EC#$02#$00#$00#$34#$0A#$04#$00+GetMagic(0,$2D99,$62)+GetMagic(player[x].activechar.BankCount,$2D99,$62);
        a:=#0#0#0#0#0#0#0#0#0#0#0#0;
        for i:=0 to player[x].activechar.BankCount-1 do begin
            move(player[x].activechar.personalbank[i].ItemType,a[1],4);
            move(player[x].activechar.personalbank[i].unk,a[5],4);
            l:=FindItem(player[x].activechar.personalbank[i].ItemType,player[x].activechar.personalbank[i].ItemSub,
                player[x].activechar.personalbank[i].unk, player[x].activechar.personalbank[i].ItemEntry);
            if l = -1 then l:=0
            else l:=items[l].sellingprice;
            move(l,a[9],4);
            s:=s+a;
        end;
        l:=length(s);
        move(l,s[1],4);
        sendtoplayer(x,s);
    end else begin
        sendtoplayer(x,makeerror(ERR_SHOPMISSING,'Shop data missing'));

    end;

end;

procedure SellItemsToShop(x:integer;s:ansistring);
var i,l,m,k:integer;
    itm:PItemCode;
    cnt:pword;
    a,b:ansistring;
    id:dword;
begin
    i:=13;
    while i < length(s)-4 do begin
        //process every entry
        itm:=@s[i];
        cnt:=@s[i+16];
        l:=FindItem(itm.iType,itm.sub,itm.unk,itm.entry);
        if l = -1 then l:=0
        else l:=items[l].sellingprice;

        a:=#$5C#$00#$00#$00#$34#$05#$04#$00#$01#$00#$00#$00#$13#$09#$00#$00 //new mesetas int64
            +#$00#$00#$00#$00#$0E#$90#$00#$00#$3F#$46#$7F#$0E#$BF#$F2#$97#$04
            +#$03#$00#$03#$00#$00#$00#$68#$02#$00#$00#$01#$00#$00#$00#$00#$00
            +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$0D#$00#$00#$00#$32#$00#$00#$00#$01#$00#$00#$00;

        //find the item in bank
        for m:=0 to player[x].activechar.BankCount-1 do
            if (pdword(@s[i+8])^ = player[x].activeChar.PersonalBank[m].id1)
                and (pdword(@s[i+12])^ = player[x].activeChar.PersonalBank[m].id2) then begin
                    move(player[x].activeChar.PersonalBank[m],a[$19],$38);

                    k:=byte(a[$2b]);
                    if player[x].activeChar.PersonalBank[m].ItemType = 3 then begin
                        //a[$2b]:=chr(cnt^);
                        if cnt^ > k then begin
                            //error selling more than we have
                            sendtoplayer(x,makeerror(ERR_SHOPQTY,'Shop qty error'));
                            exit;
                        end;
                        dec(k,cnt^);

                    end else k:=0;
                    if removeFromStorage(x,pdword(@s[i+8])^,pdword(@s[i+12])^,cnt^,player[x].activechar.personalbank,@player[x].activechar.bankcount,50) <> 0 then begin
                        //error cant delete
                        sendtoplayer(x,makeerror(ERR_SHOPRMV,'Shop inventory error'));
                        exit;
                    end;
                    inc(player[x].activeChar.Mesetas,l);
                    move(player[x].activechar.mesetas,a[$d],4);
                    move(k,a[$51],2); //count
                    sendtoplayer(x,a);
                    break;
                end;

        inc(i,20);
    end;
end;


procedure BuyItemsFromShop(x:integer; s:ansistring);
var i,l,m,k:integer;
    itm:PItemCode;
    cnt:pword;
    a,b:ansistring;
    id:dword;
begin
    i:=13;
    while i < length(s)-4 do begin
        //process every entry
        itm:=@s[i];
        cnt:=@s[i+10];

        //find the item in shop
        l:=$15;
        while l<length(player[x].lastshop)-4 do
            if copy(s,i,8) = copy(player[x].lastshop,l,8) then break
            else l:=l+$40;

        if l>=length(player[x].lastshop)-4 then begin
            //shop error, item not found
            sendtoplayer(x,makeerror(ERR_ITMSHOPNOTFOUND,'Item not aviable in this shop'));
            exit;
        end;
        //remove the mesetas
        move(player[x].lastshop[l+$38],m,4); //single price
        m:=m*cnt^; //lot price
        if player[x].activeChar.Mesetas < m then begin
            sendtoplayer(x,makeerror(ERR_SHOPMESETAS,'Not enought mesetas...'));
            exit;
        end;
        dec(player[x].activeChar.Mesetas,m);
        UpdateMesetas(x);

        //non stacking item can also be purchased in qty
        k:=0;
        while k < cnt^ do begin

        //add it to inventory
        a:=#$58#$00#$00#$00#$34#$03#$04#$00#$01#$00#$00#$00#$7D#$08#$00#$00
        +#$00#$00#$00#$00#$3C#$85#$00#$00#$02#$48#$BB#$34#$B4#$5E#$B4#$04
        +#$03#$00#$01#$00#$00#$00#$01#$00#$00#$00#$01#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$01#$00#$01#$00#$3D#$85#$00#$00;

        //the count isnt right

        id:=CreateItemID(x);
        move(id,a[$19],4);
        move(player[x].gid,a[$1d],4);
        move(player[x].activechar.mesetas,a[$d],4);
        move(player[x].lastshop[l],a[$21],$30);

        if player[x].lastshop[l] = #9 then
            a[$2a]:=a[$3c]; //disk lvl

        if player[x].lastshop[l] = #3 then //stacking
            a[$2b] := ansichar(cnt^);

        a[$51] := ansichar(cnt^);

        l:=AddToStorage(x,@a[$19],player[x].activechar.personalbank,@player[x].activechar.bankcount,50);

        //if stacking find the id used
        if l = -1 then begin
            sendtoplayer(x,makeerror(ERR_SHOPINVENTORY,'Inventory error'));
            exit;
        end;

        //copy the final data
        move(player[x].activechar.personalbank[l],a[$19],$38);
        if a[$21] = #3 then begin//stacking
            a[$53] := #0; //a[$2b];
            if cnt^ = byte(a[$2b]) then a[$53]:=#1;
            k:=cnt^;
        end else begin
            inc(k);
            a[$53] := #1; //add it
            a[$51] := #1;  //qty
        end;



        //send the result
        b:=#$18#$00#$00#$00#$34#$09#$04#$00#$C3#$A7#$00#$00#$02#$48#$BB#$34
        +#$B4#$5E#$B4#$04#$1E#$00#$00#$00;
        move(a[$19],b[$d],8);

        sendtoplayer(x,b);

        sendtoplayer(x,a);
        end;

        i:=i+12;
    end;

end;


procedure ChangePetName(x:integer;s:ansistring);
var a:widestring;
    b:ansistring;
    i,l:integer;
    id:dword;
begin
    //test if 500 mesetas
    move(s[9],id,4);
    if player[x].activeChar.Mesetas >= 500 then begin
        for i:=0 to player[x].activechar.PetCount-1 do
        if player[x].activechar.Pets[i].PID = id then begin

            //confirm it
            a:='Rename'#9+inttostr(player[x].GID)+'lld'#9+player[x].activechar.base.name
                +#9+inttostr(id)+#9'1'#9+pwidechar(@s[13])+#9+player[x].activechar.Pets[i].name
                +#9'0'#0;
            if length(a) and 1 = 1 then a:=a+#0;
            setlength(b,length(a)*2);
            move(a[1],b[1],length(a)*2);
            b:=#0#0#0#0#$32#$01#$04#$00#$0A#$00#$00#$00+GetMagic(length(a),$FE99,$83) +b;
            l:=length(b);
            move(l,b[1],4);
            sendtolobby(player[x].room,-1,b);

            b:=#$2C#$00#$00#$00#$40#$05#$00#$00#$00#$00#$00#$00#$B2#$69#$64#$05
            +#$43#$00#$68#$00#$75#$00#$62#$00#$62#$00#$79#$00#$00#$00#$F1#$00
            +#$00#$00#$00#$00#$F9#$00#$00#$00#$00#$00#$00#$00;
            move(s[9],b[13],24);
            sendtolobby(player[x].room,-1,b);

            //change the name
            move(s[13],player[x].activechar.Pets[i].name[0],20);

            //update it
            UseWeaponPalette(x,player[x].activeChar.weppal);
        end;
        //remove mesetas
        dec(player[x].activeChar.Mesetas,500);
        //update client
        UpdateMesetas(x);
    end;

end;

end.
