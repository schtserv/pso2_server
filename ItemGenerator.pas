unit ItemGenerator;

interface
uses types,sysutils,classes,windows;

{const
    OPT_DISABLE_SELECT = OPT_INT_43 = $43;
    disable visibility = 3B
    opt_option = 3A 00 00 00  //for teleporter( 0 = transporter, 1 = campship, 2 = block select)

    000000: D4 00 00 00 08 0B 00 00 1D 02 00 00 00 00 00 00     �...............
000010: 06 00 00 00 00 00 00 00 00 00 00 3C 00 00 33 41     ...........<..3A
000020: E3 58 00 00 6F 61 5F 74 72 61 6E 73 66 65 72 5F     �X..oa_transfer_
000030: 6D 61 63 68 69 6E 65 00 00 75 66 75 00 72 00 6E     machine..ufu.r.n
000040: 00 00 00 00 11 00 00 00 00 00 00 00 00 00 00 00     ................  //11 , the 0x10 = display E to use
000050: 00 00 00 00 00 00 00 00 04 00 00 00 1D 00 00 00     ................ //1d = size of option in dword
000060: 14 0A 01 00 00 00 00 00 02 00 00 00 00 00 00 00     ................ //14 = header extra size, 0a = 10 int flag, 01 = 1 float flag 0 =  ?, 0 =0 ref id, 02 = active?
000070: 00 00 00 00 00 00 00 00 00 E6 59 0D 32 00 00 00     .........�Y.2...
000080: 01 00 00 00 2F 00 00 00 01 00 00 00 31 00 00 00     ..../.......1...
000090: 01 00 00 00 34 00 00 00 00 00 00 00 3B 00 00 00     ....4.......;...
0000A0: 01 00 00 00 43 00 00 00 00 00 00 00 3C 00 00 00     ....C.......<...
0000B0: FF FF FF FF 3A 00 00 00 00 00 00 00 3F 00 00 00     ����:.......?...
0000C0: 00 00 00 00 42 00 00 00 01 00 00 00 39 00 00 00     ....B.......9...
0000D0: 00 00 80 40     ..�@
}

type
    TItemParam = record
      name:ansistring;
      value:integer;
    end;

    TVerUpgrade = record
        from,oid,nid:integer;
    end;


function GenerateObjFile(fn:ansistring;t,l:integer):ansistring;
function GenerateObjData(st:tstringlist;fn:ansistring;t:integer=-1;lvl:integer=-1):ansistring;

const MaxVer = 5;
    VarUpg:array[1..4,0..39] of TVerUpgrade = (
    (
        (from:0; oid:60; nid:65;),
        (from:1; oid:54; nid:56;),
        (from:0; oid:64; nid: 69 ;),
        (from:1; oid:56; nid: 58;),
        (from:0; oid:52; nid: 57;),
        (from:0; oid:63; nid: 68;),
        (from:2; oid:0; nid: 0;),
        (from:2; oid:9; nid: 9;),
        (from:0; oid:48; nid: 51;),
        (from:0; oid:45; nid: 48;),
        (from:2; oid:2; nid: 2;),
        (from:1; oid:57; nid: 59;),
        (from:1; oid:55; nid: 57;),
        (from:0; oid:61; nid: 66;),
        (from:0; oid:58; nid: 63;),
        (from:0; oid:53; nid: 58;),
        (from:0; oid:55; nid: 60;),
        (from:0; oid:62; nid: 67;),
        (from:0; oid:54; nid: 59;),
        (from:0; oid:49; nid: 52;),
        (from:0; oid:46; nid: 49;),
        (from:0; oid:44; nid: 47;),
        (from:0; oid:47; nid: 50;),
        (from:0; oid:30; nid: 31;),
        (from:0; oid:43; nid: 46;),
        (from:0; oid:51; nid: 56;),
        (from:1; oid:0; nid: 0;),
        (from:1; oid:1; nid: 1;),
        (from:0; oid:59; nid: 64;),
        (from:0; oid:57; nid: 62;),
        (from:1; oid:43; nid: 45;),
        (from:1; oid:53; nid: 55;),
        (from:1; oid:42; nid: 44;),
        (from:0; oid:66; nid: 71;),
        (from:0; oid:65; nid: 70;),
        (from:0; oid:0; nid:0;),
        (from:0; oid:0; nid:0;),
        (from:0; oid:0; nid:0;),
        (from:0; oid:0; nid:0;),
        (from:0; oid:0; nid:0;)
     )  ,
     (
        (from:0; oid:65; nid:66;),
        (from:1; oid:56; nid:56;),
        (from:0; oid:69; nid: 70 ;),
        (from:1; oid:58; nid: 58;),
        (from:0; oid:57; nid: 58;),
        (from:0; oid:68; nid: 69;),
        (from:2; oid:0; nid: 0;),
        (from:2; oid:9; nid: 9;),
        (from:0; oid:51; nid: 52;),
        (from:0; oid:48; nid: 49;),
        (from:2; oid:2; nid: 2;),
        (from:1; oid:59; nid: 59;),
        (from:1; oid:57; nid: 57;),
        (from:0; oid:66; nid: 67;),
        (from:0; oid:63; nid: 64;),
        (from:0; oid:58; nid: 59;),
        (from:0; oid:60; nid: 61;),
        (from:0; oid:67; nid: 68;),
        (from:0; oid:59; nid: 60;),
        (from:0; oid:52; nid: 53;),
        (from:0; oid:49; nid: 50;),
        (from:0; oid:47; nid: 48;),
        (from:0; oid:50; nid: 51;),
        (from:0; oid:31; nid: 32;),
        (from:0; oid:46; nid: 47;),
        (from:0; oid:56; nid: 57;),
        (from:1; oid:0; nid: 0;),
        (from:1; oid:1; nid: 1;),
        (from:0; oid:64; nid: 65;),
        (from:0; oid:62; nid: 63;),
        (from:1; oid:45; nid: 45;),
        (from:1; oid:44; nid: 44;),
        (from:0; oid:71; nid: 72;),
        (from:0; oid:70; nid: 71;),
        (from:1; oid:55; nid: 55;),
        (from:0; oid:73; nid: 74;),
        (from:0; oid:74; nid:75;),
        (from:0; oid:0; nid:0;),
        (from:0; oid:0; nid:0;),
        (from:0; oid:0; nid:0;)
     ),
     (
        (from:0; oid:66; nid:66;),
        (from:1; oid:56; nid:59;),
        (from:0; oid:70; nid: 70 ;),
        (from:1; oid:58; nid: 61;),
        (from:0; oid:58; nid: 58;),
        (from:0; oid:69; nid: 69;),
        (from:2; oid:0; nid: 0;),
        (from:2; oid:9; nid: 9;),
        (from:0; oid:52; nid: 52;),
        (from:0; oid:49; nid: 49;),
        (from:2; oid:2; nid: 2;),
        (from:1; oid:59; nid: 62;),
        (from:1; oid:57; nid: 60;),
        (from:0; oid:67; nid: 67;),
        (from:0; oid:64; nid: 64;),
        (from:0; oid:59; nid: 59;),
        (from:0; oid:61; nid: 61;),
        (from:0; oid:68; nid: 68;),
        (from:0; oid:60; nid: 60;),
        (from:0; oid:53; nid: 53;),
        (from:0; oid:50; nid: 50;),
        (from:0; oid:48; nid: 48;),
        (from:0; oid:51; nid: 51;),
        (from:0; oid:32; nid: 32;),
        (from:0; oid:47; nid: 47;),
        (from:0; oid:57; nid: 57;),
        (from:1; oid:0; nid: 0;),  //??
        (from:1; oid:1; nid: 1;), //??
        (from:0; oid:65; nid: 65;),
        (from:0; oid:63; nid: 63;),
        (from:1; oid:45; nid: 48;),
        (from:1; oid:44; nid: 47;),
        (from:0; oid:72; nid: 72;),
        (from:0; oid:71; nid: 71;),
        (from:0; oid:74; nid: 74;),
        (from:1; oid:55; nid: 58;),
        (from:0; oid:75; nid:75;),
        (from:0; oid:0; nid:0;),
        (from:0; oid:0; nid:0;),
        (from:0; oid:0; nid:0;)
     ),
     (
        (from:0; oid:20; nid: 23;), //??
        (from:0; oid:21; nid: 24;), //??
        (from:0; oid:32; nid: 35;), //??
        (from:0; oid:47; nid: 50;), //??
        (from:0; oid:48; nid: 51;),
        (from:0; oid:49; nid: 52;),
        (from:0; oid:50; nid: 53;),
        (from:0; oid:51; nid: 54;),
        (from:0; oid:52; nid: 55;),
        (from:0; oid:53; nid: 56;),
        (from:0; oid:57; nid: 60;),
        (from:0; oid:58; nid: 61;),
        (from:0; oid:59; nid: 62;),
        (from:0; oid:60; nid: 63;),
        (from:0; oid:61; nid: 64;),
        (from:0; oid:63; nid: 66;),
        (from:0; oid:64; nid: 67;),
        (from:0; oid:65; nid: 68;),
        (from:0; oid:66; nid:69;),
        (from:0; oid:67; nid: 70;),
        (from:0; oid:68; nid: 71;),
        (from:0; oid:69; nid: 72;),
        (from:0; oid:70; nid: 73 ;),
        (from:0; oid:71; nid: 74;), //??
        (from:0; oid:72; nid: 75;), //??
        (from:0; oid:74; nid: 77;),
        (from:0; oid:75; nid: 78;),

        (from:1; oid:0; nid: 0;),
        (from:1; oid:1; nid: 1;),
        (from:1; oid:45; nid: 45;),
        (from:1; oid:47; nid: 47;),
        (from:1; oid:48; nid: 48;),
        (from:1; oid:58; nid: 58;),
        (from:1; oid:59; nid:59;),
        (from:1; oid:60; nid: 60;),
        (from:1; oid:61; nid: 61;),
        (from:1; oid:62; nid: 62;),

        (from:2; oid:0; nid: 0;),
        (from:2; oid:2; nid: 2;),
        (from:2; oid:9; nid: 9;)
     )
    );



implementation

uses UPsoUtil, UDebug, UQuestBoard;


function ConvertParamVersion(t,ver,id:integer):integer;
var i,c:integer;
begin
    result:=id;
    c:=0;
    if ver = maxver then exit;
    while ver < maxver do begin
        c:=0;
        for i:=0 to 39 do
            if (VarUpg[ver,i].from = t) and (VarUpg[ver,i].oid = result) then begin
              result:=VarUpg[ver,i].nid;
              c:=1;
              break;
            end;
        if c = 0 then begin
            debug(DBG_CRITICAL,'Unknow param id for conversion Type: '+inttostr(t)+' ID: '+inttostr(result)+' VER: '+inttostr(ver));
        end;
        inc(ver);
    end;

end;

function GenerateObjData(st:tstringlist;fn:ansistring;t:integer=-1;lvl:integer=-1):ansistring;
var ver:integer;
    x,i,y,l:integer;
    a,b,re:ansistring;
    ItemParams:array[0..100] of TItemParam;
    ItemParamCount:integer;
begin
    result:='';
    ItemParamCount:=0;
    re:='';
    for i:=0 to st.Count-1 do begin
        //process the data
        a:=st[i];
        if copy(a,1,4) = 'ver ' then begin
            delete(a,1,4);
            ver:=strtoint(a);
        end else
        if copy(a,1,7) = 'define ' then begin
            delete(a,1,7);
            x:=pos(' = ',a);
            ItemParams[ItemParamCount].name:=copy(a,1,x-1);
            delete(a,1,x+2);
            ItemParams[ItemParamCount].value:=strtoint(a);
            inc(ItemParamCount);
        end else
        if copy(a,1,7) = 'object ' then begin
            if re <> '' then begin
                x:=length(re);
                safemove(x,re,1,4);
                result:=result+re;
            end;
            re:=#$D4#$00#$00#$00#$08#$0B#$00#$00#$1B#$02#$00#$00#$00#$00#$00#$00
            +#$06#$00#$00#$00#$00#$00#$A8#$B9#$00#$00#$A8#$39#$38#$53#$00#$00
            +#$4D#$D6#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$00#$00#$00#$00#$11#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$00#$00#$00#$00#$00#$00#$00#$00#$04#$00#$00#$00#$00#$00#$00#$00;
            delete(a,1,7);
            move(a[1],re[$25],length(a));
        end else
        if copy(a,1,8) = 'trigger ' then begin
            if re <> '' then begin
                x:=length(re);
                safemove(x,re,1,4);
                result:=result+re;
            end;
            re:=#$58#$00#$00#$00#$08#$05#$00#$00#$1B#$02#$00#$00#$00#$00#$00#$00
            +#$06#$00#$00#$00#$00#$00#$A8#$B9#$00#$00#$A8#$39#$38#$53#$00#$00
            +#$4D#$D6#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$00#$00#$00#$00#$11#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$00#$00#$00#$00#$00#$00#$00#$00;
            delete(a,1,8);
            move(a[1],re[$25],length(a));
        end else
        if copy(a,1,4) = 'npc ' then begin
            if re <> '' then begin
                x:=length(re);
                safemove(x,re,1,4);
                result:=result+re;
            end;
            re:=#$7C#$00#$00#$00#$08#$0C#$04#$00#$97#$02#$00#$00#$00#$00#$00#$00
            +#$06#$00#$00#$00#$00#$00#$FF#$37#$00#$00#$ED#$3A#$60#$4A#$E6#$4B
            +#$EB#$59#$00#$00#$4E#$70#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$00#$00#$A0#$41#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$01#$00#$00#$00#$25#$9F#$00#$00#$00#$00#$00#$00;
            delete(a,1,4);
            move(a[1],re[$25],length(a));
        end else
        if copy(a,1,9) = 'colision ' then begin
            if re <> '' then begin
                x:=length(re);
                safemove(x,re,1,4);
                result:=result+re;
            end;
            re:=#$7C#$00#$00#$00#$08#$09#$00#$00#$97#$02#$00#$00#$00#$00#$00#$00
            +#$06#$00#$00#$00#$00#$00#$FF#$37#$00#$00#$ED#$3A#$60#$4A#$E6#$4B
            +#$EB#$59#$00#$00#$4E#$70#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$00#$00#$00#$00;
            delete(a,1,9);
            move(a[1],re[$25],length(a));
        end else
        if copy(a,1,12) = 'self_object ' then begin
            delete(a,1,12);
            if (t > -1) and (lvl>-1) then begin
                for x:=0 to 63 do
                  if team[t].ObjTrigger[lvl,x].id = 0 then begin
                      move(re[9],team[t].ObjTrigger[lvl,x].id,4); //copy id
                      move(re[$15],team[t].ObjTrigger[lvl,x].pos[0],16); //copy position
                      team[t].ObjTrigger[lvl,x].action:=1;
                      team[t].ObjTrigger[lvl,x].name:=a;
                      break;
                  end;
            end;
        end else
        if copy(a,1,3) = 'id ' then begin
            delete(a,1,3);
            x:=strtoint(a);
            safemove(x,re,9,4);
            if pos('\quest\',lowercase(fn))>0 then begin re[$13]:=#$a9; re[$14]:=#$11; end;
        end else
        if copy(a,1,9) = 'rotation ' then begin
            delete(a,1,9);
            x:=pos(' ',a);
            pword(@re[$15])^ := floattohalf(strtofloat(copy(a,1,x-1)));
            delete(a,1,x);
            x:=pos(' ',a);
            pword(@re[$17])^ := floattohalf(strtofloat(copy(a,1,x-1)));
            delete(a,1,x);
            x:=pos(' ',a);
            pword(@re[$19])^ := floattohalf(strtofloat(copy(a,1,x-1)));
            delete(a,1,x);
            pword(@re[$1b])^ := floattohalf(strtofloat(a));
        end else
        if copy(a,1,9) = 'position ' then begin
            delete(a,1,9);
            x:=pos(' ',a);
            pword(@re[$1d])^ := floattohalf(strtofloat(copy(a,1,x-1)));
            delete(a,1,x);
            x:=pos(' ',a);
            pword(@re[$1f])^ := floattohalf(strtofloat(copy(a,1,x-1)));
            delete(a,1,x);
            x:=pos(' ',a);
            pword(@re[$21])^ := floattohalf(strtofloat(copy(a,1,x-1)));
            delete(a,1,x);
            pword(@re[$23])^ := floattohalf(strtofloat(a));
        end else
        if copy(a,1,10) = 'item_flag ' then begin
            delete(a,1,10);
            x:=$45;
            while a<>'' do begin
                re[x]:=ansichar(strtoint('$'+copy(a,1,2)));
                delete(a,1,3);
                inc(x);
            end;
            if pansichar(@re[$25]) = 'oa_touch_screen_async' then pword(@re[$53])^ := $11a9;
        end else
        if copy(a,1,10) = 'byte_flag ' then begin
            delete(a,1,10);
            b:='';
            while a<>'' do begin
                b:=b+ansichar(strtoint('$'+copy(a,1,2)));
                delete(a,1,3);
            end;
            if re[6] = #9 then re:=re+b
            else begin
            re:=re+ansichar(length(b))+#0#0#0#0#0#0#0+b; //count space
            inc(pdword(@re[$5d])^,(length(b) div 4)+2);
            end;

            if pansichar(@re[$25]) = 'oa_transfer_multi' then pdword(@re[$79])^:=$40; //patch for christmas update

        end else
        if copy(a,1,7) = 'OPT_INT' then begin
            inc(pdword(@re[$5d])^,2);
            x:=pos(' ',a);
            for y:=0 to ItemParamCount-1 do
                if ItemParams[y].name = copy(a,1,x-1) then begin
                    b:=#0#0#0#0#0#0#0#0;
                    l:=ConvertParamVersion(0,ver,ItemParams[y].value);
                    move(l,b[1],4);
                    delete(a,1,x);
                    pinteger(@b[5])^:=strtoint(a);
                    re[$62]:=ansichar(byte(re[$62])+1);
                    re:=re+b;
                    break;
                end;
        end else
        if copy(a,1,9) = 'OPT_FLOAT' then begin
            inc(pdword(@re[$5d])^,2);
            x:=pos(' ',a);
            for y:=0 to ItemParamCount-1 do
                if ItemParams[y].name = copy(a,1,x-1) then begin
                    b:=#0#0#0#0#0#0#0#0;
                    l:=ConvertParamVersion(1,ver,ItemParams[y].value);
                    move(l,b[1],4);
                    delete(a,1,x);
                    psingle(@b[5])^:=strtofloat(a);
                    re[$63]:=ansichar(byte(re[$63])+1);
                    re:=re+b;
                    break;
                end;
        end else
        if copy(a,1,10) = 'OPT_VERTEX' then begin
            inc(pdword(@re[$5d])^,3);
            x:=pos(' ',a);
            for y:=0 to ItemParamCount-1 do
                if ItemParams[y].name = copy(a,1,x-1) then begin
                    b:=#0#0#0#0#0#0#0#0#0#0#0#0;
                    l:=ItemParams[y].value;
                    move(l,b[1],4);
                    delete(a,1,x);
                    l:=pos(' ',a);
                    pword(@b[5])^:=floattohalf(strtofloat(copy(a,1,l-1)));
                    delete(a,1,l);
                    l:=pos(' ',a);
                    pword(@b[7])^:=floattohalf(strtofloat(copy(a,1,l-1)));
                    delete(a,1,l);
                    l:=pos(' ',a);
                    pword(@b[9])^:=floattohalf(strtofloat(copy(a,1,l-1)));
                    delete(a,1,l);
                    pword(@b[11])^:=floattohalf(strtofloat(a));
                    re[$64]:=ansichar(byte(re[$64])+1);
                    re:=re+b;
                    break;
                end;
        end else
        if copy(a,1,6) = 'OPT_ID' then begin
            inc(pdword(@re[$5d])^,4);
            x:=pos(' ',a);
            for y:=0 to ItemParamCount-1 do
                if ItemParams[y].name = copy(a,1,x-1) then begin
                    b:=#0#0#0#0#0#0#0#0#0#0#0#0;
                    if pos('\quest\',lowercase(fn))>0 then b:=b+#6#0#$a9#$11
                    else b:=b+#6#0#0#0;
                    l:=ConvertParamVersion(2,ver,ItemParams[y].value);
                    move(l,b[1],4);
                    delete(a,1,x);
                    pdword(@b[5])^:=strtoint(a);
                    if pdword(@b[5])^ = 0 then pdword(@b[13])^:=0;
                    re[$65]:=ansichar(byte(re[$65])+1);
                    re:=re+b;
                    break;
                end;
        end;

    end;
    if re <> '' then begin
        x:=length(re);
        move(x,re[1],4);
        result:=result+re;
    end;
end;

function GenerateObjFile(fn:ansistring;t,l:integer):ansistring;
var st:tstringlist;
begin
    st:=tstringlist.Create;
    st.LoadFromFile(fn);
    result:=GenerateObjData(st,fn,t,l);
    st.Free;
end;

end.
