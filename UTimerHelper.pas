unit UTimerHelper;

interface
uses types,sysutils,windows,Generics.Collections;
type
    TTimerHelper = record
        player,gid:integer;
        tick,id:dword;
        sec:dword;
        action:integer;
    end;
    TTimerEvent = record
        teamid:integer;
        BeginTime:dword;
        Time,floor:integer;
        CallBack:ansistring;
    end;

procedure RegisterTimer(x,gid,sec,action,refid:integer);
procedure InitPso2Timer();
procedure RemoveTimer(x,gid,action:integer);
procedure Pso2TimerTicks();

//in game timer for script
procedure HandleTimer(x:integer);
procedure InitTimers();

var Pso2Timer:TList<TTimerHelper>;
    Timers:array[0..300] of TTimerEvent;

implementation

uses UPlayer, fmain, UTeamFnc, UQuestBoard, UForest, UScriptEngine;

procedure InitTimers();
var t:integer;
begin
    for t:=0 to 300 do
      timers[t].teamid:=-1;
end;

procedure HandleTimer(x:integer);
var i,l,k:integer;
begin
    if gettickcount()-timers[x].BeginTime >= timers[x].Time then begin
        //find a player in there if any

        for i:=0 to srvcfg.MaxPlayer-1 do
          if playerok(i) and (player[i].TeamID = timers[x].teamid) {and (player[i].Floor = timers[x].floor)} then
          begin
              //find callback
              for l:=0 to 127 do
                if team[player[i].TeamID].CallBacks[l].name = timers[x].CallBack then begin
                    //run the script
                    for k:=0 to team[player[i].TeamID].CallBacks[l].cmd.Count-1 do
                        ProcessQuestEvent(i,player[i].TeamID,team[player[i].TeamID].CallBacks[l].cmd[k]);
                end;

              break;
          end;
        timers[x].teamid:=-1;
    end;

end;

procedure RegisterTimer(x,gid,sec,action,refid:integer);
var mt:TTimerHelper;
begin
    mt.player:=x;
    mt.gid:=gid;
    mt.tick:=gettickcount();
    mt.sec:=sec;
    mt.action:=action;
    mt.id:=refid;
    Pso2Timer.Add(mt);
end;

procedure InitPso2Timer();
begin
    Pso2Timer:=TList<TTimerHelper>.create;
end;

procedure Pso2TimerAction1(idx,x:integer);
var s:ansistring;
begin
    //this section is for after 30 sec
      s:=#$50#$00#$00#$00#$04#$15#$44#$00#$7C#$79#$9B#$00#$00#$00#$00#$00
        +#$04#$00#$00#$00#$EA#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
        +#$00#$00#$00#$00#$7C#$79#$9B#$00#$00#$00#$00#$00#$04#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$EE#$5C#$00#$00#$57#$69#$64#$67#$65#$74#$43#$6C#$6F#$73#$65#$00;
      move(player[x].gid,s[9],4);
      move(player[x].gid,s[$25],4);
      pdword(@s[$15])^:= Pso2Timer[idx].id;

      sendtoparty(x,s);

      s:=#$50#$00#$00#$00#$04#$15#$44#$00#$7C#$79#$9B#$00#$00#$00#$00#$00
        +#$04#$00#$00#$00#$EA#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
        +#$00#$00#$00#$00#$EA#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$D0#$5C#$00#$00#$43#$6F#$75#$6E#$74#$44#$6F#$77#$6E#$00#$00#$00;
      move(player[x].gid,s[9],4);
      pdword(@s[$15])^:= Pso2Timer[idx].id;
      pdword(@s[$25])^:= Pso2Timer[idx].id;
      sendtoparty(x,s);

      s:=#$54#$00#$00#$00#$04#$15#$44#$00#$7C#$79#$9B#$00#$00#$00#$00#$00
        +#$04#$00#$00#$00#$EA#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
        +#$00#$00#$00#$00#$EA#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$EC#$5C#$00#$00#$4D#$75#$6C#$74#$69#$54#$72#$61#$6E#$73#$66#$65
        +#$72#$00#$00#$00;
      move(player[x].gid,s[9],4);
      pdword(@s[$15])^:= Pso2Timer[idx].id;
      pdword(@s[$25])^:= Pso2Timer[idx].id;
      sendtoparty(x,s);

      s:=#$48#$00#$00#$00#$1E#$06#$04#$00#$46#$C9#$00#$00#$75#$69#$5F#$6D
        +#$61#$69#$6E#$6D#$6F#$64#$65#$00#$46#$C9#$00#$00#$53#$79#$6E#$63
        +#$68#$72#$6F#$6E#$69#$7A#$65#$00#$38#$C9#$00#$00#$53#$79#$6E#$63
        +#$68#$72#$6F#$6E#$69#$7A#$65#$4D#$75#$6C#$74#$69#$42#$6F#$73#$73
        +#$54#$72#$61#$6E#$73#$00#$00#$00;
      sendtoparty(x,s);

      s:=#$54#$00#$00#$00#$04#$15#$44#$00#$7C#$79#$9B#$00#$00#$00#$00#$00
        +#$04#$00#$00#$00#$EA#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
        +#$00#$00#$00#$00#$7C#$79#$9B#$00#$00#$00#$00#$00#$04#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$EC#$5C#$00#$00#$54#$72#$61#$6E#$73#$66#$65#$72#$53#$74#$61#$72
        +#$74#$00#$00#$00;
      move(player[x].gid,s[9],4);
      move(player[x].gid,s[$25],4);
      pdword(@s[$15])^:= Pso2Timer[idx].id;
      sendtoparty(x,s);
      //player[x].inboss:=1;
end;

procedure Pso2TimerTicks();
var i:integer;
begin
    i:=0;
    while i < Pso2Timer.Count do begin
        if gettickcount()-Pso2Timer[i].tick >= Pso2Timer[i].sec*1000 then begin
            if player[Pso2Timer[i].player].GID = Pso2Timer[i].gid then begin
                //sanity check ok
                //process the action
                if Pso2Timer[i].action = 1 then Pso2TimerAction1(i,Pso2Timer[i].player);
            end;
            Pso2Timer.Delete(i);
        end else inc(i);
    end;

end;

procedure RemoveTimer(x,gid,action:integer);
var i:integer;
begin
    i:=0;
    while i < Pso2Timer.Count do begin
        if (Pso2Timer[i].player = x) and (Pso2Timer[i].gid = gid) and (Pso2Timer[i].action = action) then begin
            Pso2Timer.Delete(i);
            break;
        end else inc(i);
    end;
end;

end.
