unit UPlayer;

interface
uses types,sysutils,ScktComp;

type
    TFriend = record
        id,empty1:dword;
        playername:array[0..$21] of widechar;
        charname:array[0..17] of widechar;
        info1:array[0..4] of dword;
        unk:array[0..$3f] of byte;
        time1,time2:dword;
        unk1,unk2:dword;
    end;

    TInGameAction = record
        name:ansistring;
        action:integer;
        types,element:integer;
        mul:single;
        PA:integer;
    end;

    TInventoryItem = packed record
        id1,id2:dword;
        ItemType,ItemSub,unk,ItemEntry:word;
        unk2:word;
        count:byte;
        unk3:array[0..$24] of byte;
    end;
    PInventoryItem = ^TInventoryItem;

    TJobStats = record
        lvl,lvl2:word;
        exp:dword;
    end;
    THSVColor = packed record
        Hue,Saturation,Value:word;
        
    end;
    TPet = packed record
        PID,CharID:dword;
        name:array[0..19] of widechar;
        ItemType,ItemSub,unk,ItemEntry:word;
        unk3:array[0..$1b] of byte;
    end;
    TCharBase = packed record
        fill:array[0..2] of dword;
        name:array[0..17] of widechar;
        unk1,race,sex,unk2:word;
        apearance:array[0..$a7] of byte;
        bodycolor,outfitcolor,legcolor,armcolor:THSVColor;
        apearence2:array[0..$47] of byte;
        outfitid:word;
        ap3:array[0..$19] of byte;
        legid,armid:word;
        ap4:array[0..$43] of byte;
        job,subjob:byte;
        unk3:word;
        aviablejob:dword;
        jobstat:array[0..11] of TJobStats;
        unk4:array[0..$5b] of byte;
    end;

    TCandy = packed record
        Owner:dword;
        ItemType,ItemSub,unk,ItemEntry:word;
        x,y:byte;
        data:array[0..$39] of byte;
    end;
    tskills = record
        entry:dword;
        job,n3,pts,u1:byte;
        name:array[0..31] of widechar;
        lvl:array[0..$83] of byte;
    end;
    TCharData = record
        charID,GID:dword;
        base:TCharBase;//array[0..$267] of byte;
        playtime:dword; //play time in seconds
        Mesetas:int64;
        PersonalBank:array[0..99] of TInventoryItem;
        CharBank:array[0..199] of TInventoryItem;
        BankCount,CharBankCount:smallint;
        Palette:array[0..$4df] of byte; //both from 21 03
        Palette2:array[0..$8bf] of byte;
        weppal,subpal:integer;
        outfit:TInventoryItem;
        NextItemID,flagitem:dword;
        Cast_leg:TInventoryItem;
        Cast_arm:TInventoryItem;
        units,rings:array[0..2] of TInventoryItem;
        PALvl:array[0..$12f] of byte;
        PetCount:integer;
        Pets:array[0..19] of tpet;
        candycount:integer;
        candybox:array[0..1279] of TCandy;

        SkillListCount:integer;
        SkillList:array[0..19] of tskills;
        questflag:array[0..511] of byte; //eatch byte = 1 quest 8 dif
    end;
    TBlackList = record
        gid,e1:dword;
        name:array[0..31] of widechar;
        u1,u2,u3:dword;
    end;

    tmagstat = record
        lvl,pts:word;
    end;
    tmag = packed record
        id1,id2:dword;  //8
        cath,outfit:word;  //12
        satk,ratk,tatk,sdef,rdef,tdef,dex:tmagstat;  //40
        unk1,pb:byte;    //42
        energy:word;   //44
        unk2:word;       //46
        AutoAction1:byte;    //47
        TriggerCount:word;   //49
        AutoAction2:byte;    //50
        unk4:array[0..5] of byte;  //57
        triggers:array[0..7] of dword;
    end;
    tmaglist = record
        count,equiped:integer;
        mags:array[0..9] of TMag;
    end;
    tcliento = array[0..1023] of byte;
    TAccount = record
        pass:array[0..31] of ansichar;
        GID:dword;
        KeyConfig:array[0..$3fff] of ansichar;
        char:array[0..19] of TCharData;
        BasicStorage,OTPStorage,extraStorage:array[0..199] of TInventoryItem;
        BasicStorageCount,OTPStorageCount,extraStorageCount:integer;
        BankMesetas:int64;
        CasinoCoins:dword;
        LastTickets:dword;
        FriendCount:integer;
        FriendList:array[0..99] of TFriend;
        blacklistcount:integer;
        blacklist:array[0..99] of TBlackList;
        ArksName:array[0..31] of widechar;
        ArksCard:array[0..$793] of byte;
        Tutorial:array[0..$7ff] of byte;
        option:array[0..1] of dword;
        shortcut:array[0..$A4f] of byte;
        autoword:array[0..$696b] of byte;
        victorypose:array[0..7] of byte;

        teamid:dword;

        ClientOrder:array[0..19] of Tcliento;
        mags:array[0..19] of tmaglist;
        LastPlayTime:array[0..19] of dword;
    end;
    PAccount = ^TAccount;

    Tvec4 = record
        x,y,z,r:word;
    end;

    

    TPlayer = record
        Socket: TCustomWinSocket;
        GID:dword;
        din,dout:ansistring;
        aname,username:ansistring;
        cipher:integer;
        save:TAccount;
        Token:dword;
        todc:integer;
        activeChar:^TCharData;
        room:integer;
        Rotation:Tvec4;
        CurPos:Tvec4;
        CountTime:integer;
        LogedTime:dword;
        TeamID:integer;
        Floor:integer;
        FloorArea:array[0..15] of ansistring;
        firstping:integer;
        lastshop:ansistring;
        teammission:integer;
        hp,maxhp,petloaded:integer;
        questcompleted:integer;
        casinogame:integer;
        charloaded:boolean;
        isgm,gmunlocked:integer;
        ping:int64;
        pingticks:dword;
        movetimer:integer;
        status:integer;

        //new drop
        FloorObj,FloorItems:array[0..15] of ansistring;

        //in game
        lastaction,petaction:TInGameAction;
        pipeid:dword;
        pipefloor:integer;

        partyid:dword;

        //casino game status
        shooter_status:integer;
        shooterround:integer;

        //motion
        last81,lastarea1,lastarea2:ansistring;
        invitlist:ansistring;

        //pet
        pet_maxhp,pet_hp:array[0..19] of integer;
        pet_stunned:array[0..19] of dword;

        //boost
        ShiftaTick:dword;
        ShiftaBoost:integer;
        DebandTick:dword;
        DebandBoost:integer;
        statustime:dword;
        statuseffect:integer;
        statusefft:integer;
        cmap:ansistring;

        //client order
        ClientOrder:^tcliento;
        maglist:^tmaglist;
        magfeedtime:array[0..19] of dword;

        inboss:integer;
        //boss fail
        failfrom,failtype:ansistring;

        //tresure shop
        LastTS:ansistring;

        //team invitation
        TeamInvitation:ansistring;
    end;
    TLoginToken = record
        user:ansistring;
        token:dword;
    end;


    procedure initusers();
    function CreateToken(un:ansistring):dword;
    function GetUserFromToken(t:dword):ansistring;
    Function FindPlayer( Socket: TCustomWinSocket ):integer;
    Function GetPlayerSlot():integer;
    procedure SendToPlayer(x:integer;s:ansistring);
    procedure SendToLobby(Room,Excl:integer;s:ansistring);
    procedure SendLobbyChar2(Room,excl:integer;s:ansistring);
    procedure SendToParty(x:integer;s:ansistring);
    function PlayerOK(x:integer):boolean;
    function GetPlayerMaxHp(x:integer):integer;
    procedure SendToFloor(Room,fl:integer;s:ansistring;mob:integer=0);
    procedure ChangeStatus (x:integer;s:ansistring);
    procedure saveoption1(x:integer;s:ansistring);


Var
    player:array[0..200] of TPlayer;
    tokens:array[0..300] of TLoginToken;

Const
    Room_Lobby = 0;
    Room_Casino = 1;
    Room_Cafe = 2;
    Room_Bridge = 3;
    Room_Shop = 0;
    Room_CasinoGame = 4; {4 - 11 = shooter,}
    Room_Dressing = 12;
    Room_BattleLobby = 13;
    Room_ChalengeLobby = 14;
    Room_Team = $100;
    Room_PersonnalRoom = $200;

    Job_Hunter =0;
    Job_Ranger =1;
    Job_Force =2;
    Job_Fighter =3;
    Job_Gunner =4;
    Job_Techer =5;
    Job_Braver =6;
    Job_Bouncer =7;
    Job_Challenger =8;
    Job_Summoner =9;

    Race_Human = 0;
    Race_Newman = 1;
    Race_Cast = 2;
    Race_Dewman = 4;

    Status_Shock = 10020;

    BasePA : array[0..9] of ansistring = (
    //hunter 0
    #$00#$01#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00,

    //ranger 1
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$01#$00#$00#$00#$00#$00#$00#$01#$01#$00#$00#$00#$00#$00
	+#$01#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00,

    //force 2
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00,

    //fighter 3
    #$00#$01#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00,

    //gunner 4
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$01#$00#$00#$00#$00#$00#$00#$01#$01#$00#$00#$00#$00#$00
	+#$01#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00,

    //techer 5
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00,

    //braver 6
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$01#$01#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00,

    //bouncer 7
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$01#$01#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00,

    //challenger 8
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$01#$01#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00,

    //summoner 9
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    );

implementation

uses fmain, UEnc, UDebug, UStats, Uskills;

procedure initusers();
var x:integer;
begin
    for x:=0 to 200 do begin
        player[x].GID:=0; //gid 0 = free
    end;
    for x:=0 to 300 do tokens[x].user:='';

end;

procedure saveoption1(x:integer;s:ansistring);
begin
    move(s[9],player[x].save.option[0],8);
end;

procedure ChangeStatus (x:integer;s:ansistring);
var a:ansistring;
begin
    move(s[17],player[x].status,4);
    //confirm it to all
    a:=#$14#$00#$00#$00#$2C#$01#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#0#0#0#0;
    move(s[17],a[17],4);
    move(player[x].gid,a[9],4);
    sendtolobby(player[x].room,-1,a);
end;


function PlayerOK(x:integer):boolean;
begin
    result:=false;
    if x>srvcfg.MaxPlayer-1 then exit;
    if x < 0 then exit;
    if player[x].GID>0 then
        if player[x].charloaded then result:=true;
end;

function CreateToken(un:ansistring):dword;
var i:integer;
begin
    result:=0;

    while result = 0 do begin
        result:=random($7EFEFEFE) or $01010101;
        for i:=0 to 300 do
            if length(tokens[i].user) > 0 then
                if tokens[i].token = result then result:=0;
    end;
    //find a free token
    for i:=0 to 300 do
        if length(tokens[i].user) = 0 then begin
            tokens[i].user:=un;
            tokens[i].token:=result;
            break;
        end;

end;


function GetUserFromToken(t:dword):ansistring;
var i:integer;
begin
    result:='';
    for i:=0 to 300 do
        if tokens[i].token = t then begin
            result:=tokens[i].user;
            tokens[i].user:='';
        end;
end;


Function GetPlayerSlot():integer;
var i:integer;
begin
    result:=-1;
    for i:=0 to SrvCfg.MaxPlayer-1 do begin
        if player[i].GID = 0 then begin
            player[i].GID:=$ffffffff;
            player[i].charloaded:=false;
            player[i].CountTime:=0;
            player[i].firstping:=3;
            player[i].last81:='';
            player[i].invitlist:='';
            player[i].movetimer:=0;
            player[i].partyid:=0;
            player[i].status:=0;
            player[i].statuseffect:=0;
            player[i].TeamInvitation:='';
            result:=i;
            break;
        end;
    end;
end;

Function FindPlayer( Socket: TCustomWinSocket ):integer;
var i:integer;
begin
    result:=-1;
    for i:=0 to SrvCfg.MaxPlayer-1 do
        if (player[i].GID <> 0) then
            if player[i].Socket = socket then begin
                result:=i;
                break;
            end;

end;

procedure SendToPlayer(x:integer;s:ansistring);
begin
    if length(s) = 0 then exit;
    debug(DBG_log,'S: '+inttohex(byte(s[6]),2)+inttohex(byte(s[5]),2));
    writetolog('Server '+inttostr(x)+':',s);
    player[x].dout:=player[x].dout+PSOEnc(s,x);
end;

procedure SendToLobby(Room,Excl:integer;s:ansistring);
var x:integer;
begin
    for x:=0 to SrvCfg.MaxPlayer-1 do
        if (player[x].GID > 0) and (player[x].room = room) and (x<>excl) then begin
             sendtoplayer(x,s);
        end;
end;

procedure SendToFloor(Room,fl:integer;s:ansistring;mob:integer=0);
var x:integer;
begin
    for x:=0 to SrvCfg.MaxPlayer-1 do
        if (player[x].GID > 0) and (player[x].room = room) and (player[x].Floor = fl) then begin
             if (mob = 0) or (player[x].questcompleted = 0) then sendtoplayer(x,s);
        end;
end;

procedure SendToParty(x:integer;s:ansistring);
var i:integer;
begin
    for i:=0 to SrvCfg.MaxPlayer-1 do
        if (player[i].GID > 0) and (player[x].partyid = player[i].partyid) then begin
             sendtoplayer(i,s);
        end;
end;

procedure SendLobbyChar2(Room,excl:integer;s:ansistring);
var x:integer;
begin
    for x:=0 to SrvCfg.MaxPlayer-1 do
        if (player[x].GID > 0) and (player[x].room = room) and (x<>excl) then begin
            move(player[x].gid,s[9],4);
            sendtoplayer(x,s);
        end;
end;

function GetPlayerMaxHp(x:integer):integer;
var i,j,k:integer;
begin
    j:=player[x].activechar.base.job;
    result:= lvlstats[j,player[x].activeChar.base.jobstat[j].lvl,job_HP];
    i:= (player[x].activechar.base.race shl 1) or player[x].activechar.base.sex;
    result:=round(result*racemod[i,RM_HP]);

    //any armor with hp bonus?
    i:=FindArmorStat(pint64(@player[x].activeChar.outfit.ItemType)^);
    if i>-1 then inc(result,ArmorStats[i].hp);
    for k:=0 to 2 do begin
        i:=FindArmorStat(pint64(@player[x].activeChar.units[0])^);
        if i>-1 then  inc(result,ArmorStats[i].hp);
        i:=FindArmorStat(pint64(@player[x].activeChar.rings[0])^);
        if i>-1 then  inc(result,ArmorStats[i].hp);
    end;

    if j = 9 then begin
        //summoner
        inc(result,Skill_HPUp[ReadCurrentSkillValue(x,SU_HPUp)]);
        inc(result,Skill_HPUp[ReadCurrentSkillValue(x,SU_HPHighUp)]);
    end;
    if j = 0 then begin
        //hunter
        inc(result,Skill_HPUp1[ReadCurrentSkillValue(x,3)]);
        inc(result,Skill_4_75[ReadCurrentSkillValue(x,HU_HPUp2)]);
        inc(result,Skill_5_100[ReadCurrentSkillValue(x,HU_HPUp3)]);
    end;
    if j = 2 then begin
        //force
        inc(result,Skill_HPUp1[ReadCurrentSkillValue(x,FO_HPUp1)]);
    end;
    if j = 3 then begin
        //fighter
        inc(result,Skill_HPUp1[ReadCurrentSkillValue(x,FI_HPUp1)]);
        inc(result,Skill_HPUp1[ReadCurrentSkillValue(x,FI_HPUp2)]);
    end;
    if j = 4 then begin
        //gunner
        inc(result,Skill_HPUp1[ReadCurrentSkillValue(x,GU_HPUp1)]);
    end;


end;




end.
