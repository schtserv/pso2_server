unit UDebug;

interface
uses sysutils;

procedure debug(lvl:integer;s:ansistring);
procedure WriteToLog(from,data:ansistring);
function DataToHex(data:ansistring):ansistring;
procedure initLog;
procedure closeLog;

var LogFile:integer;

const DBG_WARNING = 1;
    DBG_ERROR = 2;
    DBG_CRITICAL = 3;
    DBG_LOG = 0;

implementation

uses fmain;

procedure debug(lvl:integer;s:ansistring);
var a:ansistring;
begin
    if lvl = DBG_WARNING then a:='[Warning] ';
    if lvl = DBG_ERROR then a:='[ERROR] ';
    if lvl = DBG_CRITICAL then a:='[CRITICAL]** ';
    if lvl = DBG_LOG then a:='[Log] ';
    if form1.ComboBox3.ItemIndex = 2 then form1.MemoDebug.Lines.Add(a+s);
    if (form1.ComboBox3.ItemIndex = 1) and (lvl >= DBG_ERROR) then form1.MemoDebug.Lines.Add(a+s);
end;


procedure initLog;
begin
    LogFile:=filecreate('log.txt');
end;

procedure closeLog;
begin
    fileclose(LogFile);
end;

procedure WriteToLog(from,data:ansistring);
var x,y:integer;
    a,b:ansistring;
const return:ansistring = #13#10;
begin
    if debugme = 0 then exit;
    filewrite(logfile,from[1],length(from));
    filewrite(logfile,return[1],2);
    filewrite(logfile,return[1],2);

    a:='000000: ';
    b:='';
    y:=0;
    for x:=1 to length(data) do begin
        if y>=16 then begin
            a:=a+'    '+b+#13#10;
            filewrite(logfile,a[1],length(a));
            y:=0;
            a:=inttohex(x-1,6)+': ';
            b:='';
        end;
        a:=a+inttohex(byte(data[x]),2)+' ';
        if byte(data[x]) < 32 then b:=b+'.'
        else b:=b+data[x];
        inc(y);
    end;
    a:=a+'    '+b+#13#10;
    filewrite(logfile,a[1],length(a));
    filewrite(logfile,return[1],2);
    filewrite(logfile,return[1],2);
end;

function DataToHex(data:ansistring):ansistring;
var x,y:integer;
    a,b:ansistring;
const return = #13#10;
begin

    result:='';
    a:='000000: ';
    b:='';
    y:=0;
    for x:=1 to length(data) do begin
        if y>=16 then begin
            a:=a+'    '+b+#13#10;
            result:=result+a;
            y:=0;
            a:=inttohex(x-1,6)+': ';
            b:='';
        end;
        a:=a+inttohex(byte(data[x]),2)+' ';
        if byte(data[x]) < 32 then b:=b+'.'
        else b:=b+data[x];
        inc(y);
    end;
    a:=a+'    '+b+#13#10;
    result:=result+a;
end;

end.
