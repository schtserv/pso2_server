unit UUseItem;

interface
uses windows,types,sysutils;

procedure PlayerUseItem(x:integer;itm:ansistring);
procedure ApplyBoostToPlayer(x:integer;s,b:ansistring);
function FindItemPos(x:integer;t,s,u,e:word):integer;
procedure ApplyBoostToMoonster(x,mid:integer;s,b:ansistring);
procedure ApplyBoostToNPC(x,nid:integer;s,b:ansistring);

implementation

uses UPlayer, UPsoUtil, UForest, UInventory, UStats, fmain, UQuestBoard;

function FindItemPos(x:integer;t,s,u,e:word):integer;
var i:integer;
begin
    result:=-1;

    for i:=0 to player[x].activechar.bankcount-1 do
    if (player[x].activeChar.PersonalBank[i].ItemType = t)
        and (player[x].activeChar.PersonalBank[i].ItemSub = s)
        and (player[x].activeChar.PersonalBank[i].unk = u)
        and (player[x].activeChar.PersonalBank[i].ItemEntry = e) then begin

            result:=i;
            break;
        end;
end;
//3 1 0 = monomate

procedure PlayerUseMate(x,v:integer);
var s:ansistring;
    p,l,j,k,i:integer;
begin
    p:=FindItempos(x,3,1,0,v);
    if (p>-1) then begin

        l:=player[x].hp;
        j:=player[x].activechar.base.job;
        {k:=lvlstats[j,player[x].activeChar.base.jobstat[j].lvl,job_HP];
        i:= (player[x].activechar.base.race shl 1) or player[x].activechar.base.sex;
        k:=round(k*racemod[i,RM_HP]);  //k = max hp   }
        k:=player[x].maxhp;

        i:=k;
        if v = 0 then i:=(i*30) div 100;
        if v = 1 then i:=(i*60) div 100;

        if player[x].hp + i > k then i:=k-player[x].hp;
        inc(player[x].hp,i);

        i:=0-i;

        s:=#$50#$00#$00#$00#$04#$52#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
        +#$04#$00#$00#$00#$C3#$00#$00#$00#$00#$00#$00#$00#$04#$00#$00#$00
        +#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00#$A3#$08#$69#$63
        +#$31#$00#$00#$00#$B3#$00#$00#$00#$01#$00#$00#$00#$8F#$4F#$4A#$B8
        +#$EF#$D5#$00#$00#$09#$00#$08#$20#$00#$00#$A0#$00#$00#$00#$00#$00;
        move(player[x].gid,s[$15],4);
        move(player[x].gid,s[$21],4);
        move(player[x].gid,s[$9],4);
        move(i,s[$31],4);  //damage
        move(player[x].hp,s[$35],4); //life
        move(player[x].CurPos,s[$3d],8);
        k:=FloatToHalf(HalfPrecisionToFloat(player[x].CurPos.y)+1.2);
        move(k,s[$3f],2);
        sendtolobby(player[x].room,-1,s);

        s:=#$1C#$00#$00#$00#$0F#$06#$04#$00#$F0#$AD#$00#$00#$35#$D7#$AD#$A1
        +#$B9#$28#$A6#$04#$02#$00#$01#$00#$02#$00#$00#$00;
        move(player[x].activeChar.PersonalBank[p].id1,s[13],4);
        move(player[x].activeChar.PersonalBank[p].id2,s[17],4);
        s[$15]:=ansichar(player[x].activeChar.PersonalBank[p].count-1);

        removeFromStorage(x,player[x].activeChar.PersonalBank[p].id1,player[x].activeChar.PersonalBank[p].id2,1
            ,player[x].activeChar.PersonalBank,@player[x].activechar.bankcount,50);
        sendtoplayer(x,s);
    end;
end;


procedure PlayerUseMoon(x:integer);
var s:ansistring;
    p,l,j,k,i:integer;
begin
    p:=FindItempos(x,3,1,0,$7);
    if (p>-1) then begin

        s:=#$1C#$00#$00#$00#$0F#$06#$04#$00#$F0#$AD#$00#$00#$35#$D7#$AD#$A1
        +#$B9#$28#$A6#$04#$02#$00#$01#$00#$02#$00#$00#$00;
        move(player[x].activeChar.PersonalBank[p].id1,s[13],4);
        move(player[x].activeChar.PersonalBank[p].id2,s[17],4);
        s[$15]:=ansichar(player[x].activeChar.PersonalBank[p].count-1);

        removeFromStorage(x,player[x].activeChar.PersonalBank[p].id1,player[x].activeChar.PersonalBank[p].id2,1
            ,player[x].activeChar.PersonalBank,@player[x].activechar.bankcount,50);
        sendtoplayer(x,s);

        s:=#$48#$00#$00#$00#$04#$2D#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
          +#$04#$00#$00#$00#$B1#$E2#$CF#$00#$00#$00#$00#$00#$04#$00#$00#$00
          +#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00#$00#$00#$00#$00
          +#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$62#$00#$00#$00
          +#$00#$00#$00#$00#$04#$00#$00#$00;
        move(player[x].gid,s[9],4);
        move(player[x].gid,s[$21],4);

        //find dead player around
        for i:=0 to srvcfg.MaxPlayer-1 do
        if playerok(i) and (player[i].room = player[x].room) and (player[i].Floor = player[x].Floor)
            and (player[i].hp = 0) then begin
            if round(sqrt(sqr(HalfPrecisionToFloat(player[x].CurPos.x) - HalfPrecisionToFloat(player[i].CurPos.x)) +
                sqr(HalfPrecisionToFloat(player[x].CurPos.y) - HalfPrecisionToFloat(player[i].CurPos.y)))) < 15 then begin
                move(player[i].gid,s[$15],4);
                player[i].hp:=player[i].maxhp;  //resurected
                move(player[i].hp,s[$3d],4);
                sendtolobby(player[x].room,-1,s);
            end;
        end;

    end;
end;


procedure PlayerShiftaRideS(x:integer);
var s:ansistring;
    p,l,j,k,i:integer;
begin
    p:=FindItempos(x,3,1,0,$b);
    if (p>-1) then begin

        s:=#$1C#$00#$00#$00#$0F#$06#$04#$00#$F0#$AD#$00#$00#$35#$D7#$AD#$A1
        +#$B9#$28#$A6#$04#$02#$00#$01#$00#$02#$00#$00#$00;
        move(player[x].activeChar.PersonalBank[p].id1,s[13],4);
        move(player[x].activeChar.PersonalBank[p].id2,s[17],4);
        s[$15]:=ansichar(player[x].activeChar.PersonalBank[p].count-1);

        removeFromStorage(x,player[x].activeChar.PersonalBank[p].id1,player[x].activeChar.PersonalBank[p].id2,1
            ,player[x].activeChar.PersonalBank,@player[x].activechar.bankcount,50);
        sendtoplayer(x,s);

        s:=#$50#$00#$00#$00#$06#$02#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
          +#$04#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
          +#$6D#$BF#$08#$6B#$00#$00#$00#$00#$00#$00#$00#$00#$FF#$FF#$FF#$FF
          +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
          +#$24#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;
        move(player[x].gid,s[9],4);
        move(player[x].gid,s[$15],4);
        sendtoplayer(x,s);

    end;
end;

procedure PlayerDebandRideS(x:integer);
var s:ansistring;
    p,l,j,k,i:integer;
begin
    p:=FindItempos(x,3,1,0,$c);
    if (p>-1) then begin

        s:=#$1C#$00#$00#$00#$0F#$06#$04#$00#$F0#$AD#$00#$00#$35#$D7#$AD#$A1
        +#$B9#$28#$A6#$04#$02#$00#$01#$00#$02#$00#$00#$00;
        move(player[x].activeChar.PersonalBank[p].id1,s[13],4);
        move(player[x].activeChar.PersonalBank[p].id2,s[17],4);
        s[$15]:=ansichar(player[x].activeChar.PersonalBank[p].count-1);

        removeFromStorage(x,player[x].activeChar.PersonalBank[p].id1,player[x].activeChar.PersonalBank[p].id2,1
            ,player[x].activeChar.PersonalBank,@player[x].activechar.bankcount,50);
        sendtoplayer(x,s);

        s:=#$50#$00#$00#$00#$06#$02#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
          +#$04#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
          +#$FE#$8F#$F5#$31#$00#$00#$00#$00#$00#$00#$00#$00#$FF#$FF#$FF#$FF
          +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
          +#$24#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;
        move(player[x].gid,s[9],4);
        move(player[x].gid,s[$15],4);
        sendtoplayer(x,s);

    end;
end;

procedure PlayerUsePipe(x:integer);
var s:ansistring;
    w:word;
    p:integer;
begin
    p:=FindItempos(x,3,7,0,0);
    if (p>-1) then begin
        s:=#$30#$00#$00#$00#$0B#$24#$00#$00#$a9#$11#$00#$00#$00#$00#$00#$00
        +#$10#$00#$00#$00#$f8#$0c#$00#$00#$00#$00#$00#$00#$0D#$00#$53#$01      //party id
        +#$B8#$00#$00#$00#$02#$00#$00#$00#$06#$00#$a9#$11#$00#$00#$00#$00;
        move(player[x].gid,s[$21],4);
        move(player[x].partyid,s[$15],4);
        //move(player[x].activeChar.charID,s[$1d],4);
        sendtolobby(player[x].room,-1,s);

        s:=#$B4#$00#$00#$00#$08#$0B#$00#$00#$B8#$00#$00#$00#$02#$00#$00#$00
        +#$06#$00#$a9#$11#$00#$00#$1F#$36#$00#$00#$64#$BB#$FB#$56#$CC#$3E
        +#$AB#$49#$00#$00#$6F#$61#$5F#$74#$65#$6C#$65#$70#$69#$70#$65#$5F
        +#$69#$74#$65#$6D#$00#$73#$79#$6E#$63#$00#$6C#$69#$73#$69#$6F#$6E
        +#$00#$69#$70#$65#$10#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$15#$00#$00#$00
        +#$14#$05#$00#$00#$01#$00#$00#$00#$02#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$08#$00#$00#$00#$00#$00#$2F#$00#$00#$00
        +#$FF#$FF#$FF#$FF#$2C#$00#$00#$00#$01#$00#$00#$00#$2E#$00#$00#$00
        +#$02#$00#$00#$00#$35#$00#$00#$00#$00#$00#$00#$00#$36#$00#$00#$00
        +#$00#$00#$00#$00#$02#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
        +#$04#$00#$00#$00;
        move(player[x].gid,s[$9],4);
        move(player[x].gid,s[$a9],4);
        move(player[x].Rotation,s[$15],8);
        move(player[x].CurPos,s[$1d],8);
        AddObjectToFloor(player[x].teamid,player[x].Floor,s);
        //sendtolobby(player[x].room,-1,s);

        s:=#$50#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
        +#$04#$00#$00#$00#$B8#$00#$00#$00#$02#$00#$00#$00#$06#$00#$a9#$11
        +#$00#$00#$00#$00#$f8#$0c#$00#$00#$00#$00#$00#$00#$0D#$00#$53#$01
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$EF#$5C#$00#$00#$53#$65#$74#$50#$61#$72#$74#$79#$49#$64#$00#$00;
        move(player[x].gid,s[$9],4);
        move(player[x].gid,s[$15],4);
        move(player[x].partyid,s[$25],4);
        //move(player[x].activeChar.charID,s[$2d],4);
        sendtolobby(player[x].room,-1,s);

        s:=#$1C#$00#$00#$00#$0F#$06#$04#$00#$F0#$AD#$00#$00#$35#$D7#$AD#$A1
        +#$B9#$28#$A6#$04#$02#$00#$01#$00#$02#$00#$00#$00;
        move(player[x].activeChar.PersonalBank[p].id1,s[13],4);
        move(player[x].activeChar.PersonalBank[p].id2,s[17],4);
        s[$15]:=ansichar(player[x].activeChar.PersonalBank[p].count-1);

        removeFromStorage(x,player[x].activeChar.PersonalBank[p].id1,player[x].activeChar.PersonalBank[p].id2,1
            ,player[x].activeChar.PersonalBank,@player[x].activechar.bankcount,50);
        sendtoplayer(x,s);
    end;
end;


procedure PlayerUseStar(x:integer);
var s:ansistring;
    p,l,j,k,i:integer;
begin
    p:=FindItempos(x,3,1,0,8);
    if (p>-1) then begin

        l:=player[x].hp;
        j:=player[x].activechar.base.job;
        {k:=lvlstats[j,player[x].activeChar.base.jobstat[j].lvl,job_HP];
        i:= (player[x].activechar.base.race shl 1) or player[x].activechar.base.sex;
        k:=round(k*racemod[i,RM_HP]);  //k = max hp   }


        //animation
        s:=#$50#$00#$00#$00#$06#$02#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
        +#$04#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
        +#$47#$77#$8C#$9B#$00#$00#$00#$00#$00#$00#$00#$00#$FF#$FF#$FF#$FF
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$24#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;

        move(player[x].gid,s[$15],4);
        move(player[x].gid,s[$9],4);
        sendtolobby(player[x].room,-1,s);

        //find dead player around
        for i:=0 to srvcfg.MaxPlayer-1 do
        if playerok(i) and (player[i].room = player[x].room) and (player[i].Floor = player[x].Floor) then begin
            if round(sqrt(sqr(HalfPrecisionToFloat(player[x].CurPos.x) - HalfPrecisionToFloat(player[i].CurPos.x)) +
                sqr(HalfPrecisionToFloat(player[x].CurPos.y) - HalfPrecisionToFloat(player[i].CurPos.y)))) < 15 then begin
                k:=player[i].maxhp;
                l:=k div 2;
                if player[i].hp + l > k then l:=k-player[i].hp;
                inc(player[i].hp,l);

                l:=0-l;

                s:=#$50#$00#$00#$00#$04#$52#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
                +#$04#$00#$00#$00#$C3#$00#$00#$00#$00#$00#$00#$00#$04#$00#$00#$00
                +#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00#$A3#$08#$69#$63
                +#$31#$00#$00#$00#$B3#$00#$00#$00#$01#$00#$00#$00#$8F#$4F#$4A#$B8
                +#$EF#$D5#$00#$00#$09#$00#$08#$20#$00#$00#$A0#$00#$00#$00#$00#$00;
                move(player[i].gid,s[$15],4);
                move(player[i].gid,s[$21],4);
                move(player[i].gid,s[$9],4);
                move(l,s[$31],4);  //damage
                move(player[i].hp,s[$35],4); //life
                move(player[i].CurPos,s[$3d],8);
                k:=FloatToHalf(HalfPrecisionToFloat(player[i].CurPos.y)+1.2);
                move(k,s[$3f],2);
                sendtolobby(player[x].room,-1,s);

                s:=#$50#$00#$00#$00#$06#$02#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
                  +#$04#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
                  +#$1D#$55#$AC#$AF#$00#$00#$00#$00#$00#$00#$00#$00#$FF#$FF#$FF#$FF
                  +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                  +#$24#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;

                move(player[i].gid,s[$15],4);
                move(player[i].gid,s[$9],4);
                sendtolobby(player[x].room,-1,s);
            end;
        end;



        s:=#$1C#$00#$00#$00#$0F#$06#$04#$00#$F0#$AD#$00#$00#$35#$D7#$AD#$A1
        +#$B9#$28#$A6#$04#$02#$00#$01#$00#$02#$00#$00#$00;
        move(player[x].activeChar.PersonalBank[p].id1,s[13],4);
        move(player[x].activeChar.PersonalBank[p].id2,s[17],4);
        s[$15]:=ansichar(player[x].activeChar.PersonalBank[p].count-1);

        removeFromStorage(x,player[x].activeChar.PersonalBank[p].id1,player[x].activeChar.PersonalBank[p].id2,1
            ,player[x].activeChar.PersonalBank,@player[x].activechar.bankcount,50);
        sendtoplayer(x,s);
    end;
end;


procedure PlayerUseSol(x:integer);
var s:ansistring;
    p,l,j,k,i:integer;
begin
    p:=FindItempos(x,3,1,0,6);
    if (p>-1) then begin

        //find dead player around
        for i:=0 to srvcfg.MaxPlayer-1 do
        if playerok(i) and (player[i].room = player[x].room) and (player[i].Floor = player[x].Floor) then begin
            if round(sqrt(sqr(HalfPrecisionToFloat(player[x].CurPos.x) - HalfPrecisionToFloat(player[i].CurPos.x)) +
                sqr(HalfPrecisionToFloat(player[x].CurPos.y) - HalfPrecisionToFloat(player[i].CurPos.y)))) < 15 then begin

                //animation
                s:=#$2C#$00#$00#$00#$04#$25#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
                    +#$04#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
                    +#$24#$27#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;
                move(player[i].gid,s[$15],4);
                move(player[i].gid,s[$9],4);
                l:=player[i].statuseffect;
                if player[i].statuseffect = Status_Shock then l:=10020;
                move(l,s[$21],4);  //effect to nil
                if l <> 0 then sendtolobby(player[x].room,-1,s);

                s:=#$50#$00#$00#$00#$06#$02#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
                  +#$04#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
                  +#$47#$77#$8C#$9B#$00#$00#$00#$00#$00#$00#$00#$00#$FF#$FF#$FF#$FF
                  +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                  +#$24#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;

                move(player[i].gid,s[$15],4);
                move(player[i].gid,s[$9],4);
                sendtolobby(player[x].room,-1,s);
            end;
        end;



        s:=#$1C#$00#$00#$00#$0F#$06#$04#$00#$F0#$AD#$00#$00#$35#$D7#$AD#$A1
        +#$B9#$28#$A6#$04#$02#$00#$01#$00#$02#$00#$00#$00;
        move(player[x].activeChar.PersonalBank[p].id1,s[13],4);
        move(player[x].activeChar.PersonalBank[p].id2,s[17],4);
        s[$15]:=ansichar(player[x].activeChar.PersonalBank[p].count-1);

        removeFromStorage(x,player[x].activeChar.PersonalBank[p].id1,player[x].activeChar.PersonalBank[p].id2,1
            ,player[x].activeChar.PersonalBank,@player[x].activechar.bankcount,50);
        sendtoplayer(x,s);
    end;
end;

procedure PlayerUseItem(x:integer;itm:ansistring);
begin
    //test the item and distribute to function
    if itm = 'TelepipeS' then PlayerUsePipe(x);
    if itm = 'MonomateS' then PlayerUseMate(x,0);
    if itm = 'DimateS' then PlayerUseMate(x,1);
    if itm = 'TrimateS' then PlayerUseMate(x,2);
    if itm = 'ShiftaRideS' then PlayerShiftaRideS(x);
    if itm = 'DebandRideS' then PlayerDebandRideS(x);
    if itm = 'MoonAtomizerS' then PlayerUseMoon(x);
    if itm = 'StarAtomizerS' then PlayerUseStar(x);
    if itm = 'SolAtomizerS' then PlayerUseSol(x);
end;

procedure ApplyBoostToMoonster(x,mid:integer;s,b:ansistring);
var i,d:integer;
    a:ansistring;
begin

    for i:=0 to team[player[x].TeamID].MonstCount[player[x].Floor]-1 do
        if team[player[x].TeamID].Monst[player[x].Floor,i].id = mid then begin
            if s = 'Apt:Howl' then begin
                team[player[x].TeamID].Monst[player[x].Floor,i].boostflg:=team[player[x].TeamID].Monst[player[x].Floor,i].boostflg or 1;
                //notify the players
                a:=#$48#$00#$00#$00#$04#$24#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	                +#$04#$00#$00#$00#$D2#$03#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
	                +#$D2#$03#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11#$43#$0D#$03#$00
	                +#$FF#$FF#$FF#$FF#$2C#$22#$6F#$42#$00#$00#$00#$00#$04#$00#$00#$00
	                +#$00#$00#$00#$08#$00#$00#$00#$00;
                move(player[x].gid,a[9],4);
                move(mid,a[$15],4);
                move(mid,a[$21],4);
                sendtofloor(player[x].room,player[x].Floor,a);

                a:=#$48#$00#$00#$00#$04#$24#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
                  +#$04#$00#$00#$00#$D2#$03#$00#$00#$00#$00#$00#$00#$06#$00#$08#$04
                  +#$D2#$03#$00#$00#$00#$00#$00#$00#$06#$00#$08#$04#$44#$0D#$03#$00
                  +#$FF#$FF#$FF#$FF#$29#$22#$6F#$42#$00#$00#$00#$00#$04#$00#$00#$00
                  +#$00#$00#$00#$08#$00#$00#$00#$00;
                move(player[x].gid,a[9],4);
                move(mid,a[$15],4);
                move(mid,a[$21],4);
                sendtofloor(player[x].room,player[x].Floor,a);
            end;

            if s = 'Apt:Faint' then team[player[x].TeamID].Monst[player[x].Floor,i].boostflg:=
                team[player[x].TeamID].Monst[player[x].Floor,i].boostflg or 2;
            if s = 'Apt:FaintEnd' then team[player[x].TeamID].Monst[player[x].Floor,i].boostflg:=
                team[player[x].TeamID].Monst[player[x].Floor,i].boostflg and $fffffffD;

            if s = 'Apt:Rage' then begin
                a:=#$48#$00#$00#$00#$04#$24#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
                +#$04#$00#$00#$00#$02#$03#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
                +#$02#$03#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11#$4D#$0D#$03#$00
                +#$FF#$FF#$FF#$FF#$5E#$6B#$6F#$42#$00#$00#$00#$00#$04#$10#$00#$00
                +#$00#$00#$00#$08#$00#$00#$00#$00;
                move(player[x].gid,a[9],4);
                move(mid,a[$15],4);
                move(mid,a[$21],4);
                sendtofloor(player[x].room,player[x].Floor,a);
            end;

            if s = 'Env:Thunder' then begin
                ApplyDamageToMonster(x,player[x].TeamID,i,player[x].Floor,15,0,
                    copy(b,$31,8));

                if random(8) = 1 then begin
                    //paralisis
                    a:=#$48#$00#$00#$00#$04#$24#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
                      +#$06#$00#$a9#$11#$56#$5F#$9C#$00#$00#$00#$00#$00#$06#$00#$a9#$11
                      +#$c3#$06#$01#$00#$00#$00#$00#$00#$06#$00#$a9#$11#$24#$27#$00#$00 //thunder#$id
                      +#$00#$00#$00#$00#$FF#$0F#$EF#$41#$00#$00#$00#$00#$00#$00#$00#$00
                      +#$00#$00#$60#$00#$00#$00#$00#$00;
                    move(team[player[x].TeamID].Monst[player[x].Floor,i].id,a[9],4);
                    move(team[player[x].TeamID].Monst[player[x].Floor,i].id,a[$15],4);
                    sendtofloor(player[x].room,player[x].Floor,a);

                    //set the shock effect
                    team[player[x].TeamID].Monst[player[x].Floor,i].statuseffect:=Status_Shock;
                    team[player[x].TeamID].Monst[player[x].Floor,i].statusefft:=0;
                    team[player[x].TeamID].Monst[player[x].Floor,i].statustime:=gettickcount()+30000; //30 seconds
                end;
            end;

            if s = 'SE:Burn' then begin
                if team[player[x].TeamID].monst[player[x].Floor,i].statuseffect = 10015 then d:=round(team[player[x].TeamID].monst[player[x].Floor,i].mhp*0.01);
                if team[player[x].TeamID].monst[player[x].Floor,i].statuseffect = 10016 then d:=round(team[player[x].TeamID].monst[player[x].Floor,i].mhp*0.012);
                if team[player[x].TeamID].monst[player[x].Floor,i].statuseffect = 10017 then d:=round(team[player[x].TeamID].monst[player[x].Floor,i].mhp*0.012);
                if team[player[x].TeamID].monst[player[x].Floor,i].statuseffect = 10018 then d:=round(team[player[x].TeamID].monst[player[x].Floor,i].mhp*0.014);
                if team[player[x].TeamID].monst[player[x].Floor,i].statuseffect = 10019 then d:=round(team[player[x].TeamID].monst[player[x].Floor,i].mhp*0.016);
                ApplyDamageToMonster(0,player[x].TeamID,i,player[x].Floor,d,0,copy(b,$31,8));

            end;
            break;
        end;
end;


procedure PlayerResta(x,v:integer);
var s:ansistring;
    p,l,j,k,i:integer;
begin

        l:=player[x].hp;
        k:=player[x].maxhp;

        i:=k;
        i:=v;

        if player[x].hp + i > k then i:=k-player[x].hp;
        inc(player[x].hp,i);

        i:=0-i;

        s:=#$50#$00#$00#$00#$04#$52#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
        +#$04#$00#$00#$00#$C3#$00#$00#$00#$00#$00#$00#$00#$04#$00#$00#$00
        +#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00#$A3#$08#$69#$63
        +#$31#$00#$00#$00#$B3#$00#$00#$00#$01#$00#$00#$00#$8F#$4F#$4A#$B8
        +#$EF#$D5#$00#$00#$09#$00#$08#$20#$00#$00#$A0#$00#$00#$00#$00#$00;
        move(player[x].gid,s[$15],4);
        move(player[x].gid,s[$21],4);
        move(player[x].gid,s[$9],4);
        move(i,s[$31],4);  //damage
        move(player[x].hp,s[$35],4); //life
        move(player[x].CurPos,s[$3d],8);
        k:=FloatToHalf(HalfPrecisionToFloat(player[x].CurPos.y)+1.2);
        move(k,s[$3f],2);
        sendtolobby(player[x].room,-1,s);

end;

procedure NPCResta(x,id,v:integer;pos:ansistring);
var s:ansistring;
    p,l,j,k,i:integer;
begin
        for l:=0 to team[player[x].TeamID].npccount-1 do
        if team[player[x].TeamID].npcs[l].id = id then begin


            k:=team[player[x].TeamID].npcs[l].mhp;
            i:=v;

            if team[player[x].TeamID].npcs[l].hp + i > k then i:=k-team[player[x].TeamID].npcs[l].hp;
            inc(team[player[x].TeamID].npcs[l].hp,i);

            i:=0-i;

            s:=#$50#$00#$00#$00#$04#$52#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
            +#$16#$00#$8A#$03#$C3#$00#$00#$00#$00#$00#$00#$00#$16#$00#$8A#$03
            +#$56#$5F#$9C#$00#$00#$00#$00#$00#$16#$00#$8A#$03#$A3#$08#$69#$63
            +#$31#$00#$00#$00#$B3#$00#$00#$00#$01#$00#$00#$00#$8F#$4F#$4A#$B8
            +#$EF#$D5#$00#$00#$09#$00#$08#$20#$00#$00#$A0#$00#$00#$00#$00#$00;
            move(id,s[$15],4);
            move(id,s[$21],4);
            move(id,s[$9],4);
            move(i,s[$31],4);  //damage
            move(team[player[x].TeamID].npcs[l].hp,s[$35],4); //life
            move(pos[1],s[$3d],8);
            k:=FloatToHalf(HalfPrecisionToFloat(pword(@pos[3])^)+1.2);
            move(k,s[$3f],2);
            sendtolobby(player[x].room,-1,s);

        end;
end;


procedure ApplyBoostToPlayer(x:integer;s,b:ansistring);
var a:ansistring;
    t:single;
    lvl,i,l,d:integer;
begin
    //find the caster
    if s = 'Tech:Resta' then begin

            lvl:=0;
            for i:=0 to srvcfg.MaxPlayer-1 do
                if playerok(i) and (player[i].GID = pdword(@b[9])^) then begin
                    lvl:=player[i].activechar.PALvl[104-1];
                    l:=round((getplayertatk(i) / 4)*(palvlstats[10,lvl]/100));
                end;
            if lvl = 0 then begin
                for i:=0 to team[player[x].teamid].npccount-1 do
                    if team[player[x].teamid].npcs[i].id = pdword(@b[9])^ then begin
                        lvl:=(team[player[x].teamid].npcs[i].lvl div 12)+1;
                        l:=round((team[player[x].teamid].npcs[i].TATK / 4)*(palvlstats[10,lvl]/100));
                    end;
            end;
            PlayerResta(x,l);

    end;
    if s = 'Obj:RecoveryPod' then begin

        //PlayerResta(x,l);
        inc(player[x].hp,258);
        if player[x].hp > player[x].maxhp then player[x].hp:=player[x].maxhp;

        a:=#$50#$00#$00#$00#$04#$52#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
        +#$04#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
        +#$CD#$02#$00#$00#$00#$00#$00#$00#$06#$00#$E3#$0A#$6E#$E4#$C4#$25
        +#$FE#$FE#$FF#$FF#$02#$01#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00
        +#$00#$00#$00#$00#$24#$00#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00;
        move(player[x].gid,a[9],4);
        move(player[x].gid,a[$15],4);
        move(b[9],a[$21],4);
        move(player[x].hp,a[$35],4);
        SendToLobby(player[x].room,-1,a);

    end;
    if s = 'Tech:ShiftaRide' then begin
        if copy(b,$2d,4) = #0#0#0#0 then begin
            player[x].ShiftaTick := gettickcount()+14300; //14.3 sec
            //apply according to the caster
            lvl:=0;
            for i:=0 to srvcfg.MaxPlayer-1 do
                if playerok(i) and (player[i].GID = pdword(@b[9])^) then lvl:=player[i].activechar.PALvl[84-1];
            if lvl = 0 then begin
                for i:=0 to team[player[x].teamid].npccount-1 do
                    if team[player[x].teamid].npcs[i].id = pdword(@b[9])^ then lvl:=(team[player[x].teamid].npcs[i].lvl div 12)+1;
            end;
            player[x].ShiftaBoost:=palvlstats[84,lvl];
        end else begin
            inc(player[x].ShiftaTick ,14300);
        end;
        a:=#$48#$00#$00#$00#$04#$76#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
        +#$04#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
        +#$FA#$FB#$01#$00#$95#$F0#$62#$42#$03#$00#$00#$00#$56#$5F#$9C#$00
        +#$00#$00#$00#$00#$04#$00#$00#$00#$FF#$FF#$FF#$FF#$04#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00;
        move(player[x].gid,a[9],4);
        move(player[x].gid,a[$15],4);
        move(player[x].gid,a[$2d],4);
        t:=(player[x].ShiftaTick-gettickcount) / 1000;
        if t < 0 then t:=0;
        move(t,a[$35],4);
        SendToLobby(player[x].room,-1,a);

    end;
    if s = 'Tech:DebandRide' then begin
        if copy(b,$2d,4) = #0#0#0#0 then begin
            player[x].DebandTick := gettickcount()+14300; //14.3 sec
            lvl:=0;
            for i:=0 to srvcfg.MaxPlayer-1 do
                if playerok(i) and (player[i].GID = pdword(@b[9])^) then lvl:=player[i].activechar.PALvl[89-1];
            if lvl = 0 then begin
                for i:=0 to team[player[x].teamid].npccount-1 do
                    if team[player[x].teamid].npcs[i].id = pdword(@b[9])^ then lvl:=(team[player[x].teamid].npcs[i].lvl div 12)+1;
            end;
            player[x].DebandBoost:=palvlstats[89,lvl];
        end else begin
            inc(player[x].DebandTick ,14300);
        end;
        a:=#$48#$00#$00#$00#$04#$24#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
        +#$04#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
        +#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00#$FB#$FB#$01#$00
        +#$00#$00#$00#$00#$69#$5E#$6D#$41#$03#$00#$00#$00#$08#$10#$00#$00
        +#$00#$00#$00#$08#$00#$00#$00#$00;
        move(player[x].gid,a[9],4);
        move(player[x].gid,a[$15],4);
        move(player[x].gid,a[$21],4);
        t:=(player[x].DebandTick-gettickcount) / 1000;
        if t < 0 then t:=0;
        move(t,a[$35],4);
        SendToLobby(player[x].room,-1,a);

    end;
    if s = 'Item:ShiftaRide' then begin
        if copy(b,$2d,4) = #0#0#0#0 then begin
            player[x].ShiftaTick := gettickcount()+14300; //14.3 sec
            player[x].ShiftaBoost:=palvlstats[84,1];
        end else begin
            inc(player[x].ShiftaTick ,14300);
        end;
        a:=#$48#$00#$00#$00#$04#$76#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
        +#$04#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
        +#$FA#$FB#$01#$00#$95#$F0#$62#$42#$03#$00#$00#$00#$56#$5F#$9C#$00
        +#$00#$00#$00#$00#$04#$00#$00#$00#$FF#$FF#$FF#$FF#$04#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00;
        move(player[x].gid,a[9],4);
        move(player[x].gid,a[$15],4);
        move(player[x].gid,a[$2d],4);
        t:=(player[x].ShiftaTick-gettickcount) / 1000;
        if t < 0 then t:=0;
        move(t,a[$35],4);
        SendToLobby(player[x].room,-1,a);

    end;
    if s = 'Item:DebandRide' then begin
        if copy(b,$2d,4) = #0#0#0#0 then begin
            player[x].DebandTick := gettickcount()+14300; //14.3 sec
            player[x].DebandBoost:=palvlstats[89,1];
        end else begin
            inc(player[x].DebandTick ,14300);
        end;
        a:=#$48#$00#$00#$00#$04#$24#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
        +#$04#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
        +#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00#$FB#$FB#$01#$00
        +#$00#$00#$00#$00#$69#$5E#$6D#$41#$03#$00#$00#$00#$08#$10#$00#$00
        +#$00#$00#$00#$08#$00#$00#$00#$00;
        move(player[x].gid,a[9],4);
        move(player[x].gid,a[$15],4);
        move(player[x].gid,a[$21],4);
        t:=(player[x].DebandTick-gettickcount) / 1000;
        if t < 0 then t:=0;
        move(t,a[$35],4);
        SendToLobby(player[x].room,-1,a);

    end;

    if s = 'Env:Thunder' then begin
        ApplyDamageToPlayer(x,player[x].TeamID,player[x].Floor,15,#$c3#$06#$01#$00#$00#$00#$00#$00#$06#$00#$a9#$11,
            copy(b,$31,8));

        if random(8) = 1 then begin
            //paralisis
            a:=#$48#$00#$00#$00#$04#$24#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
              +#$04#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
              +#$c3#$06#$01#$00#$00#$00#$00#$00#$06#$00#$a9#$11#$24#$27#$00#$00 //thunder#$id
              +#$00#$00#$00#$00#$FF#$0F#$EF#$41#$00#$00#$00#$00#$00#$00#$00#$00
              +#$00#$00#$60#$00#$00#$00#$00#$00;
            move(player[x].gid,a[9],4);
            move(player[x].gid,a[$15],4);
            sendtofloor(player[x].room,player[x].Floor,a);

            //set the shock effect
            player[x].statuseffect:=Status_Shock;
            player[x].statusefft:=0;
            player[x].statustime:=gettickcount()+30000; //30 seconds
        end;
    end;

    if s = 'Env:Lava' then begin
        ApplyDamageToPlayer(x,player[x].TeamID,player[x].Floor,15,#$c3#$06#$01#$00#$00#$00#$00#$00#$06#$00#$a9#$11,
            copy(b,$31,8));
            if random(10) = 1 then begin
                if (player[x].statuseffect>= 10015) and (player[x].statuseffect<= 10019) then begin
                    if player[x].statuseffect < 10019 then inc(player[x].statuseffect);

                end else player[x].statuseffect:=10015; //minimum burn
                //burn
                a:=#$48#$00#$00#$00#$04#$24#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
                  +#$04#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
                  +#$c3#$06#$01#$00#$00#$00#$00#$00#$06#$00#$a9#$11#$24#$27#$00#$00 //burn#$id
                  +#$00#$00#$00#$00#$FF#$0F#$EF#$41#$00#$00#$00#$00#$00#$00#$00#$00
                  +#$00#$00#$60#$00#$00#$00#$00#$00;
                move(player[x].gid,a[9],4);
                move(player[x].gid,a[$15],4);
                move(player[x].statuseffect,a[$2d],4);
                sendtofloor(player[x].room,player[x].Floor,a);

                player[x].statusefft:=0;
                player[x].statustime:=gettickcount()+30000; //30 seconds
            end;

    end;

    if s = 'SE:Burn' then begin
        d:=round(player[x].maxhp*0.01);
        if player[x].statuseffect = 10016 then d:=round(player[x].maxhp*0.012);
        if player[x].statuseffect = 10017 then d:=round(player[x].maxhp*0.012);
        if player[x].statuseffect = 10018 then d:=round(player[x].maxhp*0.014);
        if player[x].statuseffect = 10019 then d:=round(player[x].maxhp*0.016);
        ApplyDamageToPlayer(x,player[i].TeamID,player[i].Floor,d,#0#0#0#0#0#0#0#0#0#0#0#0,copy(b,$31,8));
    end;

end;

procedure ApplyBoostToNPC(x,nid:integer;s,b:ansistring);
var a:ansistring;
    t:single;
    lvl,i,l,c:integer;
begin
    //find the npc
    for c:=0 to team[player[x].TeamID].npccount-1 do
    if team[player[x].TeamID].npcs[c].id = nid then begin
    if s = 'Tech:Resta' then begin

            lvl:=0;
            for i:=0 to srvcfg.MaxPlayer-1 do
                if playerok(i) and (player[i].GID = pdword(@b[9])^) then begin
                    lvl:=player[i].activechar.PALvl[104-1];
                    l:=round((getplayertatk(i) / 4)*(palvlstats[10,lvl]/100));
                end;
            if lvl = 0 then begin
                for i:=0 to team[player[x].teamid].npccount-1 do
                    if team[player[x].teamid].npcs[i].id = pdword(@b[9])^ then begin
                        lvl:=(team[player[x].teamid].npcs[i].lvl div 12)+1;
                        l:=round((team[player[x].teamid].npcs[i].TATK / 4)*(palvlstats[10,lvl]/100));
                    end;
            end;
            NPCResta(x,nid,l,copy(b,$31,8));

    end;
    if s = 'Tech:ShiftaRide' then begin
        if copy(b,$2d,4) = #0#0#0#0 then begin
            team[player[x].teamid].npcs[i].ShiftaTick := gettickcount()+14300; //14.3 sec
            team[player[x].teamid].npcs[i].boostflg := team[player[x].teamid].npcs[i].boostflg or 1;
            //apply according to the caster
            lvl:=0;
            for i:=0 to srvcfg.MaxPlayer-1 do
                if playerok(i) and (player[i].GID = pdword(@b[9])^) then lvl:=player[i].activechar.PALvl[84-1];
            if lvl = 0 then begin
                for i:=0 to team[player[x].teamid].npccount-1 do
                    if team[player[x].teamid].npcs[i].id = pdword(@b[9])^ then lvl:=(team[player[x].teamid].npcs[i].lvl div 12)+1;
            end;
            team[player[x].teamid].npcs[i].ShiftaBoost:=palvlstats[84,lvl];
        end else begin
            inc(team[player[x].teamid].npcs[i].ShiftaTick ,14300);
        end;
        a:=#$48#$00#$00#$00#$04#$76#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
        +#$16#$00#$8A#$03#$56#$5F#$9C#$00#$00#$00#$00#$00#$16#$00#$8A#$03
        +#$FA#$FB#$01#$00#$95#$F0#$62#$42#$03#$00#$00#$00#$56#$5F#$9C#$00
        +#$00#$00#$00#$00#$16#$00#$8A#$03#$FF#$FF#$FF#$FF#$04#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00;
        move(nid,a[9],4);
        move(nid,a[$15],4);
        move(nid,a[$2d],4);
        t:=(team[player[x].teamid].npcs[i].ShiftaTick-gettickcount) / 1000;
        if t < 0 then t:=0;
        move(t,a[$35],4);
        SendToLobby(player[x].room,-1,a);

    end;
    if s = 'Tech:DebandRide' then begin
        if copy(b,$2d,4) = #0#0#0#0 then begin
            team[player[x].teamid].npcs[i].DebandTick := gettickcount()+14300; //14.3 sec
            team[player[x].teamid].npcs[i].boostflg := team[player[x].teamid].npcs[i].boostflg or 2;
            lvl:=0;
            for i:=0 to srvcfg.MaxPlayer-1 do
                if playerok(i) and (player[i].GID = pdword(@b[9])^) then lvl:=player[i].activechar.PALvl[89-1];
            if lvl = 0 then begin
                for i:=0 to team[player[x].teamid].npccount-1 do
                    if team[player[x].teamid].npcs[i].id = pdword(@b[9])^ then lvl:=(team[player[x].teamid].npcs[i].lvl div 12)+1;
            end;
            team[player[x].teamid].npcs[i].DebandBoost:=palvlstats[89,lvl];
        end else begin
            inc(team[player[x].teamid].npcs[i].DebandTick ,14300);
        end;
        a:=#$48#$00#$00#$00#$04#$24#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
        +#$16#$00#$8A#$03#$56#$5F#$9C#$00#$00#$00#$00#$00#$16#$00#$8A#$03
        +#$56#$5F#$9C#$00#$00#$00#$00#$00#$16#$00#$8A#$03#$FB#$FB#$01#$00
        +#$00#$00#$00#$00#$69#$5E#$6D#$41#$03#$00#$00#$00#$08#$10#$00#$00
        +#$00#$00#$00#$08#$00#$00#$00#$00;
        move(nid,a[9],4);
        move(nid,a[$15],4);
        move(nid,a[$21],4);
        t:=(team[player[x].teamid].npcs[i].DebandTick-gettickcount) / 1000;
        if t < 0 then t:=0;
        move(t,a[$35],4);
        SendToLobby(player[x].room,-1,a);

    end;
    if s = 'Item:ShiftaRide' then begin
        if copy(b,$2d,4) = #0#0#0#0 then begin
            team[player[x].teamid].npcs[i].ShiftaTick := gettickcount()+14300; //14.3 sec
            team[player[x].teamid].npcs[i].boostflg := team[player[x].teamid].npcs[i].boostflg or 1;
            team[player[x].teamid].npcs[i].ShiftaBoost:=palvlstats[84,1];
        end else begin
            inc(team[player[x].teamid].npcs[i].ShiftaTick ,14300);
        end;
        a:=#$48#$00#$00#$00#$04#$76#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
        +#$16#$00#$8A#$03#$56#$5F#$9C#$00#$00#$00#$00#$00#$16#$00#$8A#$03
        +#$FA#$FB#$01#$00#$95#$F0#$62#$42#$03#$00#$00#$00#$56#$5F#$9C#$00
        +#$00#$00#$00#$00#$16#$00#$8A#$03#$FF#$FF#$FF#$FF#$04#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00;
        move(nid,a[9],4);
        move(nid,a[$15],4);
        move(nid,a[$2d],4);
        t:=(team[player[x].teamid].npcs[i].ShiftaTick-gettickcount) / 1000;
        if t < 0 then t:=0;
        move(t,a[$35],4);
        SendToLobby(player[x].room,-1,a);

    end;
    if s = 'Item:DebandRide' then begin
        if copy(b,$2d,4) = #0#0#0#0 then begin
            team[player[x].teamid].npcs[i].DebandTick := gettickcount()+14300; //14.3 sec
            team[player[x].teamid].npcs[i].boostflg := team[player[x].teamid].npcs[i].boostflg or 2;
            team[player[x].teamid].npcs[i].DebandBoost:=palvlstats[89,1];
        end else begin
            inc(team[player[x].teamid].npcs[i].DebandTick ,14300);
        end;
        a:=#$48#$00#$00#$00#$04#$24#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
        +#$16#$00#$8A#$03#$56#$5F#$9C#$00#$00#$00#$00#$00#$16#$00#$8A#$03
        +#$56#$5F#$9C#$00#$00#$00#$00#$00#$16#$00#$8A#$03#$FB#$FB#$01#$00
        +#$00#$00#$00#$00#$69#$5E#$6D#$41#$03#$00#$00#$00#$08#$10#$00#$00
        +#$00#$00#$00#$08#$00#$00#$00#$00;
        move(nid,a[9],4);
        move(nid,a[$15],4);
        move(nid,a[$21],4);
        t:=(team[player[x].teamid].npcs[i].DebandTick-gettickcount) / 1000;
        if t < 0 then t:=0;
        move(t,a[$35],4);
        SendToLobby(player[x].room,-1,a);

    end;
    end;
end;

end.
