unit UMag;

interface

uses windows,types,sysutils;

type
    tMagLvl = record
        lvl,pts:integer;
    end;
    TMagFeed = record
        star,satk,ratk,tatk,dex,sdef,rdef,tdef:integer;
    end;

const
    MagPB_HelixProi = 0;
    MagPB_HelixImera = 1;
    MagPB_HelixNykta = 2;
    MagPB_AjaxProi  = $10;
    MagPB_AjaxImera = $11;
    MagPB_AjaxNykta = $12;
    MagPB_KetosProi  = $20;
    MagPB_KetosImera  = $21;
    MagPB_KetosNykta  = $22;
    MagPB_JuliusProi  = $30;
    MagPB_JuliusImera  = $31;
    MagPB_JuliusNykta  = $32;
    MagPB_IliosProi  = $40;
    MagPB_IliosImera  = $41;
    MagPB_IliosNykta  = $42;

    DefaultMag = #$01#$00#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$FF#$00#$00#$00#$00#$00#$03#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$01#$00#$00#$FF#$00#$00#$00#$FF#$00#$00#$00#$FF#$00#$00#$00
        +#$FF#$00#$00#$00#$FF#$00#$00#$00#$FF#$00#$00#$00#$FF#$00#$00#$00;

    MagLvl:array [0..8] of tMagLvl = (
      (lvl: 49; pts: 30),
      (lvl: 59; pts: 70),
      (lvl: 69; pts: 90),
      (lvl: 79; pts: 110),
      (lvl: 89; pts: 130),
      (lvl: 99; pts: 150),
      (lvl: 109; pts: 180),
      (lvl: 119; pts: 210),
      (lvl: 200; pts: 240)
    );

    MagFeedTable:array[0..8,1..9] of TMagFeed = (
      ((star:1; satk: 18; dex: 2; sdef:-6),
      (star:2; satk: 27; dex: 3; sdef:-9),
      (star:3; satk: 36; dex: 4; sdef:-12),
      (star:4; satk: 45; dex: 5; sdef:-15),
      (star:5; satk: 54; dex: 6; sdef:-18),
      (star:6; satk: 63; dex: 7; sdef:-21),
      (star:7; satk: 80; dex: 8; sdef:-24),
      (star:8; satk: 104; dex: 9; sdef:-27),
      (star:9; satk: 128; dex: 10; sdef:-30)),

      ((star:1; ratk: 18; dex: 2; rdef:-6),
      (star:2; ratk: 27; dex: 3; rdef:-9),
      (star:3; ratk: 36; dex: 4; rdef:-12),
      (star:4; ratk: 45; dex: 5; rdef:-15),
      (star:5; ratk: 54; dex: 6; rdef:-18),
      (star:6; ratk: 63; dex: 7; rdef:-21),
      (star:7; ratk: 80; dex: 8; rdef:-24),
      (star:8; ratk: 104; dex: 9; rdef:-27),
      (star:9; ratk: 128; dex: 10; rdef:-30)),

      ((star:1; tatk: 18; dex: 2; tdef:-6),
      (star:2; tatk: 27; dex: 3; tdef:-9),
      (star:3; tatk: 36; dex: 4; tdef:-12),
      (star:4; tatk: 45; dex: 5; tdef:-15),
      (star:5; tatk: 54; dex: 6; tdef:-18),
      (star:6; tatk: 63; dex: 7; tdef:-21),
      (star:7; tatk: 80; dex: 8; tdef:-24),
      (star:8; tatk: 104; dex: 9; tdef:-27),
      (star:9; tatk: 128; dex: 10; tdef:-30)),

      ((star:1; tatk: -6; dex: 2; tdef:18),
      (star:2; tatk: -9; dex: 3; tdef:27),
      (star:3; tatk: -12; dex: 4; tdef:36),
      (star:4; tatk: -15; dex: 5; tdef:45),
      (star:5; tatk: -18; dex: 6; tdef:54),
      (star:6; tatk: -21; dex: 7; tdef:63),
      (star:7; tatk: -24; dex: 8; tdef:80),
      (star:8; tatk: -27; dex: 9; tdef:104),
      (star:9; tatk: -30; dex: 10; tdef:128)),

      ((star:1; ratk: -6; dex: 2; rdef:18),
      (star:2; ratk: -9; dex: 3; rdef:27),
      (star:3; ratk: -12; dex: 4; rdef:36),
      (star:4; ratk: -15; dex: 5; rdef:45),
      (star:5; ratk: -18; dex: 6; rdef:54),
      (star:6; ratk: -21; dex: 7; rdef:63),
      (star:7; ratk: -24; dex: 8; rdef:80),
      (star:8; ratk: -27; dex: 9; rdef:104),
      (star:9; ratk: -30; dex: 10; rdef:128)),

      ((star:1; satk: -6; dex: 2; sdef:18),
      (star:2; satk: -9; dex: 3; sdef:27),
      (star:3; satk: -12; dex: 4; sdef:36),
      (star:4; satk: -15; dex: 5; sdef:45),
      (star:5; satk: -18; dex: 6; sdef:54),
      (star:6; satk: -21; dex: 7; sdef:63),
      (star:7; satk: -24; dex: 8; sdef:80),
      (star:8; satk: -27; dex: 9; sdef:104),
      (star:9; satk: -30; dex: 10; sdef:128)),

      ((star:1; dex: -9; sdef: 6; rdef:6; tdef:6),
      (star:2; dex: -12; sdef: 8; rdef:8; tdef:8),
      (star:3; dex: -15; sdef: 10; rdef:10; tdef:10),
      (star:4; dex: -18; sdef: 12; rdef:12; tdef:12),
      (star:5; dex: -21; sdef: 14; rdef:14; tdef:14),
      (star:6; dex: -21; sdef: 14; rdef:14; tdef:14),
      (star:7; dex: -21; sdef: 14; rdef:14; tdef:14),
      (star:8; dex: -21; sdef: 14; rdef:14; tdef:14),
      (star:9; dex: -21; sdef: 14; rdef:14; tdef:14)),

      ((star:1; satk: -10; dex: -10; sdef:5),
      (star:2; ratk: -10; dex: -10; rdef:5),
      (star:3; tatk: -10; dex: -10; tdef:5),
      (star:4; satk: 5; dex: -10; sdef:-10),
      (star:5; ratk: 5; dex: -10; rdef:-10),
      (star:6; tatk: 5; dex: -10; tdef:-10),
      (star:7; tatk: 5; dex: -10; tdef:-10),
      (star:8; tatk: 5; dex: -10; tdef:-10),
      (star:9; tatk: 5; dex: -10; tdef:-10)),

      ((star:1; satk: 4; dex: -8; sdef:4),
      (star:2; ratk: 4; dex: -8; rdef:4),
      (star:3; tatk: 4; dex: -8; tdef:4),
      (star:4; satk: -2; ratk: -2; tatk: -2; dex: 4),
      (star:5; satk: 4; ratk: 4; tatk: 4; dex: -10),
      (star:6; dex: -10; sdef: 4; rdef: 4; tdef: 4),
      (star:7; dex: 30; sdef: -3; rdef: -3; tdef: -3),
      (star:6; dex: 150; sdef: -3; rdef: -3; tdef: -3),
      (star:9; tatk: 0; dex: 0; tdef: 0))

    );

    magfoodwep:array[1..18] of byte = (0,0,0,0,0,0,0,1,1,1,2,2,2,0,1,0,0,0);
    magfoodunit:array[1..4] of byte = (3,4,5,6);
    magfooditem:array[0..12] of byte = (1,2,3,9,9,9,4,6,5,9,9,3,3);
    //tech disk to 7
    //custom tech to 8

    type3mag:array[0..12,0..2] of byte = (
     //base, type, PB
     (0,    1   ,MagPB_HelixProi),
     (0,    2   ,MagPB_HelixImera),
     (0,    0   ,MagPB_HelixNykta),

     (1,    5   ,MagPB_AjaxImera),
     (1,    4   ,MagPB_AjaxProi),
     (1,    3   ,MagPB_AjaxNykta),

     (2,    6   ,MagPB_KetosImera),
     (2,    8   ,MagPB_KetosNykta),
     (2,    7   ,MagPB_KetosProi),

     (3,    11  ,MagPB_JuliusProi),
     (3,    10  ,MagPB_JuliusImera),
     (3,    9   ,MagPB_JuliusNykta),
     (3,    12  ,MagPB_JuliusProi)
    ) ;
    {type 3
            0 = Libra
            1 = Delphinus
            2 = Cygnus
            3 = Caelum
            4 = Cepheus
            5 = Tucana
            6 = Carina
            7 = Monoceros
            8 = Orion
            9 = Apus
            10 = Corvus
            11 = Leo
            12 = Crux    }



procedure SendMagList(x:integer);
procedure DisplayMag(x:integer);
procedure EquipMag(x:integer;s:ansistring);
procedure FeedMag(x:integer;s:ansistring);
procedure UnequipMag(x:integer);

implementation

uses fmain, UPsoUtil, UPlayer, UInventory;

//send the mag names
procedure SendMagNames(x:integer);
var ids,a,len:ansistring;
    txt,b:widestring;
    i,l,c2:integer;
begin
    ids:='';
    txt:='';
    len:='';
    c2:=0;

    for l:=0 to itemcount-1 do
        if (items[l].ItemType = $fd ) then begin
            a:=#0#0#0#0#0#0#0#0;
            move(items[l].ItemType,a[1],8);
            ids:=ids+a;
            inc(c2);
            b:=items[l].name;
            txt:=txt+b;
            len:=len+chr(length(b));
        end;

    txt:=txt+#0;
    while length(len) and 3 > 0 do len:=len+#0;
    if c2 > 0 then begin
    //Xor: 9E22  Sub: 46
    setlength(a,length(txt)*2);
    move(txt[1],a[1],length(txt)*2);
    a:=#$74#$00#$00#$00#$0F#$30#$04#$00 + GetMagic(c2,$9e22,$46) + ids + GetMagic(length(txt),$9e22,$46) + a;
    if length(txt) and 1 = 1 then a:=a+#0#0;
    a:=a+GetMagic(c2,$9e22,$46) +len;
    l:=length(a);
    move(l,a[1],4);

    sendtoplayer(x,a);
    end;

end;

//send the mag list
procedure SendMagList(x:integer);
var s:ansistring;
begin
    SendMagNames(x);

    if player[x].maglist.count > 0 then begin
        setlength(s,player[x].maglist.count*sizeof(tmag));
        move(player[x].maglist.mags[0],s[1],player[x].maglist.count*sizeof(tmag));
    //Xor: A200  Sub: F8
        s:=#$68#$00#$00#$00#$2A#$04#$04#$00#$14#$00#$00#$00+getmagic(player[x].maglist.count,$A200,$f8)+s;

        sendtoplayer(x,s);
    end;
end;

function GetMagPtsMax(lvl:integer):integer;
var i:integer;
begin
    for i:=0 to 8 do
        if lvl <= MagLvl[i].lvl then break;
    result:=MagLvl[i].pts;
end;

function GetDominantStats(ss,rs,ts,ds:integer):integer;
begin
    result:=-1;
    if (ss > rs) and (ss > ts) and (ss > ds) then result:=0;
    if (rs > ss) and (rs > ts) and (rs > ds) then result:=1;
    if (ts > rs) and (ts > ss) and (ts > ds) then result:=2;
    if (ds > rs) and (ds > ts) and (ds > ss) then result:=3;
end;

function GetDominantStats2(ss,rs,ts,ds:integer):integer;
begin
    result:=-1;
    if (ss > rs+ds) and (ss > ts+ds) then result:=0;
    if (rs > ss+ds) and (rs > ts+ds) then result:=1;
    if (ts > rs+ds) and (ts > ss+ds) then result:=2;
end;

procedure ApplyMagStatsBonus(var s:tmagstat; maglvl,value:integer);
var i,l:integer;
begin
    l:=s.pts;
    l:=l+value;
    if l<0 then l:=0;
    i:=GetMagPtsMax(s.lvl);
    if l>=i then begin
        if maglvl >= 200 then l:=i-1
        else begin
            inc(s.lvl);
            l:=l-i;
        end;
    end;
    s.pts:=l;
end;

//feed mag
procedure FeedMag(x:integer;s:ansistring);
var a:ansistring;
    i,c,mid,iid,l,itm:integer;
    id1,id2:dword;
    tb:^TMagFeed;
    te:integer;
    ss,rs,ts,ds,pl:integer;
begin
    //find the mag
    move(s[9],id1,4);
    move(s[13],id2,4);
    for i:=0 to player[x].maglist.count-1 do
        if (player[x].maglist.mags[i].id1 = id1) and (player[x].maglist.mags[i].id2 = id2) then mid:=i;

    //adjust energy
    id1:=(gettickcount-player[x].magfeedtime[mid]) div 1000;
    if id1 > player[x].maglist.mags[mid].energy then player[x].maglist.mags[mid].energy:=0
    else dec(player[x].maglist.mags[mid].energy,id1);

    player[x].magfeedtime[mid]:=gettickcount;

    //find the item
    move(s[17],id1,4);
    move(s[21],id2,4);
    for i:=0 to player[x].activechar.BankCount-1 do
        if (player[x].activechar.PersonalBank[i].id1 = id1) and (player[x].activechar.PersonalBank[i].id2 = id2) then iid:=i;
    itm:=iid;

    for i:=0 to itemcount-1 do
        if (items[i].ItemType = player[x].activechar.PersonalBank[iid].ItemType)
            and (items[i].ItemSub = player[x].activechar.PersonalBank[iid].ItemSub)
            and (items[i].unk = player[x].activechar.PersonalBank[iid].unk)
            and (items[i].ItemEntry = player[x].activechar.PersonalBank[iid].ItemEntry) then begin
                iid:=i;
                break;
            end;

    //find the table and entry to use
    if items[iid].ItemType = 1 then begin
        //weapon
        tb:=@MagFeedTable[magfoodwep[items[iid].ItemSub]];
        te := items[iid].star;
        if te < 1 then te:=1;
        if te>9 then te:=9;
    end;
    if items[iid].ItemType = 5 then begin
        //weapon
        tb:=@MagFeedTable[magfoodunit[items[iid].ItemSub]];
        te := items[iid].star;
        if te < 1 then te:=1;
        if te>9 then te:=9;
    end;
    if items[iid].ItemType = 3 then begin
        //weapon
        tb:=@MagFeedTable[8];
        te := magfooditem[items[iid].ItemEntry];
        if te < 1 then te:=1;
        if te>9 then te:=9;
    end;
    if items[iid].ItemType = 9 then begin
        //weapon
        tb:=@MagFeedTable[8];
        te := 7;
    end;

    l:=player[x].maglist.mags[mid].satk.lvl+player[x].maglist.mags[mid].ratk.lvl+player[x].maglist.mags[mid].tatk.lvl
        +player[x].maglist.mags[mid].sdef.lvl+player[x].maglist.mags[mid].rdef.lvl+player[x].maglist.mags[mid].tdef.lvl
        +player[x].maglist.mags[mid].dex.lvl;
    pl:=l;
    //add the mag stats

    ApplyMagStatsBonus(player[x].maglist.mags[mid].satk,pl,tb.satk);
    ApplyMagStatsBonus(player[x].maglist.mags[mid].ratk,pl,tb.ratk);
    ApplyMagStatsBonus(player[x].maglist.mags[mid].tatk,pl,tb.tatk);
    ApplyMagStatsBonus(player[x].maglist.mags[mid].dex,pl,tb.dex);
    ApplyMagStatsBonus(player[x].maglist.mags[mid].sdef,pl,tb.sdef);
    ApplyMagStatsBonus(player[x].maglist.mags[mid].rdef,pl,tb.rdef);
    ApplyMagStatsBonus(player[x].maglist.mags[mid].tdef,pl,tb.tdef);

    inc(player[x].maglist.mags[mid].energy,800);
    if player[x].maglist.mags[mid].energy > 2400 then player[x].maglist.mags[mid].energy:=2400;

    //evolve it if needed
    l:=player[x].maglist.mags[mid].satk.lvl+player[x].maglist.mags[mid].ratk.lvl+player[x].maglist.mags[mid].tatk.lvl
        +player[x].maglist.mags[mid].sdef.lvl+player[x].maglist.mags[mid].rdef.lvl+player[x].maglist.mags[mid].tdef.lvl
        +player[x].maglist.mags[mid].dex.lvl;
    if (l >= 30) and (l < 100) and ((l mod 5 = 0) or (pl < 30)) then begin
        ss:=player[x].maglist.mags[mid].satk.lvl + player[x].maglist.mags[mid].sdef.lvl;
        rs:=player[x].maglist.mags[mid].ratk.lvl + player[x].maglist.mags[mid].rdef.lvl;
        ts:=player[x].maglist.mags[mid].tatk.lvl + player[x].maglist.mags[mid].tdef.lvl;
        ds:=player[x].maglist.mags[mid].dex.lvl;
        c:=GetDominantStats(ss,rs,ts,ds);
        if c > -1 then begin
            //evolution
            if (player[x].maglist.mags[mid].cath <> 2) or (player[x].maglist.mags[mid].outfit <> c) then
                player[x].maglist.mags[mid].pb:=c*16;
            player[x].maglist.mags[mid].cath:=2;
            player[x].maglist.mags[mid].outfit:=c;
        end;
    end;
    if (l >= 100) and ((l mod 5 = 0) or (pl < 100)) and (player[x].maglist.mags[mid].cath <> 4) then begin
        ss:=player[x].maglist.mags[mid].satk.lvl + player[x].maglist.mags[mid].sdef.lvl;
        rs:=player[x].maglist.mags[mid].ratk.lvl + player[x].maglist.mags[mid].rdef.lvl;
        ts:=player[x].maglist.mags[mid].tatk.lvl + player[x].maglist.mags[mid].tdef.lvl;
        ds:=player[x].maglist.mags[mid].dex.lvl;
        c:=GetDominantStats2(ss,rs,ts,ds);
        if type3mag[player[x].maglist.mags[mid].outfit,0] = 3 then c:=GetDominantStats(ss,rs,ts,ds);
        if c > -1 then begin
            //evolution
            if (player[x].maglist.mags[mid].cath <> 2) or (player[x].maglist.mags[mid].outfit <> c) then
                player[x].maglist.mags[mid].pb:=type3mag[(player[x].maglist.mags[mid].outfit*3)+c,2];
            if player[x].maglist.mags[mid].cath = 2 then begin
                player[x].maglist.mags[mid].outfit:=type3mag[(player[x].maglist.mags[mid].outfit*3)+c,1];
            end else begin
                player[x].maglist.mags[mid].outfit:=type3mag[(type3mag[player[x].maglist.mags[mid].outfit,0]*3)+c,1];
            end;
            player[x].maglist.mags[mid].cath:=3;

        end;
    end;

    //erase item
    move(s[17],id1,4);
    move(s[21],id2,4);

    //confirm
    a:=#$74#$00#$00#$00#$2A#$03#$00#$00#$01#$00#$01#$01#$42#$5B#$8A#$94
      +#$42#$A2#$B9#$0D#$01#$00#$01#$00#$00#$00#$12#$00#$00#$00#$05#$00
      +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$12#$00
      +#$00#$00#$00#$00#$05#$FF#$4A#$09#$00#$00#$00#$03#$00#$00#$00#$00
      +#$00#$00#$00#$00#$00#$01#$00#$00#$FF#$00#$00#$00#$FF#$00#$00#$00
      +#$FF#$00#$00#$00#$FF#$00#$00#$00#$FF#$00#$00#$00#$FF#$00#$00#$00
      +#$FF#$00#$00#$00#$EA#$5C#$07#$EA#$E9#$9A#$52#$0D#$00#$00#$00#$00
      +#$01#$00#$00#$00;
    move(player[x].maglist.mags[mid],a[13],$58);
    move(s[17],a[$65],8);

    if player[x].activechar.PersonalBank[itm].ItemType = 3 then begin
        if player[x].activechar.PersonalBank[itm].count > 1 then begin
            a[$71]:=#0;
            a[$6d]:=ansichar(player[x].activechar.PersonalBank[itm].count-1);
        end;
    end;
    removeFromStorage(x,id1,id2,1,player[x].activeChar.PersonalBank,@player[x].activeChar.BankCount,50);

    sendtoplayer(x,a);

    if player[x].maglist.equiped = mid then begin
    a:=#$6C#$00#$00#$00#$2A#$07#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00#$42#$5B#$8A#$94#$42#$A2#$B9#$0D#$01#$00#$01#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;
    move(player[x].maglist.mags[mid],a[$15],$58);
    move(player[x].gid,a[9],4);
    sendtolobby(player[x].room,x,a);
    end;


    {000000: 18 00 00 00 2A 02 00 00 42 5B 8A 94 42 A2 B9 0D     ....*...B[��B��. //mag id
     000010: EA 5C 07 EA E9 9A 52 0D     �\.��R.	//item id?}
end;

procedure DisplayMag(x:integer);
var a:ansistring;
    id1:dword;
begin
    if player[x].maglist.count = 0 then exit;
    if player[x].maglist.equiped = -1 then exit;

    id1:=(gettickcount-player[x].magfeedtime[player[x].maglist.equiped]) div 1000;
    if id1 > player[x].maglist.mags[player[x].maglist.equiped].energy then player[x].maglist.mags[player[x].maglist.equiped].energy:=0
    else dec(player[x].maglist.mags[player[x].maglist.equiped].energy,id1);

    player[x].magfeedtime[player[x].maglist.equiped]:=gettickcount;

    a:=#$6C#$00#$00#$00#$2A#$07#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00#$42#$5B#$8A#$94#$42#$A2#$B9#$0D#$01#$00#$01#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;
    move(player[x].maglist.mags[player[x].maglist.equiped],a[$15],$58);
    move(player[x].gid,a[9],4);
    sendtolobby(player[x].room,-1,a);
end;

//equip mag

procedure EquipMag(x:integer;s:ansistring);
var a:ansistring;
    i:integer;
    id1,id2:dword;
begin
    move(s[9],id1,4);
    move(s[13],id2,4);
    for i:=0 to player[x].maglist.count -1 do begin
        player[x].maglist.mags[i].unk1:=0;
        if (player[x].maglist.mags[i].id1 = id1) and (player[x].maglist.mags[i].id2 = id2) then begin
            player[x].maglist.equiped:=i;
            player[x].maglist.mags[i].unk1:=1;
        end;
    end;
    a:=#$6C#$00#$00#$00#$2A#$07#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00#$42#$5B#$8A#$94#$42#$A2#$B9#$0D#$01#$00#$01#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;

    id1:=(gettickcount-player[x].magfeedtime[player[x].maglist.equiped]) div 1000;
    if id1 > player[x].maglist.mags[player[x].maglist.equiped].energy then player[x].maglist.mags[player[x].maglist.equiped].energy:=0
    else dec(player[x].maglist.mags[player[x].maglist.equiped].energy,id1);

    player[x].magfeedtime[player[x].maglist.equiped]:=gettickcount;

    move(player[x].maglist.mags[player[x].maglist.equiped].id1,a[$15],$58);
    move(player[x].gid,a[9],4);
    sendtolobby(player[x].room,-1,a);
end;

//unequip mag
procedure UnequipMag(x:integer);
var a:ansistring;
begin
    a:=#$1C#$00#$00#$00#$2A#$10#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00#$42#$5B#$8A#$94#$42#$A2#$B9#$0D;
    move(player[x].gid,a[9],4);
    move(player[x].maglist.mags[player[x].maglist.equiped],a[$15],$8);
    sendtolobby(player[x].room,-1,a);
    player[x].maglist.mags[player[x].maglist.equiped].unk1:=0;
    player[x].maglist.equiped:=-1;
end;


//1d = mag type
    //1f = mag form
        {
         type 1
            form 1 = mag

         type 2
            0 = Lyra
            1 = Fornax
            2 = Lepus
            3 = Antila

         type 3
            0 = Libra
            1 = Delphinus
            2 = Cygnus
            3 = Caelum
            4 = Cepheus
            5 = Tucana
            6 = Carina
            7 = Monoceros
            8 = Orion
            9 = Apus
            10 = Corvus
            11 = Leo
            12 = Crux

        }
    //21 = striking lvl
    //23 = striking value
    //25 = ranged lvl
    //27 = ranged value
    //29 = tech
    //2b = tech lvl
    //2d = striking def lvl
    //2f = sdef value
    //31 = rdef lvl
    //33 =  rdef value
    //35 = tdef lvl
    //37 = tdef value
    //39 = dex lvl
    //3b = dex value
    //3e = photon blast
    //3f = energy?
    //43 = auto action 1
    //44 = trigger count
    //46 = auto action 2?
    //4d = first trigger

end.
