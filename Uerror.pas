unit Uerror;

interface
uses types,sysutils;

function MakeError(id:integer;msg:ansistring):ansistring;

const
    ERR_FULL = 10;

    ERR_WRONGUSER = 20;
    ERR_BADTOKEN = 21;
    ERR_BANNED = 22;

    ERR_CHARLIMIT = 30;
    ERR_TEAMLIMIT = 31;

    ERR_SHOPNOTFOUND = 40;
    ERR_ITMSHOPNOTFOUND = 41;
    ERR_SHOPMESETAS = 42;
    ERR_SHOPMISSING = 43;
    ERR_SHOPINVENTORY = 44;
    ERR_SHOPQTY = 45;
    ERR_SHOPRMV = 46;

    ERR_GMBAN = 50;

implementation


function MakeError(id:integer;msg:ansistring):ansistring;
var i:integer;
begin
    msg:='('+inttostr(id)+') '+msg;
    result:=#$D8#$01#$00#$00#$11#$01#$04#$00#$01#$00#$00#$00#$A3#$8A#$00#$00;

    setlength(result,$1d8);
    fillchar(result[17],$1c8,0);
    for i:=0 to length(msg)-1 do
        result[(i*2)+17]:=msg[i+1];
    result:=result+#0#0;
end;

{
//error message
 offset   0  1  2  3  4  5  6  7   8  9  A  B  C  D  E  F
00000000  90 00 00 00 11 2C 00 00  00 00 00 00 01 00 00 00    �....,..........
00000010  00 00 00 00 13 00 01 04  4E 00 00 00 96 00 00 00    ........N...�...
00000020  4E 00 00 00 00 00 00 00  42 00 2D 00 30 00 32 00    N.......B.-.0.2.
00000030  35 00 3A 00 A8 30 AF 30  B9 30 C8 30 E9 30 CF 30    5.:.�0�0�0�0�0�0
00000040  FC 30 C9 30 A8 63 68 59  00 00 00 00 00 00 00 00    �0�0�chY........
00000050  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00    ................
00000060  00 00 00 00 00 00 00 00  D2 BD D0 89 F9 2E 03 00    ........ҽЉ�...
00000070  9C 00 10 27 DC DA 08 00  16 00 00 00 00 00 28 00    �..'��........(.
00000080  4E 00 96 00 00 00 00 00  00 00 00 00 00 00 00 00    N.�.............}

end.
