unit UFriendList;

interface

uses types,sysutils;

procedure GetPlayerQuickInfo(x:integer;s:ansistring);
procedure GetFriendList(x:integer);
procedure UnlockPlayerMenu(x:integer);
procedure InviteFriend(x:integer;s:ansistring);
procedure AcceptFriend(x:integer;s:ansistring);
procedure DeleteFriend(x:integer;id:dword);
procedure AddToBlackList(x:integer;id:dword);
procedure RemoveFromBlackList(x:integer;id:dword);
procedure EditArksCard(x:integer; s:ansistring);
procedure RequestArksCard(x:integer;id:dword);
procedure SendBlackList(x:integer);
procedure GetPlayerInfo(x:integer; id:dword);
procedure GetShortcut(x:integer);
procedure SaveShortcut(x:integer; s:ansistring);
procedure GetAutoWord(x:integer);
procedure SaveAutoWord(x:integer; s:ansistring);
procedure GetVictoryPose(x:integer);
procedure SaveVictoryPose(x:integer; s:ansistring);

implementation

uses UPlayer, fmain, UPsoUtil, UTeamFnc;


procedure GetVictoryPose(x:integer);
var s:ansistring;
begin
    s:=#$14#$00#$00#$00#$24#$08#$40#$00#0#0#0#0#0#0#0#0#0#0#0#0;
    safemove(player[x].save.victorypose[0],s,13,$8);
    sendtoplayer(x,s);
end;

procedure SaveVictoryPose(x:integer; s:ansistring);
begin
    move(s[$15],player[x].save.victorypose[0],$8);
end;

procedure GetShortcut(x:integer);
var s:ansistring;
    i:integer;
begin
    setlength(s,$a50);
    move(player[x].save.shortcut[0],s[1],$a50);
    //fillchar(s[1],$a50,0);
    for i:=0 to 9 do
        safemove(i,s,1+(i*$108),4);
    s:=#$68#$0A#$00#$00#$24#$05#$04#$00#$00#$00#$00#$00#$22#$B6#$00#$00+s+#$00#$00#$00#$00#$2C#$B6#$00#$00;
    sendtoplayer(x,s);
end;

procedure SaveShortcut(x:integer; s:ansistring);
var i,l:integer;
begin
    i:=$19;
    while i < length(s) do begin
        move(s[i],l,4);
        if l < 10 then
        move(s[i],player[x].save.shortcut[l*$108],$108);
        inc(i,$108);
    end;

end;

procedure GetAutoWord(x:integer);
var s:ansistring;
    i,l:integer;
begin
    setlength(s,$696c);
    safemove(player[x].save.autoword[0],s,1,$696c);
    l:=0;
    for i:=0 to 102 do begin
        safemove(i,s,1+(i*$106),4);
        s[1+(i*$106)]:=ansichar(i div 3);
        s[2+(i*$106)]:=ansichar(i mod 3);
    end;
    s:=#$7c#$69#$00#$00#$24#$02#$04#$00#$00#$00#$00#$00#$9e#$25#$00#$00+s;
    sendtoplayer(x,s);
end;

procedure SaveAutoWord(x:integer; s:ansistring);
var i,l:integer;
begin
    i:=$19;
    while i < length(s)-4 do begin
        l:=byte(s[i]);
        if l < 34 then
        move(s[i],player[x].save.autoword[(byte(s[i+1])*$106) + (l*$312)],$106);
        inc(i,$106);
    end;

end;



procedure GetPlayerQuickInfo(x:integer;s:ansistring);
var a:ansistring;
    id:dword;
    i,l,c:integer;
    te:TteamEntry;
begin
    move(s[9],id,4);

    for i:=0 to srvcfg.MaxPlayer-1 do
        if (player[i].GID = id) and (playerok(i)) then begin
          a:=#$DC#$00#$00#$00#$1C#$1F#$00#$00#$01#$00#$00#$00#$DC#$A6#$9B#$00
          +#$00#$00#$00#$00#$4D#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
          +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
          +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
          +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
          +#$00#$00#$00#$00#$00#$00#$00#$00#$43#$00#$00#$00#$00#$00#$00#$00
          +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
          +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$01#$04#$01#$00
          +#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00#0#0#0#0#$86#$03#$00#$00
          +#$00#$00#$04#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
          +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
          +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
          +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
          +#$00#$00#$00#$00#$00#$00#$00#$00;

          safemove(id,a,$d,4);
          safemove(player[i].activechar.base.name[0],a,$59,$24);
          for l:=0 to length(player[i].aname)-1 do
            a[$15+(i*2)] := player[i].aname[1+i];


          if player[x].save.teamid <> 0 then begin
             c:=0;
             l:=fileopen(path+'team\'+inttostr(player[x].save.teamid),$40);
             if l>0 then begin
                 fileread(l,te,sizeof(te));
                 fileclose(l);

                 for l:=0 to 199 do
                    if player[x].gid = te.members[l].id then begin
                       a[$97]:=#1;
                       safemove(player[x].save.teamid,a,$99,4);
                       safemove(te.name[0],a,$9d,32);
                       safemove(te.date,a,$c1,4);
                       c:=1;
                    end;
             end;
             if c = 0 then player[x].save.teamid:=0; //team not valid
          end;

          sendtoplayer(x,a);
        end;

end;


procedure GetFriendList(x:integer);
var s:ansistring;
    i,l:integer;
begin
    //send empty list for now
    {Xor: 2E0E  Sub: 73
Xor: 2E1E  Sub: 63   <--
Xor: 2E2E  Sub: 53  xx
Xor: 2E3E  Sub: 43
Xor: 2E4E  Sub: 33  xx
Xor: 2E5E  Sub: 23 66
Xor: 2E6E  Sub: 13  xx
Xor: 2E7E  Sub: 03
Xor: 2E8E  Sub: F3  xx
Xor: 2E9E  Sub: E3
Xor: 2EAE  Sub: D3  xx
Xor: 2EBE  Sub: C3
Xor: 2ECE  Sub: B3  xx
Xor: 2EDE  Sub: A3
Xor: 2EEE  Sub: 93  xx
Xor: 2EFE  Sub: 83
#$DC#$A6#$9B#$00#$00#$00#$00#$00+'M'+#$00+'I'+#$00
	    +#$55#$00#$52#$00#$45#$00#$54#$00#$48#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$43#$00#$65#$00#$72#$00#$65#$00#$7A#$00#$61#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$01#$04#$01#$00#$00#$00#$00#$00#$FF#$FF#$FF#$FF
        +#$00#$00#$00#$00#$FF#$FF#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00  //block?
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$A9#$79#$F0#$57#$94#$89#$F0#$57
        +#$00#$00#$00#$00#$4A#$2D#$7F#$FB;
        }
     //update list status
     for i:=0 to player[x].save.FriendCount-1 do begin
         player[x].save.friendlist[i].info1[0]:=0; //offline
         for l:=0 to srvcfg.MaxPlayer-1 do
            if playerok(l) and (player[l].GID = player[x].save.friendlist[i].id) then begin
                if (player[l].room > 99) then player[x].save.friendlist[i].unk[0] := 1   //in game
                else player[x].save.friendlist[i].unk[0] := 0;   //in lobby
                player[x].save.friendlist[i].info1[0]:=$10401;
                move(player[l].activeChar.base.name, player[x].save.friendlist[i].charname,24);
            end;
     end;

    setlength(s,sizeof(tfriend)*player[x].save.FriendCount);
    safemove(player[x].save.friendlist[0],s,1,sizeof(tfriend)*player[x].save.friendcount);

    {s:=#$DC#$A6#$9B#$00#$00#$00#$00#$00+'M'+#$00+'I'+#$00
	    +#$55#$00#$52#$00#$45#$00#$54#$00#$48#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$43#$00#$65#$00#$72#$00#$65#$00#$7A#$00#$61#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$01#$04#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00  //01 04 01 = onlineflag, in lobby?
        +#$00#$00#$00#$00#$FF#$FF#$00#$00#$3#$00#$00#$00#$00#$00#$00#$00  //FF FF = block? next dword = where(0 = lobby, 1 = quest,2 = room,3=team room)
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$A9#$79#$F0#$57#$94#$89#$F0#$57
        +#$00#$00#$00#$00#$4A#$2D#$7F#$FB;
    player[x].save.friendcount:=1;//to test       }


    s:=#$28#$00#$00#$00#$18#$15#$04#$00#$00#$00#$00#$00#$00#$00#$01#$00
        +GetMagic(player[x].save.friendcount,$2E1E,$63)+s+TextToUni(player[x].aname,$2E1E,$63);
    i:=length(s);
    move(i,s[1],4);
    sendtoplayer(x,s);

    //send invit list
    s:=#$AC#$01#$00#$00#$18#$17#$04#$00#$00#$00#$00#$00+getmagic(length(player[x].invitlist) div $19c,$3901,$61)+player[x].invitlist;
    i:=length(s);
    move(i,s[1],4);
    sendtoplayer(x,s);
end;

procedure UnlockPlayerMenu(x:integer);
begin
    sendtoplayer(x,#$14#$00#$00#$00#$1C#$11#$04#$00#$02#$00#$00#$00#$7C#$0D#$00#$00
	+#$7C#$0D#$00#$00);
end;

procedure InviteFriend(x:integer;s:ansistring);
var i,l:integer;
    id:integer;
    a:ansistring;
begin
    move(s[9],id,4);
    for i:=0 to srvcfg.MaxPlayer-1 do
        if playerok(i) and (player[i].GID = id) then begin
            //send invit to player
            a:=#$A4#$01#$00#$00#$18#$19#$00#$00;
            setlength(a,$1a4);
            fillchar(a[9],$19c,0);
            safemove(id,a,$11,4);
            safemove(player[x].gid,a,$9,4);
            for l:=0 to length(player[x].aname)-1 do   //target player
                a[$19+(l*2)]:= player[x].aname[l+1];
            for l:=0 to length(player[i].aname)-1 do   //sending player
                a[$5d+(l*2)]:= player[i].aname[l+1];

            safemove(s[$15],a,$a1,length(s)-$14);

            l:=Round((now() - 25569) * 86400);
            safemove(l,a,$1a1,4); //date time

            sendtoplayer(i,a);
            player[i].invitlist:=player[i].invitlist+copy(a,9,$19c);

            //send confirmation
            a:=#$7c#$02#$00#$00#$18#$1a#$00#$00;
            setlength(a,$27c);
            fillchar(a[9],$274,0);
            safemove(id,a,$15,4);
            safemove(player[x].gid,a,$d,4);
            for l:=0 to length(player[x].aname)-1 do   //target player
                a[$61+(l*2)]:= player[x].aname[l+1];
            for l:=0 to length(player[i].aname)-1 do   //sending player
                a[$1d+(l*2)]:= player[i].aname[l+1];

            safemove(s[$15],a,$a5,length(s)-$14);

            l:=Round((now() - 25569) * 86400);
            safemove(l,a,$1a5,4); //date time
            l:=$4FFFF;
            safemove(l,a,$22d,4); //unk

            sendtoplayer(x,a);

        end;
end;

procedure addfriend(x,p:integer);
var i,l:integer;
begin
    if player[x].save.FriendCount >= 100 then begin
         sendsystemmsg(x,'Friend limit reatched');
         exit; 
    end;

    for i:=0 to player[x].save.FriendCount-1 do begin
        if player[x].save.FriendList[i].id = player[p].GID then exit;
    end;
    i:=player[x].save.Friendcount;
    inc(player[x].save.Friendcount);

    fillchar(player[x].save.FriendList[i],sizeof(tfriend),0);
    player[x].save.FriendList[i].id:=player[p].GID;

    for l:=1 to length(player[p].aname) do player[x].save.FriendList[i].playername[l-1]:=widechar(byte(player[p].aname[l]));
    player[x].save.FriendList[i].time1:=Round((now() - 25569) * 86400); //friend time

end;


procedure AcceptFriend(x:integer;s:ansistring);
var i,l,k,j:integer;
    id1,id2:dword;
    a:ansistring;
begin
    i:=1;
    move(s[9],id1,4);
    while i < length(player[x].invitlist) do begin
        move(player[x].invitlist[i],id2,4);
        if id1 = id2 then begin
            //confirm
            for k:=0 to srvcfg.MaxPlayer-1 do
            if playerok(k) and (player[k].GID = id1) then begin
                a:=#$E4#$00#$00#$00#$18#$1C#$00#$00;
                setlength(a,$e4);
                fillchar(a[9],$dc,0);
                a[$d]:=#1;
                safemove(player[k].gid,a,$11,4);
                for l:=0 to length(player[k].aname)-1 do   //target player
                    a[$19+(l*2)]:= player[k].aname[l+1];

                safemove(player[k].activechar.base.name[0],a,$5d,$24); //char name
                j:=$10401;
                safemove(j,a,$81,4);

                sendtoplayer(x,a);
                //add it to the player list
                addfriend(x,k);


                //confirm to the other player
                fillchar(a[9],$58,0);
                safemove(player[x].gid,a,$11,4);
                for l:=0 to length(player[x].aname)-1 do   //target player
                    a[$19+(l*2)]:= player[x].aname[l+1];
                safemove(player[x].activechar.base.name[0],a,$5d,$24); //char name
                delete(a,9,4);
                a[1]:=#$e0;
                a[6]:=#$1d;
                sendtoplayer(k,a);
                addfriend(k,x);

            end;
            delete(player[x].invitlist,i,$19c);
            break;
        end else inc(i,$19c);

    end;

end;

procedure DeleteFriend(x:integer;id:dword);
var i:integer;
begin
    for i:=0 to player[x].save.FriendCount-1 do
        if player[x].save.FriendList[i].id = id then begin
            dec(player[x].save.FriendCount);
            move(player[x].save.FriendList[i+1],player[x].save.FriendList[i],(player[x].save.FriendCount-i)*sizeof(tfriend));
            GetFriendList(x);
            break;
        end;
end;

procedure RemoveFromBlackList(x:integer;id:dword);
var i,l:integer;
    s:ansistring;
begin
    for i:=0 to player[x].save.blacklistcount-1 do
        if player[x].save.blacklist[i].gid = id then begin
            for l:=i+1 to player[x].save.blacklistcount-1 do
                move(player[x].save.blacklist[l],player[x].save.blacklist[l-1],$54);
            dec(player[x].save.blacklistcount);
            s:=#$14#$00#$00#$00#$16#$05#$00#$00#$06#$DE#$9C#$00#$00#$00#$00#$00
	            +#$00#$00#$00#$00 ;
            safemove(id,s,9,4);
            sendtoplayer(x,s); //confirm
            break;
        end;
end;

procedure AddToBlackList(x:integer;id:dword);
var i:integer;
    s:ansistring;
begin
    for i:=0 to srvcfg.MaxPlayer-1 do
        if player[i].GID = id then begin
          if player[x].save.blacklistcount < 99 then begin
              player[x].save.blacklist[player[x].save.blacklistcount].gid:=id;
              move(player[i].save.ArksName[0],player[x].save.blacklist[player[x].save.blacklistcount].name[0],$40);
              player[x].save.blacklist[player[x].save.blacklistcount].u2:=$586985c3;
              player[x].save.blacklist[player[x].save.blacklistcount].e1:=0;
              player[x].save.blacklist[player[x].save.blacklistcount].u1:=0;
              player[x].save.blacklist[player[x].save.blacklistcount].u3:=0;
              setlength(s,$54);
              safemove(player[x].save.blacklist[player[x].save.blacklistcount],s,1,$54);
              s:=#$5C#$00#$00#$00#$16#$03#$00#$00+s;
              sendtoplayer(x,s);
              inc(player[x].save.blacklistcount);
          end;
        break;
    end;
end;

procedure SendBlackList(x:integer);
var i:integer;
    s:ansistring;
begin
    for i:=0 to player[x].save.blacklistcount-1 do begin
              setlength(s,$54);
              safemove(player[x].save.blacklist[i],s,1,$54);
              s:=#$5C#$00#$00#$00#$16#$03#$00#$00+s;
              sendtoplayer(x,s);
    end;
end;


procedure EditArksCard(x:integer; s:ansistring);
begin
    move(s[$15],player[x].save.arkscard[0],$794);
end;

procedure RequestArksCard(x:integer;id:dword);
var i,l:integer;
    s:ansistring;
begin
    for i:=0 to srvcfg.MaxPlayer-1 do
        if player[i].GID = id then begin
            setlength(s,$794);
            safemove(player[i].save.arkscard[0],s,1,$794);
            s:=#$38#$08#$00#$00#$27#$02#$00#$00#$B1#$88#$69#$58#$56#$5F#$9C#$00
            +#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0
            +#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0
            +#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0
            +#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0
            +#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0
            +#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0
            +#$00#$00#$00#$00#$00#$20#$20#$20#$20#$20#$20#$20#$20#$20#$20#$20
            +#$20#$20#$20#$20#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0 + #0#0#0#0+s;
            l:=random($7FFFFFFF);
            safemove(l,s,9,4);
            safemove(id,s,$d,4);
            safemove(player[i].save.ArksName[0],s,$15,$40);
            safemove(player[i].activeChar.base.name[0],s,$55,32);
            sendtoplayer(x,s);
            break;
        end;
end;

procedure GetPlayerInfo(x:integer; id:dword);
var s:ansistring;
    i:integer;
begin
    s:=#$9C#$02#$00#$00#$11#$3A#$04#$00#$8A#$85#$CD#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00#$76#$56#$66#$05#$8A#$85#$CD#$00#$2A#$66#$23#$57
	+#$F1#$00#$28#$23#$00#$00#$16#$43#$09#$74#$00#$30#$E0#$7F#$34#$92
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$01#$00#$00#$00#$90#$E8#$8D#$E1#$A8#$E4#$F0#$D8#$5C#$E0#$43#$FB
	+#$10#$27#$09#$03#$EC#$FD#$0E#$FD#$F5#$E8#$10#$F9#$D5#$F6#$0F#$EE
	+#$A4#$1F#$44#$03#$9A#$F6#$EF#$0B#$3A#$D9#$F0#$D8#$0C#$EB#$F0#$D8
	+#$F0#$D8#$46#$16#$B8#$FF#$08#$0E#$10#$27#$68#$F5#$1C#$DA#$27#$01
	+#$F0#$D8#$F0#$D8#$EE#$0D#$89#$E1#$F0#$D8#$4B#$F7#$91#$F1#$CE#$F8
	+#$53#$19#$90#$E8#$8D#$E1#$A8#$E4#$F0#$D8#$5C#$E0#$43#$FB#$10#$27
	+#$09#$03#$EC#$FD#$0E#$FD#$F5#$E8#$10#$F9#$89#$E1#$F0#$D8#$4B#$F7
	+#$91#$F1#$CE#$F8#$53#$19#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$10#$00#$00#$00#$00#$00#$00#$00#$00#$00#$0E#$7F#$15#$03
	+#$F3#$1A#$0E#$7F#$15#$03#$F3#$1A#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$FF#$83#$D7#$65#$10#$27#$00#$00#$00#$00#$00#$00
	+#$2F#$12#$DC#$85#$64#$19#$94#$15#$5F#$46#$B0#$1D#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$9C#$3D#$10#$27
	+#$3C#$05#$15#$00#$3C#$28#$8E#$28#$2E#$27#$2E#$27#$3A#$27#$1A#$36
	+#$00#$00#$4E#$3E#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$31#$75
	+#$35#$75#$10#$27#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$88#$08#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$E8#$05#$02#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$05#$03#$03#$00#$FF#$02#$00#$00#$4B#$00#$55#$00#$A3#$20#$69#$02
	+#$4A#$00#$4F#$00#$E0#$2F#$45#$02#$01#$00#$01#$00#$00#$00#$00#$00
	+#$4B#$00#$50#$00#$1B#$58#$5B#$02#$28#$00#$28#$00#$9A#$DF#$15#$00
	+#$40#$00#$45#$00#$A3#$D1#$34#$01#$4B#$00#$59#$00#$7B#$AA#$69#$02
	+#$4B#$00#$55#$00#$16#$FC#$70#$02#$01#$00#$00#$00#$00#$00#$00#$00
	+#$02#$00#$02#$00#$55#$01#$00#$00#$01#$00#$01#$00#$00#$00#$00#$00
	+#$01#$00#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$4B#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$29#$59#$CC#$58#$6E#$30#$80#$62#$E7#$5D#$05#$80#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$A7#$52#$00#$00#$75#$30#$7E#$30#$7F#$30#$06#$26
	+#$6A#$30#$45#$30#$01#$FF#$00#$00#$7E#$1C#$00#$00;
    for i:=0 to srvcfg.MaxPlayer-1 do
        if player[i].GID = id then begin
            safemove(id,s,9,4);
            safemove(id,s,$19,4);
            safemove(player[i].activeChar.charID,s,$15,4);
            safemove(player[i].activeChar.base.name[0],s,$29,$26c);
            sendtoplayer(x,s);
            break;
        end;
end;

end.
