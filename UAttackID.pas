unit UAttackID;

interface
    uses types,uforest;
    type
        TAttackIDDef = record
            id:dword;
            action:integer;
            types,element:integer;
            mul:single;
            PA:integer;
        end;

const

    AT_SATK = 0;
    AT_RATK = 1;
    AT_TATK = 2;

    {ACTION_NONE = 0;
    ACTION_ATTACK = 1;
    ACTION_USE = 3;
    ACTION_DEF = 4;
    ACTION_BOOST = 5;

    ELE_NONE = 0;
    ELE_FIRE = 1;
    ELE_ICE = 2;
    ELE_LIGHTNING = 3;
    ELE_WIND = 4;
    ELE_LIGHT = 5;
    ELE_DARK = 6;
    ELE_MIX = 7;}
Action0601count = 640;
Action0601 : array[0..639] of TAttackIDDef = (
//lava
    (id: $D9A05A68; action: ACTION_BOOST; types: AT_SATK; element: ELE_FIRE; mul: 1; PA: 601;),
//wave hit
    (id: $AF38147A; action: ACTION_ATTACK; types: AT_SATK; element: ELE_DARK; mul: 3; PA: 0;),
    (id: $124D063F; action: ACTION_ATTACK; types: AT_SATK; element: ELE_DARK; mul: 2; PA: 0;),
//machine gun hit
    (id: $6E67249F; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 3; PA: 0;),
//falling stalactite
    (id: $987921AA; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.5; PA: 0;),
//flame burst
  (id: $D05177D1; action: ACTION_ATTACK; types: AT_SATK; element: ELE_FIRE; mul: 2.5; PA: 0;),
//burn
    (id: $37D9599A; action: ACTION_BOOST; types: AT_TATK; element: ELE_FIRE; mul: 1; PA: 10;),
    (id: $37D95996; action: ACTION_BOOST; types: AT_TATK; element: ELE_FIRE; mul: 1; PA: 10;),
    (id: $37D95997; action: ACTION_BOOST; types: AT_TATK; element: ELE_FIRE; mul: 1; PA: 10;),
    (id: $37D95999; action: ACTION_BOOST; types: AT_TATK; element: ELE_FIRE; mul: 1; PA: 10;),
    (id: $37D95998; action: ACTION_BOOST; types: AT_TATK; element: ELE_FIRE; mul: 1; PA: 10;),
//mine
    (id: $0159BA31; action: ACTION_ATTACK; types: AT_SATK; element: ELE_FIRE; mul: 1; PA: 0;), // trap
    (id: $502B8778; action: ACTION_ATTACK; types: AT_SATK; element: ELE_FIRE; mul: 1; PA: 0;),
//recovery pod
    (id: $25C4E46E; action: ACTION_BOOST; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 1;),

//Thunder
    (id: $E354F91F; action: ACTION_BOOST; types: AT_SATK; element: ELE_LIGHTNING; mul: 1; PA: 600;),
//[Enemy : Dark]
//Dagan
	(id: $D07C566C; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // beat

//[Enemy : naberius , forest]

//ArmadilloA
    (id: $178B168F; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.2; PA: 0;),

//Nab Rappy
	(id: $A5B501B0; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // 1st hit
	(id: $A5B501B3; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.2; PA: 0;), // 2nd hit

//Oodan
	(id: $D07C566C; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // beat
	(id: $6409CDE8; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.4; PA: 0;), // bump

//Za Oodan
	//(id: $FE727BAD; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.8; PA: 0;), // pick stone
	(id: $CAA32B8B; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1; PA: 0;), // throw stone
	(id: $6023A6AD; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // stone beat

//Gulf
	(id: $6023A6AD; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), //

//Fangulf  must code howl pa
	(id: $C16CF0B3; action: ACTION_BOOST; types: AT_TATK; element: ELE_NONE; mul: 1; PA: $101;), // howl // atk,def up
	(id: $6023A6AD; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // beat

//Ptero
	(id: $6F640D69; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.9; PA: 0;), //   //ratk
	(id: $3F95848B; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), //

//big beast
	(id: $C178C34A; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 0;), //  take me
	(id: $BD8D4630; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.9; PA: 0;), // crunch
	(id: $8D6111B6; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.2; PA: 0;), // trow
	(id: $70A5532D; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.3; PA: 0;), // jump on me
	(id: $65D110C2; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // punch
	(id: $7BD807FD; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // side punch
    (id: $53684E96; action: ACTION_BOOST; types: AT_SATK; element: ELE_NONE; mul: 1; PA: $102;), //rage
    (id: $6A1622D1; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: $0;), // PunchC
    (id: $E0BC65BF; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: $0;), // LurchBG

//mini boss
    (id: $5A8E1ECA; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.4; PA: 0;), // dash attack
	(id: $39F88D1C; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // sword
	(id: $2C89287F; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1; PA: 0;), // bullet


//[evemy Cave]
  //dragon raptor
   (id: $7AEEB8A4; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.1; PA: 0;), // dash attack
	(id: $74808049; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // tails attack

	//lizard man
  (id: $F4D1DB1C; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.1; PA: 0;), // physical attack
	(id: $EB5F435D; action: ACTION_ATTACK; types: AT_TATK; element: ELE_NONE; mul: 1; PA: 0;), // magic attack

  //dragon wolf
  (id: $5DE8B9B3; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.1; PA: 0;), // physical attack


//[item]
	(id: $3776D7E0; action: ACTION_USE; types: 0; element: ELE_NONE; mul: 1; PA: 1;), // monomate
	(id: $2BAA2074; action: ACTION_USE; types: 0; element: ELE_NONE; mul: 1; PA: 2;), // dimate
	(id: $7A820976; action: ACTION_USE; types: 0; element: ELE_NONE; mul: 1; PA: 3;), // Trimate
	(id: $5D77FE5F; action: ACTION_USE; types: 0; element: ELE_NONE; mul: 1; PA: 4;), // StarAtomizerS
	(id: $FF65789D; action: ACTION_USE; types: 0; element: ELE_NONE; mul: 1; PA: 5;), // TelepipeS
	(id: $78BF35AB; action: ACTION_USE; types: 0; element: ELE_NONE; mul: 1; PA: 8;), // Moon
    (id: $2831fd70; action: ACTION_USE; types: 0; element: ELE_NONE; mul: 1; PA: 9;), // SolAtomizerS

//ShiftaRide
	(id: $BDAF57F8; action: ACTION_USE; types: 0; element: ELE_NONE; mul: 1; PA: 6;), // 1st ShiftaRideS		//use ShiftaRide and DebandRideS, send 5 of 106 packs
	(id: $9E42296E; action: ACTION_BOOST; types: 0; element: ELE_NONE; mul: 1; PA: 6;), // 2-5 Tech:ShiftaRide

//DebandRide
	(id: $90C92F5B; action: ACTION_USE; types: 0; element: ELE_NONE; mul: 1; PA: 7;), // 1st DebandRideS
	(id: $ED643527; action: ACTION_BOOST; types: 0; element: ELE_NONE; mul: 1; PA: 7;), // 2-5 Tech:DebandRide

    //[Unarmed]
//Normal Attack
	(id: $C4F06D1F; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // 1		//stand ground
	(id: $98075D68; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.1; PA: 0;), // 2
	(id: $316C3207; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.2; PA: 0;), // 3
	(id: $B0CB0465; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // 1		//air
	(id: $2A1DB4CC; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.1; PA: 0;), // 2
	(id: $383E173C; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.2; PA: 0;), // 3

//Step Attack
	(id: $BBCE2188; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.3; PA: 0;), //

 //right hook �����Դ������ҹ�ȭ
	(id: $B7ED4F1B; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.4; PA: 0;), //

    //[GunSlash ǹ��]
    //Normal Attack slash
	(id: $73EC32F1; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // 1 //slash stand ground
	(id: $61ED37DF; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.1; PA: 0;), // 2
	(id: $2406C334; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.2; PA: 0;), // 3
	(id: $61EDB857; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // 1 //slash air
	(id: $AA93DB34; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.1; PA: 0;), // 2
	(id: $11451B94; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.2; PA: 0;), // 3
	(id: $80FF707A; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1; PA: 0;), // 1 //shoot
	(id: $82F47A6D; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1.1; PA: 0;), // 2
	(id: $6051A354; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1.2; PA: 0;), // 3

        //Step Attack
	(id: $64AEB58B; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.3; PA: 0;), //

        //Tri-Impact �ȥ饤����ѥ���
	(id: $BEB4D9D2; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.8; PA: 49;), // 1-2 hit	//slash
	(id: $E302840D; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1; PA: 49;), // 3rd hit

    //Rage Dance �쥤������
	(id: $8EEA239D; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.7; PA: 50;), // 1-5	//slash
	(id: $F290C9AF; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.9; PA: 50;), // 6		//shoot

    //Ein Raketen ������饱�`�ƥ�
	(id: $1F390014; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 51;), // 1st hit	//slash
	(id: $6B7CBF04; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1; PA: 51;), // 2rd hit	//shoot

    //Serpent Air ���`�ڥ�ȥ���
	(id: $A5F3EDBB; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.7; PA: 52;), // 1-4	//slash
	(id: $1939086D; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.8; PA: 52;), // 5

    //Aiming Shot �����ߥ󥰥���å�
	(id: $22679535; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.9; PA: 53;), //
	(id: $2267952A; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1.6; PA: 53;), // S

    //Thrillsplosion �����`�ץ��`��
    (id: $96EF9CD4; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.7; PA: 54;), //

    //Slash Rave ����å���쥤��
	(id: $6047E619; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 55;), // 1
	(id: $FFC8B6FE; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 55;), // 2-4
	(id: $FFC8B6FF; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 55;), //5
	(id: $810850BA; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.6; PA: 55;), // 6
	(id: $451B7993; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 55;), // 7
	
    //Additional Bullet ���ǥ������Х�å�
	(id: $B9CD0B65; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.8; PA: 56;), // 1		//kick
	(id: $40DD0B74; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.8; PA: 56;), // 2-4	//shoot

    //Kreisenschlag ���饤���󥷥��`��
	(id: $6F8C3467; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 133;), // 1
	(id: $6BC79B1E; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.7; PA: 133;), // 2

    //Regenschlag ��`���󥷥��`��
	(id: $819CA4A9; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.8; PA: 149;), // 1
	(id: $05897D7B; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.9; PA: 149;), // 2
	(id: $05897D7A; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.7; PA: 149;), // 3-5

    //Strezwei ����ȥ�ĥ�����
	(id: $83319C27; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.7; PA: 177;), // 1
    (id: $1A04ADD8; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.7; PA: 177;),
	(id: $4915F7E4; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.75; PA: 177;), // 2-4
	(id: $4915F7E2; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.8; PA: 177;), // 5
    (id: $4915F0A1; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.75; PA: 177;),

    //[Sword ��]
    //Normal Attack
	(id: $E3505742; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // 1	//stand ground
	(id: $E3505743; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.1; PA: 0;), // 2
	(id: $E3505740; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.2; PA: 0;), // 3
	(id: $E353FA83; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // 1 //air
	(id: $E353FA80; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.1; PA: 0;), // 2
	(id: $E353FA81; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.2; PA: 0;), // 3

    //Step Attack
	(id: $E35319FD; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.3; PA: 0;), //

//Rising Edge �饤���󥰥��å� ����
	(id: $B895727D; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.8; PA: 1;), //
	(id: $B895727D; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.8; PA: 1;), //  S 1hit
	(id: $E35284BB; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.8; PA: 1;), //  S 2-3 hit

//Twister Fall �ĥ������`�ե��`�루��ն��
	(id: $E35284BA; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.75; PA: 2;), //  1		//no diffence by gear max
	(id: $48FFCD09; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.9; PA: 2;), //  2
    (id: $48FFCD0E; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.9; PA: 2;), //  3
    (id: $48FFCD15; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.9; PA: 2;), //  4

//Nova Strike �Υ������ȥ饤�� ����ն
	(id: $48FFCDCD; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.8; PA: 3;), //  S,SS 1st hit
	(id: $48FFCDD6; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.8; PA: 3;), //  S,SS 2nd hit
	(id: $C29D8911; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.8; PA: 3;), //  S,SS 3rd hit	//if not focus , only this hit

//Ride Slasher �饤�ɥ���å���` ���ϴ�ͻ��
	(id: $E35284B8; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.8; PA: 4;), //

//Over End ���`�Щ`����� ��ର�
	(id: $48FFCE4B; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 5;), //  1-5
	(id: $CCE4BAA4; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.7; PA: 5;), //  6
	(id: $48FFCE4F; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.7; PA: 5;), //  1-5  //gear max
	(id: $CCE4BAA4; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.5; PA: 5;), //  6

//Stun Concido �����󥳥󥵥��� ǿ��
	(id: $E35284BE; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.6; PA: 6;), //

//Sonic Arrow ���˥å������� Բ��
	(id: $48FFCEC9; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.6; PA: 7;), //
	(id: $48FFCEC8; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 7;), //  S
	(id: $B880CF83; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.2; PA: 7;), //  S
	(id: $48FFCECD; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 7;), //  SS

//Cruel Throw ����`���륹���` �̴�Ͷ��
	(id: $C8147DBE; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.7; PA: 8;), //  1st hit
	(id: $E35284BC; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.7; PA: 8;), //  2
	(id: $97B89BBA; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.2; PA: 8;), //  3
	(id: $810850BA; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.7; PA: 8;), //  4 //throw hit other enemy

//Guilty Break ����ƥ��֥쥤�� ��ײһ��
	(id: $48FFC342; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.9; PA: 127;), //
	(id: $4ACC8243; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.9; PA: 127;), //
	(id: $48FFC341; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.95; PA: 127;), // S
	(id: $4ACC72D0; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.95; PA: 127;), // S
	(id: $48FFC35E; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 127;), // SS
	(id: $4AAA1C47; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 127;), // SS

//Sacrifice's Bite ������ե������Х��� ��ȡ������
	(id: $FFFE448C; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.7; PA: 143;), // 1		//hit enemy
	(id: $0BABA608; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 143;), // 2-4	//hit enemy
	(id: $33F5D58D; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.9; PA: 143;), // 5		//hit enemy
	(id: $4908FBA3; action: ACTION_BOOST; types: AT_SATK; element: ELE_NONE; mul: 40; PA: 143;), // 1					//buff self lv1
	(id: $4908FBA0; action: ACTION_BOOST; types: AT_SATK; element: ELE_NONE; mul: 50; PA: 143;), // 2					//buff self lv2
	(id: $4908FBA1; action: ACTION_BOOST; types: AT_SATK; element: ELE_NONE; mul: 60; PA: 143;), // 3					//buff self lv3

//Ignition Parry �����ʥ��ȥѥ��
	(id: $48FFCD63; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 176;), // 1-4		//S,SS same
	(id: $D81B8896; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.85; PA: 176;), // 5-6
    (id: $EFFCDF37; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.85; PA: 176;), // //JustGuard sucess
	(id: $B77AD4DD; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.85; PA: 176;), // //JustGuard sucess
	(id: $D81B8897; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.9; PA: 176;), // S 5-6
	(id: $D81B889A; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.95; PA: 176;), // SS 5-6

    //[Partisan ��ì]
//Normal Attack
	(id: $F27DB47B; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // 1 	//stand ground
	(id: $8509121C; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.1; PA: 0;), // 2
	(id: $0C46EF03; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.2; PA: 0;), // 3
	(id: $7282E5F5; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // 1 //air
	(id: $D24358BA; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.1; PA: 0;), // 2
	(id: $5F3A3D07; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.2; PA: 0;), // 3

//Step Attack
	(id: $317DAF5C; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.3; PA: 0;), //

//Slide Shaker ���饤�ɥ��������` ��ת
	(id: $31A6C073; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 17;), // 1-5
	(id: $884A3652; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 17;), // 6
	(id: $311BFE68; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 17;), // 1-5 //use gear
	(id: $0B439185; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 17;), // 6

//Speed Rain ���ԩ`�ɥ쥤�� ն��
	(id: $1F8C0E57; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.7; PA: 18;), // 1-5
	(id: $71440EF6; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.7; PA: 18;), // 1-5	//use gear

//Rising Flag �饤���󥰥ե�å� ����
	(id: $9561C138; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.7; PA: 19;), // 1
	(id: $06F50868; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.4; PA: 19;), // 2
	(id: $6B957844; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.7; PA: 19;), // 1		//use gear
	(id: $F1B6ACA5; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.4; PA: 19;), // 2

//Zenith Throw �ԩ`�����åץ����` ͻ��
	(id: $392941CE; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.7; PA: 20;), // 1-2
	(id: $A7A730DB; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.7; PA: 20;), // 3
	(id: $BF62BBCE; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 20;), // 4
	(id: $19DDA7A0; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 20;), // 5
	(id: $1BEC9341; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.7; PA: 20;), // 1-2	//use gear
	(id: $C49D7714; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.7; PA: 20;), // 3
	(id: $CA5C9575; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 20;), // 4
	(id: $ADAF69ED; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 20;), // 5

//Trick Rave �ȥ�å��쥤�� ���û�ǹ��
	(id: $FE09A5C4; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.8; PA: 21;), // 1-5
	(id: $2E141F2A; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.8; PA: 21;), // 6
	(id: $C142A9C7; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.8; PA: 21;), // 1-5	//use gear
	(id: $5F517723; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.8; PA: 21;), // 6
//Slide End ���饤�ɥ���� �ӻ�
	(id: $E07EA778; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.95; PA: 22;), //
    (id: $ED9D5239; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.6; PA: 22;), // S
	(id: $4FA15304; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.95; PA: 22;), //	//use gear
    (id: $59FFB992; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.6; PA: 22;), // S

//Assault Buster ������ȥХ����` �ӽ�ͻ��
	(id: $24E2416A; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.85; PA: 23;), //
    (id: $510C02AB; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.85; PA: 23;), // S
	(id: $1ED35CE7; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.6; PA: 23;), //	//use gear
    (id: $429CB054; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.6; PA: 23;), // S

//Bandersnatch �Х󥿩`���ʥå� ��������
	(id: $9AB4767A; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.85; PA: 24;), //
	(id: $C19A282E; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.85; PA: 24;), //  //use gear

//Sacred Skewer ��������åɥ����奢 �콵��ì
	(id: $EFEDBA14; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.85; PA: 129;), //
	(id: $68600B9B; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.85; PA: 129;), //  //use gear

//Vol Graptor �����륰��ץ��` ��ըǹ
	(id: $9B47D6DF; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.6; PA: 145;), // 1
	(id: $37D8EDC9; action: ACTION_BOOST; types: AT_SATK; element: ELE_NONE; mul: 1.6; PA: 145;), // 2 //to self

//Tear Grid �ƥ����`������å�
	(id: $009EC459; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.5; PA: 23;), //
    (id: $4A446B90; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.3; PA: 23;), // end
	(id: $7B9B5392; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.5; PA: 23;), //  //use gear
    (id: $14C4870D; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.3; PA: 23;), // end


    //[WLance ����]
//Normal Attack
	(id: $BC4794F5; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // 1
	(id: $BC4794F4; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.1; PA: 0;), // 2-3
	(id: $BC4794F7; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.2; PA: 0;), // 4

//Step Attack
	(id: $BC476EB0; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.3; PA: 0;), //

//Bind Through �Х���ɥ���` ˤ
	(id: $570004E2; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 9;), // 1
	(id: $BC4769C6; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 9;), // 2
	(id: $512B9E56; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.5; PA: 9;), // 3
	(id: $ABD09C73; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.5; PA: 9;), // 4
	(id: $570004E2; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 9;), // 1		//use a gear
	(id: $6364AC37; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 9;), // 2
	(id: $89B66E90; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.5; PA: 9;), // 3
	(id: $ABD09C73; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.5; PA: 9;), // 4

//Grapple Charge ����åץ����`�� ��
	(id: $C73B1FE5; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 10;), // 1
	(id: $BC4769C7; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.5; PA: 10;), // 2
	(id: $034E1D57; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.1; PA: 10;), // //if 1st hit miss��this instead of BC4769C7 ,damage is less than 30%.

//Heavenly Fall �إ֥��`�ե��`�� ˤ
	(id: $31AF6BC5; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 11;), // 1
	(id: $BC4769C4; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 11;), // 2
	(id: $5B09F8BE; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.5; PA: 11;), // 3
	(id: $B848970F; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.5; PA: 11;), // 4

//Other Cyclone �����`���������� ��ת
	(id: $2B67147A; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.7; PA: 12;), // 1
	(id: $5A037AC4; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.7; PA: 12;), // 2-5
	(id: $BC4769C5; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.7; PA: 12;), // 6
	(id: $97202F09; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.7; PA: 12;), // 7

//Other Spin �����`���ԥ� ������
	(id: $06B2B7CA; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 13;), // 1
	(id: $631B3324; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.85; PA: 13;), // 2-4
	(id: $BC4769DA; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 13;), // 5
	(id: $7D6AA340; action: ACTION_BOOST; types: AT_SATK; element: ELE_NONE; mul: 1; PA: $10c;), // enemy faint start
	(id: $A396B6F3; action: ACTION_BOOST; types: AT_SATK; element: ELE_NONE; mul: 1; PA: $10d;), // enemy faint end

//Holding Current �۩`��ǥ��󥰥����� ���
	(id: $D379CB9A; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.5; PA: 14;), // 1
	(id: $6364A2E4; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.5; PA: 14;), // 2
	(id: $623DD64B; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.5; PA: 14;), // 3-6
	(id: $6364A2E8; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 14;), // 7-9
	(id: $BC4769DB; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.5; PA: 14;), // 10

//Air Pocket Swing �����ݥ��åȥ����� ��ǧ
	(id: $BC4769D8; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.75; PA: 15;), //

//Cerberus Dance ���`�٥饹���� ɨ
	(id: $BC4769D9; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 16;), // 1-4
	(id: $F084DF77; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.9; PA: 16;), // 5

//Wild Round �磻��ɥ饦��� ˫�ڹ��Ӷ�
	(id: $BC4769DE; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.7; PA: 128;), //

//Kaiser Rise �������`�饤�� ��Ȫ
	(id: $6364AC1C; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.75; PA: 144;), //

//Adapting Spin �����ץȥ��ԥ�
	(id: $6364AC1D; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 176;), //       181



    //[Knuckle ȭ��]
//Normal Attack
	(id: $C2FC1D96; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // 1		//stand ground
	(id: $556C5930; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.1; PA: 0;), // 2
	(id: $A94D4112; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.2; PA: 0;), // 3
	(id: $4BF910D0; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // 1		//air
	(id: $B7FDF797; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.1; PA: 0;), // 2
	(id: $63CB22C7; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.2; PA: 0;), // 3

//Step Attack
	(id: $62A35812; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.3; PA: 0;), //

//Ducking Blow ���å��󥰥֥��� ��Ǳһ��
	(id: $A2AEFEA7; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.6; PA: 41;), //

//Flicker Jab �ե�å��`����� 3����
	(id: $4C5B05C1; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.9; PA: 42;), //

//Pendulum Roll �ڥ�ǥ�����`�� 8��ȭ
	(id: $B1355330; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.8; PA: 43;), //

//Slide Uppercut ���饤�ɥ��åѩ` �Ϲ�ȭ
	(id: $E0FB8B86; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.6; PA: 44;), //

//Straight Charge ���ȥ쥤�ȥ���`�� ͻ��ȭ
	(id: $A3D5A199; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.65; PA: 45;), // 1-2
	(id: $241650EC; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.7; PA: 45;), // 3
	(id: $21622775; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.9; PA: 45;), // 1-2 S
	(id: $96DA68EE; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.95; PA: 45;), // 3

//Quake Howling ���������ϥ���� ���
	(id: $397E17C7; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.6; PA: 46;), //
	(id: $7D6AA340; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: $10c;), // enemy faint
	(id: $A396B6F3; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: $10d;), // enemy faint end

//Surprise Knuckle ���ץ饤���ʥå��� 3��ȭ
	(id: $76DAB0E0; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.9; PA: 47;), //

//Flash Thousand �ե�å��奵������� �����Ҵ�
	(id: $FB22B9E5; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.7; PA: 48;), //
	(id: $4FEAD176; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 48;), //

//Backhand Smash �Хå��ϥ�ɥ��ޥå��� �ص���ȭ
	(id: $9FCBE777; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.5; PA: 132;), //

//Meteor Fist ��ƥ��ե����� ȭ֮��
	(id: $D896DD2F; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 148;), // 1st hit //to self , alpha bug, this skill no other hit
	(id: $D896DD2E; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.4; PA: 148;), // 1st hit S

//Heartless Impact �ϩ`�ȥ쥹����ѥ���
    (id: $4D5D4C11; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.7; PA: 172;), // 1
	(id: $F87D76CB; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.4; PA: 172;), // 2    //207


    //[DSaber ˫ͷ��]
//Normal Attack
	(id: $4EBBB363; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // 1-2		//stand ground
	(id: $404AE815; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.1; PA: 0;), // 3
	(id: $2ABE93B8; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.2; PA: 0;), // 4-6
	(id: $08F2CA4F; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // 1-2		//air
	(id: $177C08AC; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.1; PA: 0;), // 3-4
	(id: $B0F23739; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.2; PA: 0;), // 5-6

//Step Attack
	(id: $F4487611; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.3; PA: 0;), //

//Tornado Dance �ȥ�ͩ`�ɥ��� ����ͻ
	(id: $175E0455; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.75; PA: 33;), //

//Scissor Edge �����`���å� ����
	(id: $FAEE7953; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.15; PA: 34;), //  1
	(id: $4114C62A; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 34;), //  2-5

//Rumbling Moon ���֥�󥰥�`�� ����
	(id: $1CF04533; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 35;), //  1-4
	(id: $C9B1685E; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.1; PA: 35;), //  5

//Illusion Rave �����`�����쥤������Ӱ��
	(id: $9B577BAB; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.55; PA: 36;), //  1-7
	(id: $9B577BAB; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.55; PA: 36;), //  8-14
	(id: $93441200; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.9; PA: 36;), //  15

//Fake Capture �ե���������ץ��� ˤ
	(id: $984EBC52; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 37;), //  1
	(id: $F08A08C7; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 37;), //  2
	(id: $62DE6249; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 37;), //  3
	(id: $0C59C687; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 37;), //  4
	(id: $810850BA; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 37;), //  5	//enemy impact other enemy

//Surprise Dunk ���ץ饤������ ����
	(id: $2F2987A9; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.65; PA: 38;), //  1-3
	(id: $5AA0DE56; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.1; PA: 38;), //  4
	(id: $7D6AA340; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: $10c;), //  enemy faint start
	(id: $A396B6F3; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: $10d;), //  enemy faint end

//Deadly Archer �ǥåɥ�`���`����` ��Ȧ
	(id: $1519B392; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 39;), //

//Acro Effect ���������ե�����
	(id: $99DAB479; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.85; PA: 40;), //

//Deadly Circle �ǥåɥ�`���`���� ����һȦ
	(id: $FF4F5DD0; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 131;), //

//Chaos Riser ���������饤���` ԭ��������
	(id: $EDFFD322; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.5; PA: 147;), //
	(id: $EDFFD323; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.8; PA: 147;), // S			//gear lv2
	(id: $EDFFD324; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 2.2; PA: 147;), // SS			//gear lv3

//Hurricane Sender �ϥꥱ�`�󥻥���` �ӳ�������
	(id: $3ED87E87; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.2; PA: 179;), //
	(id: $3ED87E86; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.5; PA: 179;), // S			//gear lv2
	(id: $3ED87E85; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.7; PA: 179;), // SS			//gear lv3


//[Twin Dagger]
//Normal Attack
	(id: $2F642DDD; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // 1-2		//stand ground
	(id: $1CC6E6EC; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.1; PA: 0;), // 3-4
	(id: $AA01AEF7; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.2; PA: 0;), // 5-6
	(id: $B19DC071; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // 1-2		//air
	(id: $FA2C09AA; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.1; PA: 0;), // 3-4
	(id: $668DDABC; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.2; PA: 0;), // 5-6
	
//Weapon Action
	(id: $9F277558; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), //

//Step Attack
	(id: $74558653; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.3; PA: 0;), //

//Raging Waltz �쥤���󥰥���					279
	(id: $8DC2A126; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.6; PA: 25;), //										100%

//Shoot Polka ����`�ȥݥ륫						762
	(id: $8DC2A125; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.9; PA: 26;), // 1-3									34%

//Scar Fugue �������թ`�� �߷���ˤ					932
	(id: $3D8212F8; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.8; PA: 27;), // 1										25%
	(id: $D1A45534; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.8; PA: 27;), // 2										25%
	(id: $976F2460; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.8; PA: 27;), // 3										25%
	(id: $57ACF5B6; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 27;), // 4										50%
	(id: $C022DBE3; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 27;), // 5 //wall hit							50%
	
//Wild Rhapsody �磻��ɥ�ץ��ǥ�					825
	(id: $8DC2A123; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.8; PA: 28;), // 1-4									25%

//Dark Scherzo ���`��������ĥ�					736
	(id: $0D5DD698; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.8; PA: 29;), // 1-2									25%
	(id: $0D5DD699; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 29;), // 3										50%

//Quick March �����å��ީ`�� ����					611
	(id: $0D5DC68B; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.8; PA: 30;), // 1-2									25%
	(id: $0D5DC68A; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 30;), // 3										50%

//Symphonic Drive ����ե��˥å��ɥ饤�� �߷�		695
	(id: $0D5C37FA; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 31;), // 1										50%
	(id: $0D5C37FB; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 31;), // 2										50%

//Orchestra �����륱���ȥ�` 8����					1111
	(id: $0D5C27AA; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 32;), // 1-7									10%
	(id: $0D5C27AB; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.9; PA: 32;), // 8										30%

//Bloody Sarabande �֥�åǥ�����Х�� ��������	960
	(id: $0D5C949B; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 130;), // 1-4									13%
	(id: $F5EF513C; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.7; PA: 130;), // 5-7 //gear lv0-1						16%
	(id: $F5EF513A; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.7; PA: 130;), // 5-7 //gear lv2-3						16%

//Facet Folia �ե����åȥե��ꥢ					1155
	(id: $F5C951DD; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.62; PA: 146;), // 1										12%
	(id: $F5C951DC; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.63; PA: 146;), // 2-6									13%
	(id: $F5C951DF; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.8; PA: 146;), // 7										23%

//Fall Nocturne �ե��`��Υ����`�� ���³�			598
	(id: $612C69E3; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.9; PA: 178;), // //gear lv1								33%
	(id: $612C69E2; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.9; PA: 178;), // //gear lv2								33%
	(id: $612C69E1; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.9; PA: 178;), // //gear lv3								33%

    //[Rifle ��ǹ]
//Normal Attack
	(id: $7D59C6D1; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1; PA: 0;), //

//Piercing Shell �ԥ��å��󥰥����� �ᴩ��
	(id: $6EA76553; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1.6; PA: 57;), //

//Grenade Shell ����ͩ`�ɥ����� ����
	(id: $D591152A; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1.6; PA: 58;), //
	(id: $6EA7655C; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1.6; PA: 58;), // sputtering damage

//One Point ���ݥ���� 9����
	(id: $6EA7655D; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.6; PA: 59;), //

//Diffuse Shell �ǥ��ե�`�������� ɢ��
	(id: $6EA7655E; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1.6; PA: 60;), //

//Homing Emission �۩`�ߥ󥰥��ߥå���� ׷�ٵ�
	(id: $6EA7655F; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.7; PA: 61;), //

//Impact Slider ����ѥ��ȥ��饤���`
	(id: $6EA76558; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.7; PA: 62;), // 1-4
	(id: $16E93EC9; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.75; PA: 62;), // 5

//Sneak Shooter ���˩`������`���` �ѻ�
	//(id: $EA6FC5F7; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1; PA: 63;), // 			//player crawl
	(id: $6EA76559; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1.6; PA: 63;), // 1st hit	//hit enemy

//Glorious Rain �����`��`�쥤�� �ӵ���
	(id: $557FC030; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.6; PA: 64;), // 1st hit	//shoot sky
	(id: $6EA7655A; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.6; PA: 64;), // 2-11		//bullet fall

//Parallel Slider �ѥ��륹�饤���` ������
	(id: $6EA7655B; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.6; PA: 134;), //

//Satellite Cannon ���ƥ饤�ȥ��Υ� ������
	(id: $B1BB1E89; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.55; PA: 150;), //
	(id: $B1BB1E88; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.6; PA: 150;), // S
	(id: $B1BB1E8B; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.7; PA: 150;), // SS
	
//End Attraction ����ɥ��ȥ饯�� ��͸��
	(id: $B1BB6E3A; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1.05; PA: 182;), //
	(id: $B1BB6E3B; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1.5; PA: 182;), // S
	(id: $B1BB6E38; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1.6; PA: 182;), // SS    //255

    //[Launcher ����]
//Normal Attack
	(id: $DFADE9A1; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1; PA: 0;), // 1st hit
	(id: $DC862DC5; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1.1; PA: 0;), // 2nd hit
	(id: $E23DC1AB; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1.2; PA: 0;), // 3rd hit
	(id: $FBED4F1B; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.5; PA: 0;), // 1st hit 	// sputtering damage
	(id: $FBED4F1A; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.55; PA: 0;), // 2nd hit 	// sputtering damage
	(id: $FBED4F1D; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.56; PA: 0;), // 3rd hit 	// sputtering damage

//Divine Launcher �ǥ��Х��������` ǿ��һ��
	(id: $6971C3EC; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1.6; PA: 65;), //
	(id: $236BFC18; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1.6; PA: 65;), //			// sputtering damage
     
//Concentrate One ���󥻥�ȥ쥤�ȥ�� 3����
	(id: $3E8BD465; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.9; PA: 66;), //
	(id: $236BFC07; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.9; PA: 66;), //			// sputtering damage

//Cluster Bullet ���饹���`�Х�å� �콵һ��
	(id: $3FBC8D17; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1.6; PA: 67;), // 1st hit
	(id: $236BFC06; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1.6; PA: 67;), // 2nd hit	// sputtering damage

//Cracker Bullet ����å��`�Х�å� �ӱ���
	(id: $3CFF5369; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.5; PA: 68;), // 1st hit
	(id: $A17A864F; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.5; PA: 68;), // 2-x hit
    (id: $236BFC05; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1.25; PA: 68;), //
	
//Zero Distance �����ǥ������� �������
	(id: $236BFC04; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1.6; PA: 69;), //
	
//Rodeo Drive ���ǥ��ɥ饤�� �����ڵ�ײ��
	(id: $A17A85BC; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.8; PA: 70;), //
	
//Crazy Smash ���쥤���`���ޥå��� ��������
	(id: $A17A858F; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1.6; PA: 71;), // 1st hit
	(id: $A17A8579; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1.6; PA: 71;), // 2nd hit	//enemy fall hit ground
	(id: $236BFC02; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1.6; PA: 71;), // 3rd hit 	//enemy fall hit other

//Fake Silhouette �ե��������륨�å� ����
	(id: $A859ECBB; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1; PA: 72;), // 1st hit	//alpha bug , no Silhouette

//Flame Bullet �ե쥤��Х�å� ���
	(id: $236BFC00; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.6; PA: 135;), //

//Cosmos Breaker �����⥹�֥쥤���` ������
	(id: $6DB6F1E6; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.6; PA: 151;), // 1st hit
	(id: $A17A82F7; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.6; PA: 151;), // 2-x hit

//Sphere Eraser ���ե������쥤���` ������
	(id: $A17A82E8; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1.6; PA: 174;), //  //279


//[Twingun ˫ǹ]
//Normal Attack
	(id: $5B1CF740; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1; PA: 0;), // 1-4
	(id: $5B1CF743; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1; PA: 0;), // 5-8
	(id: $5B1CF742; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1; PA: 0;), // 9-11

//Weapon Action attack
	(id: $DB9C0D37; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1; PA: 0;), //
	(id: $3946C9F5; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1; PA: 0;), //	//slow time


//Aerial Shooting ���ꥢ�륷��`�ƥ��� ����
	(id: $5206DEE2; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.85; PA: 73;), //
	(id: $5206DE23; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.8; PA: 73;), // 1  //focus
	(id: $5206DE22; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.95; PA: 73;), // 2
	(id: $5206DE21; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.95; PA: 73;), // 3

//Bullet Squall �Х�åȥ����`�� ����
	(id: $20D22CD9; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.7; PA: 74;), // 1			//kick
	(id: $618E374F; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.53; PA: 74;), // 2-x		//shoot
	
//Dead Approach �ǥåɥ��ץ��`�� ײ
	(id: $20D22CD8; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1.6; PA: 75;), // 			//hit
	(id: $7D6AA340; action: ACTION_BOOST; types: AT_RATK; element: ELE_NONE; mul: 1; PA: $10c;), // enemy faint start
	(id: $A396B6F3; action: ACTION_BOOST; types: AT_RATK; element: ELE_NONE; mul: 1; PA: $10d;), // enemy faint end

//Messiah Time �᥷�������� �ӵ�ʱ��
	(id: $6785DC93; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.55; PA: 76;), //

//Infinite Fire ����ե��˥ƥ��ե����� ���޻���
	(id: $12555D4F; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.54; PA: 77;), // 1-5
	(id: $839F7BD8; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.57; PA: 77;), // 6-x

//Satellite Aim ���ƥ饤�ȥ�����
	(id: $9C2343D8; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1; PA: 78;), // 1
	(id: $9C2343D9; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1; PA: 78;), // 2

//Elder Rebellion ������`��٥ꥪ�� 9����
	(id: $3F0D9090; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1; PA: 79;), // 1-6
	(id: $B0796F4F; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1; PA: 79;), // 7-9

//Reverse Tap ��Щ`�����å� ������
	(id: $20D22CD3; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.82; PA: 80;), //

//Heel Stab �ҩ`�륹���å� ����
	(id: $9C2313AF; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.8; PA: 136;), // 1 kick donw
	(id: $6E69A177; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.9; PA: 136;), // 2 kick up
	(id: $9C2313AF; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.8; PA: 136;), // 1 kick down		//focus
	(id: $6E69A176; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.9; PA: 136;), // 2 kick up
	(id: $B0796F4F; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.2; PA: 136;), // 3-8 shoot

//Shift Period ���եȥԥꥪ�� �ӵ�����
	(id: $6D4300B3; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.6; PA: 152;), // 1-3
	(id: $6D4300B2; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.6; PA: 152;), // 4-6
	(id: $6D432FC0; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.8; PA: 152;), // 7
	(id: $6D432FC1; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.8; PA: 152;), // 8
	(id: $6D432FC2; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.8; PA: 152;), // 9

//Grim Barrage �����Х�`����
	(id: $52011621; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.75; PA: 177;), // 1-2
	(id: $52011622; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.75; PA: 177;), // 3-7     //313

    //[Rod ����]
//Normal Attack
	(id: $BC30BB8A; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // 1
	(id: $BC30BBB5; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // 2
	(id: $BC30BBB4; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // 3
	(id: $9F569702; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // 		//learned RodShoot

//[Talis ����]
//Normal Attack
	(id: $46D4EDC5; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // 1
	(id: $46D4EDC4; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // 2
	(id: $46D4EDC7; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // 3

//[Wand ����]
//Normal Attack
	(id: $CB4655E8; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // 1
	(id: $CB4655EB; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // 2
	(id: $CB4655EA; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // 3
	(id: $9BDB532D; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // Wand boom //use gear //324


    //[Bow ��]
//Normal Attack
	(id: $9965E9C4; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1; PA: 0;), //
	(id: $9965E9FB; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1.4; PA: 0;), //  S //focus

//Master Shot �ޥ����`����`�� ���ؼ�
	(id: $118A22F4; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.8; PA: 119;), //
	(id: $118A22F7; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.8; PA: 119;), //  S

//Penetrating Arrow �ڥͥȥ쥤�ȥ����� ��͸��
	(id: $118A1205; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1.35; PA: 120;), //
	(id: $118A1206; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1.6; PA: 120;), //  S

//Torrential Arrow �ȥ�󥷥�륢���� ����
	(id: $E06443C7; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.7; PA: 121;), //
	(id: $118A8FD1; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.7; PA: 121;), //  S

//Gravity Point ��������ƥ��ݥ���� ������
	(id: $14DFD89D; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.6; PA: 122;), //  1
	(id: $14D2DA49; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.8; PA: 122;), //  2-4
	(id: $14DFD892; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.6; PA: 122;), //  1 S
	(id: $14D2DA48; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.8; PA: 122;), //  2-5 S

//Kamikaze Arow ���ߥ��������� ���ײ
	(id: $14D95A12; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.95; PA: 123;), //  1
	(id: $14D98A0B; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.85; PA: 123;), //  2
	(id: $14D95A11; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1.4; PA: 123;), //  1 S
	(id: $14D98A0A; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1.1; PA: 123;), //  2

//Sharp Bomber ����`�ץܥީ` ������ը��
	(id: $E06E4614; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1.6; PA: 124;), //

//Final Nemesis �饹�ȥͥ᥷�� �ѻ���
	(id: $34A5AC1B; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1.6; PA: 125;), //

//Tritt Shooter �ȥ�åȥ���`���` ��Ϲ���
	(id: $E06E4790; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.6; PA: 126;), //  1-3	//kick
	(id: $E06E4788; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.7; PA: 126;), //  4		//shoot
	(id: $118AA9AF; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.6; PA: 126;), //  5 S		//kick , focus repeat 1-4 hits

//Banishing Arrow �Х˥å��奢���� �����˺���
	(id: $34A5AC19; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 1; PA: 155;), //  1
	(id: $14C93905; action: ACTION_BOOST; types: AT_RATK; element: ELE_NONE; mul: 1; PA: 155;), // 		//buff self , alpha bug, invalid

//Million Storm �ߥꥪ�󥹥ȩ`�� ħ����
	(id: $E06E453C; action: ACTION_ATTACK; types: AT_RATK; element: ELE_NONE; mul: 0.6; PA: 156;), //

//Chase Arrow �������������� �Զ�׷����
	(id: $DAEDEE06; action: ACTION_BOOST; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 174;), // 		//buff self , alpha bug, invalid


//    [Katana �ε�]
//JustGuard Counter hit
	(id: $A0F14B9E; action: ACTION_DEF; types: AT_SATK; element: ELE_NONE; mul: 1.3; PA: 175;), // //didn't learn Katana gear
	(id: $F6F6425C; action: ACTION_DEF; types: AT_SATK; element: ELE_NONE; mul: 1.4; PA: 175;), // //gear max
	(id: $7670F6D8; action: ACTION_DEF; types: AT_SATK; element: ELE_NONE; mul: 1.6; PA: 175;), // //gear max and learn Counter Edge

//Normal Attack
	(id: $3F5B24A2; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // 1		//stand ground
	(id: $3F5B24A1; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.1; PA: 0;), // 2-3
	(id: $3F5B24A0; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.2; PA: 0;), // 4
	(id: $3F614A76; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // 1		//air
	(id: $3F614A75; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.1; PA: 0;), // 2
	(id: $3F614A74; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.2; PA: 0;), // 3-4

//Step Attack
	(id: $3F60D52F; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.3; PA: 0;), //

//Tsukimi-sazanka �ĥ��ߥ�����(�¼�ɽ�軨)
	(id: $3F5B8A72; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.6; PA: 111;), //

//Gekka-zakuro ���å�������(��������)
	(id: $C7EBFAEA; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.8; PA: 112;), // 1
	(id: $B19AD05A; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.4; PA: 112;), // 2

//Kanran-kikiyou �����󥭥��祦(��魽۹�)
	(id: $3F5B8A70; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.9; PA: 113;), //
	(id: $BBAB72E8; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 113;), // S

//Sakura-endo �����饨���(�@��)
	(id: $3F5B8A71; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.9; PA: 114;), //
	(id: $BBAB72A9; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 114;), // S

//Hien-tsubaki �ҥ���ĥХ� �����഻��
	(id: $3F5B8A76; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.74; PA: 115;), //

//Asagiri-rendan ������������󣨳���������
	(id: $3F5B8A77; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.62; PA: 116;), // 1-6
	(id: $BAC414FA; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.8; PA: 116;), // 7

//Hatou-rindou �ϥȥ����ɥ������θo����
	(id: $626E7D10; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 117;), // 1
	(id: $0C543920; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.75; PA: 117;), // 2
	(id: $301C2050; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 117;), // 1 S
	(id: $0C5434DB; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.75; PA: 117;), // 2
	(id: $0C5434DA; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.75; PA: 117;), // 3

//Shunka-shunran ����󥫥�����󣨴���������
	(id: $B1841A6A; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 118;), //
	(id: $9A1B017C; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 118;), // 1 S
	(id: $64ABCC33; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.7; PA: 118;), // 2
	(id: $B06EC0AF; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.75; PA: 118;), // 3
	(id: $907039B9; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.8; PA: 118;), // 4
	(id: $4E11CF20; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.85; PA: 118;), // 5

//Kazan-nadeshiko ������ʥǥ�������ɽ���ӣ�
	(id: $3F5B8A6A; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.6; PA: 153;), //

//Fudou-kuchinashi �եɥ������ʥ�������ɽ���ӣ�
	(id: $BBAB7377; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.6; PA: 154;), //
	(id: $7D6AA340; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 154;), // enemy faint start
	(id: $A396B6F3; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 154;), // enemy faint end

//Guren-tessen
	(id: $84B7DFB1; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.7; PA: 181;), //
	(id: $69EA819A; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.4; PA: 181;), //     386

    //[JetBoots ħװ��]
//Normal Attack
	(id: $A660C96A; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // 1
	(id: $A660C969; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.1; PA: 0;), // 2-3
	(id: $A660C968; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.2; PA: 0;), // 4

//Step Attack
	(id: $A67D3A0B; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.3; PA: 0;), //

//StrikeGust ���ȥ饤�������� ����
	(id: $E9AC5C52; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.54; PA: 163;), //
	(id: $BF23D284; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.56; PA: 163;), //
    (id: $E9AC5C5B; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 163;), // 1-4 S								10%
	(id: $842F27C1; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.68; PA: 163;), // 5									15%
	(id: $54C30F2B; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 163;), // add_action_hit						50%


//Gran Wave ����󥦥����� �����
	(id: $A61FD07B; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.7; PA: 164;), // 1
	(id: $E4234947; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 164;), // 2-6
    (id: $99055847; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.72; PA: 164;), // add_action_hit						15%

//Moment Gale ��`���ȥ����� ��ǧ��
	(id: $A61FD07A; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 165;), //
    (id: $D11CC7DC; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.55; PA: 165;), // add_action_hit						5%

//Vinto Gigue ��`���ȥ����� ����
	(id: $E9AC5B1C; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 166;), //   396
    (id: $E9AC5B16; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 166;), // //gear lv2							50%
	(id: $E9AC5B17; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.3; PA: 166;), // //gear lv3							75%
	(id: $6589C1A0; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.6; PA: 166;), // add_action_hit //gear lv1			100%
	(id: $9F6BA144; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 2.6; PA: 166;), // add_action_hit //gear lv2			250%
	(id: $BE616EFD; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 3.9; PA: 166;), // add_action_hit //gear lv3			420%

//     [Dual Blades ���轣]
//Normal Attack
	(id: $F7C272B2; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // 1		//stand ground
	(id: $F7C272B3; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.1; PA: 0;), // 2
	(id: $F7C272B4; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.2; PA: 0;), // 3-4
	(id: $F7C1DD68; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // 1		//air
	(id: $F7C1DD69; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.1; PA: 0;), // 2
	(id: $F7C1DD6A; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.2; PA: 0;), // 3-4

//Step Attack 
	(id: $F7C2B63F; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.3; PA: 0;), // 1
	(id: $BE272CD2; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.3; PA: 0;), // 2-7 //photon blade �ե��ȥ�֥�`�� ���ӷ���

//Distraction Wing �ǥ����ȥ饯�ȥ����� ���
	(id: $3B0BB924; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 167;), //

//Dispersion Shrike �ǥ����ѩ`������饤�� ����
	(id: $F7C28489; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 168;), //

//Heavenly Kite �إ֥��`������ ��ն
	(id: $3B0BB994; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 169;), // 1
	(id: $3B0BB995; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 169;), // 2

//Justice Crow ���㥹�ƥ��������� ���ǲ�
	(id: $3B0BB86A; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 170;), // 1-6
	(id: $3B0BB858; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.05; PA: 170;), // 7

//Starling Fall �����`��󥰥ե��`�� �ϳ�
	(id: $3B0BB81A; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 171;), // 1-12
	(id: $3B0BB819; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.6; PA: 171;), // 13

//Kestrel Rampage �����ȥ����ک`�� �һ�
	(id: $F7C28485; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.54; PA: 172;), //

//Immortal Dove ��
	(id: $3B0BB8AB; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 0.9; PA: 182;), //
	(id: $3B0BB89A; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.3; PA: 182;), //          415
    (id: $3B0BB89F; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.6; PA: 182;), //

//    [baton ������ ָ�Ӱ�]
//Normal Attack
	(id: $1050345B; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;), // 1
	(id: $1050345A; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.1; PA: 0;), // 2
	(id: $285DA0EA; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1.2; PA: 0;), // 3

//Wanda Slicer
	(id: $37B89AD9; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 1;), // 1
	(id: $37B89AD8; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 1;), // 2

//Wanda Break
	(id: $A6091C03; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 2;), // 1
	(id: $E9169B04; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 2;), // 2

//Wanda Assault
	(id: $A6091C1D; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 3;), // 1
	(id: $C1BC79FF; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 3;), // 2        //424


    //[Magic]
    (id: $708637B4; action: ACTION_ATTACK; types: AT_TATK; element: ELE_FIRE; mul: 0.9; PA: 81;), // Foie; action: ACTION_ATTACK; types: AT_TATK; element: ELE_NONE; mul: 0.9; PA: 3;), //		//S=focus
    (id: $708637B5; action: ACTION_ATTACK; types: AT_TATK; element: ELE_FIRE; mul: 1.6; PA: 81;), // FoieS
    (id: $2A00539A; action: ACTION_ATTACK; types: AT_TATK; element: ELE_FIRE; mul: 0.75; PA: 82;), // Gifoie
    (id: $2A00539B; action: ACTION_ATTACK; types: AT_TATK; element: ELE_FIRE; mul: 0.9; PA: 82;), // GifoieS
    (id: $8275CC39; action: ACTION_ATTACK; types: AT_TATK; element: ELE_FIRE; mul: 0.9; PA: 83;), // Rafoie
    (id: $8275CC38; action: ACTION_ATTACK; types: AT_TATK; element: ELE_FIRE; mul: 1.6; PA: 83;), // RafoieS
    (id: $0A426EBB; action: ACTION_ATTACK; types: AT_TATK; element: ELE_FIRE; mul: 0.75; PA: 84;), // Safoie
    (id: $0A426EB8; action: ACTION_ATTACK; types: AT_TATK; element: ELE_FIRE; mul: 0.9; PA: 84;), // SafoieS
    (id: $AA2750CC; action: ACTION_BOOST; types: AT_TATK; element: ELE_FIRE; mul: 1; PA: 85;), // Shifta  85
    (id: $AA2750CD; action: ACTION_BOOST; types: AT_TATK; element: ELE_FIRE; mul: 2; PA: 85;), // ShiftaS
    (id: $1CD910F5; action: ACTION_ATTACK; types: AT_TATK; element: ELE_FIRE; mul: 1.7; PA: 137;), // Nafoie	hit
    (id: $B6EE6B2C; action: ACTION_ATTACK; types: AT_TATK; element: ELE_FIRE; mul: 0.7; PA: 137;), // Nafoie	burn
    (id: $1CD910F6; action: ACTION_ATTACK; types: AT_TATK; element: ELE_FIRE; mul: 2.6; PA: 137;), // NafoieS 1st hit
    (id: $B6EE6B2B; action: ACTION_ATTACK; types: AT_TATK; element: ELE_FIRE; mul: 2.6; PA: 137;), // NafoieS 2nd hit
    (id: $1D1CF48F; action: ACTION_ATTACK; types: AT_TATK; element: ELE_FIRE; mul: 0.9; PA: 137;), // NafoieS burn
    (id: $6E241D09; action: ACTION_ATTACK; types: AT_TATK; element: ELE_FIRE; mul: 0.95; PA: 157;), // Ilfoie
    (id: $6E241D0A; action: ACTION_ATTACK; types: AT_TATK; element: ELE_FIRE; mul: 1.6; PA: 157;), // IlfoieS

    (id: $A7812F4A; action: ACTION_ATTACK; types: AT_TATK; element: ELE_ICE; mul: 0.9; PA: 86;), // Barta
    (id: $A7812F75; action: ACTION_ATTACK; types: AT_TATK; element: ELE_ICE; mul: 1.6; PA: 86;), // BartaS
    (id: $F4B46A6F; action: ACTION_ATTACK; types: AT_TATK; element: ELE_ICE; mul: 0.6; PA: 87;), // Gibarta
    (id: $F4B46A6C; action: ACTION_ATTACK; types: AT_TATK; element: ELE_ICE; mul: 0.9; PA: 87;), // GibartaS
    (id: $AD9E4811; action: ACTION_ATTACK; types: AT_TATK; element: ELE_ICE; mul: 0.6; PA: 88;), // Rabarta
    (id: $AD9E4810; action: ACTION_ATTACK; types: AT_TATK; element: ELE_ICE; mul: 0.85; PA: 88;), // RabartaS
    (id: $65D8AF11; action: ACTION_ATTACK; types: AT_TATK; element: ELE_ICE; mul: 0; PA: 89;), // Sabarta 1st hit
    (id: $FEC5C75B; action: ACTION_ATTACK; types: AT_TATK; element: ELE_ICE; mul: 0.98; PA: 89;), // Sabarta 2nd hit
    (id: $65D8AF10; action: ACTION_ATTACK; types: AT_TATK; element: ELE_ICE; mul: 0; PA: 89;), //  SabartaS 1
    (id: $719F051A; action: ACTION_ATTACK; types: AT_TATK; element: ELE_ICE; mul: 0.9; PA: 89;), // SabartaS 2
    (id: $719F051B; action: ACTION_ATTACK; types: AT_TATK; element: ELE_ICE; mul: 0.9; PA: 89;), // SabartaS 3
    (id: $719F0518; action: ACTION_ATTACK; types: AT_TATK; element: ELE_ICE; mul: 0.9; PA: 89;), // SabartaS 4
    (id: $568B3573; action: ACTION_BOOST; types: AT_TATK; element: ELE_ICE; mul: 1; PA: 90;), // Deband  90
    (id: $568B3570; action: ACTION_BOOST; types: AT_TATK; element: ELE_ICE; mul: 2; PA: 90;), // DebandS
    (id: $6EC94E59; action: ACTION_ATTACK; types: AT_TATK; element: ELE_ICE; mul: 1.6; PA: 138;), // Nabarta
    (id: $96B5B95E; action: ACTION_ATTACK; types: AT_TATK; element: ELE_ICE; mul: 1; PA: 158;), // Ilbarta
    (id: $96B5B95F; action: ACTION_ATTACK; types: AT_TATK; element: ELE_ICE; mul: 1.6; PA: 158;), // IlbartaS

    (id: $EAD00978; action: ACTION_ATTACK; types: AT_TATK; element: ELE_LIGHTNING; mul: 0.9; PA: 91;), // Zonde
    (id: $EAD00979; action: ACTION_ATTACK; types: AT_TATK; element: ELE_LIGHTNING; mul: 1.6; PA: 91;), // ZondeS
    (id: $66AC17B7; action: ACTION_ATTACK; types: AT_TATK; element: ELE_LIGHTNING; mul: 0.9; PA: 92;), // Gizonde
    (id: $66AC17B0; action: ACTION_ATTACK; types: AT_TATK; element: ELE_LIGHTNING; mul: 1.6; PA: 92;), // GizondeS
    (id: $A2CC0B70; action: ACTION_ATTACK; types: AT_TATK; element: ELE_LIGHTNING; mul: 0.9; PA: 93;), // Razonde
    (id: $A2CC0B7F; action: ACTION_ATTACK; types: AT_TATK; element: ELE_LIGHTNING; mul: 1.6; PA: 93;), // RazondeS
    (id: $B8FDB4B3; action: ACTION_ATTACK; types: AT_TATK; element: ELE_LIGHTNING; mul: 0.9; PA: 94;), // Sazonde hit
    (id: $B8FDB4B2; action: ACTION_ATTACK; types: AT_TATK; element: ELE_LIGHTNING; mul: 1.6; PA: 94;), // SazondeS hit
    (id: $D604B6E4; action: ACTION_ATTACK; types: AT_TATK; element: ELE_LIGHTNING; mul: 0.8; PA: 94;), // //Sazonde and SazondeS sidewipe hit
    (id: $21394F8B; action: ACTION_ATTACK; types: AT_TATK; element: ELE_LIGHTNING; mul: 0.9; PA: 95;), // Zondeel activate hit
    (id: $21394F88; action: ACTION_ATTACK; types: AT_TATK; element: ELE_LIGHTNING; mul: 0.85; PA: 95;), // ZondeelS activate hit
    (id: $8454373E; action: ACTION_ATTACK; types: AT_TATK; element: ELE_LIGHTNING; mul: 0.6; PA: 139;), // Nazonde 1st hit
    (id: $A91B8061; action: ACTION_ATTACK; types: AT_TATK; element: ELE_LIGHTNING; mul: 0.57; PA: 139;), // Nazonde 2-x hit
    (id: $8454373F; action: ACTION_ATTACK; types: AT_TATK; element: ELE_LIGHTNING; mul: 0.6; PA: 139;), // NazondeS 1st hit
    (id: $A91B806E; action: ACTION_ATTACK; types: AT_TATK; element: ELE_LIGHTNING; mul: 0.57; PA: 139;), // NazondeS 2-x hit
    (id: $758662C7; action: ACTION_ATTACK; types: AT_TATK; element: ELE_LIGHTNING; mul: 0.9; PA: 159;), // Ilzonde
    (id: $758662C6; action: ACTION_ATTACK; types: AT_TATK; element: ELE_LIGHTNING; mul: 1.6; PA: 159;), // IlzondeS

    (id: $4A929BB0; action: ACTION_ATTACK; types: AT_TATK; element: ELE_WIND; mul: 0.9; PA: 96;), // Zan
    (id: $4A929BBF; action: ACTION_ATTACK; types: AT_TATK; element: ELE_WIND; mul: 0.9; PA: 96;), // ZanS
    (id: $86275BE3; action: ACTION_ATTACK; types: AT_TATK; element: ELE_WIND; mul: 1; PA: 97;), // Gizan
    (id: $86275BE0; action: ACTION_ATTACK; types: AT_TATK; element: ELE_WIND; mul: 0.6; PA: 97;), // GizanS 1-4 hit
    (id: $51AF6CEE; action: ACTION_ATTACK; types: AT_TATK; element: ELE_WIND; mul: 1.1; PA: 97;), // GizanS 5 hit
    (id: $7636BED4; action: ACTION_ATTACK; types: AT_TATK; element: ELE_WIND; mul: 1; PA: 98;), // Razan
    (id: $7636BED5; action: ACTION_ATTACK; types: AT_TATK; element: ELE_WIND; mul: 0.9; PA: 98;), // RazanS
    (id: $C097A37E; action: ACTION_ATTACK; types: AT_TATK; element: ELE_WIND; mul: 0.7; PA: 99;), // Sazan
    (id: $C097A37D; action: ACTION_ATTACK; types: AT_TATK; element: ELE_WIND; mul: 0.9; PA: 99;), // SazanS
    (id: $BC9EFCB0; action: ACTION_ATTACK; types: AT_TATK; element: ELE_WIND; mul: 1; PA: 100;), // Nazan
    (id: $BC9EFCB3; action: ACTION_ATTACK; types: AT_TATK; element: ELE_WIND; mul: 1.6; PA: 100;), // NazanS
    (id: $F6748797; action: ACTION_BOOST; types: AT_TATK; element: ELE_WIND; mul: 1; PA: 140;), // Zanverse	 140	//to self
    (id: $F6748790; action: ACTION_BOOST; types: AT_TATK; element: ELE_WIND; mul: 2; PA: 140;), // ZanverseS		//to self
    (id: $23A36A45; action: ACTION_ATTACK; types: AT_TATK; element: ELE_WIND; mul: 1; PA: 160;), // Ilzan
    (id: $23A36A42; action: ACTION_ATTACK; types: AT_TATK; element: ELE_WIND; mul: 1.6; PA: 160;), // IlzanS

    (id: $768CC191; action: ACTION_ATTACK; types: AT_TATK; element: ELE_LIGHT; mul: 0.9; PA: 101;), // Grantz
    (id: $768CC190; action: ACTION_ATTACK; types: AT_TATK; element: ELE_LIGHT; mul: 0.8; PA: 101;), // GrantzS
    (id: $0EC67293; action: ACTION_ATTACK; types: AT_TATK; element: ELE_LIGHT; mul: 0.7; PA: 102;), // Gigrantz 1st hit
    (id: $DAE96BA7; action: ACTION_ATTACK; types: AT_TATK; element: ELE_LIGHT; mul: 0.7; PA: 102;), // Gigrantz 2nd hit
    (id: $0EC67292; action: ACTION_ATTACK; types: AT_TATK; element: ELE_LIGHT; mul: 0.85; PA: 102;), // GigrantzS 1st hit
    (id: $55A5D5F5; action: ACTION_ATTACK; types: AT_TATK; element: ELE_LIGHT; mul: 0.85; PA: 102;), // GigrantzS 2nd hit
    (id: $55A5D5F2; action: ACTION_ATTACK; types: AT_TATK; element: ELE_LIGHT; mul: 0.85; PA: 102;), // GigrantzS 3-6
    (id: $5D428B81; action: ACTION_ATTACK; types: AT_TATK; element: ELE_LIGHT; mul: 0.9; PA: 103;), // Ragrantz
    (id: $5D428B82; action: ACTION_ATTACK; types: AT_TATK; element: ELE_LIGHT; mul: 0.8; PA: 103;), // RagrantzS
    (id: $529CFF0C; action: ACTION_BOOST; types: AT_TATK; element: ELE_LIGHT; mul: 1; PA: 104;), // Resta 1st hit  104
    (id: $52996BA1; action: ACTION_BOOST; types: AT_TATK; element: ELE_LIGHT; mul: 1; PA: 104;), // Resta 2nd hit
    (id: $927D8132; action: ACTION_BOOST; types: AT_TATK; element: ELE_LIGHT; mul: 2; PA: 104;), // RestaS 1st hit
    (id: $926FAF7B; action: ACTION_BOOST; types: AT_TATK; element: ELE_LIGHT; mul: 2; PA: 104;), // RestaS 2-4 hit
//Anti  105
    (id: $C4D5E211; action: ACTION_ATTACK; types: AT_TATK; element: ELE_LIGHT; mul: 0.7; PA: 141;), // Nagrantz
    (id: $C4D5E210; action: ACTION_ATTACK; types: AT_TATK; element: ELE_LIGHT; mul: 0.7; PA: 141;), // NagrantzS
    (id: $47F4A861; action: ACTION_ATTACK; types: AT_TATK; element: ELE_LIGHT; mul: 0.56; PA: 161;), // IlGrantz
    (id: $47F4A860; action: ACTION_ATTACK; types: AT_TATK; element: ELE_LIGHT; mul: 0.61; PA: 161;), // IlGrantzS

    (id: $9527E3CA; action: ACTION_ATTACK; types: AT_TATK; element: ELE_DARK; mul: 0.9; PA: 106;), // Megido
    (id: $E935EE24; action: ACTION_ATTACK; types: AT_TATK; element: ELE_DARK; mul: 0.9; PA: 106;), // 			// sputtering damage
    (id: $9527E3CB; action: ACTION_ATTACK; types: AT_TATK; element: ELE_DARK; mul: 1.6; PA: 106;), // MegidoS
    (id: $E935EE25; action: ACTION_ATTACK; types: AT_TATK; element: ELE_DARK; mul: 1.6; PA: 106;), // 			// sputtering damage
    (id: $A19AB817; action: ACTION_ATTACK; types: AT_TATK; element: ELE_DARK; mul: 0.9; PA: 107;), // Gimegid
    (id: $A19AB816; action: ACTION_ATTACK; types: AT_TATK; element: ELE_DARK; mul: 1.6; PA: 107;), // GimegidS
    (id: $C0B0092B; action: ACTION_ATTACK; types: AT_TATK; element: ELE_DARK; mul: 0.6; PA: 108;), // Ramegid hit
    (id: $B86F6881; action: ACTION_ATTACK; types: AT_TATK; element: ELE_DARK; mul: 0.6; PA: 108;), // Ramegid  			// sputtering damage
    (id: $C0B0092A; action: ACTION_ATTACK; types: AT_TATK; element: ELE_DARK; mul: 0.6; PA: 108;), // RamegidS hit
    (id: $B86F6880; action: ACTION_ATTACK; types: AT_TATK; element: ELE_DARK; mul: 0.6; PA: 108;), // RamegidS  			// sputtering damage
    (id: $664E75CE; action: ACTION_ATTACK; types: AT_TATK; element: ELE_DARK; mul: 0.9; PA: 109;), // Samegid
    (id: $664E75CF; action: ACTION_ATTACK; types: AT_TATK; element: ELE_DARK; mul: 0.9; PA: 109;), // SamegidS
    (id: $B1D856A7; action: ACTION_BOOST; types: AT_TATK; element: ELE_DARK; mul: 1; PA: 110;), // Megiverse	110	//to self
    (id: $B1D856A4; action: ACTION_BOOST; types: AT_TATK; element: ELE_DARK; mul: 2; PA: 110;), // MegiverseS		//to self
    (id: $7676FCF6; action: ACTION_ATTACK; types: AT_TATK; element: ELE_DARK; mul: 0.9; PA: 142;), // Namegid
    (id: $7676FCF7; action: ACTION_ATTACK; types: AT_TATK; element: ELE_DARK; mul: 1.6; PA: 142;), // NamegidS
    (id: $FD062C64; action: ACTION_ATTACK; types: AT_TATK; element: ELE_DARK; mul: 0.6; PA: 162;), // Ilmegid
    (id: $FD062C67; action: ACTION_ATTACK; types: AT_TATK; element: ELE_DARK; mul: 0.9; PA: 162;), // IlmegidS



    //keren and ethan
	(id: $2F642DDD; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;),
	(id: $1CC6E6EC; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;),
	(id: $AA01AEF7; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;),
	(id: $0D5C949B; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;),
	(id: $32B9ADE8; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;),
	(id: $F5EF513C; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;),
	(id: $0D5C27AA; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;),
	(id: $0FFBD2E3; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;),
	(id: $0D5C27AB; action: ACTION_ATTACK; types: AT_SATK; element: ELE_NONE; mul: 1; PA: 0;),
     ()
    );
{
[HU]

JustGuard
	(id: $6023A6AD JustGuard hit enemy //Sword, Partisan, WLance
	(id: $DC9A1CC3 JustGuard and restore hp //learned skill Healing Guard
[FI]
[RA]
[GU]

ShowTime
	(id: $469FDBC7 1st start //hit self
	(id: $7836B90A 2-x taunt //if enemy nearby .hit enemys.
[FO]
	(id: $9EC4C2BE PhotonFlare
	(id: $2519532C TalisTecFastThrowStance
[TE]
	(id: $100104F8 Wand Lovers
	(id: $809117EF PPConvert
[BR]
[BO]
[SU]

	(id: $F945C9F6 Pet Switch Strike
	(id: $28371D69 Pet Switch Shoot

                        }

implementation

end.
