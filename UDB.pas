unit UDB;

interface
uses types,sysutils,UPlayer;

function LoadAccount(usr:ansistring;acc:PAccount):integer;
function SaveAccount(usr:ansistring;acc:PAccount):integer;
function FileGetPacket(s:ansistring):ansistring;
function CreateCharID():dword;
function FixNPCGID(pl:integer;s:ansistring):ansistring;
function CreateItemID(x:integer):dword;

var NextID:dword=$FFFFFFFF;

implementation

uses fmain, UDebug, UPsoUtil;


function LoadAccount(usr:ansistring;acc:PAccount):integer;
var f:integer;
begin
    result:=-1;
    fillchar(acc.pass[0],sizeof(Taccount),0); // empty all
    f:=fileopen(path+'DB\'+usr,$40);
    if f>0 then begin
        fileread(f,acc.pass[0],sizeof(Taccount));
        fileclose(f);
        result:=0;
        //acc.GID:=$9c5f56;
        //acc.char[0].Mesetas:=646546;
        if acc.KeyConfig[0] = #0 then begin
            //load default key config
            f:=fileopen(path+'data\KeyConfig.def',$40);
            fileread(f,acc.KeyConfig[0],$4000);
            fileclose(f);
        end;
    end;
end;

function SaveAccount(usr:ansistring;acc:PAccount):integer;
var f:integer;
begin
    result:=-1;
    //fillchar(acc.pass[0],sizeof(Taccount),0); // empty all
    f:=filecreate(path+'DB\'+usr);
    if f>0 then begin
        filewrite(f,acc.pass[0],sizeof(Taccount));
        fileclose(f);
        result:=0;
    end else Debug(DBG_WARNING,'Could not save the account '+usr);
end;

function CreateCharID():dword;
var f:integer;
begin
    if NextID = $FFFFFFFF then begin
        //if not loaded yet load it
        f:=fileopen(path+'data\nextid.dat',$40);
        fileread(f,NextID,4);
        fileclose(f);
        if NextID = $FFFFFFFF then NextID:=$1234;
    end;
    result:=NextID;
    inc(NextID);
    f:=filecreate(path+'data\nextid.dat',$40);
    filewrite(f,NextID,4);
    fileclose(f);
end;

function CreateItemID(x:integer):dword;
var f:integer;
begin
    result:=player[x].activechar.nextitemid;
    inc (player[x].activechar.nextitemid);
end;

function FileGetPacket(s:ansistring):ansistring;
var f,i:integer;
begin
    result:='';
    if fileexists(path+s) then begin
      f:=fileopen(path+s,$40);
      if f > -1 then begin
      i:=fileseek(f,0,2);
      fileseek(f,0,0);
      setlength(result,i);
      fileread(f,result[1],i);
      fileclose(f);
      end;
    end;
end;

function FixNPCGID(pl:integer;s:ansistring):ansistring;
var x,i:integer;
begin
    result:=s;
    x:=1;
    while x <= length(s) do begin
        move(s[x],i,4);
        safemove(player[pl].GID,result,x+8,4);
        inc(x,i);
    end;

end;


end.

