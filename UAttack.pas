unit UAttack;

interface
uses types,sysutils,uforest,uplayer;

function GetActionHandler(id:dword):tingameaction;

implementation

uses UAttackID, UDebug;

{
he action packet is 0x00440804.

the hit packet is 0x106 .size 0x50

i upload a txt to show them .

in sage's server , damage is base on hit pack : skillID ,hit position ,other bouns flag .

 

 

i explane what 106 did. this is

 

00000106 hit packet . size 0x50.
        8  attacker
        14 who is being attacked
        20 skillID
        2c Hitting position
        40  //or,00000001 JustAttack
            //or,00000002 defened
            //or,00000004 unknow
            //or,00000008 unknow
            //or,00000010 if set, cant got twingun zero distance shoot bouns
            //or,00000080 got ?????? gear damage bounds 25%
            //or,00000600 zonda damgae 25%up , core hit
            //or,00000800 zonda damgae 50%up , focus and core hit
            //or,00001000 RA standing shoot damage bouns
            //or,00040000 unknow
            //or,00080000 unknow
            //or,00200000 gear lv 1
            //or,00400000 gear lv 2
            //or,00600000 gear lv 3max //eg : twingun damage bouns
            //or,10000000 who is being attacked is in air ,not stand in ground
            //or,20000000 if set is front attack ,otherwise is back attack
        44  //or,00000001 pp is low than 50%
            //or,00000008 unknow
            //or,00000010 rolling attack   //eg : twingun damage bouns
            //or,00000088 ??? gear max in super mode.
            //or,00000800 hp is low than 25%
            //or,00010000 ??? hit ,//eg : FO skill damage bouns
        4c  //just attack count

 

SkillID : the name is show in action pack. some skill have mutilple hit , ID is diffent.
 eg: Hu Skill OverEnd have 6 hits,1st is 0x48ffce4b ,2-5 is 0x48ffce4f , 6 is 0xcce4baa4


}

function GetActionHandler(id:dword):tingameaction;
var i:integer;
begin
    result.name:='';
    result.action:=-1;
    result.types:=0;
    result.mul:=0;
    for i:=0 to Action0601count-1 do
    if Action0601[i].id = id then begin
        result.action:=Action0601[i].action;
        result.types:=Action0601[i].types;
        result.element:=Action0601[i].element;
        result.mul:=Action0601[i].mul;
        result.PA:=Action0601[i].PA;

        if result.action = action_use then begin //handle items
            if result.PA = 5 then result.name := 'TelepipeS';
            if result.PA = 1 then result.name := 'MonomateS';
            if result.PA = 2 then result.name := 'DimateS';
            if result.PA = 3 then result.name := 'TrimateS';
            if result.PA = 6 then result.name := 'ShiftaRideS';
            if result.PA = 7 then result.name := 'DebandRideS';
            if result.PA = 8 then result.name := 'MoonAtomizerS';
            if result.PA = 4 then result.name := 'StarAtomizerS';
            if result.PA = 9 then result.name := 'SolAtomizerS';

        end;
        if result.action = action_boost then begin //handle boost
            if result.PA = 1 then result.name := 'Obj:RecoveryPod';
            if result.PA = 6 then result.name := 'Item:ShiftaRide';
            if result.PA = 7 then result.name := 'Item:DebandRide';
            if result.PA = 10 then result.name := 'SE:Burn';
            if (result.PA = 85) and (result.mul = 1) then result.name := 'Tech:ShiftaRide';
            if (result.PA = 85) and (result.mul = 2) then result.name := 'Tech:ShiftaRide';
            if (result.PA = 90) and (result.mul = 1) then result.name := 'Tech:DebandRide';
            if (result.PA = 90) and (result.mul = 2) then result.name := 'Tech:DebandRide';
            if (result.PA = 140) and (result.mul = 1) then result.name := 'Tech:Zanverse';
            if (result.PA = 140) and (result.mul = 2) then result.name := 'Tech:Zanverse';
            if (result.PA = 110) and (result.mul = 1) then result.name := 'Tech:Megiverse';
            if (result.PA = 110) and (result.mul = 2) then result.name := 'Tech:Megiverse';
            if (result.PA = 104) and (result.mul = 1) then result.name := 'Tech:Resta';
            if (result.PA = 104) and (result.mul = 2) then result.name := 'Tech:Resta';
            if (result.PA = 105) and (result.mul = 1) then result.name := 'Tech:Anti';


            if result.PA = $101 then result.name := 'Apt:Howl';
            if result.PA = $102 then result.name := 'Apt:Rage';
            if result.PA = $10c then result.name := 'Apt:Faint';
            if result.PA = $10d then result.name := 'Apt:FaintEnd';


            if (result.PA = 600) and (result.mul = 1) then result.name := 'Env:Thunder';
            if (result.PA = 601) and (result.mul = 1) then result.name := 'Env:Lava';
        end;


    end;
    if result.action = -1 then debug(2,'Unknown ActionID: '+inttohex(id,8));
end;

end.
