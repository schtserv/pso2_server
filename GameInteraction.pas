unit GameInteraction;

interface
uses windows,types,classes,sysutils,UQuestBoard;


    procedure hitHeliDrop(x,t,lvl,id:integer);
    Procedure CreateHeliDrop(t:integer;lvl:integer;pos,rot:TPSO2Vector);
    procedure DwarfHit(x,id:integer);

implementation

uses RoomAction, UForest, UPlayer, ItemGenerator, ULobby, UMonster,
  UPsoUtil;

Procedure CreateHeliDrop(t:integer;lvl:integer;pos,rot:TPSO2Vector);
var st:tstringlist;
    i:integer;
    s:ansistring;
begin
    i:=GetNextTeamItemID(t);
    GetNextTeamItemID(t); //reserv 3 id total
    GetNextTeamItemID(t);
    st:=tstringlist.Create;
    //put it in the floor data for next action
    st.Add('ver 5');
    st.Add('define OPT_FLOAT_1 = 1');
    st.Add('define OPT_FLOAT_0 = 0');
    st.Add('define OPT_INT_55 = 55');
    st.Add('define OPT_INT_52 = 52');
    st.Add('define OPT_INT_53 = 53');
    st.Add('define OPT_INT_51 = 51');
    st.Add('define OPT_INT_54 = 54');

    st.Add('object ob_0210_0154');
    st.Add('id '+inttostr(i+1));
    st.Add(format('rotation %.3f %.3f %.3f %.3f',[rot.x,rot.y,rot.z,rot.w]));
    st.Add(format('position %.3f %.3f %.3f %.3f',[pos.x,pos.y,pos.z,pos.w]));
    st.Add('item_flag 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 04 00 00 00');
    st.Add('byte_flag 02 00 00 00 00 00 00 00 00 00 00 00 00 00 00 80 00 00 68 89');
    st.Add('OPT_INT_54 -1');
    st.Add('OPT_INT_51 1');
    st.Add('OPT_INT_53 0');
    st.Add('OPT_INT_52 0');
    st.Add('OPT_INT_55 1');
    st.Add('OPT_FLOAT_0 1');
    st.Add('OPT_FLOAT_1 1');
    s:=GenerateObjData(st,'\quest\');
    st.clear;

    sendtofloor(t+room_team,lvl,s); //send event
    team[t].FloorObj[lvl]:= team[t].FloorObj[lvl] + s;

    st.Add('ver 5');
    st.Add('define OPT_ID_6 = 6');
    st.Add('define OPT_FLOAT_1 = 1');
    st.Add('define OPT_FLOAT_0 = 0');
    st.Add('define OPT_INT_65 = 65');
    st.Add('define OPT_INT_55 = 55');
    st.Add('define OPT_INT_56 = 56');
    st.Add('define OPT_INT_52 = 52');
    st.Add('define OPT_INT_53 = 53');
    st.Add('define OPT_INT_51 = 51');
    st.Add('define OPT_INT_54 = 54');

    st.Add('object oa_helicopter_n_drop');
    st.Add('id '+inttostr(i));
    st.Add(format('rotation %.3f %.3f %.3f %.3f',[rot.x,rot.y,rot.z,rot.w]));
    st.Add(format('position %.3f %.3f %.3f %.3f',[pos.x,pos.y,pos.z,pos.w]));
    st.Add('item_flag 05 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 01 00 00 00 05 00 00 00');
    st.Add('byte_flag 02 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 04 00 00 00');
    st.Add('OPT_INT_54 196942');
    st.Add('OPT_INT_51 1');
    st.Add('OPT_INT_53 2');
    st.Add('OPT_INT_52 0');
    st.Add('OPT_INT_56 2');
    st.Add('OPT_INT_55 1');
    st.Add('OPT_INT_65 3842');
    st.Add('OPT_FLOAT_0 1045');
    st.Add('OPT_FLOAT_1 1045');
    st.Add('OPT_ID_6 '+inttostr(i+1));

    s:=GenerateObjData(st,'\quest\');
    st.free;

    sendtofloor(t+room_team,lvl,s); //send event


end;

procedure hitHeliDrop(x,t,lvl,id:integer);
var i:integer;
    s,a:ansistring;
    st:tstringlist;
begin
    st:=tstringlist.Create;
    st.Add('ver 5');
    st.Add('define OPT_INT_55 = 55');
    st.Add('define OPT_INT_52 = 52');
    st.Add('define OPT_INT_53 = 53');
    st.Add('define OPT_INT_51 = 51');
    st.Add('define OPT_INT_54 = 54');

    st.Add('object ob_9900_0303');
    st.Add('id '+inttostr(id+1));
    st.Add('rotation 0,000 0,951 0,000 0,309');
    st.Add('position 41,531 -0,740 -97,688 0,000');
    st.Add('item_flag 12 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 05 00 00 00');
    st.Add('byte_flag 02 00 00 00 00 00 00 00 00 00 00 00 00 00 00 80 00 00 44 41');
    st.Add('OPT_INT_54 -1');
    st.Add('OPT_INT_51 1');
    st.Add('OPT_INT_53 2');
    st.Add('OPT_INT_52 0');
    st.Add('OPT_INT_55 1');

    s:=GenerateObjData(st,'\quest\');
    st.Free;

    i:=1;
    while i < length(team[t].FloorObj[lvl]) do begin
        if pdword(@team[t].FloorObj[lvl][i+8])^ = id then begin
            //found break it
            a:=#$24#$00#$00#$00#$04#$79#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
              +#$04#$00#$00#$00#$53#$00#$00#$00#$00#$00#$00#$00#$06#$00#$A9#$11
              +#$04#$00#$00#$00;
            move(player[x].GID,a[9],4);
            move(id,a[$15],4);
            sendtofloor(t+room_team,lvl,a);
            //make new item
            safemove(team[t].FloorObj[lvl][i+$14],s,$15,16); //copy position
            //add new item to floor
            sendtofloor(t+room_team,lvl,s);
            team[t].FloorObj[lvl]:=team[t].FloorObj[lvl]+s;
            break;
        end;

        inc(i,pdword(@team[t].FloorObj[lvl][i])^);
    end;

end;

procedure DwarfHit(x,id:integer);
var s:ansistring;
begin
    s:=#$4C#$00#$00#$00#$15#$01#$04#$00#$56#$02#$00#$00#$00#$00#$00#$00
      +#$06#$00#$a9#$11#$00#$00#$00#$00#$0D#$82#$00#$00#$4E#$70#$63#$43
      +#$6F#$6D#$4F#$6E#$46#$69#$72#$73#$74#$44#$61#$6D#$61#$67#$65#$00
      +#$61#$82#$00#$00#$70#$82#$00#$00#$69#$69#$5F#$64#$77#$61#$72#$66
      +#$5F#$64#$65#$66#$65#$6E#$73#$65#$00#$00#$00#$00;
    sendtofloor(player[x].room,player[x].Floor,s);
end;

end.
