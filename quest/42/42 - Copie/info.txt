ID: $754E
Floor: 4
folder: arks

//reward
exp: 14
mesetas: 14

//multi team setting
team1 1
team2 2
team3 3

//icon
time 2
team 2

//dificulty data

dificulty 1
req_lvl 1
monst_lvl 5
item1 01 00 0C 00 00 00 55 00
item2 01 00 10 00 00 00 D3 00
monst1 1570802465 6
monst2 646695654 2
monst3 492573406 2

#dificulty 2
#req_lvl 20
#monst_lvl 23
#item1 01 00 02 00 00 00 23 00
#item2 01 00 01 00 00 00 54 00
#monst1 1570802465 6
#monst2 646695654 2
#monst3 492573406 2

#dificulty 3
#req_lvl 40
#monst_lvl 42
#item1 01 00 07 00 00 00 22 00
#item2 01 00 08 00 00 00 06 01
#monst1 1570802465 6
#monst2 646695654 2
#monst3 492573406 2

#dificulty 4
#req_lvl 50
#monst_lvl 62
#item1 01 00 0D 00 00 00 79 00
#item2 01 00 0B 00 00 00 2F 00
#monst1 1570802465 6
#monst2 646695654 2
#monst3 492573406 2


