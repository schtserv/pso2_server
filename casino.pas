unit casino;

interface
uses windows,types,sysutils,dateutils;

type
    TShooterStar = record
        id,timer,value,hit,atk:integer;
    end;
    tShooterRoom = record
        shooter_status,shooter_Time:integer;
        shooterstar:array[0..4] of TShooterStar;
        ShooterID:word;
        mode:array[0..3] of byte;
        RoundGhost,gauge:integer;
        ticks:integer;
    end;

    TBJPlayer = record
        stat,count,bet:integer;
        card:array[0..4] of word;
    end;
    TBlackJackTable = record
        cards:ansistring; //this hold the card aviable for pickup
        player:array[0..5] of TBJPlayer;
        timer:integer;
        stat,count,cp,cpr:integer;
    end;

procedure TicketExchange(x:integer);
procedure ExchangeTicket(x,qty:integer);
procedure LoginForCasinoTickets(x:integer);
procedure CasinoTicks(x:integer);
procedure SitInGame(x:integer;s:ansistring);
procedure GoInCasinoGame(x:integer;s:ansistring);
procedure Shooter_HitTarget(x,id:integer);
procedure Shooter_End(x:integer;s:ansistring);
procedure LeaveShooter(x:integer);
procedure initcasino();
procedure sendtoTable(x:integer;s:ansistring);
procedure RequestBJOption(x,v:integer);
procedure CasinoBJBet(x:integer;s:ansistring);
procedure Shooter_keepplaying(x:integer;s:ansistring);
procedure HitOrStay(x,v:integer);


const CASINO_SHOOTER = 1;
    CASINOID_SHOOTER = 1086;
    CASINOID_BLACKJACK = $475;

var CasinoShooterObj:array[0..31] of integer;
    CasinoBJObj:array[0..29] of integer;
    shooterroom:array[0..7] of tShooterRoom;
    BJTable:array[0..5] of TBlackJackTable;

implementation

uses UPlayer, UDB, UInventory, ULobby, Ulogin, UMovement, fmain, UPsoUtil,
  ItemGenerator;


//*****************************************************************************
//
// Common procedure for casino
//

procedure sendtoTable(x:integer;s:ansistring);
var i,l:integer;
begin
    l:=(player[x].shooter_status div 5) * 5;
    for i:=0 to 4 do
        if CasinoBJObj[l+i] > -1 then
            sendtoplayer(CasinoBJObj[l+i],s);
end;

procedure initcasino();
var i,l:integer;
begin
    for i:=0 to 31 do CasinoShooterObj[i]:=-1; //free to use
    for i:=0 to 29 do CasinoBJObj[i]:=-1;
    for i:=0 to 7 do begin
        shooterroom[i].shooter_status:=0;
        shooterroom[i].ShooterID:=1;
        for l:=0 to 3 do shooterroom[i].mode[l]:=random(3);
    end;

    for i:=0 to 5 do begin
        bjtable[i].cards:=''; //empty card
        bjtable[i].stat:=0;
        bjtable[i].cpr:=0;
    end;

end;
procedure UpdateCasinoCoins(x:integer);
var s:ansistring;
begin
    s:=#$10#$00#$00#$00#$3E#$1D#$00#$00#$FF#$FF#$FF#$FF#$64#$00#$00#$00;
    safemove(player[x].save.CasinoCoins,s,$d,4);
    sendtoplayer(x,s);
end;

procedure RemoveCasinoCoin(x,qty:integer);
begin
    //remove coins
    dec(player[x].save.CasinoCoins,qty);
    UpdateCasinoCoins(x);
end;

procedure TicketExchange(x:integer);
begin
    sendtoplayer(x,#$20#$00#$00#$00#$3E#$29#$00#$00#$00#$00#$00#$00#$64#$00#$00#$00
	+#$F4#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00); //unlock 100 - 500 CC
end;

procedure ExchangeTicket(x,qty:integer);
var l,c:integer;
    s:ansistring;
begin
    //make sure we have enought
    c:=-1;
    for l:=0 to player[x].activechar.bankcount-1 do
        if (player[x].activechar.personalbank[l].ItemType = 3)
            and (player[x].activechar.personalbank[l].ItemSub = 3)
            and (player[x].activechar.personalbank[l].ItemEntry = $0268) then
                if (player[x].activechar.personalbank[l].count >= qty) then c:=l;
    if c = -1 then exit; //not enought tickets...
    //remove the ticket
    s:=#$1C#$00#$00#$00#$0F#$06#$04#$00#$F0#$AD#$00#$00#$17#$BC#$34#$75
	+#$F4#$58#$5B#$05#$00#$00#$01#$00#$00#$00#$00#$00;
    safemove(qty,s,$19,4);
    safemove(player[x].activechar.personalbank[c].id1,s,$d,8);
    sendtoplayer(x,s);

    removeFromStorage(x,pdword(@s[$d])^,pdword(@s[$11])^,qty,
        player[x].activechar.personalbank,@player[x].activechar.bankcount,50);

    //add the pts
    inc(player[x].save.CasinoCoins,100);
    if qty = 5 then inc(player[x].save.CasinoCoins,400);

    //update pts
    UpdateCasinoCoins(x);

    //notify the amount won
    s:=#$1C#$00#$00#$00#$3E#$27#$04#$00#$00#$00#$00#$00#$B1#$BC#$00#$00
	+#$64#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;
    if qty = 5 then s:=#$1C#$00#$00#$00#$3E#$27#$04#$00#$00#$00#$00#$00#$B1#$BC#$00#$00
	+#$f4#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;
    sendtoplayer(x,s);
end;

procedure LoginForCasinoTickets(x:integer);
var s:ansistring;
    i,l:integer;
    d:dword;
begin
    //test if can give it
    i:=DayOfTheYear(now);
    if i <> player[x].save.LastTickets then begin
        player[x].save.LastTickets:=i;
      //add the item
      s:=#$44#$00#$00#$00#$0F#$05#$00#$00#$17#$BC#$34#$75#$F4#$58#$5B#$05
        +#$03#$00#$03#$00#$00#$00#$68#$02#$00#$00#$01#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00;
      d:=CreateItemID(x);
      safemove(d,s,9,4);
      safemove(player[x].gid,s,13,4);
      l:=AddToStorage(x,@s[9],player[x].activechar.PersonalBank,@player[x].activechar.bankcount,50);
      if l > -1 then begin   //do we have room?

        //copy the final data
        safemove(player[x].activechar.personalbank[l],s,$9,$38);
        sendtoplayer(x,s);
      end;
    end;
    UpdateCasinoCoins(x);
end;

procedure SitInGame(x:integer;s:ansistring);
var i,r:integer;
begin
    r:=1; //fail

    move(s[13],i,4);
    if (i >= CASINOID_SHOOTER) and (i < CASINOID_SHOOTER+32) then
        if CasinoShooterObj[i-CASINOID_SHOOTER] = -1 then r:=0;

    sendtoplayer(x,#$1C#0#0#0#$3e#$46#0#0+copy(s,9,$10)+chr(r)+#0#0#0);
   // form1.memo1.lines.add('casino id: '+inttostr(i));
end;


//*****************************************************************************
//
// Black Nyack procedure for casino
//

procedure CasinoBJBet(x:integer;s:ansistring);
var a:ansistring;
    c:integer;
begin
    //to-do, remove the CC
   // form1.Memo1.Lines.Add('PLAYER BET');
     a:=#$14#$00#$00#$00#$3E#$3E#$00#$00#$02#$00#$00#$00#$00#$00#$00#$00
                +#$00#$00#$00#$00;
     sendtotable(x,a);

    a:=#$2C#$00#$00#$00#$3E#$40#$04#$00#$20#$CC#$00#$00#$56#$5F#$9C#$00
        +#$00#$00#$00#$00#$04#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$0A#$00#$00#$00#$00#$00#$00#$00;     //bet 0 for now and whait 10 sec
    a[$19]:=ansichar(player[x].shooter_status mod 5);
    move(s[9],c,4);
    bjtable[player[x].shooter_status div 5].player[1+(player[x].shooter_status mod 5)].bet:=c;
    dec(player[x].save.CasinoCoins,c);
    UpdateCasinoCoins( x);
      safemove(player[x].gid,a,$d,4);
    safemove(s[9],a,$1d,4);
    sendtotable(x,a);
end;

procedure RequestBJOption(x,v:integer);
var s:ansistring;
begin
   if v = 2 then begin
    //form1.Memo1.Lines.Add('OPTION REQUESTED');
     sendtoplayer(x,#$20#$00#$00#$00#$3E#$47#$04#$00#$06#$74#$00#$00#$0A#$00#$00#$00
                              +#$14#$00#$00#$00#$1E#$00#$00#$00#$28#$00#$00#$00#$32#$00#$00#$00); //send the 10 - 50 CC options
     sendtoplayer(x,#$14#$00#$00#$00#$3E#$3E#$00#$00#$09#$00#$00#$00#$00#$00#$00#$00#0#0#0#0); //set stat and timer
     {s:=#$2C#$00#$00#$00#$3E#$40#$04#$00#$20#$CC#$00#$00#$56#$5F#$9C#$00
                      +#$00#$00#$00#$00#$04#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                      +#$00#$00#$00#$00#$0A#$00#$00#$00#$00#$00#$00#$00;     //bet 0 for now and whait 10 sec
     move(player[x].gid,s[$d],4);

     s[$19]:=chr(player[x].shooter_status mod 5);
     sendtoTable(x,s);               }
   end;
end;

function GetBJCard(x:integer):word;
var i,l:integer;
begin
    if bjtable[x].cards = '' then begin
        for i:=1 to 4 do
        for l:=1 to 13 do
            bjtable[x].cards:=bjtable[x].cards+chr(i)+chr(l);
    end;
    l:=length(bjtable[x].cards) div 2;
    l:=random(l);

    move(bjtable[x].cards[1+(l*2)],result,2);
    delete( bjtable[x].cards,1+(l*2),2);
end;

function getcardvalue(x,l:integer):integer;
var v,a,i:integer;
begin
    a:=0;
    result:=0;
    for v:=0 to bjtable[x].player[l].count-1 do begin
        if bjtable[x].player[l].card[v] shr 8 = 1 then begin
            inc(a);
            inc(result,11);
        end else begin
            i:=(bjtable[x].player[l].card[v] shr 8);
            if i> 10 then i:=10;
            result:=result+ i;
        end;
    end;
    while (result > 21) and (a>0) do begin
        dec(a);
        dec(result,10);
    end;
end;

procedure HitOrStay(x,v:integer);
var t,i,c,l,k:integer;
    s:ansistring;
begin
    t:=player[x].shooter_status;
    i:=t mod 5;
    t:=t div 5;
    if bjtable[t].cp = i then begin
        //only if my turn
        //send the action to table
        s:=#$28#$00#$00#$00#$3E#$42#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	      +#$04#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$02#$00#$00#$00
	      +#$00#$00#$00#$00#$00#$00#$00#$00;
        safemove(i,s,$1d,4);

        if bjtable[t].player[i+1].count = 5 then v:=1; //force stay
        if v = 1 then begin//stay
            bjtable[t].player[i+1].stat:=2;
            //form1.Memo1.Lines.Add('STAY');
        end;
        //hit
        if v = 2 then begin
            //give a card
            //form1.Memo1.Lines.Add('HIT');
            s[$15] := #2; //hit
            c:= GetBJCard(t);
            safemove(c,s,$19,2);
            s[$1b]:=#1; //reversed
            bjtable[t].player[i+1].card[bjtable[t].player[i+1].count]:=c;
            inc(bjtable[t].player[i+1].count);
            k:=getcardvalue(t,i+1);
            if k > 21 then begin
                //s[$25]:=#1; //burst
                bjtable[t].player[i+1].stat:=2;
            end;
            //if bjtable[t].player[i+1].count = 5 then s[$15] := #1; //max card, should stay
        end;

        if v = 3 then begin
            //double down??
        end;

        sendtotable(x,s);

        //switch player
        c:=(bjtable[t].cp+1) mod 5;
        while (casinobjobj[(t*5)+c] = -1) or (bjtable[t].player[c+1].stat=0 ) do c:=(c+1) mod 5;
        bjtable[t].cp:=c;

        //test if all stay switch to stat 4
        l:=0;
        bjtable[t].timer:=26;
        for c:=1 to 5 do
            if casinobjobj[(t*5)+c-1] > -1 then
            if bjtable[t].player[c].stat = 1 then l:=1;
        if l = 0 then begin
            //form1.Memo1.Lines.Add('FINAL');
            //switch
            sendtotable(x,#$14#$00#$00#$00#$3E#$3E#$00#$00#$08#$00#$00#$00#$00#$00#$00#$00
	                +#$00#$00#$00#$00 );

            k:=0;
            //send every player card
            for c:=0 to 4 do
                if casinobjobj[(t*5)+c] > -1 then
                if bjtable[t].player[c+1].stat > 0 then begin
                    s:=#$1C#$00#$00#$00#$3E#$43#$04#$00#$00#$00#$00#$00+getmagic(bjtable[t].player[c+1].count,$5D0B,$32) ;
                    for l:=0 to bjtable[t].player[c+1].count-1 do begin
                        s:=s+chr(bjtable[t].player[c+1].card[l])+chr( bjtable[t].player[c+1].card[l] shr 8)
                        +#0#0+chr(c)+#0#0#0+#0#0#0#0;
                    end;
                    inc(k);
                    safemove(k,s,9,4);
                    l:=length(s);
                    safemove(l,s,1,4);
                    sendtotable(x,s);
                end;

            //send croupier card
            s:=#$1C#$00#$00#$00#$3E#$43#$04#$00#$00#$00#$00#$00+getmagic(bjtable[t].player[0].count,$5D0B,$32) ;
            for l:=0 to bjtable[t].player[0].count-1 do begin
                s:=s+chr(bjtable[t].player[0].card[l])+chr( bjtable[t].player[0].card[l] shr 8)
                +#0#0+#$ff#$ff#$ff#$FF+#0#0#0#0;
            end;
            inc(k);
            safemove(k,s,9,4);
            l:=length(s);
            safemove(l,s,1,4);
            sendtotable(x,s);

            bjtable[t].count:=k;
            bjtable[t].stat:=4;
            bjtable[t].timer:=8;
        end else begin
            //form1.Memo1.Lines.Add('NEXT PLAYER');
            s:=#$14#$00#$00#$00#$3E#$3E#$00#$00#$00#$00#$00#$00+chr(bjtable[t].cp)+#$00#$00#$00#$a#0#0#0;
            sendtotable(x,s);
        end;
    end;
end;



procedure BJGameTick(x:integer);
var i,c,l,k,r:integer;
    s,a:ansistring;
begin
     //do we have player in it
     c:=0;
     for i:=0 to 4 do
        if CasinoBJObj[(x*5)+i] > -1 then inc(c);
     if c > 0 then begin
         //player in

         if bjTable[x].stat = 0 then begin
            dec(BJTable[x].timer);
             if BJTable[x].timer=20 then begin
                for i:=0 to 4 do if CasinoBJObj[(x*5)+i] > -1 then begin
                    a:=#$2C#$00#$00#$00#$3E#$40#$04#$00#$20#$CC#$00#$00#$56#$5F#$9C#$00
                          +#$00#$00#$00#$00#$04#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                          +#$00#$00#$00#$00#$0A#$00#$00#$00#$00#$00#$00#$00;     //bet 0 for now and whait 10 sec
                    safemove(player[CasinoBJObj[(x*5)+i]].gid,a,$d,4);
                    a[$19]:=ansichar(i);
                    sendtotable(CasinoBJObj[(x*5)+i],a);
                end;
             end;
             l:=0;
             for i:=0 to 4 do if CasinoBJObj[(x*5)+i] > -1 then
                if BJTable[x].player[1+i].bet = 0 then l:=1;
             if l = 0 then BJTable[x].timer := 0;
             
            if BJTable[x].timer<=0 then begin
                bjtable[x].stat:=1;
                bjtable[x].timer:=23;
            end;
         end;

         if BJTable[x].stat = 1 then begin
             //test if all player have bet, if yes move to next
             //also test if player stat = 0 then bring him in game
             dec(BJTable[x].timer);
             if BJTable[x].timer<=0 then begin
                //form1.Memo1.Lines.Add('NEW GAME STARTED');
                 //kick all lazy player
                 BJTable[x].player[0].count:=0;
                 for i:=0 to 4 do if CasinoBJObj[(x*5)+i] > -1 then begin
                    BJTable[x].player[i+1].count:=0;

                    if BJTable[x].player[1+i].stat = 0 then
                        sendtoplayer(CasinoBJObj[(x*5)+i],#$14#$00#$00#$00#$3E#$3E#$00#$00#$06#$00#$00#$00#$00#$00#$00#$00#0#0#0#0);
                    BJTable[x].player[1+i].stat:=1;
                    if BJTable[x].player[1+i].bet = 0 then begin
                        //kick him
                        BJTable[x].player[1+i].stat:=0; //keep him out
                        CasinoBJObj[(x*5)+i]:=-1;
                    end;
                    

                 end;
                 //set ready
                 BJTable[x].count:=0;
                 s:=#$1C#$00#$00#$00#$3E#$43#$04#$00#$00#$00#$00#$00#$39#$5D#$00#$00;
                 //give 2 card eatch player
                 k:=0;
                 for c:=0 to 1 do begin
                     for i:=-1 to 4 do begin
                        l:=0;
                        if i = -1 then l:=1;
                        if i>-1 then if (CasinoBJObj[(x*5)+i] > -1) then
                            if BJTable[x].player[i+1].stat>0 then l:=1;
                         if l=1 then begin
                             a:=#$02#$01#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00;
                             //move(BJTable[x].count,a[$9],4);
                             //inc(BJTable[x].count);
                             l:=GetBJCard(x);
                             safemove(l,a,$1,2);
                             BJTable[x].player[i+1].card[c]:=l;
                             inc(BJTable[x].player[i+1].count);
                             safemove(i,a,$5,4);
                             a[$3]:=ansichar(c);
                             //set the amount of special
                             s:=s+a;
                             inc(k);
                         end;
                     end;
                 end;
                 {Xor: 5D0B  Sub: 32
                  Xor: 5D1B  Sub: 22
                  Xor: 5D2B  Sub: 12
                  Xor: 5D3B  Sub: 02
                  Xor: 5D4B  Sub: 72
                  Xor: 5D5B  Sub: 62
                  Xor: 5D6B  Sub: 52
                  Xor: 5D7B  Sub: 42
                  Xor: 5D8B  Sub: B2
                  Xor: 5D9B  Sub: A2
                  Xor: 5DAB  Sub: 92
                  Xor: 5DBB  Sub: 82
                  Xor: 5DCB  Sub: F2
                  Xor: 5DDB  Sub: E2
                  Xor: 5DEB  Sub: D2
                  Xor: 5DFB  Sub: C2}
                 a:=GetMagic(k,$5D0B,$32);  //max 15 card for now
                 safemove(a[1],s,$d,4);
                 i:=length(s);
                 safemove(i,s,1,4);
                 k:=-1;
                 BJTable[x].cp:=BJTable[x].cpr;

                 for i:=0 to 4 do if CasinoBJObj[(x*5)+((i+BJTable[x].cp) mod 5)] > -1 then
                    if BJTable[x].player[((i+BJTable[x].cp) mod 5)+1].stat > 0 then begin
                    if k = -1 then k:=((i+BJTable[x].cp) mod 5);
                    sendtoplayer(CasinoBJObj[(x*5)+((i+BJTable[x].cp) mod 5)],s);
                    a:=#$14#$00#$00#$00#$3E#$3E#$00#$00#$00#$00#$00#$00+chr(k)+#$00#$00#$00#$a#0#0#0; //set stat and timer and active player
                    sendtoplayer(CasinoBJObj[(x*5)+((i+BJTable[x].cp) mod 5)],a);
                 end;
                 BJTable[x].cp:=k;//(k+1) mod 5;
                 BJTable[x].cpr:=(k+1) mod 5;
                 BJTable[x].stat:=2;
                 BJTable[x].timer:=25; //safe timer in case the user dced
                 //form1.Memo1.Lines.Add('GO TO DECISION');
             end;
         end;
         if BJTable[x].stat = 2 then begin
             //decrease timer, if hit 0 then too late take it as stay
             dec(BJTable[x].timer);

             if BJTable[x].timer<=0 then begin
                //form1.Memo1.Lines.Add('FORCED STAY CP : '+inttostr(BJTable[x].cp));
                 HitOrStay(CasinoBJObj[(x*5)+BJTable[x].cp],1);
             end;
         end;
         
         if BJTable[x].stat = 4 then begin
             //decrese timer for a fake thinking time
             dec(BJTable[x].timer);
             if BJTable[x].timer<=0 then begin
                //form1.Memo1.Lines.Add('CROUPIER');
                l:=getcardvalue(x,0);

                k:=0;
                s:=#$1C#$00#$00#$00#$3E#$43#$04#$00#$00#$00#$00#$00 ;
                a:='';
                while l < 17 do begin
                 //do we take an other card or no? if yes repete 3 else give or take reward and go status 0

                   i:=GetBJCard(x);
                   bjtable[x].player[0].card[bjtable[x].player[0].count]:=i;
                   inc(bjtable[x].player[0].count);
                   a:=a+chr(i)+chr( i shr 8)
                      +#0#0+#$ff#$ff#$ff#$FF+#0#0#0#0;
                   inc(k);

                  l:=getcardvalue(x,0);
                 end ;
                 s:=s+getmagic(k,$5D0B,$32)+a;
                 inc(bjtable[x].count);
                  k:= bjtable[x].count;

                  safemove(k,s,9,4);
                  l:=length(s);
                  safemove(l,s,1,4);
                  for l:=0 to 4 do
                    if casinobjobj[(x*5)+l] > -1 then
                        sendtoplayer(casinobjobj[(x*5)+l],s);
                  BJTable[x].timer:=6;


                 //croupier stay
                 s:=#$44#$00#$00#$00#$3E#$44#$04#$00#$B6#$E3#$00#$00#$56#$5F#$9C#$00
                   +#$00#$00#$00#$00#$04#$00#$00#$00#$00#$00#$00#$00#$ff#$ff#$ff#$ff
                   +#$0A#$00#$00#$00#$14#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                   +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$B6#$E3#$00#$00
                   +#$00#$00#$00#$00 ;
                 r:=getcardvalue(x,0);

                 for l:=0 to 4 do
                    if casinobjobj[(x*5)+l] > -1 then
                    if bjtable[x].player[l+1].stat > 0 then begin
                         k:=getcardvalue(x,l+1);
                         safemove(player[casinobjobj[(x*5)+l]].gid,s,$d,4);
                         safemove(bjtable[x].player[l+1].bet,s,$21,4);
                         safemove(l,s,$19,4);
                         i:=-1;
                         safemove(i,s,$1d,4);
                         i:=0;
                         safemove(i,s,$25,4);
                         i:=bjtable[x].player[l+1].bet*2;
                         if (k < 22) and ((k>=r) or (r>21)) then begin
                             //player win
                             safemove(i,s,$25,4);
                             inc(player[casinobjobj[(x*5)+l]].save.CasinoCoins,i);
                             i:=1;
                             safemove(i,s,$1d,4);

                             UpdateCasinoCoins( casinobjobj[(x*5)+l]);

                         end;
                         bjtable[x].player[l+1].bet:=0;
                         sendtotable(casinobjobj[(x*5)+l],s);
                         sendtoplayer(casinobjobj[(x*5)+l],#$14#$00#$00#$00#$3E#$3E#$00#$00#$07#$00#$00#$00#$00#$00#$00#$00#0#0#0#0);
                    end;
                 bjtable[x].stat:=0;
                 bjtable[x].timer:=46;
                 //form1.Memo1.Lines.Add('GO STAT 0');

             end;

         end;

     end else begin
        BJTable[x].stat:=0; //no game
        BJTable[x].timer:=23;
        BJTable[x].cp:=0;
     end;
end;


//*****************************************************************************
//
// Shooter casino procedure
//





procedure Shooter_keepplaying(x:integer;s:ansistring);
begin
     RemoveCasinoCoin(x,100); //to-do find how much we need to remove
end;


procedure GoShooter(x,id:integer);
var s:ansistring;
    l:integer;
begin
    leavelobby(x);
    l:=CASINOID_SHOOTER+id;
    s:=#$24#$00#$00#$00#$04#$79#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00#$3F#$04#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
    +#$04#$00#$00#$00;
    safemove(l,s,$15,4);
    SendLobbyChar2(ROOM_CASINO,x,s);

    CasinoShooterObj[id]:=x;
    player[x].room:=Room_CasinoGame+(id div 4);
    player[x].casinogame:=CASINO_SHOOTER;

    sendtoplayer(x,#$0C#$00#$00#$00#$1E#$0C#$00#$00#$82#$00#$00#$00);



    s:=#$54#$00#$00#$00#$03#$00#$00#$00#$7F#$08#$00#$00#$00#$00#$00#$00
    +#$05#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$9D#$FF#$FF#$FF#$69#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
    +#$00#$00#$00#$00;
    safemove(player[x].gid,s,$15,4);
    sendtoplayer(x,s);


    //sendtoplayer(x,#$08#$00#$00#$00#$03#$2A#$00#$00);

    s:=#$14#$00#$00#$00#$06#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00;
    safemove(player[x].gid,s,9,4);
    sendtoplayer(x,s);

    sendtoplayer(x,#$08#$00#$00#$00#$03#$2B#$00#$00);



    s:=#$00#$00#$00#$3C#$00#$00#$00#$80#$8C#$45#$80#$4A
    +#$58#$54#$00#$0;

    move(s[9],player[x].curpos,8);
    move(s[1],player[x].rotation,8);
    SpawnPlayer(x);

    //send map obj
    s:=GenerateObjFile(path+'map\casino_shooter.obj',-1,-1);
    sendObjtoplayer(x,s);

    s:=#$54#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00+chr($47+(id and 3))+#$0D#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
    +#$00#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$EB#$5C#$00#$00#$53#$69#$74#$53#$75#$63#$63#$65#$73#$73#$41#$75
    +#$74#$6F#$00#$00;

    safemove(player[x].gid,s,9,4);
    safemove(player[x].gid,s,$25,4);
    sendtolobby(player[x].room,-1,s);

    sendtoplayer(x,#$1C#$00#$00#$00#$3E#$0B#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$01#$00#$02#$02#$10#$58#$6F#$41#$00#$00#$00#$00); //not sure

    //UpdateCasinoCoins(x);
    RemoveCasinoCoin(x,100); //to-do find how much we need to remove

    sendtoplayer(x,#$08#$00#$00#$00#$03#$23#$00#$00);



    sendtoplayer(x,#$0C#$00#$00#$00#$19#$0F#$00#$00#$7D#$00#$00#$00);

    //sendArmor(x);

    //send npc
    //s:=FileGetPacket('map\casino.npc');
    //sendtoplayer(x,FixNPCGID(x,s));
    player[x].floor:=-1;
    player[x].firstping:=0;
    player[x].shooter_status:=id div 4; //whait for loading
    player[x].shooterround:=3;

end;

procedure GoInCasinoGame(x:integer;s:ansistring);
var i:integer;
begin
    //depending of the id and option
    //select game and remove the CC
    {000000: 24 00 00 00 03 31 00 00 00 00 00 00 00 00 00 00     $....1..........
000010: 10 00 00 00 3F 04 00 00 00 00 00 00 06 00 00 00     ....?...........
000020: 00 00 00 00     ....
}
    move(s[$15],i,4);

    if (i >= CASINOID_SHOOTER) and (i < CASINOID_SHOOTER+32) then begin
        //this is the shooter game
        GoShooter(x,i-CASINOID_SHOOTER);
    end;

end;

procedure Shooter_HitTarget(x,id:integer);
var i,l:integer;
    s:ansistring;
begin
    for i:=0 to 4 do
        if shooterroom[player[x].shooter_status].shooterstar[i].id = id then begin
            if shooterroom[player[x].shooter_status].shooterstar[i].value < 5 then begin
            inc(shooterroom[player[x].shooter_status].shooterstar[i].hit);
            l:=3;
            if shooterroom[player[x].shooter_status].shooterstar[i].value = 3 then l:=10;
            if shooterroom[player[x].shooter_status].shooterstar[i].value = 4 then l:=70;
            if shooterroom[player[x].shooter_status].shooterstar[i].hit >= l then begin
                //kill it and create an new entry
                shooterroom[player[x].shooter_status].shooterstar[i].id:=0;
                shooterroom[player[x].shooter_status].shooterstar[i].timer:=random(6)+3;
                s:=#$1C#$00#$00#$00#$3E#$07#$04#$00#$01#$00#$00#$00#$00#$00#$00#$00
                    +#$AF#$05#$00#$00#$00#$00#$00#$00
                    +#0+#$00#$00#$00;
                s[$19]:=ansichar(shooterroom[player[x].shooter_status].shooterstar[i].value);
                if s[$19] = #3 then begin
                    //king , drop many
                    s:=#$34#$00#$00#$00#$3E#$07#$04#$00#$01#$00#$00#$00#$00#$00#$00#$00
                    +#$Ac#$05#$00#$00
                    +#$00#$00#$00#$00#$02#$00#$00#$00
                    +#$00#$00#$00#$00#$02#$00#$00#$00
                    +#$00#$00#$00#$00#$01#$00#$00#$00
                    +#$00#$00#$00#$00#$00#$00#$00#$00;
                end;
                safemove(id,s,9,4);
                sendtolobby(player[x].room,-1,s);
                inc(shooterroom[player[x].shooter_status].gauge,1);
                if shooterroom[player[x].shooter_status].gauge mod 10 = 0 then begin
                    //refresh gauge
                    sendtolobby(player[x].room,-1,
                        #$0C#$00#$00#$00#$3E#$04#$00#$00+chr(shooterroom[player[x].shooter_status].gauge div 10)+#$00#$00#$00);
                end;
            end;
            end else begin
                //single hit monster
                if shooterroom[player[x].shooter_status].shooterstar[i].value = 5 then begin
                    //ghost make it drop bombs
                    s:=#$34#$00#$00#$00#$3E#$07#$04#$00#$01#$00#$00#$00#$00#$00#$00#$00
                    +#$Ac#$05#$00#$00
                    +#$00#$00#$00#$00#$07#$00#$00#$00
                    +#$00#$00#$00#$00#$08#$00#$00#$00
                    +#$00#$00#$00#$00#$07#$00#$00#$00
                    +#$00#$00#$00#$00#$08#$00#$00#$00;
                    safemove(id,s,9,4);
                    sendtolobby(player[x].room,-1,s);
                    shooterroom[player[x].shooter_status].shooterstar[i].id:=0;
                    shooterroom[player[x].shooter_status].shooterstar[i].timer:=random(6)+3;
                    for l:=0 to 5 do begin
                    dec(shooterroom[player[x].shooter_status].gauge);
                    if shooterroom[player[x].shooter_status].gauge mod 10 = 9 then
                        sendtolobby(player[x].room,-1,
                            #$0C#$00#$00#$00#$3E#$04#$00#$00+chr(shooterroom[player[x].shooter_status].gauge div 10)+#$00#$00#$00);
                    end;

                end;
                if shooterroom[player[x].shooter_status].shooterstar[i].value >= 6 then begin
                    //ghost make it drop bombs
                    s:=#$1C#$00#$00#$00#$3E#$07#$04#$00#$01#$00#$00#$00#$00#$00#$00#$00
                    +#$AF#$05#$00#$00#$00#$00#$00#$00#0#$00#$00#$00;
                    shooterroom[player[x].shooter_status].shooterstar[i].id:=0;
                    shooterroom[player[x].shooter_status].shooterstar[i].timer:=random(6)+3;
                    s[$19]:=ansichar(random(4)+3);
                    safemove(id,s,9,4);
                    sendtolobby(player[x].room,-1,s);
                end;

            end;
        end;

end;

procedure Shooter_End(x:integer;s:ansistring);
begin
    //need to figure out the format

    sendtoplayer(x,#$08#$00#$00#$00#$3E#$0F#$00#$00);

end;

procedure LeaveShooter(x:integer);
var i,l:integer;
    s:ansistring;
begin
    for i:=0 to 31 do
       if CasinoShooterObj[i] = x then begin
            CasinoShooterObj[i]:=-1;
            l:=CASINOID_SHOOTER+i;
            s:=#$24#$00#$00#$00#$04#$79#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
            +#$04#$00#$00#$00#$3F#$04#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
            +#$04#$00#$00#$00;
            safemove(l,s,$15,4);
            SendLobbyChar2(ROOM_CASINO,x,s);
       end;
    GoCasino(x,1);
end;



procedure CasinoTicks(x:integer);
var i:integer;
    s:ansistring;
const
    ap:array[0..8] of dword =(
    $195b0939, $01e89ed4, $772a426F, $d4c8feFF, $50e73041, $66171246, $D0d90d8F, $3d999eFE, $87b82eB0);
begin
    if (x >= 8) and (x<14) then BJGameTick(x-8);
    if x < 8 then begin  //shooter game
        if shooterroom[x].shooter_status = 2 then begin
            inc(shooterroom[x].ticks);
            if shooterroom[x].ticks > 10 then begin
                if shooterroom[x].gauge > 0 then dec(shooterroom[x].gauge);
                shooterroom[x].ticks:=0;
                if shooterroom[x].gauge mod 10 = 9 then
                        //refresh gauge
                        sendtolobby(player[x].room,-1,
                            #$0C#$00#$00#$00#$3E#$04#$00#$00+chr(shooterroom[x].gauge div 10)+#$00#$00#$00);
            end;

            //process stars
            for i:=0 to 4 do
                if shooterroom[x].shooterstar[i].id = 0 then begin
                    //make them apear if the time has come
                    dec(shooterroom[x].shooterstar[i].timer);
                    if shooterroom[x].shooterstar[i].timer <= 0 then begin
                        //time to make it apear
                        inc(shooterroom[x].ShooterID);
                        if shooterroom[x].ShooterID = 0 then inc(shooterroom[x].ShooterID); //never allow 0
                        shooterroom[x].shooterstar[i].id:=shooterroom[x].ShooterID;
                        shooterroom[x].shooterstar[i].timer:=23; //about 12 seconds
                        shooterroom[x].shooterstar[i].hit:=0;
                        shooterroom[x].shooterstar[i].value:=random(3);
                        if shooterroom[x].mode[0] = 0 then begin
                            if shooterroom[x].shooterstar[i].value = 2 then shooterroom[x].shooterstar[i].value:=random(3);
                        end;
                        if shooterroom[x].mode[0] = 1 then begin
                            if shooterroom[x].shooterstar[i].value = 0 then shooterroom[x].shooterstar[i].value:=random(3);
                        end;
                        if shooterroom[x].mode[0] = 2 then inc(shooterroom[x].shooterstar[i].value);

                        shooterroom[x].shooterstar[i].atk:=random(10)+7; //when it will attack
                        if shooterroom[x].shooterstar[i].atk = 7 then shooterroom[x].shooterstar[i].atk:=12312; //never

                        if random(30) = 1 then begin
                            shooterroom[x].shooterstar[i].timer:=14; //about 7 seconds
                            shooterroom[x].shooterstar[i].value:=6;
                            shooterroom[x].shooterstar[i].atk:=12312; //never
                        end;
                        if random(500) = 1 then begin
                            shooterroom[x].shooterstar[i].timer:=14; //about 7 seconds
                            shooterroom[x].shooterstar[i].value:=7;
                            shooterroom[x].shooterstar[i].atk:=12312; //never
                        end;
                        //test if can make a ghost apear
                        if shooterroom[x].RoundGhost = 0 then begin
                            if random(30) = 1 then begin
                                shooterroom[x].RoundGhost:=1;
                                shooterroom[x].shooterstar[i].timer:=30; //about 15 seconds
                                shooterroom[x].shooterstar[i].value:=5;
                                shooterroom[x].shooterstar[i].atk:=12312; //never
                            end;
                        end;
                        //enemy type:
                        // 0 = bronze
                        // 1 = silver
                        // 2 = gold
                        // 3 = king
                        // 4 = queen over the field?
                        // 5 = ghost
                        // 6 = running squirel
                        // 7 = geni dans une lampe


                        //chose a direction and make it apear
                        s:=#$14#$00#$00#$00#$3E#$09#$00#$00#$04#$00#$00#$00#$00#$00#$00#$00#$AA#$13#$0E#$97;
                        safemove(shooterroom[x].shooterstar[i].id,s,9,4);
                        safemove(shooterroom[x].shooterstar[i].value,s,13,4);
                        safemove(ap[random(9)],s,$11,4);
                        sendtolobby(Room_CasinoGame+x,-1,s);
                    end;
                end else begin
                    //handle them and make them attack if needed
                    dec(shooterroom[x].shooterstar[i].timer);
                    if shooterroom[x].shooterstar[i].timer = shooterroom[x].shooterstar[i].atk then begin
                        //time to attack
                        s:=#$1C#$00#$00#$00#$3E#$07#$04#$00#$01#$00#$00#$00#$02#$00#$00#$00
                        +#$AF#$05#$00#$00#$00#$00#$00#$00+#7+#$00#$00#$00;   //9= chrystal,8 = mega bomb, 7 = bomb , 6 = x2 , 5 = speed up , 4 = big catcher, 3 = powerup
                        if random(5) = 1 then s[$19]:=#8;
                        //if random(9) = 5 then s[$19]:=chr(random(7)+1);
                        safemove(shooterroom[x].shooterstar[i].id,s,9,4);
                        sendtolobby(Room_CasinoGame+x,-1,s);
                    end;
                    if shooterroom[x].shooterstar[i].timer <= 0 then begin
                        //forget them and make them ready for next
                        shooterroom[x].shooterstar[i].id:=0;
                        shooterroom[x].shooterstar[i].timer:=random(6)+3;
                    end;
                end;


            dec(shooterroom[x].shooter_time,500);
            if shooterroom[x].shooter_time <= 0 then begin
                for i:=0 to 2 do shooterroom[x].mode[i] := shooterroom[x].mode[i+1];
                shooterroom[x].mode[3]:=random(3); //future wave
                s:=#$10#$00#$00#$00#$3E#$0C#$00#$00#$00#$00#$02#$00#$00#$02#$02#$02;
                safemove(shooterroom[x].mode[0],s,$d,4); //current mode and upcomming
                shooterroom[x].shooter_status:=0;
                for i:=0 to 3 do
                    if CasinoShooterObj[(x*4)+i] >-1 then begin
                        dec(player[CasinoShooterObj[(x*4)+i]].shooterround);
                        s[11]:=ansichar(player[CasinoShooterObj[(x*4)+i]].shooterround);
                        sendtoplayer(CasinoShooterObj[(x*4)+i],s);
                        shooterroom[x].shooter_status:=1;
                    end;
                shooterroom[x].shooter_time:=10500;

            end;
        end;

        if shooterroom[x].shooter_status = 1 then begin
            //only whait 5 second so decrease the time and when reatch <0 then start the game
            dec(shooterroom[x].shooter_time,500);
            if shooterroom[x].shooter_time <= 0 then begin                                          //raise    turn left   3 coin color
                s:=#$10#$00#$00#$00#$3E#$0C#$00#$00#$01#$00#$03#$00#$01#$00#$02#$02;
                safemove(shooterroom[x].mode[0],s,$d,4); //current mode and upcomming
                for i:=0 to 3 do
                    if CasinoShooterObj[(x*4)+i] >-1 then begin
                        s[11]:=ansichar(player[CasinoShooterObj[(x*4)+i]].shooterround);
                        sendtoplayer(CasinoShooterObj[(x*4)+i],s);
                    end;
                shooterroom[x].RoundGhost:=0; //allow it to apear
                shooterroom[x].shooter_status:=2; //first round
                shooterroom[x].shooter_time:=45000;
                shooterroom[x].shooterstar[0].id:=0; //set the first star data
                shooterroom[x].shooterstar[0].timer:=random(3);
                shooterroom[x].shooterstar[1].id:=0;
                shooterroom[x].shooterstar[1].timer:=random(4)+2;
                shooterroom[x].shooterstar[2].id:=0;
                shooterroom[x].shooterstar[2].timer:=random(5)+5;
                shooterroom[x].shooterstar[3].id:=0;
                shooterroom[x].shooterstar[3].timer:=random(6)+7;
                shooterroom[x].shooterstar[4].id:=0;
                shooterroom[x].shooterstar[4].timer:=random(7)+10;
                shooterroom[x].ShooterID:=0;
            end;
        end;

    end;

end;

end.
