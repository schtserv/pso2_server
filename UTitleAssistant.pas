unit UTitleAssistant;

interface

uses types,classes;

type
  TTitleEntry = record
      id,folder:dword;
      name:widestring;
      desc:widestring;
      items:array[0..$2f] of byte;
  end;

const TitleCount = 1;
      Titles : array [0..0] of TTitleEntry = (
        (id:$de;
        folder: $E1;
        name:'Title Master';
        desc:'You''r the fucking master!!';
        items:(1,0,2,0,0,0,2,0,0,0,0,0,0,0,0,0
              ,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
              ,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0))
      );

procedure SendTitleName(x:integer);
procedure SendEarnedTitle(x:integer);
procedure GetFullTitleList(x:integer);
Procedure GetTitleDescription(x,id:integer);

implementation

uses UPsoUtil, UPlayer, UInventory;

procedure SendTitleName(x:integer);
var s,a,b:ansistring;
    i,l:integer;
begin
    //basicly make the list of id vs name to be displayed
    //i wont bother to send only the minimum, i send all at once by slice of 10 names max
    //XOR: 57E6   SUB: 92
    i:=0;
    s:='';
    a:='';
    b:='';
    for l:=0 to TitleCount-1 do begin
        s:=s+#0#0#0#0;
        move(Titles[l].id,s[1+(i*4)],4);
        a:=a+WideToAnsi(Titles[l].name);
        b:=b+ansichar(length(titles[l].name));
        inc(i);
        if i=10 then begin
            a:=a+#0#0;
            while length(a) and 3 <> 0 do a:=a+#0;
            while length(b) and 3 <> 0 do b:=b+#0;
            s:=#$A8#$00#$00#$00#$31#$05#$04#$00+getmagic(i,$57E6,$92)+s
              +getmagic(length(a) div 2,$57E6,$92)+a+getmagic(i,$57E6,$92)+b;
            i:=length(s);
            move(i,s[1],4);
            sendtoplayer(x,s);
            i:=0;
            s:='';
            a:='';
            b:='';
        end;

    end;
    if i>0 then begin
        a:=a+#0#0;
        while length(a) and 3 <> 0 do a:=a+#0;
        while length(b) and 3 <> 0 do b:=b+#0;
        s:=#$A8#$00#$00#$00#$31#$05#$04#$00+getmagic(i,$57E6,$92)+s
          +getmagic(length(a) div 2,$57E6,$92)+a+getmagic(i,$57E6,$92)+b;
        i:=length(s);
        move(i,s[1],4);
        sendtoplayer(x,s);
    end;

end;

procedure SendEarnedTitle(x:integer);
var s:ansistring;
begin
    SendTitleName(x);

    {
    Xor: C60D  Sub: 11
    Xor: C61D  Sub: 01
    Xor: C62D  Sub: 31
    Xor: C63D  Sub: 21
    Xor: C64D  Sub: 51
    Xor: C65D  Sub: 41
    Xor: C66D  Sub: 71
    Xor: C67D  Sub: 61
    Xor: C68D  Sub: 91
    Xor: C69D  Sub: 81
    Xor: C6AD  Sub: B1
    Xor: C6BD  Sub: A1
    Xor: C6CD  Sub: D1
    Xor: C6DD  Sub: C1
    Xor: C6ED  Sub: F1
    Xor: C6FD  Sub: E1
    }
    s:=#$10#$00#$00#$00#$31#$02#$04#$00+getmagic(0,$c60d,$11)+#$de#$00#$00#$00;

    sendtoplayer(x,s);
end;

procedure GetFullTitleList(x:integer);
var s:ansistring;
    l:integer;
begin
    //fill it with the proper data
    setlength(s,TitleCount*$38);
    fillchar(s[1],TitleCount*$38,0);
    for l:=0 to TitleCount-1 do begin
        move(titles[l].id, s[1+(l*$38)],4);
        move(titles[l].id, s[5+(l*$38)],4);
        move(titles[l].folder, s[9+(l*$38)],2);
        //s[$b+(l*$38)] := #1; //make it aviable?
        move(titles[l].items[0], s[$19+(l*$38)],$30);
    end;

    //send the item name
    SendItemsName(x,TitleCount,$38,@s[17]);

    s:=#0#0#0#0#$31#4#4#0+getmagic(titlecount,$D280,$59)+s;
    l:=length(s);
    move(l,s[1],4);
    sendtoplayer(x,s);
end;

Procedure GetTitleDescription(x,id:integer);
var l,i:integer;
    s:ansistring;
begin
    for l:=0 to TitleCount-1 do
      if titles[l].id = id then begin
          s:=#$2C#$00#$00#$00#$31#$07#$04#$00#$DC#$08#$D5#$1D;
          move(id,s[9],4);
          {*Xor: 6301  Sub: 48
Xor: 6321  Sub: 68
*Xor: 6341  Sub: 08
Xor: 6361  Sub: 28
Xor: 6381  Sub: C8
Xor: 63A1  Sub: E8
Xor: 63C1  Sub: 88
Xor: 63E1  Sub: A8   }
          s:=s+getmagic(length(titles[l].desc)+1,$6321,$68)+WideToAnsi(titles[l].desc)+#0#0;
          while length(s) and 3 <> 0 do s:=s+#0;
          i:=length(s);
          move(i,s[1],4);
          sendtoplayer(x,s);
      end;
end;

end.
