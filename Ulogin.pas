unit Ulogin;

interface
uses windows,types,sysutils;

procedure LoginWithUserPass(x:integer;pk:ansistring);
procedure LoginWithToken(x:integer;tok:ansistring);
procedure BringClientToCharList(x:integer);
procedure SendCharList(x:integer);
procedure CreateMode(x:integer);
procedure NewChar(x:integer;d:pansichar);
procedure Packet1155(x:integer);
procedure Ping(x:integer;time:pint64);
procedure RenameChar(x:integer;d:pansichar);
procedure DeleteChar(x:integer;id:pdword);
procedure RenameChar2(x:integer);
procedure FinalRename(x:integer;s:ansistring);
procedure StoreConfig(x:integer;s:ansistring);
procedure SelectChar(x:integer;id:pdword);
procedure LoadLobby(x:integer);
procedure SendPA(x:integer);
procedure ClearTutorialScreen(x:integer;p:dword);


const chardatname:array[0..5] of ansistring = (
      'warriorm',
      'warriorm',
      'warriorm',
      'warriorm',
      'warriorm',
      'warriorm'
    );


implementation

uses UDB, UPlayer, UEnc, Uerror, fmain, UParty, UInventory, UPsoUtil,
  UMovement, UDebug, UForest, UQuestBoard, UStats, ULobby, Pet_Mag, casino,
  UMail, UMonster, UFriendList, clientorder, UMag, ItemGenerator, UTeamFnc;


procedure ClearTutorialScreen(x:integer;p:dword);
var i:integer;
begin
    i:=1 shl (p and 7);
    player[x].save.Tutorial[p div 8]:=player[x].save.Tutorial[p div 8] or i;
end;

function crc16( s : ansistring; bSumPos : Boolean = FALSE ) : Word;
var
 L, crc, sum, i, x, j : Word;

begin
  Result:=0;
  L:=length(s);
  if( L > 0 ) then
   begin
    crc:=$FFFF;
    sum:=length(s);
    for i:=1 to L do
    begin
            j:=ord(s[i]); 
            sum:=sum+((i) * j);
            x:=((crc shr 8) xor j) and $FF;
            x:=x xor (x shr 4);
            crc:=((crc shl 8) xor (x shl 12) xor (x shl 5) xor x) and $FFFF;
    end;
    Result:=crc+(Byte(bSumPos) * sum);
   end;
end;

procedure LoginWithUserPass(x:integer;pk:ansistring);
var i,l:integer;
    s,usr,pass:ansistring;
begin
    i:=$35;
    l:=ReadMagic(copy(pk,i,4),$5e6,107);
    inc(i,$178+($1c*l));
    usr:=pansichar(@pk[i]);
    pass:=pansichar(@pk[i+64]);
    if form1.Memo7.Lines.IndexOf(usr) > -1 then begin
        SendToPlayer(x,MakeError(ERR_BANNED,'You are banned on this server.'));
        player[x].todc:=1;
        exit;
    end;

    i:=LoadAccount(usr,@player[x].save);
    if i = 0 then begin
        if player[x].save.pass = pass then begin
            player[x].Token:=CreateToken(usr);
            player[x].username:=usr;
            if player[x].save.ArksName[0] = #0 then begin
                //no arks name edit it
                s:='AK'+inttohex(crc16(usr),4);
                for l:=1 to length(s) do
                    player[x].save.ArksName[l-1]:=widechar(byte(s[l]));
            end;
            player[x].aname:=player[x].save.ArksName;
            //send some packet, unknow for now
            SendToPlayer(x,#$0C#$00#$00#$00#$11#$49#$00#$00#$C2#$67#$00#$A9);
            setlength(s,$d8);
            fillchar(s[1],$d8,0);
           // s[$1d]:=#$44;    //fun point
            s:=#$e8#$00#$00#$00#$11#$1B#$00#$00#$FF#$34#$03#$00#$00#$00#$00#$00+s;
            SendToPlayer(x,s);

            SendToPlayer(x,#$18#$00#$00#$00#$11#$8C#$04#$00#$00#$00#$00#$00#$01#$00#$00#$00
                    +#$00#$00#$00#$00#$DF#$29#$00#$00);

            {//send to the next port
            s:=#$24#$00#$00#$00#$11#$13#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
                +#$13#$00#$2E#$01#$7F#$00#$00#$01#$64#$32#$00#$00#$05#$78#$65#$69
                +#$56#$5F#$9C#$00;
            move(player[x].token,s[$1d],4);
            move(SrvCfg.bPort ,s[$19],2);
            move(ActiveIP[0],s[$15],4);
            //move(player[x].save.GID,s[$21],4);
            SendToPlayer(x,s);    }

            player[x].GID:=player[x].save.GID;

        {setlength(s,$c0);
        fillchar(s[1],$c0,0);
        s[$1d]:=#$cc;
        s:=#$D0#$00#$00#$00#$11#$1B#$00#$00#$FF#$34#$03#$00#$00#$00#$00#$00+s;
        SendToPlayer(x,s);   }

        //not sure yet.. block info?
        s:=#$5c#$01#$00#$00#$11#$01#$04#$00#$00#$00#$00#$00#$12#$8B#$00#$00
            +#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00#$42#$00#$2D#$00
            +#$30#$00#$30#$00#$31#$00#$3A#$00'S'#0'c'#0'h'#0't'#0
            +'S'#0'e'#0'r'#0'v'#0#$00#$00#$00#$00#$00#$00#$00#$00
            +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$8C#$42
            +#$FF#$0F#$00#$00#$50#$00#$00#$00#$01#$00#$00#$00#$00#$00#$20#$41
            +#$00#$00#$A0#$40#$10#$00#$00#$00#$00#$00#$00#$40#$00#$00#$96#$42
            +#$37#$00#$00#$00#$00#$00#$20#$41#$01#$00#$00#$00#$00#$00#$C8#$43
            +#$00#$00#$80#$41#$00#$00#$40#$41#$66#$66#$26#$41#$CD#$CC#$0C#$41
            +#$00#$00#$00#$41#$9A#$99#$99#$40#$66#$66#$66#$40#$C3#$F5#$28#$40
            +#$00#$00#$80#$3F#$9A#$99#$19#$40#$00#$00#$C8#$42#$00#$00#$D2#$42
            +#$00#$00#$DC#$42#$00#$00#$F0#$42#$00#$00#$C8#$42#$00#$00#$C8#$42
            +#$00#$00#$C8#$42#$00#$00#$C8#$42#$00#$00#$C8#$42#$00#$00#$C8#$42
            +#$00#$00#$34#$43#$00#$00#$DC#$42#$00#$00#$D2#$42#$00#$00#$C8#$42
            +#$00#$00#$C8#$42#$00#$00#$DC#$42#$00#$00#$C8#$42#$00#$00#$C8#$42
            +#$00#$00#$C8#$42#$00#$00#$B4#$42#$00#$00#$E1#$43#$00#$00#$C8#$42
            +#$00#$00#$C8#$42#$00#$00#$C8#$42#$00#$00#$C8#$42#$00#$00#$C8#$42
            +#$00#$00#$C8#$42#$00#$00#$48#$43#$00#$00#$48#$43#$00#$00#$96#$43
            +#$00#$00#$96#$43#$00#$00#$00#$00#$00#$00#$00#$00#$0F#$00#$00#$00
            +#$05#$00#$00#$00#$00#$00#$20#$42#$00#$00#$94#$41#$00#$00#$28#$41
            +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;
        move(player[x].save.GID,s[$11],4);
        SendToPlayer(x,s);

            //test if a gm
            player[x].isgm:=0;
            player[x].gmunlocked:=0;
            if form1.Memo6.Lines.IndexOf(usr) > -1 then begin
                player[x].isgm:=1;
            end;
        end else begin
            SendToPlayer(x,MakeError(ERR_WRONGUSER,'Invalid password or username.'));
            player[x].todc:=1;
        end;
    end else begin
        SendToPlayer(x,MakeError(ERR_WRONGUSER,'Invalid password or username.'));
        player[x].todc:=1;
    end;
end;


procedure LoginWithToken(x:integer;tok:ansistring);
var i:integer;
    s:ansistring;
    tk:dword;
begin
    i:=$41;
    while tok[i+4] <> #0 do begin
        inc(i,$1c);
    end;
    inc(i,$1c);
    move(tok[i],tk,4);
    player[x].username := GetUserFromToken(tk);
    if player[x].username <> '' then begin
        LoadAccount(player[x].username,@player[x].save);

        //send the begining
        player[x].GID:=player[x].save.GID;

        //not sure yet.. block info?
        s:=#$34#$01#$00#$00#$11#$01#$04#$00#$00#$00#$00#$00#$12#$8B#$00#$00
          +#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00#$53#$00#$30#$00
          +#$39#$00#$20#$00#$4D#$00#$79#$00#$52#$00#$6F#$00#$6F#$00#$6D#$00
          +#$30#$00#$36#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
          +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
          +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$8C#$42
          +#$FF#$0F#$00#$00#$50#$00#$00#$00#$01#$00#$00#$00#$00#$00#$20#$41
          +#$00#$00#$A0#$40#$10#$00#$00#$00#$00#$00#$00#$40#$00#$00#$96#$42
          +#$37#$00#$00#$00#$00#$00#$20#$41#$01#$00#$00#$00#$00#$00#$C8#$43
          +#$00#$00#$80#$41#$00#$00#$40#$41#$66#$66#$26#$41#$CD#$CC#$0C#$41
          +#$00#$00#$00#$41#$9A#$99#$99#$40#$66#$66#$66#$40#$C3#$F5#$28#$40
          +#$00#$00#$80#$3F#$9A#$99#$19#$40#$00#$00#$C8#$42#$00#$00#$D2#$42
          +#$00#$00#$DC#$42#$00#$00#$F0#$42#$00#$00#$C8#$42#$00#$00#$C8#$42
          +#$00#$00#$C8#$42#$00#$00#$C8#$42#$00#$00#$C8#$42#$00#$00#$C8#$42
          +#$00#$00#$34#$43#$00#$00#$DC#$42#$00#$00#$D2#$42#$00#$00#$C8#$42
          +#$00#$00#$C8#$42#$00#$00#$DC#$42#$00#$00#$C8#$42#$00#$00#$C8#$42
          +#$00#$00#$C8#$42#$00#$00#$B4#$42#$00#$00#$E1#$43#$00#$00#$00#$00
          +#$00#$00#$00#$00#$0F#$00#$00#$00#$05#$00#$00#$00#$00#$00#$20#$42
          +#$00#$00#$94#$41#$00#$00#$28#$41#$00#$00#$00#$00#$00#$00#$00#$00
          +#$00#$00#$00#$00;
        move(player[x].save.GID,s[$11],4);
        SendToPlayer(x,s);




    end else begin
        SendToPlayer(x,MakeError(ERR_BADTOKEN,'Login process error, please retry again.'));
        player[x].todc:=1;
    end;
end;


procedure BringClientToCharList(x:integer);
var f,i:integer;
    s,a:ansistring;
begin
    //send loginp
    f:=fileopen(path+'data\login0f.dat',$40);
    i:=fileseek(f,0,2);
    fileseek(f,0,0);
    setlength(s,i);
    fileread(f,s[1],i);
    fileclose(f);
    sendtoplayer(x,s);

    //send window
    f:=fileopen(path+'data\window.dat',$40);
    i:=fileseek(f,0,2);
    fileseek(f,0,0);
    setlength(s,i);
    fileread(f,s[1],i);
    fileclose(f);
    //sendtoplayer(x,s);

                    //send the welcome packet 11 6F

    if srvcfg.Ann = 1 then begin
        setlength(s,5244);
        fillchar(s[1],5244,0);
        a:=form1.Memo4.Text;
        if length(a) > 2620 then a:=copy(a,1,2620);
        i:=0;
        f:=1;
        while i < length(a) do begin
            if a[i+1] <> #13 then begin
            s[f]:=a[i+1];
            inc(f,2);
            end;
            inc(i);

        end;
        s:=#$88#$14#$00#$00#$11#$6F#$04#$00#$1A#$08#$00#$00+s;
        sendtoplayer(x,s);
    end;
    //sendtoplayer(x,#$18#$00#$00#$00#$11#$6F#$04#$00#$1C#$0F#$00#$00'M'#0's'#0'g'#0'0'#0'1'#0#0#0);

    sendtoplayer(x,#$0C#$00#$00#$00#$19#$05#$04#$00#$23#$8F#$00#$00);
    sendtoplayer(x,#$0C#$00#$00#$00#$19#$07#$04#$00#$0C#$9B#$00#$00); 

                    //send key config 2B 02
    move(player[x].save.KeyConfig[0],i,4);
    setlength(s,i);
    move(player[x].save.KeyConfig[0],s[1],i);
    sendtoplayer(x,s);
end;


procedure CreateMode(x:integer);
begin
    sendtoplayer(x,#$18#$00#$00#$00#$11#$42#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00#$F4#$01#$00#$00);
end;

procedure Packet1155(x:integer);
begin
    //i think this might have some info for the created char.. 
    sendtoplayer(x,#$0C#$00#$00#$00#$11#$55#$00#$00#$01#$00#$00#$00);
end;


//****************************************************
//
// The char list is as fallow
//
// 0x30d4 = last location? 20x 0x230
// 0x5c94 = play time? 20 * dword
// 0c5ce4= some flag, 20 bytes, 2 = ep4, 1 = ep 1-3, 0 = no eps
// 0x5cf8 = delete flag dword + date dword (20 entry of 8 bytes)

procedure SendCharList(x:integer);
var s,b:ansistring;
    i,l:integer;
begin
    //setlength(s,$5e30);
    //fillchar(s[1],$5e30,0);
    //s:=#$40#$5E#$00#$00#$11#$03#$00#$00#$02#$00#$00#$00#$05#$00#$00#$00+s;
    s:=filegetpacket('map\charbuf');
    //b:=copy(s,$30d5,$230); //unknown yet so i will just copy an existing entry
    i:=0;
    while player[x].save.char[i].charID > 0 do begin
        move(player[x].save.char[i].charID,s[$15+(i*$270)],$270);
        //move(b[1],s[$30d5+(i*$230)],$230);   //unknown
        move(player[x].save.char[i].playtime,s[$5c95+(i*4)],4);//play time
        s[$5ce5+i]:=#2; //pso2 version , set to ep4
        fillchar(s[$5cf9+(i*8)],8,0);//delete flag
        l:=$15180;  //setting it to 0 = GM
        move(l,s[$5cfd+(i*8)],4);
        inc(i);
        if i>19 then break; //max
    end;

    move(i,s[9],2);

    sendtoplayer(x,s);
end;


procedure NewChar(x:integer;d:pansichar);
var s,n:ansistring;
    i,l:integer;
    job,sex,race:word;
    na:widestring;
begin
    i:=0;
    while player[x].save.char[i].charID > 0 do inc(i);
    if i < 20 then begin
    player[x].save.char[i].charID:=CreateCharID;
    player[x].save.char[i].GID:=player[x].GID;
    move(d[0],player[x].save.char[i].base.fill[0],$268);
    s:=#$10#$00#$00#$00#$11#$07#$00#$00#$00#$00#$00#$00#$64#$8F#$D3#$01;
    move(player[x].save.char[i].charID,s[13],4);
    sendtoplayer(x,s);

    //fill inventory
    job:=0;
    move(d[$1a4],job,1);
    move(d[$32],race,2);
    move(d[$34],sex,2);
    n:='warrior';
    if job = Job_Hunter then n:='warrior';
    if job = Job_Ranger then n:='ranger';
    if job = Job_Force then n:='force';
    if job = Job_Bouncer then n:='bouncer';
    if job = Job_Braver then n:='archer';
    if job = Job_Summoner then n:='sumonner';

    move(BasePA[job][1],player[x].save.char[i].palvl[0],$130);



    s:=FileGetPacket('data\'+n+'.inv');
    move(s[1],player[x].save.char[i].PersonalBank[0],length(s));
    player[x].save.char[i].BankCount:=length(s) div $38;

    //generate the outfit

    if Race = Race_Cast then begin //cast use 3 part outfit
        //body
        move(d[$140],job,2);
        s:=CastID[0].item; //femmal outfit
        if sex = 0 then s:=CastID[3].item; //man outfit
        for l:=0 to ExtraIDCount-1 do
            if (ExtraID[l].id = job) and ((ExtraID[l].item[3] = #1) or (ExtraID[l].item[3] = #2)) then s:=ExtraID[l].item;
        l:=0;
        while l < CastIDCount do begin
            if CastID[l].id = job then s:=CastID[l].item;
            inc(l,3);
        end;
        s:=#0#0#0#0#0#0#0#0 //empty id for now
           +s   //item code
           +#0#0#0#0#0#0#0#0 //color holder
           +#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0 //filling
           +#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0;
        move(s[1],player[x].save.char[i].outfit,$38);

        //arm
        move(d[$15e],job,2);
        s:=CastID[1].item; //femmal outfit
        if sex = 0 then s:=CastID[4].item; //man outfit
        for l:=0 to ExtraIDCount-1 do
            if (ExtraID[l].id = job) and ((ExtraID[l].item[3] = #3) or (ExtraID[l].item[3] = #4)) then s:=ExtraID[l].item;
        l:=0;
        while l < CastIDCount do begin
            if CastID[l+1].id = job then s:=CastID[l+1].item;
            inc(l,3);
        end;
        s:=#0#0#0#0#0#0#0#0 //empty id for now
           +s   //item code
           +#0#0#0#0#0#0#0#0 //color holder
           +#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0 //filling
           +#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0;
        move(s[1],player[x].save.char[i].cast_arm,$38);

        //leg
        move(d[$15c],job,2);
        s:=CastID[2].item; //femmal outfit
        if sex = 0 then s:=CastID[5].item; //man outfit
        for l:=0 to ExtraIDCount-1 do
            if (ExtraID[l].id = job) and ((ExtraID[l].item[3] = #5) or (ExtraID[l].item[3] = #6)) then s:=ExtraID[l].item;
        l:=0;
        while l < CastIDCount do begin
            if CastID[l+2].id = job then s:=CastID[l+2].item;
            inc(l,3);
        end;
        s:=#0#0#0#0#0#0#0#0 //empty id for now
           +s   //item code
           +#0#0#0#0#0#0#0#0 //color holder
           +#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0 //filling
           +#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0;
        move(s[1],player[x].save.char[i].cast_leg,$38);

    end else begin
        move(d[$140],job,2);
        s:=ItemsVSID[0].item; //femmal outfit
        if sex = 0 then s:=ItemsVSID[2].item; //man outfit
        for l:=0 to ExtraIDCount-1 do
            if ExtraID[l].id = job then s:=ExtraID[l].item;
        for l:=0 to ItemVsIdCount do
            if ItemsVSID[l].id = job then s:=ItemsVSID[l].item;
        s:=#0#0#0#0#0#0#0#0 //empty id for now
           +s   //item code
           +#0#0#0#0#0#0#0#0 //color holder
           +#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0 //filling
           +#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0;
        move(d[$e6],s[$13],6);
        move(s[1],player[x].save.char[i].outfit,$38);
    end;
    fillchar(player[x].save.char[i].units[0],3*sizeof(tinventoryitem),0); //clean it for new char
    fillchar(player[x].save.char[i].rings[0],3*sizeof(tinventoryitem),0); //clean it for new char


    player[x].save.char[i].flagitem := 0; //when player log the server must rebuil the item id

    //fill pet and candy
    player[x].save.char[i].PetCount:=0;
    player[x].save.char[i].candycount:=0;
    player[x].save.char[i].SkillListCount:=0; // will force a rewrite of them

    if player[x].save.char[i].base.job = Job_Summoner then begin
        //must fill pet + candy
        player[x].save.char[i].PetCount:=1;
        player[x].save.char[i].candycount:=$22;
        move(DefPet[1],player[x].save.char[i].Pets[0],$40);
        move(DefCandy[1],player[x].save.char[i].candybox[0],$48*34);
        player[x].save.char[i].Pets[0].PID:=CreateCharID;
        for l:=0 to $21 do
            player[x].save.char[i].candybox[l].Owner:=player[x].save.char[i].Pets[0].PID;
        player[x].save.char[i].Pets[0].CharID:=player[x].save.char[i].charID;
        na:='Wanda'#0;
        move(na[1],player[x].save.char[i].Pets[0].name[0],12);
    end;

    SaveAccount(player[x].username,@player[x].save);

    end else begin
        sendtoplayer(x,MakeError(ERR_CHARLIMIT,'You have reatch the maximum amount of character.'));
    end;
end;

procedure DeleteChar(x:integer;id:pdword);
var i,l:integer;
begin
    for i:=0 to 19 do
        if player[x].save.char[i].charID = id^ then begin
            move(player[x].save.char[i+1],player[x].save.char[i],(19-i)*sizeof(TCharData)) ;
            move(player[x].save.ClientOrder[i+1],player[x].save.ClientOrder[i],(19-i)*sizeof(tcliento)) ;
            move(player[x].save.mags[i+1],player[x].save.mags[i],(19-i)*sizeof(tmaglist)) ;
            break;
        end;
    //i should probably send a confirmation...
    sendtoplayer(x,#$0C#$00#$00#$00#$11#$08#$00#$00#$01#$00#$00#$00);
    
end;

procedure Ping(x:integer;time:pint64);
var s:ansistring;
    ct,st:^int64;
begin
    if player[x].firstping = 2 then begin
        sendArmor(x); //player ready to get his equipement
        player[x].last81:='';
        //makeplayermove(x);
        if player[x].Floor>=0 then begin//objective
          if team[player[x].TeamID].questid > 15 then begin
              if player[x].teammission =0 then begin
                  inc(player[x].teammission);
                  s:=GetMissionPacket(x);
                  sendtoplayer(x,s);
              end;
          end else begin
              s:=#$1C#$00#$00#$00#$0B#$64#$00#$00#$42#$A9#$B0#$B6#$F6#$0B#$00#$00
              +#$00#$40#$1C#$46#$00#$00#$70#$C1#$01#$00#$00#$00;     //10000 value , -15
              if team[player[x].TeamID].ChalengeTicks = 0 then
                team[player[x].TeamID].ChalengeTicks:=time^;
              pint64(@s[9])^:=team[player[x].TeamID].ChalengeTicks;
              sendtoparty(x,s);
          end;
        end;

    end;
    if player[x].firstping < 3 then inc(player[x].firstping);
    s:=#$18#$00#$00#$00#$11#$0E#$00#$00#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0;
    ct:=@s[9];
    st:=@s[17];
    player[x].ping:=time^ + 200;
    player[x].pingticks:=gettickcount();
    ct^ := time^;
    st^ := time^ + 4650;
    sendtoplayer(x,s);
end;


procedure RenameChar(x:integer;d:pansichar);
begin
    sendtoplayer(x,#$18#$00#$00#$00#$11#$98#$00#$00#$01#$00#$00#$00+#$0#$0#$00#$00 //0$ needed
        +#$00#$8D#$27#$00#$00#$8D#$27#$00);
end;

procedure RenameChar2(x:integer);
begin
    sendtoplayer(x,#$0C#$00#$00#$00#$11#$9a#$00#$00#$01#$00#$00#$00);
end;

procedure FinalRename(x:integer;s:ansistring);
var id:^dword;
    i:integer;
begin
    id:=@s[9];
    for i:=0 to 19 do
        if player[x].save.char[i].charID = id^ then begin
        move(s[13],player[x].save.char[i].base.name[0],32);
        break;
    end;
    sendtoplayer(x,#$0C#$00#$00#$00#$11#$9c#$00#$00#$01#$00#$00#$00);
end;


procedure StoreConfig(x:integer;s:ansistring);
begin
    if length(s)>16380 then exit;
    move(s[1],player[x].save.KeyConfig[0],length(s));
    player[x].save.KeyConfig[5]:=#2;
end;

//pas doption party/friendship
//le player sefface pas lorsque lon quite
//quand on warp tout les player warp...

procedure SelectChar(x:integer;id:pdword);
var i,l:integer;
    s:ansistring;
    dw:dword;
begin
    for i:=0 to 19 do
        if player[x].save.char[i].charID = id^ then begin
            player[x].activeChar:= @player[x].save.char[i];
            player[x].ClientOrder:=@player[x].save.clientorder[i];
            player[x].Maglist:=@player[x].save.mags[i];
            player[x].LogedTime:=gettickcount();
            player[x].CountTime:=1;
            player[x].charloaded:=true;

            //sync the mags
            dw:=Round((now() - 25569) * 86400);
            dw:=dw - player[x].save.LastPlayTime[i];

            for l:=0 to player[x].maglist.count-1 do begin
                //only if not lvl 200
                if player[x].maglist.mags[l].satk.lvl+player[x].maglist.mags[l].ratk.lvl+player[x].maglist.mags[l].tatk.lvl
                    +player[x].maglist.mags[l].sdef.lvl+player[x].maglist.mags[l].rdef.lvl+player[x].maglist.mags[l].tdef.lvl
                    +player[x].maglist.mags[l].dex.lvl < 200 then
                if dw >= player[x].maglist.mags[l].energy then player[x].maglist.mags[l].energy:=0
                else dec(player[x].maglist.mags[l].energy,dw);
            end;

            break;
        end;

    if player[x].activeChar.flagitem = 0 then begin
        //rebuild the item id
        for i:=0 to player[x].activeChar.BankCount-1 do begin
            player[x].activeChar.PersonalBank[i].id1:=CreateItemID(x);
            player[x].activeChar.PersonalBank[i].id2:=player[x].activeChar.charID;
        end;
        player[x].activeChar.outfit.id1:=CreateItemID(x);
        player[x].activeChar.outfit.id2:=player[x].activeChar.charID;
        if player[x].activeChar.base.race = race_cast then begin
        player[x].activeChar.Cast_leg.id1:=CreateItemID(x);
        player[x].activeChar.Cast_leg.id2:=player[x].activeChar.charID;
        player[x].activeChar.Cast_arm.id1:=CreateItemID(x);
        player[x].activeChar.Cast_arm.id2:=player[x].activeChar.charID;
        end;
        player[x].activeChar.flagitem:=1;
    end;

    sendtoplayer(x,#$0C#$00#$00#$00#$43#$00#$04#$00#$06#$11#$00#$00);
    sendtoplayer(x,#$10#$00#$00#$00#$3D#$00#$04#$00#$00#$00#$00#$00#$51#$9F#$00#$00);

    player[x].partyid:=0;
    player[x].partyid:=getnextpartyid(x);
    SendParty(x);

    //send empty team
    sendtoplayer(x,#$28#$00#$00#$00#$0E#$0D#$04#$00#$6A#$97#$00#$00#$6D#$97#$00#$00
	+#$00#$00#$00#$00#$6D#$97#$00#$00#$00#$00#$00#$00#$01#$50#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00);


    sendtoplayer(x,#$14#$00#$00#$00#$0E#$1A#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00);

    //send saved option if any
    if (player[x].save.option[0] <> 0 ) or (player[x].save.option[1] <> 0) then begin
        s:=#$10#$00#$00#$00#$33#$02#$00#$00#$05#$01#$00#$00#$00#$00#$00#$00;
        move(player[x].save.option[0],s[9],8);
        sendtoplayer(x,s);
    end;



    sendtoplayer(x,
    #$74#0#0#0#$43#$00#$04#$00 +
    #$05#$11#$00#$00#$F9#$9F#$33#$01 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$68#$96#$8B#$57#$A0#$15#$98#$57 +
    #$09#$00#$7E#$30#$8A#$30#$4A#$30#$5F#$30#$8D#$30#$46#$30#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00
  );


    sendtoplayer(x,#$10#$00#$00#$00#$3D#$00#$04#$00#$00#$00#$00#$00#$51#$9F#$00#$00);


    sendtoplayer(x,#$08#$00#$00#$00#$03#$04#$00#$00);

    {s:=#$C4#$05#$00#$00#$0F#$30#$04#$00#$A4#$9E#$00#$00#$03#$00#$33#$00
	+#$00#$00#$00#$00#$03#$00#$34#$00#$00#$00#$00#$00#$03#$00#$35#$00
	+#$00#$00#$00#$00#$03#$00#$37#$00#$00#$00#$00#$00#$03#$00#$3D#$00
	+#$00#$00#$00#$00#$03#$00#$3E#$00#$00#$00#$00#$00#$03#$00#$3F#$00
	+#$00#$00#$00#$00#$03#$00#$40#$00#$00#$00#$00#$00#$03#$00#$45#$00
	+#$00#$00#$00#$00#$03#$00#$33#$00#$00#$00#$01#$00#$03#$00#$34#$00
	+#$00#$00#$01#$00#$03#$00#$35#$00#$00#$00#$01#$00#$03#$00#$36#$00
	+#$00#$00#$01#$00#$03#$00#$3D#$00#$00#$00#$01#$00#$03#$00#$3E#$00
	+#$00#$00#$01#$00#$03#$00#$3F#$00#$00#$00#$01#$00#$03#$00#$40#$00
	+#$00#$00#$01#$00#$03#$00#$33#$00#$00#$00#$02#$00#$03#$00#$34#$00
	+#$00#$00#$02#$00#$03#$00#$36#$00#$00#$00#$02#$00#$03#$00#$37#$00
	+#$00#$00#$02#$00#$03#$00#$3D#$00#$00#$00#$02#$00#$03#$00#$3E#$00
	+#$00#$00#$02#$00#$03#$00#$3F#$00#$00#$00#$02#$00#$03#$00#$40#$00
	+#$00#$00#$02#$00#$03#$00#$45#$00#$00#$00#$02#$00#$03#$00#$33#$00
	+#$00#$00#$03#$00#$03#$00#$34#$00#$00#$00#$03#$00#$03#$00#$35#$00
	+#$00#$00#$03#$00#$03#$00#$36#$00#$00#$00#$03#$00#$03#$00#$3E#$00
	+#$00#$00#$03#$00#$03#$00#$3F#$00#$00#$00#$03#$00#$03#$00#$33#$00
	+#$00#$00#$04#$00#$03#$00#$34#$00#$00#$00#$04#$00#$03#$00#$35#$00
	+#$00#$00#$04#$00#$03#$00#$3E#$00#$00#$00#$04#$00#$03#$00#$34#$00
	+#$00#$00#$05#$00#$03#$00#$35#$00#$00#$00#$05#$00#$03#$00#$32#$00
	+#$00#$00#$06#$00#$03#$00#$35#$00#$00#$00#$06#$00#$03#$00#$3E#$00
	+#$00#$00#$06#$00#$03#$00#$32#$00#$00#$00#$07#$00#$03#$00#$3E#$00
	+#$00#$00#$07#$00#$03#$00#$32#$00#$00#$00#$08#$00#$03#$00#$3E#$00
	+#$00#$00#$08#$00#$03#$00#$32#$00#$00#$00#$09#$00#$03#$00#$3E#$00
	+#$00#$00#$09#$00#$03#$00#$32#$00#$00#$00#$0A#$00#$03#$00#$32#$00
	+#$00#$00#$0B#$00#$03#$00#$3E#$00#$00#$00#$0B#$00#$03#$00#$32#$00
	+#$00#$00#$0C#$00#$03#$00#$3E#$00#$00#$00#$0C#$00#$03#$00#$32#$00
	+#$00#$00#$0D#$00#$03#$00#$3E#$00#$00#$00#$0E#$00#$03#$00#$3E#$00
	+#$00#$00#$0F#$00#$03#$00#$3E#$00#$00#$00#$10#$00#$03#$00#$32#$00
	+#$00#$00#$15#$00#$03#$00#$32#$00#$00#$00#$16#$00#$03#$00#$32#$00
	+#$00#$00#$17#$00#$03#$00#$32#$00#$00#$00#$18#$00#$03#$00#$14#$00
	+#$00#$00#$92#$00#$03#$00#$14#$00#$00#$00#$94#$00#$03#$00#$15#$00
	+#$00#$00#$9B#$00#$03#$00#$15#$00#$00#$00#$9F#$00#$DC#$9F#$00#$00
	+#$CA#$30#$D9#$30#$EB#$30#$ED#$30#$C3#$30#$AF#$30#$EE#$68#$97#$67
	+#$A8#$30#$E1#$30#$E9#$30#$EB#$30#$C9#$30#$EE#$68#$97#$67#$C8#$30
	+#$DE#$30#$C8#$30#$EE#$68#$97#$67#$73#$7C#$A6#$30#$A9#$30#$D1#$30
	+#$EB#$30#$B7#$30#$A7#$30#$EB#$30#$EE#$68#$97#$67#$A2#$30#$E6#$30
	+#$A2#$30#$FC#$30#$B9#$30#$B7#$30#$A7#$30#$EB#$30#$7A#$90#$E1#$8D
	+#$A8#$30#$D3#$30#$D7#$30#$C1#$30#$FB#$30#$ED#$30#$C9#$30#$B9#$30
	+#$A6#$30#$A9#$30#$D1#$30#$EB#$30#$ED#$30#$C3#$30#$AF#$30#$7A#$90
	+#$E1#$8D#$A2#$30#$E1#$30#$B8#$30#$B9#$30#$C8#$30#$7A#$90#$E1#$8D
	+#$BF#$30#$DE#$30#$CD#$30#$AE#$30#$7A#$90#$E1#$8D#$D6#$30#$C9#$30
	+#$A6#$30#$A2#$30#$AD#$30#$D3#$30#$F3#$30#$EE#$68#$97#$67#$DE#$30
	+#$B0#$30#$ED#$30#$77#$6D#$B8#$5C#$A2#$30#$B5#$30#$EA#$30#$71#$67
	+#$AC#$4E#$A8#$30#$D3#$30#$77#$6D#$B8#$5C#$69#$58#$77#$6D#$B8#$5C
	+#$B5#$30#$D5#$30#$A1#$30#$A4#$30#$A2#$30#$77#$6D#$B8#$5C#$AA#$30
	+#$EA#$30#$FC#$30#$D6#$30#$71#$67#$AC#$4E#$27#$59#$46#$8C#$EA#$30
	+#$EA#$30#$FC#$30#$D1#$30#$C1#$30#$E7#$30#$C3#$30#$AD#$30#$EE#$68
	+#$97#$67#$D6#$30#$EA#$30#$77#$6D#$B8#$5C#$E0#$30#$FC#$30#$EB#$30
	+#$9D#$8C#$02#$78#$20#$6F#$AB#$30#$CB#$30#$CA#$30#$D9#$30#$EB#$30
	+#$BF#$30#$FC#$30#$C8#$30#$EB#$30#$A2#$30#$FC#$30#$B9#$30#$ED#$30
	+#$C3#$30#$AF#$30#$71#$67#$AC#$4E#$AA#$30#$D1#$30#$FC#$30#$EB#$30
	+#$CA#$30#$D9#$30#$EB#$30#$C0#$30#$B1#$30#$02#$78#$20#$6F#$DE#$30
	+#$F3#$30#$B4#$30#$FC#$30#$7A#$90#$E1#$8D#$AD#$30#$F3#$30#$E1#$30
	+#$C0#$30#$A4#$30#$EA#$30#$EA#$30#$FC#$30#$D1#$30#$B7#$30#$A7#$30
	+#$EB#$30#$EA#$30#$EA#$30#$FC#$30#$D1#$30#$ED#$30#$C3#$30#$AF#$30
	+#$02#$78#$20#$6F#$C8#$30#$D1#$30#$FC#$30#$BA#$30#$02#$78#$20#$6F
	+#$B5#$30#$F3#$30#$B7#$30#$E7#$30#$A6#$30#$7A#$90#$E1#$8D#$B5#$30
	+#$B1#$30#$A1#$63#$98#$63#$34#$58#$E1#$8D#$E9#$30#$D4#$30#$B9#$30
	+#$E9#$30#$BA#$30#$EA#$30#$A1#$63#$98#$63#$34#$58#$E1#$8D#$B8#$30
	+#$E3#$30#$AC#$30#$A4#$30#$E2#$30#$EE#$68#$97#$67#$DE#$30#$B0#$30
	+#$ED#$30#$6E#$30#$AB#$30#$EB#$30#$D1#$30#$C3#$30#$C1#$30#$E7#$30
	+#$A1#$63#$98#$63#$34#$58#$E1#$8D#$B3#$30#$B7#$30#$E7#$30#$A6#$30
	+#$77#$6D#$B8#$5C#$CF#$30#$AE#$30#$EE#$68#$97#$67#$A2#$30#$E6#$30
	+#$6E#$30#$C8#$30#$DE#$30#$C8#$30#$6E#$71#$A6#$30#$A9#$30#$D1#$30
	+#$EB#$30#$B7#$30#$E3#$30#$FC#$30#$AF#$30#$7A#$90#$E1#$8D#$B5#$30
	+#$B1#$30#$6E#$30#$9C#$67#$9F#$5B#$BD#$30#$FC#$30#$B9#$30#$4B#$30
	+#$51#$30#$71#$67#$AC#$4E#$DE#$30#$B0#$30#$ED#$30#$7A#$90#$E1#$8D
	+#$A8#$30#$D3#$30#$6E#$30#$4B#$30#$4D#$30#$DA#$63#$52#$30#$71#$67
	+#$AC#$4E#$DE#$30#$C0#$30#$A4#$30#$77#$6D#$B8#$5C#$CF#$30#$AE#$30
	+#$6E#$30#$69#$58#$3C#$71#$4D#$30#$77#$6D#$B8#$5C#$E0#$30#$FC#$30
	+#$EB#$30#$9D#$8C#$6E#$30#$AA#$30#$A4#$30#$EB#$30#$65#$30#$51#$30
	+#$02#$78#$20#$6F#$D2#$30#$E9#$30#$E1#$30#$71#$67#$AC#$4E#$DE#$30
	+#$C0#$30#$A4#$30#$6E#$30#$67#$71#$8A#$30#$3C#$71#$4D#$30#$02#$78
	+#$20#$6F#$C8#$30#$D3#$30#$A6#$30#$AA#$30#$71#$67#$AC#$4E#$DE#$30
	+#$B0#$30#$ED#$30#$6E#$30#$4A#$30#$3A#$52#$AB#$8E#$A1#$63#$98#$63
	+#$34#$58#$E1#$8D#$A6#$30#$CA#$30#$AE#$30#$A1#$63#$98#$63#$34#$58
	+#$E1#$8D#$BF#$30#$C1#$30#$A6#$30#$AA#$30#$A1#$63#$98#$63#$34#$58
	+#$E1#$8D#$B5#$30#$E1#$30#$02#$78#$20#$6F#$D2#$30#$E9#$30#$E1#$30
	+#$6E#$30#$D1#$30#$B9#$30#$BF#$30#$A1#$63#$98#$63#$34#$58#$E1#$8D
	+#$A6#$30#$CA#$30#$AE#$30#$6E#$30#$46#$30#$6A#$30#$CD#$91#$02#$78
	+#$20#$6F#$AB#$30#$CB#$30#$6E#$30#$B3#$30#$ED#$30#$C3#$30#$B1#$30
	+#$A1#$63#$98#$63#$34#$58#$E1#$8D#$BF#$30#$C1#$30#$A6#$30#$AA#$30
	+#$6E#$30#$BD#$30#$C6#$30#$FC#$30#$A1#$63#$D6#$53#$B9#$30#$BF#$30
	+#$DF#$30#$CA#$30#$C9#$30#$EA#$30#$F3#$30#$AF#$30#$15#$FF#$10#$FF
	+#$E3#$91#$8A#$30#$B9#$30#$BF#$30#$DF#$30#$CA#$30#$C9#$30#$EA#$30
	+#$F3#$30#$AF#$30#$15#$FF#$10#$FF#$88#$30#$44#$30#$D4#$30#$C3#$30
	+#$B1#$30#$EB#$30#$88#$30#$44#$30#$E3#$91#$8A#$30#$FF#$7A#$00#$00
	+#$A4#$9E#$00#$00#$2#$2#$2#$2#$2#$2#$2#$2#$2#$2#$2#$2
	+#$2#$2#$2#$2#$2#$2#$2#$2#$2#$2#$2#$2#$2#$2#$2#$2
	+#$2#$2#$2#$2#$2#$2#$2#$2#$2#$2#$2#$2#$2#$2#$2#$2
	+#$2#$2#$2#$2#$2#$2#$2#$2#$2#$2#$2#$2#$2#$2#$2#$2
	+#$2#$2#$2#$2;

    for i:=0 to 63 do begin
        s[$211+(i*4)]:=chr($30+(i div 10));
        s[$212+(i*4)]:=#0;
        s[$213+(i*4)]:=chr($30+(i mod 10));
        s[$214+(i*4)]:=#0
    end;
    sendtoplayer(x,s);    }

    //set hp
    {i:=player[x].activechar.base.job;
    player[x].hp := lvlstats[i,player[x].activechar.base.jobstat[i].lvl,Job_HP];
    i:= (player[x].activechar.base.race shl 1) or player[x].activechar.base.sex;   }
    player[x].hp:=GetPlayerMaxHp(x);//round(player[x].hp*racemod[i,RM_HP]);
    player[x].maxhp:=player[x].hp;


end;


procedure SendPA(x:integer);
var s:ansistring;
    i:integer;
begin
    s:=#$50#$01#$00#$00#$04#$2E#$40#$00#0#0#0#0#0#0#0#0
    +#4#0#0#0#0#0#0#0#0#0#0#0#4#0#0#0;
    setlength(s,$150);
    move(player[x].gid,s[9],4);
    move(player[x].gid,s[$15],4);
    move(player[x].activechar.palvl[0],s[$21],$130);

    //test
    if form1.Memo8.Lines.IndexOf(player[x].username) > -1 then
        for i:=$21 to $150 do s[i]:=#1;
    {for i:=0 to 15 do
        s[$21+i+176]:=chr(i+1);        // i am realy at 176
    //s[$75]:=#1;
    {
    for i:=0 to 15 do
        s[$21+100+i]:=chr(i+1);    }


    sendtoplayer(x,s);

end;

procedure LoadLobby(x:integer);
var s,a,ob:ansistring;
    i:integer;
begin
    if (player[x].room = Room_BattleLobby) or (player[x].room = Room_ChalengeLobby) then
      sendtoplayer(x,#$14#$00#$00#$00#$03#$08#$00#$00#$03#$00#$EA#$07#$00#$00#$00#$00
	        +#$00#$00#$00#$00 );

    player[x].room := Room_Lobby;
    player[x].lastarea1:=#$fb#$ff#$ff#$ff;

    for i:=0 to 19 do player[x].magfeedtime[i]:=gettickcount;

    //sendtoplayer(x,#$0C#$00#$00#$00#$1E#$0C#$00#$00#$7D#$00#$00#$00);
    
    //change with lobby.dat
    s:=FileGetPacket('map\lobby.dat');
    if srvcfg.lobby = 0 then i:=$552e74f1; //aqua lobby
    if srvcfg.lobby = 1 then i:=$1ab8b522; //godziLee lobby
    if srvcfg.lobby = 2 then i:=$466a0708; //calinourse lobby

    if srvcfg.lobby = 9 then i:=$c625ae8d; //easter lobby

    move(i,s[$35],4);
    move(player[x].partyid,s[$6d],4);
    sendtoplayer(x,s);



    sendtoplayer(x,#$08#$00#$00#$00#$03#$2A#$00#$00);

    s:=#$14#$00#$00#$00#$06#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00;
    move(player[x].gid,s[9],4);
    sendtoplayer(x,s);

    s:=#$00#$00#$00#$3C#$00#$00#$00#$80#$f4#$1f#$00#$00
    +#$3a#$58#$00#$00;
    move(s[9],player[x].curpos,8);
    move(s[1],player[x].rotation,8);
    SpawnPlayer(x);
    SendBlackList(x);
    
    //send all map
    //s:=KeepStaticItem(FileGetPacket('map\lobby.obj'));

    sendObjtoplayer(x,LobbyEventObj) ;
    sendObjtoplayer(x,lobbydata);
    //s:=FileGetPacket('map\lobby.obj');
    //sendObjtoplayer(x,s);
    
    sendtoplayer(x,#8#0#0#0#$03#$23#$00#$00);

    //exit;

    //send inventory
    SendInvetory(x);
    SendStorage(x);

    //if has pet send the packets
    if player[x].activechar.PetCount > 0 then begin
        //load hp for them
        for i:=0 to player[x].activechar.PetCount-1 do begin
            player[x].pet_maxhp[i]:=Pets[0].life;
            player[x].pet_hp[i]:=Pets[0].life;
            player[x].pet_stunned[i]:=0;
        end;
        //c962 = 1
        //c903 = $22
        //c973 = 12
        //c97c = F
        s:=#$E8#$09#$00#$00#$40#$2A#$04#$00#$00#$00#$00#$00#$14#$00#$00#$00;
        setlength(a,$40 * player[x].activechar.PetCount);
        move(player[x].activechar.pets[0],a[1],$40 * player[x].activechar.PetCount);
        s:=s+getmagic(player[x].activechar.PetCount,$C94A,$27)+a;
        setlength(a,$48 * player[x].activechar.candycount);
        move(player[x].activechar.candybox[0],a[1],$48 * player[x].activechar.candycount);
        s:=s+getmagic(player[x].activechar.candycount,$C94A,$27)+a;
        i:=length(s);
        move(i,s[1],4);
        sendtoplayer(x,s);
    end;



    //sendArmor(x);

// need to be able to move?
  sendtoplayer(x,#8#0#0#0#$03#$2B#$00#$00);

  setlength(s,$800);
  move(player[x].save.tutorial[0],s[1],$800);
  s:=#$08#$08#$00#$00#$23#$06#$00#$00 + s;
  sendtoplayer(x,s);

  // Unlock menu?
  sendtoplayer(x,
    #8#$c#0#0#$23#$07#$00#$00#$FA#$07#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$08#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$02#$00#$00#$00#$00#$00#$00#$A0 +
    #$0C#$00#$10#$00#$08#$00#$00#$00#$00#$F0#$00#$00#$00#$00#$03#$00 +
    #$00#$00#$00#$00#$00#$80#$21#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$1E#$95#$03#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$A0#$01#$00#$00#$00#$A0#$31#$00#$00#$00#$00#$00#$00#$01#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$F4#$0C#$00#$00#$00#$00#$00#$00 +
    #$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$24#$02 +
    #$00#$10#$00#$00#$00#$00#$00#$00#$02#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$02#$00 +
    #$00#$80#$02#$80#$42#$00#$00#$00#$01#$00#$00#$20#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$40#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$F8#$11#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$40#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00#$00#$D8#$01#$00 +
    #$40#$03#$00#$08#$00#$00#$80#$25#$03#$00#$20#$80#$00#$02#$00#$80 +
    #$00#$00#$41#$03#$21#$08#$00#$08#$11#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$49#$82#$7F#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$10#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$03#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$44 +
    #$00#$10#$00#$80#$01#$40#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$40#$00#$04#$00#$10 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$40#$00#$00 +
    #$20#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 +
    #$00#$00#$00#$00#$00#$00#$00#$00
  );

  // palette / options?
  SendPalette(x);

  // Needed to be able to move
 sendtoplayer(x,
    #16#0#0#0#$2E#$0A#$00#$00#$1e#$00#$00#$00#$00#$00#$00#$00
  );

  //send video on wall
  sendtoplayer(x,#$0C#$00#$00#$00#$19#$0F#$00#$00#$7D#$00#$00#$00);


  LoginForCasinoTickets(x); //give daly ticket and update casino coins

  //load npc data
  //s:=FileGetPacket('map\lobby.npc');
   // sendtoplayer(x,ob);

  //load inventory
  player[x].floor:=-1;
  player[x].firstping:=0;

  UpdateMesetas(x);
  NotifyForMail(x);
  player[x].petloaded:=0;



  TestForClientOrder(x);

  SendClientOrder(x);

  SendMagList(x);

  //this is needed for the team stuff
  s:=#0#0#0#0#0#0#0#0#0#0#0#0#0;
  move(player[x].GID,s[9],4);
  GetPlayerQuickInfo(x,s);

  GetMyTeam(x);

end;

end.
