unit UPAid;

interface
uses types,sysutils;

    procedure LearnDisk(x:integer;a:ansistring);

const
PAtoID:array[0..175,0..2] of byte = (
   (1,1,0), //1 Rising Edge
  (1,1,1), //2 Twister Fall
  (1,1,2), //3 Nova Strike
  (1,1,3), //4 Ride Slasher
  (1,1,4), //5 Over End
  (1,1,5),//6 Stun Concido
  (1,1,6),//7 Sonic Arrow
  (1,1,7),//8 Cruel Throw
  (1,2,0),//9 Bind Through
  (1,2,1),//10 Grapple Charge
  (1,2,2),//11 Heavenly Fall
  (1,2,3),//12 Other Cyclone
  (1,2,4),//13 Other Spin
  (1,2,5),//14 Holding Current
  (1,2,6),//15 Air Pocket Swing
  (1,2,7),//16 Cerberus Dance
  (1,3,0),//17 Slide Shaker
  (1,3,1),//18 Speed Rain
  (1,3,2),//19 Rising Flag
  (1,3,3),//20 Zenith Throw
  (1,3,4),//21 Trick Rave
  (1,3,5),//22 Slide End
  (1,3,6),//23 Assault Buster
  (1,3,7),//24 Bandersnatch
  (1,4,0),//25 Raging Waltz
  (1,4,1),//26 Shoot Polka
  (1,4,2),//27 Scar Fugue
  (1,4,3),//28 Wild Rhapsody
  (1,4,4),//29 Dark Scherzo
  (1,4,5),//30 Quick March
  (1,4,6),//31 Symphonic Drive
  (1,4,7),//32 Orchestra
  (1,5,0),//33 Tornado Dance
  (1,5,1),//34 Scissor Edge
  (1,5,2),//35 Rumbling Moon
  (1,5,3),//36 Illusion Rave
  (1,5,4),//37 Fake Capture
  (1,5,5),//38 Surprise Dunk
  (1,5,6),//39 Deadly Archer
  (1,5,7),//40 Acro Effect
  (1,6,0),//41 Ducking Blow
  (1,6,1),//42 Flicker Jab
  (1,6,2),//43 Pendulum Roll
  (1,6,3),//44 Slide Upper
  (1,6,4),//45 Straight Charge
  (1,6,5),//46 Quake Howling
  (1,6,6),//47 Surprise Knuckle
  (1,6,7),//48 Flash Thousand
  (1,7,0),//49 Tri-Impact
  (1,7,1),//50 Rage Dance
  (1,7,2),//51 Ein Raketen
  (1,7,3),//52 Serpent Air
  (1,7,4),//53 Aiming Shot
  (1,7,5),//54 Thrillsplosion
  (1,7,6),//55 Slash Rave
  (1,7,7),//56 Additional Bullet
  (1,8,0),//57 Piercing Shell
  (1,8,1),//58 Grenade Shell
  (1,8,2),//59 One Point
  (1,8,3),//60 Diffuse Shell
  (1,8,4),//61 Homing Emission
  (1,8,5),//62 Impact Slider
  (1,8,6),//63 Sneak Shooter
  (1,8,7),//64 Glorious Rain
  (1,9,0),//65 Divine Launcher
  (1,9,1),//66 Concentrate One
  (1,9,2),//67 Cluster Bullet
  (1,9,3),//68 Cracker Bullet
  (1,9,4),//69 Zero Distance
  (1,9,5),//70 Rodeo Drive
  (1,9,6),//71 Crazy Smash
  (1,9,7),//72 Fake Silhouette
  (1,10,0),//73 Aerial Shooting
  (1,10,1),//74 Bullet Squall
  (1,10,2),//75 Dead Approach
  (1,10,3),//76 Messiah Time
  (1,10,4),//77 Infinity Fire
  (1,10,5),//78 Satellite Aim
  (1,10,6),//79 Elder Rebellion
  (1,10,7),//80 Reverse Tap
  (2,1,0),//81 Foie
  (2,1,1),//82 Gifoie
  (2,1,2),//83 Rafoie
  (2,1,3),//84 Safoie
  (2,1,4),//85 Shifta
  (2,2,0),//86 Barta
  (2,2,1),//87 Gibarta
  (2,2,2),//88 RaBarta
  (2,2,3),//89 Sabarta
  (2,2,4),//90 Deband
  (2,3,0),//91 Zonde
  (2,3,1),//92 Gizonde
  (2,3,2),//93 Razonde
  (2,3,3),//94 Sazonde
  (2,3,4),//95 Zondeel
  (2,4,0),//96 Zan
  (2,4,1),//97 Gizan
  (2,4,2),//98 Razan
  (2,4,3),//99 Sazan
  (2,4,4),//100 Nazan
  (2,5,0),//101 Grants
  (2,5,1),//102 Gigrants
  (2,5,2),//103 Ragrants
  (2,5,3),//104 Resta
  (2,5,4),//105 Anti
  (2,6,0),//106 Megid
  (2,6,1),//107 Gimegid
  (2,6,2),//108 Ramegid
  (2,6,3),//109 Samegid
  (2,6,4),//110 Megiverse
  (1,14,0),//111 Tsukimi-sazanka
  (1,14,1),//112 Gekka-zakuro
  (1,14,2),//113 Kanran-kikyou
  (1,14,3),//114 Sakura-endo
  (1,14,4),//115 Hien-tsubaki
  (1,14,5),//116 Asagiri-rendan
  (1,14,6),//117 Hatou-rindou
  (1,14,7),//118 Shunka-shunran
  (1,15,0),//119 Master Shot
  (1,15,1),//120 Penetrating Arrow
  (1,15,2),//121 Torrential Arrow
  (1,15,3),//122 Gravity Point
  (1,15,4),//123 Kamikaze Arrow
  (1,15,5),//124 Sharp Bomber
  (1,15,6),//125 Final Nemesis
  (1,15,7),//126 Tritt Shooter
  (1,1,8),//127 Guilty Break
  (1,2,8), //128 Wild Round
  (1,3,8),//129 Sacred Skewer
  (1,4,8),  //130 Bloody Sarabande
  (1,5,8),  //131 Deadly Circle
  (1,6,8),  //132 Backhand Smash
  (1,7,8),//133 Kreisenschlag
  (1,8,8),  //134 Parallel Slider
  (1,9,8),  //135 Flame Bullet
  (1,10,8),  //136 Heel Stab
  (2,1,5),//137 Nafoie
  (2,2,5),//138 Nabarta
  (2,3,5),//139 Nazonde
  (2,4,5),//140 Zanverse
  (2,5,5),//141 Nagrants
  (2,6,5),//142 Namegid
  (1,1,9),//143 Sacrifice's Bite
  (1,2,9),//144 Kaiser Rise
  (1,3,9),//145 Vol Graptor
  (1,4,9),    //146 Facet Folia
  (1,5,9),  //147 Chaos Riser
  (1,6,9),  //148 Meteor Fist
  (1,7,9),  //149 Regenschlag
  (1,8,9),  //150 Satellite Cannon
  (1,9,9),  //151 Cosmos Breaker
  (1,10,0),  //152 Shift Period
  (1,14,8),//153 Kazan-nadeshiko
  (1,14,9),//154 Fudou-kuchinashi
  (1,15,8),  //155 Banishing Arrow
  (1,15,9),//156 Million Storm
  (2,1,6),//157 Ilfoie
  (2,2,6),//158 Ilbarta
  (2,3,6),//159 Ilzonde
  (2,4,6),//160 Ilzan
  (2,5,6),//161 Ilgrants
  (2,6,6),//162 Ilmegid
  (1,16,0),//163 Strike Gust
  (1,16,1),//164 Gran Wave
  (1,16,2),//165 Moment Gale
  (1,16,3),//166 Vinto Gigue
  (1,17,0),//166 Distraction Wing
  (1,17,1),//167 Dispersion Shrike
  (1,17,2),//168 Heavenly Kite
  (1,17,3),//169 Justice Crow
  (1,17,4),//170 Starling Fall
  (1,17,5),//171 Kestrel Rampage
  (1,6,10),//172 Heartless Impact
  (1,9,10),  //173 Sphere Eraser
  (1,15,10),//174 Chase Arrow
  (1,1,10));//175 Ignition Parry

implementation

uses UInventory, UPlayer, Ulogin;

procedure LearnDisk(x:integer;a:ansistring);
var id1,id2:dword;
    i,l:integer;
    s:ansistring;
begin
    move(a[9],id1,4);
    move(a[13],id2,4);
    //find the item code
    for i:=0 to player[x].activechar.BankCount-1 do begin
        if (player[x].activechar.PersonalBank[i].id1 = id1) and (player[x].activechar.PersonalBank[i].id2 = id2) then begin
            if player[x].activechar.PersonalBank[i].ItemType = 9 then begin
                //disk
                for l:=0 to 174 do
                    if (player[x].activechar.PersonalBank[i].ItemSub = PAtoID[l,0]) and
                        (player[x].activechar.PersonalBank[i].unk = PAtoID[l,1]) and
                        (player[x].activechar.PersonalBank[i].ItemEntry = PAtoID[l,2]) then begin
                            //learn it
                            player[x].activechar.PALvl[l]:=player[x].activechar.PersonalBank[i].unk2 shr 8;
                            //delete it
                            s:=#$1C#$00#$00#$00#$0F#$06#$04#$00#$F0#$AD#$00#$00#$35#$D7#$AD#$A1
                              +#$B9#$28#$A6#$04#$00#$00#$01#$00#$02#$00#$00#$00;
                              move(player[x].activeChar.PersonalBank[i].id1,s[13],4);
                              move(player[x].activeChar.PersonalBank[i].id2,s[17],4);
                              //s[$15]:=chr(player[x].activeChar.PersonalBank[i].count-1);
                            removeFromStorage(x,player[x].activeChar.PersonalBank[i].id1,player[x].activeChar.PersonalBank[i].id2,1
                                ,player[x].activeChar.PersonalBank,@player[x].activechar.bankcount,50);
                            sendtoplayer(x,s);
                            sendpa(x);

                            s:=#$24#$00#$00#$00#$0F#$33#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
                            +#$04#$00#$00#$00#$01#$00#$00#$00#$57#$00#$00#$00#$09#$00#$02#$00
                            +#$02#$00#$02#$00;
                            move(player[x].gid,s[9],4);
                            s[$15] := ansichar(player[x].activechar.PersonalBank[i].unk2 shr 8);
                            s[$19] := ansichar(l);
                            s[$1f] := ansichar(PAtoID[l,0]);
                            s[$21] := ansichar(PAtoID[l,1]);
                            s[$23] := ansichar(PAtoID[l,2]);
                            sendtolobby(player[x].room,-1,s);
                            break;
                        end;

            end;
            break;
        end;
    end;

end;

end.
