unit ULobby;

interface
uses types,sysutils, windows, Classes;

type
    TCafeDesign = record
        id:integer;
        filename:string;
    end;

procedure GoCasino(x,p:integer);
procedure GoCafe(x:integer);
procedure PlayerChat(x:integer;s:ansistring);
procedure GoBridge(x:integer);
procedure GoLobby(x:integer; id:integer);
procedure BlockSelect(x:integer);
procedure GoBootCamp(x,from:integer);
procedure sendObjtoplayer(x:integer;s:ansistring);
procedure leavelobby(x:integer);
Procedure ChangeLobbyEvent(s:ansistring);
function KeepStaticItem(s:ansistring):ansistring;
procedure AddPtsToGoal(x,g,id:integer);
procedure KickBall(x:integer;s:ansistring);
procedure LeaveToLobby(x:integer);
procedure GoDressingRoom(x:integer);
procedure GoMyRoom(x,roomid:integer);
procedure GoBattleLobby(x,id:integer);
procedure setfencestatus(t,l,id,st:integer);

var LobbyEventObj:ansistring;
    Goal1Pts:integer = 0;
    Goal2Pts:integer = 0;
    goal1id:integer=0;
    goal2id:integer=0;

const CafeDesign : array[0..3] of TCafeDesign  = (
    (id:$a0; filename:'map\cafe.obj.city2'),
    (id:$a2; filename:'map\cafe.obj.city'),
    (id:$a4; filename:'map\cafe.obj.garden'),
    (id:$a3; filename:'map\cafe.obj.winter')
);

implementation

uses UPlayer, UDB, fmain, UInventory, UMovement, UDebug, UQuestBoard,
  casino, UParty, UPsoUtil, UForest, Uerror, clientorder, ItemGenerator,
  UMyRoom, UTimerHelper, RoomAction;





procedure LeaveToLobby(x:integer);
begin
    //leave party
    LeaveParty(x);
    //go to lobby
    sendtoplayer(x,#$0C#$00#$00#$00#$0E#$13#$00#$00#$00#$00#$00#$00);
    //GoLobby(x,2);

    sendtoplayer(x,#$14#$00#$00#$00#$0E#$1A#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00);
end;

procedure CleanLobbyEvent();
var i,l:integer;
    a:ansistring;
begin
    i:=1;
    while i < length(LobbyEventObj) do begin
        move(LobbyEventObj[i],l,4);

        //remove it
        a:=#$20#$00#$00#$00#$04#$06#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
        +#$04#$00#$00#$00#$B9#$00#$00#$00#$00#$00#$00#$00#$06#$00#$A9#$11;

        move(LobbyEventObj[i+8],a[$15],12); //real id
        sendtolobby(room_lobby,-1,a);
        inc(i,l);
    end;
    LobbyEventObj:='';

end;

function CleanStaticItems(s:ansistring):ansistring;
var i,l,k:integer;
    sl:tstringlist;
begin
    //keep only the obj
    result:='';
    i:=1;
    //find sit item
    sl:=tstringlist.create;
    while i < length(s) do begin
        move(s[i],l,4);
        if pansichar(@s[i+$24]) = 'oa_sit_point' then begin
            sl.Add(inttostr(pdword(@s[i+$b8])^));
        end;
        i:=i+l;
    end;

    i:=1;

    while i < length(s) do begin
        move(s[i],l,4);

        if (s[i+4] = #8) and (s[i+5] = #$b) then
            if copy(pansichar(@s[i+$24]),1,3) = 'ob_' then begin
                if sl.IndexOf(inttostr(pdword(@s[i+$8])^)) = -1 then
                if copy(pansichar(@s[i+$24]),1,12) <> 'ob_9900_0399' then begin //jumppad
                    k:=length(result);    //change the id to fit
                    result:=result+copy(s,i,l);
                    result[11+k]:=#1;
                end;
            end;
            if pansichar(@s[i+$24]) = 'oa_movie_object' then begin
                k:=length(result);
                result:=result+copy(s,i,l);
                result[11+k]:=#1;
            end;
        i:=i+l;
    end;
    sl.Free;
end;

function KeepStaticItem(s:ansistring):ansistring;
var i,l:integer;
    sl:tstringlist;
begin
    //keep only the obj
    result:='';
    i:=1;
    //find sit item
    sl:=tstringlist.create;
    while i < length(s) do begin
        move(s[i],l,4);
        if pansichar(@s[i+$24]) = 'oa_sit_point' then begin
            sl.Add(inttostr(pdword(@s[i+$b8])^));
        end;
        i:=i+l;
    end;
    i:=1;
    while i < length(s) do begin
        move(s[i],l,4);

        if (s[i+4] = #8) and (s[i+5] = #$b) then begin
            if (copy(pansichar(@s[i+$24]),1,3) <> 'ob_') or (copy(pansichar(@s[i+$24]),1,12) = 'ob_9900_0399') or
                (sl.IndexOf(inttostr(pdword(@s[i+$8])^)) > -1) then begin
                if pansichar(@s[i+$24]) <> 'oa_movie_object' then
                result:=result+copy(s,i,l);
            end;

        end else result:=result+copy(s,i,l);
        i:=i+l;
    end;
    sl.Free;
end;

Procedure ChangeLobbyEvent(s:ansistring);
begin
    CleanLobbyEvent();

    LobbyEventObj:=CleanStaticItems(GenerateObjFile(path+s,-1,-1));
    sendtolobby(room_lobby,-1,LobbyEventObj);
end;

function FindPlayerByansistring(s:ansistring):integer;
var i,id:integer;
begin
    result:=-1;
    //test player ark name
    for i:=0 to srvcfg.MaxPlayer-1 do
        if player[i].GID <> 0 then if playerok(i) then begin
            if lowercase(s) = lowercase(player[i].save.ArksName) then begin
                result:=i;
                exit;
            end;
        end;

    //test player gid
    id:=strtointdef(s,0);
    if id <> 0 then begin
    for i:=0 to srvcfg.MaxPlayer-1 do
        if player[i].GID = id then begin

                result:=i;
                exit;

        end;
    end;
end;

procedure PlayerChat(x:integer;s:ansistring);
var cmd,i:integer;
    txt,a:ansistring;
begin
    move(player[x].gid,s[9],4);
    s[$11]:=#4;
    //test command
    cmd:=0;
    txt:=pwidechar(@s[$1d]);
    if form1.Memo8.Lines.IndexOf(player[x].username) > -1 then begin
        if copy(txt,1,5) = '/exp 'then begin
            a:=txt;
            delete(a,1,5);
            addplayerexp(x,strtoint(a));
            sendsystemmsg(x,'You received '+a+' exp');
            cmd:=1;
        end;
    end;
    if player[x].isgm = 1 then begin
        //gm command
        if txt = '/gm '+srvcfg.gmpass then begin
            player[x].gmunlocked:=1;
            sendsystemmsg(x,'GM command unlocked.');
            cmd:=1;
        end;

        if txt = '/unhide map' then begin
            cmd:=1;
            for i:=0 to 15 do begin
                player[x].FloorArea[i]:=#$ff#$ff#$ff#$ff#$ff#$ff#$ff#$ff#$ff#$ff#$ff#$ff#$ff#$ff#$ff#$ff;
                a:=#$34#$00#$00#$00#$0B#$13#$00#$00#$A9#$11#$00#$00#$00#$00#$00#$00
                  +#$10#$00#$00#$00#$7B#$36#$00#$00#$00#$00#$00#$00#$0D#$00#$53#$01
                  +#0#0#0#0#$00#$18#$60#$00#$01#$04#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;
                move(i,a[$21],4);
                move(player[x].partyid,a[$15],4);
                move(player[x].FloorArea[i][1],a[$25],$10);
                sendtoplayer(x,a);
            end;
        end;

        if txt = '/reloadmap' then begin
            cmd:=1;
            ReloadFloor(x);
        end;

        if player[x].gmunlocked = 1 then begin
            //ban kick and other command
            if copy(txt,1,5) = '/ban ' then begin
                cmd:=1;
                i:=FindPlayerByansistring(copy(txt,6,length(txt)-5));
                if i > -1 then begin
                    form1.Memo7.Lines.Add(player[i].username);
                    form1.memo7.Lines.SaveToFile(path+'banlist.txt');
                    sendtoplayer(i,MakeError(ERR_GMBAN,'You have been banned by a GM'));
                    player[i].todc:=1;
                    sendsystemmsg(x,'Player banned.');
                end else sendsystemmsg(x,'Player not found.');
            end;

            if copy(txt,1,6) = '/kick ' then begin
                cmd:=1;
                i:=FindPlayerByansistring(copy(txt,7,length(txt)-6));
                if i > -1 then begin
                    sendtoplayer(i,MakeError(ERR_GMBAN,'You have been kicked by a GM'));
                    player[i].todc:=1;
                    sendsystemmsg(x,'Player kicked.');                 
                end else sendsystemmsg(x,'Player not found.');
            end;

            if copy(txt,1,6) = '/item ' then begin
                cmd:=1;
                a:=copy(txt,7,length(txt)-6);
                while length(a) < 16 do a:=a+'0';
                a:=ansichar(strtoint('$'+copy(a,1,2)))+ansichar(strtoint('$'+copy(a,3,2)))
                    +ansichar(strtoint('$'+copy(a,5,2)))+ansichar(strtoint('$'+copy(a,7,2)))
                    +ansichar(strtoint('$'+copy(a,9,2)))+ansichar(strtoint('$'+copy(a,11,2)))
                    +ansichar(strtoint('$'+copy(a,13,2)))+ansichar(strtoint('$'+copy(a,15,2)));
                AddItemToInv(x,a);
            end;
        end;

    end;

    if cmd = 0 then begin
    if s[$15] = #0 then begin
        if player[x].room >= room_team then sendtofloor(player[x].room,player[x].Floor,s)
        else SendToLobby(player[x].room,-1,s);
    end;
    if s[$15] = #1 then SendToParty(x,s);
    end;
end;

//call this when a player leave a room
procedure leavelobby(x:integer);
var s:ansistring;
    i:integer;
begin
    for i:=0 to 31 do
       if CasinoShooterObj[i] = x then CasinoShooterObj[i]:=-1;

    s:=#$0C#$00#$00#$00#$11#$0A#$00#$00#$56#$5F#$9C#$00;
    move(player[x].gid,s[9],4);
    sendtolobby(player[x].room,-1,s);

    //send remove item

    s:=#$20#$00#$00#$00#$04#$06#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
        +#$04#$00#$00#$00#$B9#$00#$00#$00#$00#$00#$00#$00#$04#$00#$00#$00;

        move(player[x].gid,s[$15],4);
        sendtolobby(player[x].room,-1,s);

    if player[x].petloaded = 1 then begin
        s:=#$20#$00#$00#$00#$04#$06#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	        +#$04#$00#$00#$00#$B2#$00#$00#$00#$01#$00#$00#$00#$06#$00#$a9#$11;    
        move(player[x].gid,s[$9],4);
        move(player[x].gid,s[$15],4);
        sendtolobby(player[x].room,-1,s);
        player[x].petloaded:=0;
    end;
end;


procedure sendObjtoplayer(x:integer;s:ansistring);
var a:ansistring;
    i,l,o:integer;
begin

    i:=1;
    while i < length(s) do begin
        move(s[i],l,4);
        //form1.Memo1.Lines.Add('id: '+inttohex(byte(s[i+4]),2)+' '+inttohex(byte(s[i+5]),2)+' '+inttohex(l,2)+' '+pansichar(@s[i+$24]));


            if pansichar(@s[i+$24]) = 'ob_9900_0418' then begin
                move(s[i+8],o,4);
                if (o = goal1id) or (goal1id = 0) then begin
                    if goal1id = 0 then goal1id:=o;
                    move(goal1pts,s[i+$a8],4);

                end else begin
                    if goal2id = 0 then goal2id:=o;
                    move(goal2pts,s[i+$a8],4);
                end;
            end;

        {if (s[i+4] = #8) and (s[i+5] = #$c) then begin
            move(s[i+8],a[$15],12);
            move(s[i+8],a[$25],12);

            move(s[i+8],a[$65],12);
            move(s[i+8],a[$75],12);

            result:=result+a;
        end;   }

        if (s[i+4] <> #8) or (s[i+5] <> #$e) then sendtoplayer(x,copy(s,i,l));

        if (s[i+4] = #8) and (s[i+5] = #$b) then
            if pansichar(@s[i+$24]) = 'oa_movie_object' then begin
                //make it go warning if has too
                a:=#$54#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
                +#$04#$00#$00#$00#$AF#$03#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
                +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                +#$EB#$5C#$00#$00;//'EmergencyStart'+#0#0;
                move(s[i+8],a[$15],12); //item id
                if redalert = 1 then a:=a+'EmergencyStart'+#0#0
                else a:=a+'EmergencyStop'+#0#0#0 ;
                if redalert = 1 then sendtoplayer(x,a);
            end;
        i:=i+l;
    end;
end;

procedure GoDressingRoom(x:integer);
var s:ansistring;
begin
    leavelobby(x);

    player[x].room:=Room_Dressing;

    sendtoplayer(x,#$0C#$00#$00#$00#$1E#$0C#$00#$00#$7B#$00#$00#$00);

    s:=#$54#$00#$00#$00#$03#$00#$00#$00#$2A#$0D#$01#$00#$00#$00#$00#$00
	+#$05#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$FC#$FF#$FF#$FF#$28#$23#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
	+#$00#$00#$00#$00;

    move(player[x].gid,s[$15],4);
    sendtoplayer(x,s);

    s:=#$14#$00#$00#$00#$06#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00;
    move(player[x].gid,s[$9],4);
    sendtoplayer(x,s);

    sendtoplayer(x,#$08#$00#$00#$00#$03#$2B#$00#$00);


    sendtoplayer(x,#$10#$00#$00#$00#$1E#$0E#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00);


    s:=#$00#$00#$00#$00#$00#$00#$00#$3C#$00#$00#$00#$45
	+#$00#$00#$00#$00;


    move(s[9],player[x].curpos,8);
    move(s[1],player[x].rotation,8);
    SpawnPlayer(x,1);

    sendtoplayer(x,#$08#$00#$00#$00#$03#$23#$00#$00);

    sendtoplayer(x,#$88#$00#$00#$00#$34#$23#$04#$00#$DF#$3D#$00#$00#$BB#$30#$AC#$30
	+#$C0#$30#$A4#$30#$EC#$30#$AF#$30#$C8#$30#$00#$00#$DF#$3D#$00#$00
	+#$A8#$30#$B9#$30#$C6#$30#$29#$52#$28#$75#$D1#$30#$B9#$30#$00#$00
	+#$A7#$3D#$00#$00#$A7#$3D#$00#$00#$A7#$3D#$00#$00#$A6#$3D#$00#$00
	+#$01#$00#$00#$00#$A6#$3D#$00#$00#$03#$00#$15#$00#$00#$00#$01#$00
	+#$A6#$3D#$00#$00#$01#$07#$08#$00#$F4#$01#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$05#$40#$00#$00#$00#$00
	+#$01#$00#$01#$00#$00#$00#$00#$00);
end;

procedure GoMyRoom(x,roomid:integer);
var s:ansistring;
    i,l,c,v:integer;
begin
    if roomid = 0 then roomid := player[x].gid; //to my room

    leavelobby(x);

    i:=-1;
    for c:=0 to 199 do
        if PersonnalRooms[i].ownerid = roomid then i:=c;

    //find a free slot
    if i = -1 then
    for i:=0 to 199 do
        if PersonnalRooms[i].ownerid = $ffffffff then begin
            //load the room data
            LoadRoom(roomid,i);

            break;
        end;

    player[x].room:=Room_PersonnalRoom+i;

    sendtoplayer(x,#$0C#$00#$00#$00#$1E#$0C#$00#$00#$34#$00#$00#$00);

    //send player owner name
    s:=#$4C#$00#$00#$00#$11#$19#$00#$00#$50#$00#$69#$00#$6B#$00#$61#$00
        +#$73#$00#$6F#$00#$6F#$00#$00#$00#$08#$45#$98#$04#$00#$00#$00#$00
        +#$0C#$00#$00#$00#$00#$00#$00#$00#$F0#$22#$05#$40#$01#$00#$00#$00
        +#$20#$67#$12#$06#$00#$00#$00#$00#$F0#$22#$05#$40#$01#$00#$00#$00
        +#$08#$45#$98#$04#$00#$00#$00#$00#$00#$00#$00#$00;
    move(PersonnalRooms[i].ownername[0],s[9],64);
    sendtoplayer(x,s);



    s:=FileGetPacket('map\myroom1.dat');
    player[x].cmap:=copy(s,$25,$30);
    move(player[x].gid,s[$15],4);
    move(player[x].partyid,s[$6d],4);
    i:=$11a9;
    l:=$79;
    l:=l+((ReadMagic(copy(s,l,4),$7502,$1e)+3) and $fffC)+4;
    v:=ReadMagic(copy(s,l,4),$7502,$1e);
    inc(l,4);
    for c:=0 to v-1 do begin
        move(i,s[l],4);
        inc(l,$34);
    end;
    //move(i,s[$8d],4);
    //move(i,s[$c1],4);

    move(i,s[$21],4);
    move(i,s[$55],4);
    i:=$0153000d; //#$0D#$00#$53#$01
    move(i,s[$75],4);
    sendtoplayer(x,s);

    //full format:
    {
        header: 8 byte;
        some id: 12 byte
        player id: 12 byte
        owningitemid: 8 byte;
        area info? : FEFFFFFF9600000002000000
        random: dword;
        areainfo2: 00000000000000000000000000000000FFFFFFFFFFFFFFFF01030000
        owningitemid: 8 byte;
        unk1: 10000000000000000000000000000000
        partyid: 12 byte;
        magic: dword;
            area ansistring: magic size
        magic: floor count
            owningitemid: 8 byte;
            floor num: -2 = campship, 0 = first map
            data: 0x28 byte

     }


    player[x].lastarea1:=#$f8#$ff#$ff#$ff;
    //sendtoplayer(x,#$08#$00#$00#$00#$03#$2A#$00#$00);

    s:=#$14#$00#$00#$00#$06#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00;
    move(player[x].gid,s[9],4);
    sendtoplayer(x,s);

    s:=#$00#$00#$00#$3C#$00#$00#$00#$80#$EC#$A9#$00#$00#$16#$43#$00#$00;


    move(s[9],player[x].curpos,8);
    move(s[1],player[x].rotation,8);
    player[x].floor:=-1;
    SpawnPlayer(x);

    //send map obj
    //s:=GenerateObjFile(path+'quest\'+inttohex(team[player[x].TeamID].questid,2) +'\floor0.obj');
    //sendObjtoplayer(x,s);

    SendMyRoomObject(x,player[x].room-Room_PersonnalRoom);

    sendtoplayer(x,#$08#$00#$00#$00#$03#$23#$00#$00);

    sendtoplayer(x,#$08#$00#$00#$00#$03#$2B#$00#$00);

    sendtoplayer(x,#$0C#$00#$00#$00#$19#$0F#$00#$00#$7D#$00#$00#$00);


    //send npc
    {s:=FileGetPacket('map\campship.npc');
    sendtoplayer(x,FixNPCGID(x,s));     }
    player[x].floor:=-1;
    player[x].firstping:=0;
end;


procedure GoBootCamp(x,from:integer);
var s:ansistring;
    i,l,c,v,fail:integer;
begin
    fail:=0;
    if player[x].failfrom <> '' then begin
        //set to fail
        //does it still exists
        for i:=0 to 7 do
          if team[player[x].TeamID].ActiveCode[i].used = 1 then
            if team[player[x].TeamID].ActiveCode[i].name = player[x].failfrom then
              if team[player[x].TeamID].ActiveCode[i].floor = player[x].floor then begin
                fail:=1; //will fail
                s:=#$14#$00#$00#$00#$15#$0B#$00#$00#$41#$02#$00#$00#$00#$00#$00#$00#$06#$00#$A9#$11;
                move(team[player[x].TeamID].ActiveCode[i].id3,s[9],4);
                sendtoplayer(x,s);
              end;

    end;

    leavelobby(x);

    player[x].room:=Room_Team+player[x].TeamID;

    sendtoplayer(x,#$0C#$00#$00#$00#$1E#$0C#$00#$00#$34#$00#$00#$00);


    if from = 0 then begin
        s:=FileGetPacket('quest\'+inttohex(team[player[x].TeamID].questid,2) +'\floor0.map');
        player[x].cmap:=copy(s,$25,$30);
        move(player[x].gid,s[$15],4);
        move(player[x].partyid,s[$6d],4);
        i:=$11a9;
        l:=$79;
        l:=l+((ReadMagic(copy(s,l,4),$7502,$1e)+3) and $fffC)+4;
        v:=ReadMagic(copy(s,l,4),$7502,$1e);
        inc(l,4);
        for c:=0 to v-1 do begin
            //put the selected map
            if c>0 then begin
                //this mess up if moving the wrong floor around
                //disable for map testing
                move(team[player[x].TeamID].FloorMap[c-1][$21],s[l],$34);
            end;

            //fix the fucking floor id
            move(i,s[l],4);
            inc(l,$34);
        end;

        //now fix the door data if needed
        v:=ReadMagic(copy(s,l,4),$7502,$1e);
        inc(l,4);
        for c:=0 to v-1 do begin
            if team[player[x].TeamID].door_entry_p[byte(s[l+16])] <> $ffffffff then
                pdword(@s[l+8])^ := team[player[x].TeamID].door_entry_p[byte(s[l+16])];


            inc(l,20);
        end;

        //move(i,s[$8d],4);
        //move(i,s[$c1],4);
        
        move(i,s[$21],4);
        move(i,s[$55],4);
        i:=$0153000d; //#$0D#$00#$53#$01
        move(i,s[$75],4);
        sendtoplayer(x,s);

        //full format:
        {
            header: 8 byte;
            some id: 12 byte
            player id: 12 byte
            owningitemid: 8 byte;
            area info? : FEFFFFFF9600000002000000
            random: dword;
            areainfo2: 00000000000000000000000000000000FFFFFFFFFFFFFFFF01030000
            owningitemid: 8 byte;
            unk1: 10000000000000000000000000000000
            partyid: 12 byte;
            magic: dword;
                area ansistring: magic size
            magic: floor count
                owningitemid: 8 byte;
                floor num: -2 = campship, 0 = first map
                data: 0x28 byte

         }
    end else begin
        s:=#$54#$00#$00#$00#$03#$00#$00#$00#$8E#$5C#$00#$00#$00#$00#$00#$00
        +#$05#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
        +#$A9#$11#$00#$00#$00#$00#$00#$00#$FE#$FF#$FF#$FF#$96#$00#$00#$00
        +#$02#$00#$00#$00#$5A#$E8#$8D#$C7#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
        +#$01#$03#$00#$00;
        player[x].cmap:=copy(s,$25,$30);
        move(player[x].gid,s[$15],4);
        sendtoplayer(x,s);
    end;

    player[x].lastarea1:=#$fe#$ff#$ff#$ff;
    //sendtoplayer(x,#$08#$00#$00#$00#$03#$2A#$00#$00);

    s:=#$14#$00#$00#$00#$06#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00;
    move(player[x].gid,s[9],4);
    sendtoplayer(x,s);

    s:=#$00#$00#$00#$00#$00#$00#$00#$3C#$08#$39#$00#$00
	+#$F5#$B3#$00#$00;

    if from = 1 then s:=#$00#$00#$00#$3C#$00#$00#$00#$80#$34#$BC#$00#$00 //from forest
	        +#$E7#$43#$00#$00;

    move(s[9],player[x].curpos,8);
    move(s[1],player[x].rotation,8);
    player[x].floor:=-1;
    SpawnPlayer(x);

    s:=#$40#$00#$00#$00#$0F#$BC#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00;
    move(player[x].gid,s[9],4);
    sendtoplayer(x,s);

    //send map obj
    s:=GenerateObjFile(path+'quest\'+inttohex(team[player[x].TeamID].questid,2) +'\floor0.obj',-1,-1);
    sendObjtoplayer(x,s);

    sendtoplayer(x,#$08#$00#$00#$00#$03#$23#$00#$00);

    sendtoplayer(x,#$08#$00#$00#$00#$03#$2B#$00#$00);

    sendtoplayer(x,#$0C#$00#$00#$00#$19#$0F#$00#$00#$7D#$00#$00#$00);



    if from = 1 then begin
        if fail = 1 then begin
            player[x].questcompleted:=0; //failled
            SendQuestFailled(x);
        end else
        if player[x].questcompleted = 1 then begin
            SendQuestResult(x);
        end;
    end;

    //send npc
    {s:=FileGetPacket('map\campship.npc');
    sendtoplayer(x,FixNPCGID(x,s));     }
    player[x].floor:=-1;
    player[x].firstping:=0;
    sendmeteo(player[x].teamid);
end;

procedure GoCasino(x,p:integer);
var s:ansistring;
    i,l:integer;
begin


    leavelobby(x);

    player[x].room:=Room_Casino;

    sendtoplayer(x,#$0C#$00#$00#$00#$1E#$0C#$00#$00#$7D#$00#$00#$00);



    s:=#$54#$00#$00#$00#$03#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$05#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$F4#$FF#$FF#$FF#$68#$00#$00#$00
	+#$03#$00#$00#$00#$68#$D8#$AA#$63#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$02#$00#$00#$00
	+#$00#$00#$00#$00;
    move(player[x].gid,s[$15],4);
    sendtoplayer(x,s);


    //sendtoplayer(x,#$08#$00#$00#$00#$03#$2A#$00#$00);

    s:=#$14#$00#$00#$00#$06#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00;

    move(player[x].gid,s[9],4);
    sendtoplayer(x,s);

    s:=#$00#$00#$FB#$BB#$00#$00#$68#$2C#$63#$C0#$80#$46
	+#$39#$56#$00#$00;
    if p = 1 then s:=#$00#$00#$00#$3C#$00#$00#$00#$80#$19#$BE#$CC#$B8
	+#$7F#$4D#$00#$00;

    move(s[9],player[x].curpos,8);
    move(s[1],player[x].rotation,8);
    SpawnPlayer(x);

    //send map obj
    s:=GenerateObjFile(path+'map\casino.obj',-1,-1);
    sendObjtoplayer(x,s);

    sendtoplayer(x,#$08#$00#$00#$00#$03#$23#$00#$00);

    sendtoplayer(x,#$08#$00#$00#$00#$03#$2B#$00#$00);

    sendtoplayer(x,#$0C#$00#$00#$00#$19#$0F#$00#$00#$7D#$00#$00#$00);

    //sendArmor(x);

    //send npc
    //s:=FileGetPacket('map\casino.npc');
    //sendtoplayer(x,FixNPCGID(x,s));
    player[x].floor:=-1;
    player[x].firstping:=0;


    //send shooter status
    for i:=0 to 31 do
    if CasinoShooterObj[i] <> -1 then begin
        l:=CASINOID_SHOOTER+i;
        s:=#$24#$00#$00#$00#$04#$79#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
        +#$04#$00#$00#$00#$3F#$04#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
        +#$04#$00#$00#$00;
        move(l,s[$15],4);
        move(player[x].gid,s[9],4);
        sendtoplayer(x,s);
    end;
end;

procedure GoCafe(x:integer);
var s:ansistring;
    i:integer;
begin


    LeaveLobby(x);

    player[x].room:=Room_Cafe;

    sendtoplayer(x,#$0C#$00#$00#$00#$1E#$0C#$00#$00#$7D#$00#$00#$00);



    {s:=#$54#$00#$00#$00#$03#$00#$00#$00#$02#$00#$00#$00#$00#$00#$00#$00  //beatch cofee
	+#$05#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$F2#$FF#$FF#$FF#$A0#$00#$00#$00
	+#$03#$00#$00#$00#$BB#$0B#$1B#$68#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00
	+#$00#$00#$00#$00      }
    s:=#$54#$00#$00#$00#$03#$00#$00#$00#$02#$00#$00#$00#$00#$00#$00#$00
	+#$05#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$F2#$FF#$FF#$FF#$A2#$00#$00#$00  //a0 = city  a2 = city2 //a3 = rappy snow  //a4 = spring
	+#$03#$00#$00#$00#$BB#$0B#$1B#$68#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00
	+#$00#$00#$00#$00;

    s[$2d] := ansichar(CafeDesign[form1.ComboBox5.ItemIndex].id);
    
    move(player[x].gid,s[$15],4);
    sendtoplayer(x,s);


    sendtoplayer(x,#$08#$00#$00#$00#$03#$2A#$00#$00);

    s:=#$14#$00#$00#$00#$06#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00;
    move(player[x].gid,s[9],4);
    sendtoplayer(x,s);

    s:=#$00#$00#$00#$00#$00#$00#$00#$3C#$03#$B8#$1E#$25
	+#$12#$CE#$00#$00;
        move(s[9],player[x].curpos,8);
    move(s[1],player[x].rotation,8);
    SpawnPlayer(x);


    //send map obj
    s:=GenerateObjFile(path+CafeDesign[form1.ComboBox5.ItemIndex].filename,-1,-1);
    sendObjtoplayer(x,s);

    sendtoplayer(x,#$08#$00#$00#$00#$03#$23#$00#$00);

    sendtoplayer(x,#$08#$00#$00#$00#$03#$2B#$00#$00);

    sendtoplayer(x,#$0C#$00#$00#$00#$19#$0F#$00#$00#$7D#$00#$00#$00);

    //sendArmor(x);

    //send npc
    //s:=FileGetPacket('map\cafe.npc');
    //sendtoplayer(x,FixNPCGID(x,s));
    player[x].floor:=-1;
    player[x].firstping:=0;

    TestForClientOrder(x);

end;

procedure GoBattleLobby(x,id:integer);
var s:ansistring;
    i:integer;
begin
    LeaveLobby(x);

    player[x].room:=Room_BattleLobby;
    if id = 0 then player[x].room:=Room_ChalengeLobby;

    if id = 1 then begin
        sendtoplayer(x,#$14#$00#$00#$00#$03#$08#$00#$00#$03#$00#$EA#$07#$00#$00#$00#$00
	        +#$CA#$00#$00#$00 );

    end else begin
        sendtoplayer(x,#$14#$00#$00#$00#$03#$08#$00#$00#$03#$00#$EA#$07#$00#$00#$00#$00
	        +#$6A#$00#$00#$00 );

    end;

    sendtoplayer(x,#$0C#$00#$00#$00#$1E#$0C#$00#$00#$7D#$00#$00#$00);

    if id = 0 then s:=FileGetPacket('map\chalenge.dat')
    else s:=FileGetPacket('map\battle.dat');

    move(player[x].gid,s[$15],4);
    sendtoplayer(x,s);


    sendtoplayer(x,#$08#$00#$00#$00#$03#$2A#$00#$00);

    s:=#$14#$00#$00#$00#$06#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00;
    move(player[x].gid,s[9],4);
    sendtoplayer(x,s);

    s:=#$00#$00#$00#$3C#$00#$00#$00#$80#$9F#$A9#$33#$41#$8C#$53#$00#$00;
        move(s[9],player[x].curpos,8);
    move(s[1],player[x].rotation,8);
    SpawnPlayer(x);


    //send map obj
    if id = 0 then s:=GenerateObjFile(path+'map\chalenge.obj',-1,-1)
    else s:=GenerateObjFile(path+'map\battle.obj',-1,-1);
    sendObjtoplayer(x,s);

    sendtoplayer(x,#$08#$00#$00#$00#$03#$23#$00#$00);

    sendtoplayer(x,#$08#$00#$00#$00#$03#$2B#$00#$00);

    sendtoplayer(x,#$0C#$00#$00#$00#$19#$0F#$00#$00#$7D#$00#$00#$00);

    //remove player from team
    LeaveParty(x);

    player[x].floor:=-1;
    player[x].firstping:=0;

    TestForClientOrder(x);

end;


procedure GoBridge(x:integer);
var s:ansistring;
    i:integer;
begin


    leavelobby(x);

    player[x].room:=Room_Bridge;

    sendtoplayer(x,#$0C#$00#$00#$00#$1E#$0C#$00#$00#$7D#$00#$00#$00);



    s:=#$54#$00#$00#$00#$03#$00#$00#$00#$02#$31#$00#$00#$00#$00#$00#$00
	+#$05#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$F3#$FF#$FF#$FF#$6B#$00#$00#$00
	+#$02#$00#$00#$00#$BF#$24#$50#$8D#$01#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$01#$00#$00#$00
	+#$00#$00#$00#$00;
    move(player[x].gid,s[$15],4);
    sendtoplayer(x,s);


    //sendtoplayer(x,#$08#$00#$00#$00#$03#$2A#$00#$00);

    s:=#$14#$00#$00#$00#$06#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00;
    move(player[x].gid,s[9],4);
    sendtoplayer(x,s);

    s:=#$00#$00#$00#$00#$00#$00#$00#$3C#$9F#$37#$1E#$29
	+#$23#$C8#$00#$00;
        move(s[9],player[x].curpos,8);
    move(s[1],player[x].rotation,8);
    SpawnPlayer(x);

    //send map obj
    s:=GenerateObjFile(path+'map\bridge.obj',-1,-1);
    sendObjtoplayer(x,s);

    sendtoplayer(x,#$08#$00#$00#$00#$03#$23#$00#$00);

    sendtoplayer(x,#$08#$00#$00#$00#$03#$2B#$00#$00);

    sendtoplayer(x,#$0C#$00#$00#$00#$19#$0F#$00#$00#$7D#$00#$00#$00);

    //sendArmor(x);

    //send npc
    //s:=FileGetPacket('map\bridge.npc');
    //sendtoplayer(x,FixNPCGID(x,s));
    player[x].floor:=-1;


end;


procedure GoLobby(x:integer; id:integer);
var s:ansistring;
    i:integer;
begin


    leavelobby(x) ;

    player[x].room:=Room_Lobby;
    if id = 1 then player[x].room:=Room_Shop;

    sendtoplayer(x,#$0C#$00#$00#$00#$1E#$0C#$00#$00#$7D#$00#$00#$00);

    s:=#$54#$00#$00#$00#$03#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00   // aqua lobby
    +#$05#$00#$00#$00#$66#$9D#$A3#$00#$00#$00#$00#$00#$04#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$FB#$FF#$FF#$FF#$6A#$00#$00#$00     //probably zone_id and door_id
    +#$03#$00#$00#$00#$F1#$74#$2E#$55#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00#$FF#$FF#$FF#$FF
    +#$00#$00#$00#$00;

    if srvcfg.lobby = 0 then i:=$552e74f1; //aqua lobby
    if srvcfg.lobby = 1 then i:=$1ab8b522; //godziLee lobby
    if srvcfg.lobby = 2 then i:=$466a0708; //calinourse lobby

    if srvcfg.lobby = 9 then i:=$c625ae8d; //easter lobby

    move(i,s[$35],4);



    move(player[x].gid,s[$15],4);
    sendtoplayer(x,s);


    //sendtoplayer(x,#$08#$00#$00#$00#$03#$2A#$00#$00);

    s:=#$14#$00#$00#$00#$06#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00;

    move(player[x].gid,s[9],4);
    sendtoplayer(x,s);

    if id = 0 then
        s:=#$00#$00#$00#$3C#$00#$00#$00#$80#$F3#$38#$00#$41
        +#$13#$5A#$00#$00;

    if id = 1 then
        s:=#$00#$00#$18#$BB#$00#$00#$63#$37#$12#$53#$00#$38
        +#$6F#$D5#$00#$00;

    if id = 2 then s:=#$00#$00#$00#$00#$00#$00#$00#$3C#$D5#$B2#$00#$00
	    +#$23#$57#$00#$00;

    if id = 3 then s:=#$00#$00#$67#$B9#$00#$00#$E6#$39#$69#$51#$00#$47#$6A#$D8#$00#$00 ;

        move(s[9],player[x].curpos,8);
    move(s[1],player[x].rotation,8);
    SpawnPlayer(x);

    //send map obj
    {if id = 0 then s:=FileGetPacket('map\lobby.obj');
    if id = 1 then s:=FileGetPacket('map\lobby.obj');
    if id = 2 then s:=FileGetPacket('map\lobby.obj');  }
    //s:=FileGetPacket('map\lobby.obj');
    sendObjtoplayer(x,LobbyEventObj);
    sendObjtoplayer(x,lobbydata);
    //sendObjtoplayer(x,s);

    sendtoplayer(x,#$08#$00#$00#$00#$03#$23#$00#$00);

    sendtoplayer(x,#$08#$00#$00#$00#$03#$2B#$00#$00);

    sendtoplayer(x,#$0C#$00#$00#$00#$19#$0F#$00#$00#$7D#$00#$00#$00);

    //sendArmor(x);
    player[x].lastarea1:=#$fb#$ff#$ff#$ff;


    //send npc
    //if id = 0 then s:=FileGetPacket('map\lobby.npc');
    //if id = 1 then s:=FileGetPacket('map\shop.npc');
    //if id = 2 then s:=FileGetPacket('map\lobby.npc');
    //sendtoplayer(x,FixNPCGID(x,s));
    player[x].floor:=-1;
    player[x].firstping:=0;
    TestForClientOrder(x);
end;


procedure BlockSelect(x:integer);
var s:ansistring;
begin
    s:=#$AC#$28#$00#$00#$11#$10#$00#$00+#$1+#$00#$00#$00#$10#$00#$00#$00 //count
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$2F#$01#$42#$00#$2D#$00
	+#$30#$00#$30#$00#$31#$00#$3A#$00'S'#0'c'#0'h'#0't'#0
    +'S'#0'e'#0'r'#0'v'#0#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$22  //block ip
	+#$0F#$30#$03#$00#$01#$00#$B8#$0B#$77#$A4#$09#$00#$4F#$1B#$E8#$3D
	+#$00#$00#$00#$00#$00#$00#$00#$00

    +#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$51#$01#$42#$00#$2D#$00#$30#$00#$33#$00#$37#$00#$3A#$00
	+#$AE#$30#$E3#$30#$B6#$30#$EA#$30#$F3#$30#$B0#$30#$A8#$63#$68#$59
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$20#$31#$30#$03#$00#$BF#$00#$B8#$0B
	+#$7D#$A4#$09#$00#$8F#$C2#$75#$3D#$00#$00#$00#$00#$00#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$38#$01#$42#$00#$2D#$00
	+#$30#$00#$31#$00#$32#$00#$3A#$00#$B9#$30#$FC#$30#$D1#$30#$FC#$30
	+#$CF#$30#$FC#$30#$C9#$30#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$D5
	+#$18#$30#$03#$00#$58#$00#$B8#$0B#$82#$A4#$09#$00#$29#$5C#$0F#$3E
	+#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$7F#$01#$42#$00#$2D#$00#$30#$00#$34#$00#$37#$00#$3A#$00
	+#$AE#$64#$71#$5F#$11#$54#$51#$30#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$CB#$5F#$30#$03#$00#$80#$00#$B8#$0B
	+#$75#$A4#$09#$00#$4F#$1B#$E8#$3D#$00#$00#$00#$00#$00#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$7D#$01#$42#$00#$2D#$00
	+#$30#$00#$34#$00#$35#$00#$3A#$00#$A8#$30#$AF#$30#$B9#$30#$C8#$30
	+#$EA#$30#$FC#$30#$E0#$30#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$28
	+#$5D#$30#$03#$00#$2E#$00#$B8#$0B#$8F#$A4#$09#$00#$4B#$7E#$B1#$3D
	+#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$54#$01#$42#$00#$2D#$00#$30#$00#$34#$00#$30#$00#$3A#$00
	+#$CA#$58#$16#$4E#$BF#$8A#$FB#$67#$1A#$FF#$EA#$30#$EA#$30#$FC#$30
	+#$D1#$30#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$23#$34#$30#$03#$00#$9D#$00#$B8#$0B
	+#$A6#$A4#$09#$00#$4F#$1B#$E8#$3D#$00#$00#$00#$00#$00#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$36#$01#$42#$00#$2D#$00
	+#$30#$00#$31#$00#$30#$00#$3A#$00#$D9#$30#$EA#$30#$FC#$30#$CF#$30
	+#$FC#$30#$C9#$30#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$C1
	+#$16#$30#$03#$00#$04#$00#$B8#$0B#$7D#$A4#$09#$00#$0A#$D7#$23#$3D
	+#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$35#$01#$42#$00#$2D#$00#$30#$00#$30#$00#$39#$00#$3A#$00
	+#$D9#$30#$EA#$30#$FC#$30#$CF#$30#$FC#$30#$C9#$30#$A8#$63#$68#$59
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$28#$15#$30#$03#$00#$04#$00#$B8#$0B
	+#$BD#$A4#$09#$00#$6A#$03#$1D#$3E#$00#$00#$00#$00#$08#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$6B#$01#$42#$00#$2D#$00
	+#$32#$00#$32#$00#$31#$00#$3A#$00#$5B#$00#$71#$51#$28#$75#$5D#$00
	+#$CA#$58#$16#$4E#$BF#$8A#$FB#$67#$1A#$FF#$CA#$30#$D9#$30#$EA#$30
	+#$A6#$30#$B9#$30#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$9E
	+#$4B#$30#$03#$00#$9F#$00#$B8#$0B#$4D#$A5#$09#$00#$D1#$0B#$BD#$3C
	+#$00#$00#$00#$00#$28#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$78#$01#$42#$00#$2D#$00#$35#$00#$30#$00#$32#$00#$3A#$00
	+#$5B#$00#$71#$51#$28#$75#$5D#$00#$C1#$30#$E3#$30#$EC#$30#$F3#$30
	+#$B8#$30#$AE#$64#$71#$5F#$11#$54#$51#$30#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$9F#$58#$30#$03#$00#$80#$00#$B8#$0B
	+#$7E#$A5#$09#$00#$C1#$0F#$FC#$3B#$00#$00#$00#$00#$00#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$3F#$01#$42#$00#$2D#$00
	+#$30#$00#$31#$00#$39#$00#$3A#$00#$A8#$30#$AF#$30#$B9#$30#$C8#$30
	+#$E9#$30#$CF#$30#$FC#$30#$C9#$30#$A8#$63#$68#$59#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$26
	+#$1F#$30#$03#$00#$9C#$00#$B8#$0B#$75#$A4#$09#$00#$8F#$C2#$F5#$3E
	+#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$7B#$01#$42#$00#$2D#$00#$30#$00#$34#$00#$33#$00#$3A#$00
	+#$BF#$30#$A4#$30#$E0#$30#$A2#$30#$BF#$30#$C3#$30#$AF#$30#$A8#$63
	+#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$26#$5B#$30#$03#$00#$1A#$00#$B8#$0B
	+#$C3#$A4#$09#$00#$8C#$25#$3F#$3D#$00#$00#$00#$00#$00#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$3D#$01#$42#$00#$2D#$00
	+#$30#$00#$31#$00#$37#$00#$3A#$00#$A8#$30#$AF#$30#$B9#$30#$C8#$30
	+#$E9#$30#$CF#$30#$FC#$30#$C9#$30#$A8#$63#$68#$59#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$24
	+#$1D#$30#$03#$00#$9C#$00#$B8#$0B#$7F#$A5#$09#$00#$00#$00#$80#$3F
	+#$00#$00#$80#$3F#$00#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$4E#$01#$42#$00#$2D#$00#$30#$00#$33#$00#$34#$00#$3A#$00
	+#$A8#$30#$AF#$30#$B9#$30#$C8#$30#$E9#$30#$CF#$30#$FC#$30#$C9#$30
	+#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$C1#$2E#$30#$03#$00#$9C#$00#$B8#$0B
	+#$BD#$A4#$09#$00#$0A#$D7#$A3#$3D#$00#$00#$00#$00#$00#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$3C#$01#$42#$00#$2D#$00
	+#$30#$00#$31#$00#$36#$00#$3A#$00#$B9#$30#$FC#$30#$D1#$30#$FC#$30
	+#$CF#$30#$FC#$30#$C9#$30#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$23
	+#$1C#$30#$03#$00#$58#$00#$B8#$0B#$7D#$A4#$09#$00#$6A#$03#$1D#$3E
	+#$00#$00#$00#$00#$08#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$5C#$01#$42#$00#$2D#$00#$32#$00#$30#$00#$36#$00#$3A#$00
	+#$5B#$00#$71#$51#$28#$75#$5D#$00#$D9#$30#$EA#$30#$FC#$30#$CF#$30
	+#$FC#$30#$C9#$30#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$2A#$3C#$30#$03#$00#$24#$00#$B8#$0B
	+#$97#$A4#$09#$00#$D9#$89#$1D#$3D#$00#$00#$00#$00#$08#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$63#$01#$42#$00#$2D#$00
	+#$32#$00#$31#$00#$33#$00#$3A#$00#$5B#$00#$71#$51#$28#$75#$5D#$00
	+#$A8#$30#$AF#$30#$B9#$30#$C8#$30#$E9#$30#$CF#$30#$FC#$30#$C9#$30
	+#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$2B
	+#$43#$30#$03#$00#$9E#$00#$B8#$0B#$34#$A5#$09#$00#$C9#$8D#$5C#$3D
	+#$00#$00#$00#$00#$08#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$5E#$01#$42#$00#$2D#$00#$32#$00#$30#$00#$38#$00#$3A#$00
	+#$5B#$00#$71#$51#$28#$75#$5D#$00#$B9#$30#$FC#$30#$D1#$30#$FC#$30
	+#$CF#$30#$FC#$30#$C9#$30#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$9D#$3E#$30#$03#$00#$67#$00#$B8#$0B
	+#$94#$A4#$09#$00#$C1#$0F#$7C#$3C#$00#$00#$00#$00#$08#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$66#$01#$42#$00#$2D#$00
	+#$32#$00#$31#$00#$36#$00#$3A#$00#$5B#$00#$71#$51#$28#$75#$5D#$00
	+#$A8#$30#$AF#$30#$B9#$30#$C8#$30#$E9#$30#$CF#$30#$FC#$30#$C9#$30
	+#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$9F
	+#$46#$30#$03#$00#$9E#$00#$B8#$0B#$B9#$A4#$09#$00#$D1#$0B#$BD#$3C
	+#$00#$00#$00#$00#$08#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$64#$01#$42#$00#$2D#$00#$32#$00#$31#$00#$34#$00#$3A#$00
	+#$5B#$00#$71#$51#$28#$75#$5D#$00#$A8#$30#$AF#$30#$B9#$30#$C8#$30
	+#$E9#$30#$CF#$30#$FC#$30#$C9#$30#$A8#$63#$68#$59#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$9D#$44#$30#$03#$00#$9E#$00#$B8#$0B
	+#$7E#$A5#$09#$00#$D9#$89#$1D#$3D#$00#$00#$00#$00#$08#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$68#$01#$42#$00#$2D#$00
	+#$32#$00#$31#$00#$38#$00#$3A#$00#$5B#$00#$71#$51#$28#$75#$5D#$00
	+#$A8#$30#$AF#$30#$B9#$30#$C8#$30#$E9#$30#$CF#$30#$FC#$30#$C9#$30
	+#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$2A
	+#$48#$30#$03#$00#$9E#$00#$B8#$0B#$72#$A5#$09#$00#$D9#$89#$1D#$3D
	+#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$32#$01#$42#$00#$2D#$00#$30#$00#$30#$00#$36#$00#$3A#$00
	+#$CF#$30#$FC#$30#$C9#$30#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$25#$12#$30#$03#$00#$03#$00#$B8#$0B
	+#$86#$A4#$09#$00#$0A#$D7#$A3#$3D#$00#$00#$00#$00#$08#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$6A#$01#$42#$00#$2D#$00
	+#$32#$00#$32#$00#$30#$00#$3A#$00#$5B#$00#$71#$51#$28#$75#$5D#$00
	+#$AE#$30#$E3#$30#$B6#$30#$EA#$30#$F3#$30#$B0#$30#$A8#$63#$68#$59
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$9D
	+#$4A#$30#$03#$00#$C0#$00#$B8#$0B#$C1#$A4#$09#$00#$C1#$0F#$7C#$3D
	+#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$3A#$01#$42#$00#$2D#$00#$30#$00#$31#$00#$34#$00#$3A#$00
	+#$B9#$30#$FC#$30#$D1#$30#$FC#$30#$CF#$30#$FC#$30#$C9#$30#$A8#$63
	+#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$21#$1A#$30#$03#$00#$58#$00#$B8#$0B
	+#$7F#$A4#$09#$00#$C9#$2F#$96#$3D#$00#$00#$00#$00#$00#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$4C#$01#$42#$00#$2D#$00
	+#$30#$00#$33#$00#$32#$00#$3A#$00#$A8#$30#$AF#$30#$B9#$30#$C8#$30
	+#$E9#$30#$CF#$30#$FC#$30#$C9#$30#$A8#$63#$68#$59#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$27
	+#$2C#$30#$03#$00#$9C#$00#$B8#$0B#$80#$A4#$09#$00#$8F#$C2#$F5#$3D
	+#$00#$00#$00#$00#$08#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$5A#$01#$42#$00#$2D#$00#$32#$00#$30#$00#$34#$00#$3A#$00
	+#$5B#$00#$71#$51#$28#$75#$5D#$00#$CF#$30#$FC#$30#$C9#$30#$A8#$63
	+#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$9F#$3A#$30#$03#$00#$23#$00#$B8#$0B
	+#$D5#$A4#$09#$00#$C1#$0F#$7C#$3D#$00#$00#$00#$00#$08#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$67#$01#$42#$00#$2D#$00
	+#$32#$00#$31#$00#$37#$00#$3A#$00#$5B#$00#$71#$51#$28#$75#$5D#$00
	+#$A8#$30#$AF#$30#$B9#$30#$C8#$30#$E9#$30#$CF#$30#$FC#$30#$C9#$30
	+#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$29
	+#$47#$30#$03#$00#$9E#$00#$B8#$0B#$A1#$A4#$09#$00#$C1#$0F#$FC#$3C
	+#$00#$00#$00#$00#$08#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$58#$01#$42#$00#$2D#$00#$32#$00#$30#$00#$32#$00#$3A#$00
	+#$5B#$00#$71#$51#$28#$75#$5D#$00#$CE#$30#$FC#$30#$DE#$30#$EB#$30
	+#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$9D#$38#$30#$03#$00#$22#$00#$B8#$0B
	+#$8E#$A4#$09#$00#$C1#$0F#$7C#$3D#$00#$00#$00#$00#$00#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$4F#$01#$42#$00#$2D#$00
	+#$30#$00#$33#$00#$35#$00#$3A#$00#$A8#$30#$AF#$30#$B9#$30#$C8#$30
	+#$E9#$30#$CF#$30#$FC#$30#$C9#$30#$A8#$63#$68#$59#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$CB
	+#$2F#$30#$03#$00#$9C#$00#$B8#$0B#$B0#$A4#$09#$00#$0E#$74#$5A#$3D
	+#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$52#$01#$42#$00#$2D#$00#$30#$00#$33#$00#$38#$00#$3A#$00
	+#$CA#$58#$16#$4E#$BF#$8A#$FB#$67#$1A#$FF#$CA#$30#$D9#$30#$EA#$30
	+#$A6#$30#$B9#$30#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$21#$32#$30#$03#$00#$9D#$00#$B8#$0B
	+#$75#$A4#$09#$00#$8F#$C2#$75#$3D#$00#$00#$00#$00#$08#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$61#$01#$42#$00#$2D#$00
	+#$32#$00#$31#$00#$31#$00#$3A#$00#$5B#$00#$71#$51#$28#$75#$5D#$00
	+#$A8#$30#$AF#$30#$B9#$30#$C8#$30#$E9#$30#$CF#$30#$FC#$30#$C9#$30
	+#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$29
	+#$41#$30#$03#$00#$9E#$00#$B8#$0B#$95#$A5#$09#$00#$B9#$91#$1B#$3F
	+#$00#$00#$00#$00#$18#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$57#$01#$42#$00#$2D#$00#$32#$00#$30#$00#$31#$00#$3A#$00
	+#$5B#$00#$71#$51#$28#$75#$5D#$00#$D3#$30#$AE#$30#$CA#$30#$FC#$30
	+#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$2B#$37#$30#$03#$00#$21#$00#$B8#$0B
	+#$86#$A4#$09#$00#$5F#$E8#$85#$3E#$00#$00#$00#$00#$00#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$4B#$01#$42#$00#$2D#$00
	+#$30#$00#$33#$00#$31#$00#$3A#$00#$A8#$30#$AF#$30#$B9#$30#$C8#$30
	+#$E9#$30#$CF#$30#$FC#$30#$C9#$30#$A8#$63#$68#$59#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$26
	+#$2B#$30#$03#$00#$9C#$00#$B8#$0B#$94#$A4#$09#$00#$4B#$7E#$B1#$3D
	+#$00#$00#$00#$00#$08#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$76#$01#$42#$00#$2D#$00#$32#$00#$33#$00#$32#$00#$3A#$00
	+#$5B#$00#$71#$51#$28#$75#$5D#$00#$AE#$64#$71#$5F#$11#$54#$51#$30
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$9D#$56#$30#$03#$00#$80#$00#$B8#$0B
	+#$07#$A5#$09#$00#$C1#$0F#$FC#$3D#$00#$00#$00#$00#$08#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$62#$01#$42#$00#$2D#$00
	+#$32#$00#$31#$00#$32#$00#$3A#$00#$5B#$00#$71#$51#$28#$75#$5D#$00
	+#$A8#$30#$AF#$30#$B9#$30#$C8#$30#$E9#$30#$CF#$30#$FC#$30#$C9#$30
	+#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$2A
	+#$42#$30#$03#$00#$9E#$00#$B8#$0B#$48#$A5#$09#$00#$C9#$8D#$5C#$3D
	+#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$3E#$01#$42#$00#$2D#$00#$30#$00#$31#$00#$38#$00#$3A#$00
	+#$A8#$30#$AF#$30#$B9#$30#$C8#$30#$E9#$30#$CF#$30#$FC#$30#$C9#$30
	+#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$25#$1E#$30#$03#$00#$9C#$00#$B8#$0B
	+#$7A#$A4#$09#$00#$00#$00#$80#$3F#$CD#$CC#$2C#$3F#$00#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$7A#$01#$42#$00#$2D#$00
	+#$30#$00#$34#$00#$32#$00#$3A#$00#$4A#$30#$59#$30#$59#$30#$81#$30
	+#$AF#$30#$A8#$30#$B9#$30#$C8#$30#$A8#$63#$68#$59#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$25
	+#$5A#$30#$03#$00#$A0#$00#$B8#$0B#$76#$A4#$09#$00#$4B#$7E#$B1#$3D
	+#$00#$00#$00#$00#$28#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$77#$01#$42#$00#$2D#$00#$35#$00#$30#$00#$31#$00#$3A#$00
	+#$5B#$00#$71#$51#$28#$75#$5D#$00#$C1#$30#$E3#$30#$EC#$30#$F3#$30
	+#$B8#$30#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$9E#$57#$30#$03#$00#$A4#$00#$B8#$0B
	+#$7E#$A5#$09#$00#$00#$00#$00#$00#$00#$00#$00#$00#$08#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$72#$01#$42#$00#$2D#$00
	+#$32#$00#$32#$00#$38#$00#$3A#$00#$5B#$00#$71#$51#$28#$75#$5D#$00
	+#$AB#$30#$B8#$30#$CE#$30#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$9F
	+#$52#$30#$03#$00#$93#$00#$B8#$0B#$7E#$A5#$09#$00#$D1#$0B#$3D#$3D
	+#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$53#$01#$42#$00#$2D#$00#$30#$00#$33#$00#$39#$00#$3A#$00
	+#$CA#$58#$16#$4E#$BF#$8A#$FB#$67#$1A#$FF#$CA#$30#$D9#$30#$EA#$30
	+#$A6#$30#$B9#$30#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$22#$33#$30#$03#$00#$9D#$00#$B8#$0B
	+#$B2#$A4#$09#$00#$8C#$25#$3F#$3D#$00#$00#$00#$00#$00#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$30#$01#$42#$00#$2D#$00
	+#$30#$00#$30#$00#$34#$00#$3A#$00#$CE#$30#$FC#$30#$DE#$30#$EB#$30
	+#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$23
	+#$10#$30#$03#$00#$02#$00#$B8#$0B#$82#$A4#$09#$00#$8C#$25#$BF#$3D
	+#$00#$00#$00#$00#$08#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$6E#$01#$42#$00#$2D#$00#$32#$00#$32#$00#$34#$00#$3A#$00
	+#$5B#$00#$71#$51#$28#$75#$5D#$00#$4A#$30#$59#$30#$59#$30#$81#$30
	+#$AF#$30#$A8#$30#$B9#$30#$C8#$30#$A8#$63#$68#$59#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$2A#$4E#$30#$03#$00#$A1#$00#$B8#$0B
	+#$86#$A4#$09#$00#$C1#$0F#$FC#$3C#$00#$00#$00#$00#$08#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$75#$01#$42#$00#$2D#$00
	+#$32#$00#$33#$00#$31#$00#$3A#$00#$5B#$00#$71#$51#$28#$75#$5D#$00
	+#$D7#$30#$EC#$30#$A4#$30#$B9#$30#$BF#$30#$A4#$30#$EB#$30#$EA#$81
	+#$31#$75#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$2B
	+#$55#$30#$03#$00#$2A#$00#$B8#$0B#$8E#$A5#$09#$00#$C9#$8D#$5C#$3D
	+#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$44#$01#$42#$00#$2D#$00#$30#$00#$32#$00#$34#$00#$3A#$00
	+#$A8#$30#$AF#$30#$B9#$30#$C8#$30#$E9#$30#$CF#$30#$FC#$30#$C9#$30
	+#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$D5#$24#$30#$03#$00#$9C#$00#$B8#$0B
	+#$9B#$A4#$09#$00#$29#$5C#$0F#$3E#$00#$00#$00#$00#$00#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$4D#$01#$42#$00#$2D#$00
	+#$30#$00#$33#$00#$33#$00#$3A#$00#$A8#$30#$AF#$30#$B9#$30#$C8#$30
	+#$E9#$30#$CF#$30#$FC#$30#$C9#$30#$A8#$63#$68#$59#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$28
	+#$2D#$30#$03#$00#$9C#$00#$B8#$0B#$E2#$A4#$09#$00#$8F#$C2#$F5#$3D
	+#$00#$00#$00#$00#$08#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$59#$01#$42#$00#$2D#$00#$32#$00#$30#$00#$33#$00#$3A#$00
	+#$5B#$00#$71#$51#$28#$75#$5D#$00#$CF#$30#$FC#$30#$C9#$30#$A8#$63
	+#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$9E#$60#$30#$03#$00#$23#$00#$B8#$0B
	+#$12#$A5#$09#$00#$C9#$8D#$5C#$3D#$00#$00#$00#$00#$08#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$71#$01#$42#$00#$2D#$00
	+#$32#$00#$32#$00#$37#$00#$3A#$00#$5B#$00#$71#$51#$28#$75#$5D#$00
	+#$A8#$30#$AF#$30#$B9#$30#$C8#$30#$EA#$30#$FC#$30#$E0#$30#$A8#$63
	+#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$9E
	+#$51#$30#$03#$00#$33#$00#$B8#$0B#$83#$A5#$09#$00#$DD#$C8#$8D#$3D
	+#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$43#$01#$42#$00#$2D#$00#$30#$00#$32#$00#$33#$00#$3A#$00
	+#$A8#$30#$AF#$30#$B9#$30#$C8#$30#$E9#$30#$CF#$30#$FC#$30#$C9#$30
	+#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$CB#$23#$30#$03#$00#$9C#$00#$B8#$0B
	+#$CF#$A4#$09#$00#$0A#$D7#$A3#$3D#$00#$00#$00#$00#$00#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$39#$01#$42#$00#$2D#$00
	+#$30#$00#$31#$00#$33#$00#$3A#$00#$B9#$30#$FC#$30#$D1#$30#$FC#$30
	+#$CF#$30#$FC#$30#$C9#$30#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$20
	+#$19#$30#$03#$00#$58#$00#$B8#$0B#$8F#$A4#$09#$00#$8F#$C2#$75#$3D
	+#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$42#$01#$42#$00#$2D#$00#$30#$00#$32#$00#$32#$00#$3A#$00
	+#$A8#$30#$AF#$30#$B9#$30#$C8#$30#$E9#$30#$CF#$30#$FC#$30#$C9#$30
	+#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$C1#$22#$30#$03#$00#$9C#$00#$B8#$0B
	+#$74#$A4#$09#$00#$29#$5C#$0F#$3E#$00#$00#$00#$00#$08#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$73#$01#$42#$00#$2D#$00
	+#$32#$00#$32#$00#$39#$00#$3A#$00#$5B#$00#$71#$51#$28#$75#$5D#$00
	+#$BD#$30#$ED#$30#$D7#$30#$EC#$30#$A4#$30#$A8#$63#$68#$59#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$29
	+#$53#$30#$03#$00#$27#$00#$B8#$0B#$8D#$A4#$09#$00#$D9#$89#$1D#$3D
	+#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$7E#$01#$42#$00#$2D#$00#$30#$00#$34#$00#$36#$00#$3A#$00
	+#$AB#$30#$B8#$30#$CE#$30#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$C1#$5E#$30#$03#$00#$92#$00#$B8#$0B
	+#$7A#$A4#$09#$00#$CD#$CC#$CC#$3D#$00#$00#$00#$00#$00#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$48#$01#$42#$00#$2D#$00
	+#$30#$00#$32#$00#$38#$00#$3A#$00#$A8#$30#$AF#$30#$B9#$30#$C8#$30
	+#$E9#$30#$CF#$30#$FC#$30#$C9#$30#$A8#$63#$68#$59#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$23
	+#$28#$30#$03#$00#$9C#$00#$B8#$0B#$AA#$A4#$09#$00#$C9#$2F#$96#$3D
	+#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$79#$01#$42#$00#$2D#$00#$30#$00#$34#$00#$31#$00#$3A#$00
	+#$CA#$58#$16#$4E#$BF#$8A#$FB#$67#$1A#$FF#$EA#$30#$EA#$30#$FC#$30
	+#$D1#$30#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$24#$59#$30#$03#$00#$9D#$00#$B8#$0B
	+#$A1#$A4#$09#$00#$89#$88#$88#$3D#$00#$00#$00#$00#$00#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$49#$01#$42#$00#$2D#$00
	+#$30#$00#$32#$00#$39#$00#$3A#$00#$A8#$30#$AF#$30#$B9#$30#$C8#$30
	+#$E9#$30#$CF#$30#$FC#$30#$C9#$30#$A8#$63#$68#$59#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$24
	+#$29#$30#$03#$00#$9C#$00#$B8#$0B#$AF#$A4#$09#$00#$29#$5C#$0F#$3E
	+#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$31#$01#$42#$00#$2D#$00#$30#$00#$30#$00#$35#$00#$3A#$00
	+#$CF#$30#$FC#$30#$C9#$30#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$24#$11#$30#$03#$00#$03#$00#$B8#$0B
	+#$74#$A4#$09#$00#$0A#$D7#$23#$3D#$00#$00#$00#$00#$08#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$65#$01#$42#$00#$2D#$00
	+#$32#$00#$31#$00#$35#$00#$3A#$00#$5B#$00#$71#$51#$28#$75#$5D#$00
	+#$A8#$30#$AF#$30#$B9#$30#$C8#$30#$E9#$30#$CF#$30#$FC#$30#$C9#$30
	+#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$9E
	+#$45#$30#$03#$00#$9E#$00#$B8#$0B#$B5#$A4#$09#$00#$C9#$8D#$5C#$3D
	+#$00#$00#$00#$00#$08#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$5D#$01#$42#$00#$2D#$00#$32#$00#$30#$00#$37#$00#$3A#$00
	+#$5B#$00#$71#$51#$28#$75#$5D#$00#$B9#$30#$FC#$30#$D1#$30#$FC#$30
	+#$CF#$30#$FC#$30#$C9#$30#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$2B#$3D#$30#$03#$00#$67#$00#$B8#$0B
	+#$D6#$A4#$09#$00#$C9#$8D#$DC#$3D#$00#$00#$00#$00#$08#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$60#$01#$42#$00#$2D#$00
	+#$32#$00#$31#$00#$30#$00#$3A#$00#$5B#$00#$71#$51#$28#$75#$5D#$00
	+#$B9#$30#$FC#$30#$D1#$30#$FC#$30#$CF#$30#$FC#$30#$C9#$30#$A8#$63
	+#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$9F
	+#$40#$30#$03#$00#$67#$00#$B8#$0B#$40#$A5#$09#$00#$D9#$89#$1D#$3D
	+#$00#$00#$00#$00#$08#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$69#$01#$42#$00#$2D#$00#$32#$00#$31#$00#$39#$00#$3A#$00
	+#$5B#$00#$71#$51#$28#$75#$5D#$00#$A8#$30#$AF#$30#$B9#$30#$C8#$30
	+#$E9#$30#$CF#$30#$FC#$30#$C9#$30#$A8#$63#$68#$59#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$2B#$49#$30#$03#$00#$9E#$00#$B8#$0B
	+#$46#$A5#$09#$00#$D1#$0B#$BD#$3C#$00#$00#$00#$00#$08#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$5B#$01#$42#$00#$2D#$00
	+#$32#$00#$30#$00#$35#$00#$3A#$00#$5B#$00#$71#$51#$28#$75#$5D#$00
	+#$D9#$30#$EA#$30#$FC#$30#$CF#$30#$FC#$30#$C9#$30#$A8#$63#$68#$59
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$29
	+#$3B#$30#$03#$00#$24#$00#$B8#$0B#$7D#$A4#$09#$00#$C1#$0F#$7C#$3C
	+#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$47#$01#$42#$00#$2D#$00#$30#$00#$32#$00#$37#$00#$3A#$00
	+#$A8#$30#$AF#$30#$B9#$30#$C8#$30#$E9#$30#$CF#$30#$FC#$30#$C9#$30
	+#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$22#$27#$30#$03#$00#$9C#$00#$B8#$0B
	+#$A8#$A4#$09#$00#$8F#$C2#$F5#$3D#$00#$00#$00#$00#$08#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$6F#$01#$42#$00#$2D#$00
	+#$32#$00#$32#$00#$35#$00#$3A#$00#$5B#$00#$71#$51#$28#$75#$5D#$00
	+#$BF#$30#$A4#$30#$E0#$30#$A2#$30#$BF#$30#$C3#$30#$AF#$30#$A8#$63
	+#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$2B
	+#$4F#$30#$03#$00#$25#$00#$B8#$0B#$7B#$A4#$09#$00#$C1#$0F#$FC#$3C
	+#$00#$00#$00#$00#$10#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$2D#$01#$42#$00#$2D#$00#$30#$00#$30#$00#$31#$00#$3A#$00
	+#$D3#$30#$AE#$30#$CA#$30#$FC#$30#$A8#$63#$68#$59#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$20#$0D#$30#$03#$00#$01#$00#$B8#$0B
	+#$74#$A4#$09#$00#$7E#$B1#$E4#$3E#$00#$00#$00#$00#$00#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$37#$01#$42#$00#$2D#$00
	+#$30#$00#$31#$00#$31#$00#$3A#$00#$B9#$30#$FC#$30#$D1#$30#$FC#$30
	+#$CF#$30#$FC#$30#$C9#$30#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$CB
	+#$17#$30#$03#$00#$58#$00#$B8#$0B#$CC#$A4#$09#$00#$CD#$CC#$CC#$3E
	+#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$41#$01#$42#$00#$2D#$00#$30#$00#$32#$00#$31#$00#$3A#$00
	+#$A8#$30#$AF#$30#$B9#$30#$C8#$30#$E9#$30#$CF#$30#$FC#$30#$C9#$30
	+#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$28#$21#$30#$03#$00#$9C#$00#$B8#$0B
	+#$74#$A4#$09#$00#$29#$5C#$0F#$3E#$00#$00#$00#$00#$00#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$50#$01#$42#$00#$2D#$00
	+#$30#$00#$33#$00#$36#$00#$3A#$00#$A8#$30#$AF#$30#$B9#$30#$C8#$30
	+#$E9#$30#$CF#$30#$FC#$30#$C9#$30#$A8#$63#$68#$59#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$D5
	+#$30#$30#$03#$00#$9C#$00#$B8#$0B#$80#$A4#$09#$00#$8C#$25#$3F#$3D
	+#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$45#$01#$42#$00#$2D#$00#$30#$00#$32#$00#$35#$00#$3A#$00
	+#$A8#$30#$AF#$30#$B9#$30#$C8#$30#$E9#$30#$CF#$30#$FC#$30#$C9#$30
	+#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$20#$25#$30#$03#$00#$9C#$00#$B8#$0B
	+#$D1#$A4#$09#$00#$AB#$AA#$2A#$3E#$00#$00#$00#$00#$08#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$5F#$01#$42#$00#$2D#$00
	+#$32#$00#$30#$00#$39#$00#$3A#$00#$5B#$00#$71#$51#$28#$75#$5D#$00
	+#$B9#$30#$FC#$30#$D1#$30#$FC#$30#$CF#$30#$FC#$30#$C9#$30#$A8#$63
	+#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$9E
	+#$3F#$30#$03#$00#$67#$00#$B8#$0B#$94#$A4#$09#$00#$D9#$89#$1D#$3D
	+#$00#$00#$00#$00#$10#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$2E#$01#$42#$00#$2D#$00#$30#$00#$30#$00#$32#$00#$3A#$00
	+#$D3#$30#$AE#$30#$CA#$30#$FC#$30#$A8#$63#$68#$59#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$21#$0E#$30#$03#$00#$01#$00#$B8#$0B
	+#$74#$A4#$09#$00#$6A#$03#$1D#$3E#$00#$00#$00#$00#$00#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$46#$01#$42#$00#$2D#$00
	+#$30#$00#$32#$00#$36#$00#$3A#$00#$A8#$30#$AF#$30#$B9#$30#$C8#$30
	+#$E9#$30#$CF#$30#$FC#$30#$C9#$30#$A8#$63#$68#$59#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$21
	+#$26#$30#$03#$00#$9C#$00#$B8#$0B#$FD#$A4#$09#$00#$E8#$B4#$01#$3E
	+#$00#$00#$00#$00#$08#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$6C#$01#$42#$00#$2D#$00#$32#$00#$32#$00#$32#$00#$3A#$00
	+#$5B#$00#$71#$51#$28#$75#$5D#$00#$CA#$58#$16#$4E#$BF#$8A#$FB#$67
	+#$1A#$FF#$CA#$30#$D9#$30#$EA#$30#$A6#$30#$B9#$30#$A8#$63#$68#$59
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$9F#$4C#$30#$03#$00#$9F#$00#$B8#$0B
	+#$98#$A4#$09#$00#$C1#$0F#$7C#$3D#$00#$00#$00#$00#$00#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$3B#$01#$42#$00#$2D#$00
	+#$30#$00#$31#$00#$35#$00#$3A#$00#$B9#$30#$FC#$30#$D1#$30#$FC#$30
	+#$CF#$30#$FC#$30#$C9#$30#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$22
	+#$1B#$30#$03#$00#$58#$00#$B8#$0B#$C1#$A4#$09#$00#$0E#$74#$5A#$3D
	+#$00#$00#$00#$00#$08#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$6D#$01#$42#$00#$2D#$00#$32#$00#$32#$00#$33#$00#$3A#$00
	+#$5B#$00#$71#$51#$28#$75#$5D#$00#$CA#$58#$16#$4E#$BF#$8A#$FB#$67
	+#$1A#$FF#$EA#$30#$EA#$30#$FC#$30#$D1#$30#$A8#$63#$68#$59#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$29#$4D#$30#$03#$00#$9F#$00#$B8#$0B
	+#$8B#$A4#$09#$00#$C1#$0F#$FC#$3C#$00#$00#$00#$00#$00#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$33#$01#$42#$00#$2D#$00
	+#$30#$00#$30#$00#$37#$00#$3A#$00#$CF#$30#$FC#$30#$C9#$30#$A8#$63
	+#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$26
	+#$13#$30#$03#$00#$03#$00#$B8#$0B#$7C#$A4#$09#$00#$C9#$2F#$96#$3D
	+#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$4A#$01#$42#$00#$2D#$00#$30#$00#$33#$00#$30#$00#$3A#$00
	+#$A8#$30#$AF#$30#$B9#$30#$C8#$30#$E9#$30#$CF#$30#$FC#$30#$C9#$30
	+#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$25#$2A#$30#$03#$00#$9C#$00#$B8#$0B
	+#$8D#$A4#$09#$00#$0E#$74#$DA#$3D#$00#$00#$00#$00#$08#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$74#$01#$42#$00#$2D#$00
	+#$32#$00#$33#$00#$30#$00#$3A#$00#$5B#$00#$71#$51#$28#$75#$5D#$00
	+#$C1#$30#$E3#$30#$C3#$30#$C8#$30#$A8#$63#$68#$59#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$2A
	+#$54#$30#$03#$00#$28#$00#$B8#$0B#$72#$A5#$09#$00#$D1#$0B#$BD#$3D
	+#$00#$00#$00#$00#$08#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$70#$01#$42#$00#$2D#$00#$32#$00#$32#$00#$36#$00#$3A#$00
	+#$5B#$00#$71#$51#$28#$75#$5D#$00#$A2#$30#$C9#$30#$D0#$30#$F3#$30
	+#$B9#$30#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$9D#$50#$30#$03#$00#$26#$00#$B8#$0B
	+#$87#$A4#$09#$00#$C1#$0F#$7C#$3D#$00#$00#$00#$00#$00#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$34#$01#$42#$00#$2D#$00
	+#$30#$00#$30#$00#$38#$00#$3A#$00#$D9#$30#$EA#$30#$FC#$30#$CF#$30
	+#$FC#$30#$C9#$30#$A8#$63#$68#$59#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$27
	+#$14#$30#$03#$00#$04#$00#$B8#$0B#$87#$A4#$09#$00#$0E#$74#$5A#$3D
	+#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
	+#$13#$00#$7C#$01#$42#$00#$2D#$00#$30#$00#$34#$00#$34#$00#$3A#$00
	+#$A2#$30#$C9#$30#$D0#$30#$F3#$30#$B9#$30#$A8#$63#$68#$59#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$D2#$BD#$D0#$27#$5C#$30#$03#$00#$1B#$00#$B8#$0B
	+#$92#$A4#$09#$00#$C9#$2F#$96#$3D#$00#$00#$00#$00#$00#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$13#$00#$40#$01#$42#$00#$2D#$00
	+#$30#$00#$32#$00#$30#$00#$3A#$00#$A8#$30#$AF#$30#$B9#$30#$C8#$30
	+#$E9#$30#$CF#$30#$FC#$30#$C9#$30#$A8#$63#$68#$59#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$BD#$D0#$27
	+#$20#$30#$03#$00#$9C#$00#$B8#$0B#$C4#$A4#$09#$00#$30#$96#$7C#$3E
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;

    move(ActiveIP[0],s[$5d],4); //ip

    move(SrvCfg.bport, s[$61],2); //port

    if player[x].room = Room_BattleLobby then s[$d] := #$CA;
    if player[x].room = Room_ChalengeLobby then s[$d] := #$6A;

    sendtoplayer(x,s);
end;

procedure setfencestatus(t,l,id,st:integer);
var x,i,k:integer;
begin
    x:=1;
    while x<length(team[t].FloorObj[l]) do begin
        move(team[t].FloorObj[l][x],i,4);
        move(team[t].FloorObj[l][x+8],k,4);
        if k = id then begin
            team[t].FloorObj[l][x+$43]:=ansichar(st);
            break;
        end;
        inc(x,i);
    end;
end;



procedure AddPtsToGoal(x,g,id:integer);
var i:integer;
    s:ansistring;
    pts:dword;
begin

    if id = goal1id then begin
        inc(goal1pts);
        pts:=goal1pts;
        if pts = 10 then begin
        pts :=0;
        goal1pts:=0;
        end;
    end else begin
        inc(goal2pts);
        pts:=goal2pts;
        if pts = 10 then begin
        pts :=0;
        goal2pts:=0;
        end;
    end;



    s:=#$28#$00#$00#$00#$04#$BA#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00#$8B#$01#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
    +#$40#$00#$00#$00#$01#$00#$00#$00;
    move(player[x].gid,s[9],4);
    move(id,s[$15],4);
    move(pts,s[$25],4);
    sendtolobby(player[x].room,-1,s);

    s:=#$28#$00#$00#$00#$04#$BA#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00#$8B#$01#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
    +#$37#$00#$00#$00#$01#$00#$00#$00;
    move(player[x].gid,s[9],4);
    move(id,s[$15],4);
    move(pts,s[$25],4);
    sendtolobby(player[x].room,-1,s);


    if pts = 0 then begin

    s:=#$58#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00#$8C#$01#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
	+#$00#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$E6#$5C#$00#$00#$42#$41#$4C#$4C#$5F#$53#$48#$4F#$4F#$54#$5F#$47
	+#$41#$4D#$45#$5F#$53#$45#$54#$00;
    move(player[x].gid,s[9],4);
    move(player[x].gid,s[$25],4);
    move(id,s[$15],4);
    sendtolobby(player[x].room,-1,s);

    s:=#$54#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00#$8B#$01#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
	+#$00#$00#$00#$00#$8B#$01#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$EA#$5C#$00#$00#$47#$4F#$41#$4C#$53#$43#$4F#$52#$45#$5F#$52#$45
	+#$53#$45#$54#$00;
    move(player[x].gid,s[9],4);
    move(id,s[$25],4);
    move(id,s[$15],4);
    sendtolobby(player[x].room,-1,s);

    end else begin
    s:=#$58#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00#$8B#$01#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
    +#$00#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$E8#$5C#$00#$00#$42#$41#$4C#$4C#$5F#$53#$48#$4F#$4F#$54#$5F#$47
    +#$4F#$41#$4C#$5F#$31#$00#$00#$00;
    if g = 2 then s[$55]:='2';
    move(player[x].gid,s[9],4);
    move(player[x].gid,s[$25],4);
    move(id,s[$15],4);
    sendtolobby(player[x].room,-1,s);
    end;
end;

procedure KickBall(x:integer;s:ansistring);
var a:ansistring;
begin
    a:=#0#0#0#0#0#0#0#0#4#0#0#0;
    move(player[x].gid,a[1],4);
    a:=s+a;
    a[6]:=#$be;
    a[1]:=#$44;
    sendtolobby(player[x].room,-1,a);
end;

end.
