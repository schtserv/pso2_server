unit UEnc;

interface
uses types,sysutils;

procedure InitRSA();
function RSAEncrypt(KeyFile, CryptedData: ansistring):ansistring;
function RSADecrypt(KeyFile, CryptedData: ansistring):ansistring;

Function CreateKey(val: ansistring; user: integer): boolean;
Function PSOEnc(s: ansistring; user: integer): ansistring;
Function PSODec(s: ansistring; user: integer): ansistring;

procedure processKey(x:integer;s:ansistring);

var encbuf:array[0..200,0..255] of byte;
    decbuf:array[0..200,0..255] of byte;
    encI,encJ,decI,decJ:array[0..200] of integer;

implementation

uses libeay32, UDebug, OpenSSLUtils, fmain, UPlayer;

Procedure InitRSA();
begin
    OpenSSL_add_all_algorithms;
    OpenSSL_add_all_ciphers;
    OpenSSL_add_all_digests;
    ERR_load_crypto_strings;
end;

function ReadPrivateKey(AFileName: ansistring): pRSA;
var
  keyfile: pBIO;
begin
    keyfile := BIO_new(BIO_s_file());
    BIO_read_filename(keyfile, pansichar(AFilename));
    result:=nil;
    result := PEM_read_bio_RSAPrivateKey(keyfile, result, nil, nil);
    if result = nil then
        begin
          debug(DBG_WARNING,'Unable to read private key. ' + GetErrorMessage);
            exit;
        end;
end;

function RSADecrypt(KeyFile, CryptedData: ansistring):ansistring;
var
  rsa: pRSA;
  keysize: integer;

  key: pEVP_PKEY;
  cleartext, crypted: pBIO;
  rsa_in, rsa_out: pointer;
  rsa_inlen, rsa_outlen, l: integer;
begin
    // as in AskPassphrase.html
    rsa := ReadPrivateKey(path+'key\privateKey.pem');
    //rsa := EVP_PKEY_get1_RSA(key);
    //EVP_PKEY_free(key);
    if rsa = nil then
      begin
       debug(DBG_WARNING,'Error getting RSA key. ' + GetErrorMessage);
       exit;
      end;

    keysize := RSA_size(rsa);

    // Should be free if exception is raised
    rsa_in := OPENSSL_malloc(keysize * 2);
    rsa_out := OPENSSL_malloc(keysize);

    // Read the input data
    rsa_inlen := $80;
    for l:=0 to $7f do
         pansichar(rsa_in)[l]:=CryptedData[$80-l];
    //move(CryptedData[1],pansichar(rsa_in)[0],$80);
    rsa_outlen := RSA_private_decrypt(rsa_inlen, rsa_in, rsa_out, rsa, RSA_PKCS1_PADDING);
    if rsa_outlen <= 0 then
      begin
       debug(DBG_WARNING,'RSA operation error. ' + GetErrorMessage);
       exit;
      end;

    setlength(result, rsa_outlen);
    move(pansichar(rsa_out)[0],result[1], rsa_outlen);
    RSA_free(rsa);
    if rsa_in <> nil then
      OPENSSL_free(rsa_in);
    if rsa_out <> nil then
      OPENSSL_free(rsa_out);
end;



function ReadPublicKey(AFileName: TFileName): pEVP_PKEY;
var
  keyfile: pBIO;
begin
    keyfile := BIO_new(BIO_s_file());
    BIO_read_filename(keyfile, pansichar(AFilename));
    result:=nil;
    result := PEM_read_bio_PUBKEY(keyfile, result, nil,nil);
    if result = nil then begin
        debug(DBG_WARNING,'Unable to read private key. ' + GetErrorMessage);
        exit;
    end;
end;

function RSAEncrypt(KeyFile, CryptedData: ansistring):ansistring;
var
  rsa: pRSA;
  keysize: integer;

  key: pEVP_PKEY;
  cleartext, crypted: pBIO;
  rsa_in, rsa_out: pointer;
  rsa_inlen, rsa_outlen,l: integer;
begin
    // as in AskPassphrase.html
    key := ReadPublicKey(path+'key\SEGAKey.pem');
    rsa := EVP_PKEY_get1_RSA(key);
    EVP_PKEY_free(key);
    if rsa = nil then
      begin
         Debug(DBG_WARNING,'Error getting RSA key. ' + GetErrorMessage);
         exit;
      end;

    keysize := RSA_size(rsa);

    // Should be free if exception is raised
    rsa_in := OPENSSL_malloc(keysize * 2);
    rsa_out := OPENSSL_malloc(keysize);

    // Read the input data
    rsa_inlen := length(CryptedData);
    move(CryptedData[1],pansichar(rsa_in)[0],length(CryptedData));
    rsa_outlen := RSA_public_encrypt(rsa_inlen, rsa_in, rsa_out, rsa, RSA_PKCS1_PADDING);
    if rsa_outlen <= 0 then
      begin
        debug(DBG_WARNING,'RSA operation error. ' + GetErrorMessage);
        exit;
      end;

    setlength(result, rsa_outlen);
    for l:=0 to $7f do
         result[1+l]:=pansichar(rsa_out)[$7f-l];
    //move(pansichar(rsa_out)[0],, rsa_outlen);
    RSA_free(rsa);
    if rsa_in <> nil then
      OPENSSL_free(rsa_in);
    if rsa_out <> nil then
      OPENSSL_free(rsa_out);
end;



Function CreateKey(val: ansistring; user: integer): boolean;
var index1,index2,x,t:integer;
begin
    result:=false;
    if length(val) < 16 then exit; //fail if not enought
    index1 := 0;
    index2 := 0;
    try
    for x:=0 to 255 do encbuf[user,x]:=x;
    for x:=0 to 255 do begin
        index2:=(byte(val[index1+1])+encbuf[user,x]+index2) and 255;
        t:=encbuf[user,x];
        encbuf[user,x]:=encbuf[user,index2];
        encbuf[user,index2]:=t;
        index1:=(index1 + 1) and 15;
    end;

    move( encbuf[user,0], decbuf[user,0],256);
    encI[user]:=0;
    encJ[user]:=0;
    decI[user]:=0;
    decJ[user]:=0;
    result:=true;
    except
    end;
end;


Function PSOEnc(s: ansistring; user: integer): ansistring;
var
  K, LenS: Cardinal;
  t:byte;
begin
  LenS := 256;
  result:=s;
  for K := 1 to length(s)  do
    begin
      encI[user] := (encI[user] + 1) mod lenS;
      encJ[user] := (encJ[user] + encbuf[user,encI[user]]) mod lenS;
      t:= encbuf[user,encI[user]];
      encbuf[user,encI[user]] := encbuf[user,encJ[user]];
      encbuf[user,encJ[user]]:=t;
      result[K] := ansichar(byte(S[K]) xor encbuf[user,(encbuf[user,encI[user]] + encbuf[user,encJ[user]]) mod LenS]);
    end;
end;

Function PSODec(s: ansistring; user: integer): ansistring;
var
  K, LenS: Cardinal;
  t:byte;
begin
  LenS := 256;
  result:=s;
  for K := 1 to length(s)  do
    begin
      decI[user] := (decI[user] + 1) mod lenS;
      decJ[user] := (decJ[user] + decbuf[user,decI[user]]) mod lenS;
      t:= decbuf[user,decI[user]];
      decbuf[user,decI[user]] := decbuf[user,decJ[user]];
      decbuf[user,decJ[user]]:=t;
      result[K] := ansichar(byte(S[K]) xor decbuf[user,(decbuf[user,decI[user]] + decbuf[user,decJ[user]]) mod LenS]);
    end;
end;

procedure processKey(x:integer;s:ansistring);
var a:ansistring;
begin
    a:=RSADecrypt('',copy(s,9,$100));
    if CreateKey(copy(a,$11,$10),x) then begin
    a:=PSODec(copy(a,1,16),x);
    //showmessage(a);

    move( encbuf[x,0], decbuf[x,0],256);  //recoppy again to start fresh
    decI[x]:=0;
    decJ[x]:=0;
    a:=PSOEnc(#$18#0#0#0#$11#$0c#0#0+a,x);
    player[x].dout:=player[x].dout+a;
    player[x].cipher:=1;
    end else player[x].todc:=1;
end;


end.
